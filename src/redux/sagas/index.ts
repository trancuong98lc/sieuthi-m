import { all, fork } from 'redux-saga/effects';
import {
  watchLoginAccount,
  watchRegisterAccount,
  watchPhoneExists,
  watchRefreshToken,
  watchLogout
} from './auth';

export default function* rootSaga() {
  yield all([
    fork(watchPhoneExists),
    fork(watchLoginAccount),
    fork(watchRegisterAccount),
    fork(watchRefreshToken),
    fork(watchLogout)
    // fork(watchNearPlaces),
  ]);
}
