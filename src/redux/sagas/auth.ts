import ApiHelper from 'helpers/api-helper';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import { showAlert } from 'libraries/BaseAlert';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import { put, takeLatest } from 'redux-saga/effects';
import {
  CHECK_LOGIN,
  LOGOUT,
  updateCheckLogin,
  UPDATE_COUNT_NOTICE,
  UPDATE_QUANTITY_CART,
  UPDATE_USER_INFO,
  WATCH_CHECK_PHONE_EXISTS,
  WATCH_LOGIN,
  WATCH_LOGOUT,
  WATCH_REFRESH_TOKEN,
  WATCH_REGISTER
} from 'redux/actions';
import {
  goBack,
  navigate,
  popMultipleScreen,
  resetStack
} from 'routing/service-navigation';
import {
  OtpScreen,
  HomeScreen,
  StoreScreen,
  LoginScreen,
  CompleteSuccess
} from 'routing/screen-name';
import { BaseResponseLogIn, STATUS } from 'types/BaseResponse';
import { User } from 'types/user-type';
import { SCREEN_NAME } from 'helpers/utils';
import { LoginState } from 'redux/reducers/islogin-reducer';
import homeScreen from 'features/home/view/home.screen';

export interface RegisterBodyRequest {
  phoneNumber: string;
  name: string;
  email: string;
  password: string;
}

interface DataPayload {
  type: string;
  payload: any;
}

interface PhoneExistBodyRequest {
  phoneNumber: string;
}

function* updateUserToStore(user: User) {
  yield put({
    type: UPDATE_USER_INFO,
    payload: user
  });
}
// function* updateCheckLogin(data: LoginState) {
//   yield put({
//     type: CHECK_LOGIN,
//     payload: data
//   });
// }

function* handleLoginAccount(data: DataPayload) {
  showLoading();
  try {
    const res: BaseResponseLogIn = yield ApiHelper.post(
      URL_API.LOGIN,
      data.payload.body
    );
    if (res.status == STATUS.SUCCESS) {
      hideLoading();
      let user = res.user;
      yield updateUserToStore({ ...user, expiresAt: res.expiresAt });
      const store = yield AsyncStorageHelpers.getObject(StorageKey.STORE);
      getTotalProductCart(store._id);
      AsyncStorageHelpers.saveObject(StorageKey.LOGIN_SESSION, {
        ...res.user,
        expiresAt: res.expiresAt
      });
      AsyncStorageHelpers.saveObject(StorageKey.ID_TOKEN, res.token);
      AsyncStorageHelpers.saveObject(
        StorageKey.REFRESH_TOKEN,
        res.refreshToken
      );
      AsyncStorageHelpers.saveObject(StorageKey.ID_USER, user._id);
      yield (Commons.idToken = res.token);
      yield (Commons.idUser = user._id);
      if (store) {
        // yield put(updateCheckLogin({ isLogin: false, screen: HomeScreen }));
        if (data.payload.routerIndex === 2) {
          popMultipleScreen(data.payload.routerIndex - 1);
        } else if (data.payload.routerIndex > 2) {
          popMultipleScreen(data.payload.routerIndex - 2);
        } else {
          resetStack(HomeScreen);
        }
      } else {
        if (data.payload.routerIndex === 2) {
          popMultipleScreen(data.payload.routerIndex - 1);
        } else if (data.payload.routerIndex > 2) {
          popMultipleScreen(data.payload.routerIndex - 2);
        } else {
          resetStack(HomeScreen);
        }
        // yield put(updateCheckLogin({ isLogin: false, screen: HomeScreen }));
      }
    } else {
      hideLoading();
      setTimeout(() => {
        showAlert(`status.${res.status}`);
      }, 300);
      // showAlert('accounts.login_failed');
    }
  } catch (error) {
    hideLoading();
    showAlert('network_error');
  }
}

function navigateToOTPScreen(data: any) {
  navigate(OtpScreen, data);
}

function* handleLogout(data: any) {
  yield AsyncStorageHelpers.remove(StorageKey.LOGIN_SESSION);
  Commons.idUser = '';
  Commons.idToken = '';
  yield AsyncStorageHelpers.remove(StorageKey.ID_TOKEN);
  yield AsyncStorageHelpers.remove(StorageKey.ID_USER);
  try {
    const res = yield ApiHelper.post(URL_API.LOGOUT, {
      token: data.payload.token
    });
    yield updateQuantityCart(0);
    if (res.status == STATUS.SUCCESS) {
      yield put({
        type: LOGOUT,
        payload: null
      });
      resetStack(HomeScreen);
    }
  } catch (error) {
    console.log('error: ', error);
  }
}

function* updateCountNoti(quantity: number) {
  yield put({
    type: UPDATE_COUNT_NOTICE,
    data: { countNoti: quantity }
  });
}

export function* handleRefreshToken(data: { resolve?: () => void }) {
  let refreshToken = yield AsyncStorageHelpers.getObject(
    StorageKey.REFRESH_TOKEN
  );

  if (refreshToken) {
    try {
      const res: BaseResponseLogIn = yield ApiHelper.post(
        URL_API.REFRESH_TOKEN,
        {
          refreshToken: refreshToken
        }
      );
      if (res.status == STATUS.SUCCESS) {
        AsyncStorageHelpers.saveObject(StorageKey.LOGIN_SESSION, {
          ...res.user,
          expiresAt: res.expiresAt
        });
        AsyncStorageHelpers.saveObject(StorageKey.ID_TOKEN, res.token);
        AsyncStorageHelpers.saveObject(
          StorageKey.REFRESH_TOKEN,
          res.refreshToken
        );
        AsyncStorageHelpers.saveObject(StorageKey.ID_USER, res.user._id);
        yield (Commons.idToken = res.token);
        yield (Commons.idUser = res.user._id);
        if (data && data.resolve) {
          data.resolve();
        }
        yield updateUserToStore({ ...res.user, expiresAt: res.expiresAt });
      } else {
        yield updateQuantityCart(0);
        yield updateCountNoti(0);
        yield updateUserToStore({});
        yield handleLogout({
          payload: refreshToken
        });
      }
    } catch (error) {
      AsyncStorageHelpers.remove(StorageKey.LOGIN_SESSION);
      AsyncStorageHelpers.remove(StorageKey.ID_TOKEN);
      AsyncStorageHelpers.remove(StorageKey.ID_USER);
      yield put({
        type: LOGOUT,
        payload: null
      });
    }
  }
}

function* updateQuantityCart(quality: number) {
  yield put({
    type: UPDATE_QUANTITY_CART,
    data: quality
  });
}

function* getTotalProductCart(idStore: String) {
  console.log('function*getTotalProductCart -> idStore', idStore);
  ApiHelper.fetch(URL_API.CART_TOTAL_PRODUCT + idStore, null, true).then(
    (res) => {
      if (res.status == STATUS.SUCCESS) {
        updateQuantityCart(res.total || 0);
      }
    }
  );
}

function* handleCheckPhoneExists(data: DataPayload) {
  try {
    const res = yield ApiHelper.post(URL_API.CHECK_PHONE_EXIST, {
      phoneNumber: data.payload.phoneNumber
    });
    showLoading();
    if (data.payload.screen == SCREEN_NAME.FORGET_PASS) {
      if (res.status == STATUS.TEL_ALREADY_EXISTS) {
        navigateToOTPScreen(data.payload);
        hideLoading();
      } else {
        hideLoading();
        setTimeout(() => {
          showAlert(`account.not_exitst`);
        }, 300);
      }
    }
    if (data.payload.screen == SCREEN_NAME.REGISTER) {
      if (res.status === STATUS.SUCCESS) {
        hideLoading();
        navigateToOTPScreen(data.payload);
      } else {
        hideLoading();
        setTimeout(() => {
          showAlert(`status.${res.status}`);
        }, 300);
      }
    }
  } catch (error) {
    hideLoading();
    console.log(error);
    setTimeout(() => {
      showAlert('network_error');
    }, 300);
  }
}

function* handleRegisterAccount(data: DataPayload) {
  const body: RegisterBodyRequest = data.payload;
  const store = yield AsyncStorageHelpers.getObject(StorageKey.STORE);
  try {
    showLoading();
    const res: BaseResponseLogIn = yield ApiHelper.post(
      URL_API.REGISTER_ACCOUNT,
      body
    );
    if (res.status == STATUS.SUCCESS) {
      hideLoading();
      let user = res.user;
      yield updateUserToStore({ ...res.user, expiresAt: res.expiresAt });
      AsyncStorageHelpers.saveObject(StorageKey.LOGIN_SESSION, res.user);
      AsyncStorageHelpers.saveObject(StorageKey.ID_TOKEN, res.token);
      AsyncStorageHelpers.saveObject(
        StorageKey.REFRESH_TOKEN,
        res.refreshToken
      );
      getTotalProductCart(store._id);
      AsyncStorageHelpers.saveObject(StorageKey.ID_USER, user._id);
      yield (Commons.idToken = res.token);
      yield (Commons.idUser = user._id);
      navigate(CompleteSuccess);
    } else {
      hideLoading();
      setTimeout(() => {
        showAlert('account.register_failed');
      }, 300);
    }
  } catch (error) {
    console.log('error: ', error);
    hideLoading();
    setTimeout(() => {
      showAlert('network_error');
    }, 300);
  }
}

export function* watchPhoneExists() {
  yield takeLatest(WATCH_CHECK_PHONE_EXISTS, handleCheckPhoneExists);
}

export function* watchLoginAccount() {
  yield takeLatest(WATCH_LOGIN, handleLoginAccount);
}
export function* watchRegisterAccount() {
  yield takeLatest(WATCH_REGISTER, handleRegisterAccount);
}
export function* watchRefreshToken() {
  yield takeLatest(WATCH_REFRESH_TOKEN, handleRefreshToken);
}

export function* watchLogout() {
  yield takeLatest(WATCH_LOGOUT, handleLogout);
}
