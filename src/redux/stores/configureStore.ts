import { applyMiddleware, createStore } from 'redux';
import rootReducers from '../reducers/index';

export default function configureStore(sagaMiddleware: any) {
  const store = createStore(rootReducers, applyMiddleware(sagaMiddleware));
  return store;
}
