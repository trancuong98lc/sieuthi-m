import { UPDATE_CONFIG } from './action-types';
import { Config } from '../../types/ConfigType';

export const updateConfig = (config: Config) => {
  return {
    type: UPDATE_CONFIG,
    payload: config
  };
};
