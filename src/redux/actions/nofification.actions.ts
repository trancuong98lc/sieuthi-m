import { UPDATE_COUNT_NOTICE } from './action-types';

export const updateCountNoti = (quantity: number) => {
  return {
    type: UPDATE_COUNT_NOTICE,
    data: { countNoti: quantity }
  };
};
