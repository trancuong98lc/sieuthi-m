import { LoginState } from 'redux/reducers/islogin-reducer';
import { CHECK_LOGIN } from './action-types';

export const updateCheckLogin = (data: LoginState) => {
  return {
    type: CHECK_LOGIN,
    data: data
  };
};
