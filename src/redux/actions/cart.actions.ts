import { UPDATE_QUANTITY_CART } from './action-types';

export const updateQuantityCart = (quantity: number) => {
  return {
    type: UPDATE_QUANTITY_CART,
    data: quantity
  };
};
