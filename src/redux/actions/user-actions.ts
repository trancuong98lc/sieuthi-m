// import { User } from 'types/User';
// import { Provider } from 'types/provider';
// import { UPDATE_USER_INFO, UPDATE_COUNT_UNREAD_NOTI } from './actionTypes';

// export interface UserActionType {
//   type: typeof UPDATE_USER_INFO;
//   payload: User | Provider | object;
// }

// export function onUpdateUser(user: User | Provider | object): UserActionType {
//   return {
//     type: UPDATE_USER_INFO,
//     payload: user
//   };
// }

export function onUpdateCountReadNoti(count: number): any {
  return {
    // type: UPDATE_COUNT_UNREAD_NOTI,
    payload: count
  };
}
