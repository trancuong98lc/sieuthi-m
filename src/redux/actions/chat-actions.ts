import { UPDATE_MESSAGE_COUNT } from './action-types';

export const updateMessageCount = (messageCount: number) => {
  return {
    type: UPDATE_MESSAGE_COUNT,
    payload: messageCount
  };
};
