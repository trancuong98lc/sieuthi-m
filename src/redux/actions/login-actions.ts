import {
  WATCH_LOGIN,
  WATCH_CHECK_PHONE_EXISTS,
  UPDATE_USER_INFO,
  WATCH_REGISTER,
  WATCH_REFRESH_TOKEN,
  WATCH_LOGOUT,
  WATCH_CHECK_PHONE_NOT_EXISTS
} from './action-types';
import { RegisterBodyRequest } from 'redux/sagas/auth';
import { User } from 'types/user-type';
export interface LoginInput {
  phoneNumber: string;
  password: string;
}

export interface PhoneExistType {
  phoneNumber: string;
  screen: string;
}

export function onUpdateUserInfo(data?: User): any {
  return {
    type: UPDATE_USER_INFO,
    payload: data
  };
}

export function onRefreshToken(resolve?: () => void) {
  return {
    type: WATCH_REFRESH_TOKEN,
    resolve
  };
}

export function onLoginUser(data: LoginInput): any {
  return {
    type: WATCH_LOGIN,
    payload: data
  };
}

export function onLogout(data: any) {
  return {
    type: WATCH_LOGOUT,
    payload: data
  };
}

export function onRegisterUser(data: RegisterBodyRequest): any {
  return {
    type: WATCH_REGISTER,
    payload: data
  };
}

export function onCheckPhoneExists(data: PhoneExistType) {
  return {
    type: WATCH_CHECK_PHONE_EXISTS,
    payload: data
  };
}
export function onCheckPhoneNotExists(data: PhoneExistType) {
  return {
    type: WATCH_CHECK_PHONE_NOT_EXISTS,
    payload: data
  };
}
