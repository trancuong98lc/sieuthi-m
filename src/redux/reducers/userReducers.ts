import { User } from 'types/User';
import { UPDATE_USER_INFO, LOGOUT } from '../actions/action-types';

type State = {
  user?: User | {};
  token?: string;
  loadingUploadAvatar: boolean;
  loadingUploadCover: boolean;
  countUnreadNoti: number;
};

const initialState: State = {
  user: {},
  loadingUploadAvatar: false,
  loadingUploadCover: false,
  countUnreadNoti: 0
};

export const userReducers = (
  state: State = initialState,
  action: any
): State => {
  switch (action.type) {
    case UPDATE_USER_INFO: {
      const user = action.payload.user || action.payload || {};
      return {
        ...state,
        user
      };
    }
    case LOGOUT:
      return {
        ...state,
        user: null
      };
    default:
      return state;
  }
};
