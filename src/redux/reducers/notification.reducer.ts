import { UPDATE_COUNT_NOTICE } from 'redux/actions';

export interface NotifyState {
    countNoti:number
}

interface NotifyAction {
  type: string;
  data: NotifyState;
}

const initState = {
    countNoti:0
};

export const notificationReducer = (state: NotifyState = initState, action: NotifyAction) : NotifyState => {
  switch (action.type) {
    case UPDATE_COUNT_NOTICE:
      return {
        ...state,
        countNoti: action.data.countNoti
      };
    default:
      return {
        ...state
      };
  }
};
