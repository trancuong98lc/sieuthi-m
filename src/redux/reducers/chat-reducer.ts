import { UPDATE_MESSAGE_COUNT } from 'redux/actions';

interface Message {
    messageCount: number | undefined;
}

interface ChatAction {
  type: string;
  payload: number;
}

const initState = {
    messageCount: undefined
};

export const chat = (state: Message = initState, action: ChatAction): Message => {
  switch (action.type) {
    case UPDATE_MESSAGE_COUNT:
      return {
        ...state,
        messageCount: action.payload
      };
    default:
      return {
        ...state
      };
  }
};
