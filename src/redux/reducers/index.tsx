import { combineReducers } from 'redux';
import { userReducers } from './userReducers';
import { cart } from './cart.reducres';
import { isLogin } from './islogin-reducer';
import { notificationReducer } from './notification.reducer';
import { config } from './config-reducer';
import { chat } from './chat-reducer';


const rootReducers = combineReducers({
  userReducers,
  cart,
  isLogin,
  notificationReducer,
  config,
  chat
});

export default rootReducers;
export type AppState = ReturnType<typeof rootReducers>;
