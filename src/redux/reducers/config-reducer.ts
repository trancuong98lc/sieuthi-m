import { UPDATE_CONFIG } from 'redux/actions';
import { Config } from '../../types/ConfigType';

interface ConfigAction {
    type: string,
    payload: Config
}

const initState = {
    contentPayment: '',
    deliveryCondition: 0,
    estimatedDeliveryTime: 0,
    poinConversionRate: 0,
    quantityHotsale: 10
  };

export const config = (state: Config = initState, action: ConfigAction): Config => {
  switch (action.type) {
    case UPDATE_CONFIG:
        const config = action.payload;        
      return {
        ...state,
        contentPayment: config.contentPayment,
        deliveryCondition: config.deliveryCondition,
        estimatedDeliveryTime: config.estimatedDeliveryTime,
        poinConversionRate: config.poinConversionRate,
        quantityHotsale: config.quantityHotsale        
      };
    default:
      return {
        ...state
      };
  }
};
