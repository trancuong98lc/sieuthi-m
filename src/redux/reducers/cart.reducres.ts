import { UPDATE_QUANTITY_CART } from 'redux/actions';

interface Cart {
  quantity: number;
}

interface CartAction {
  type: string;
  data: number;
}

const initState = {
  quantity: 0
};

export const cart = (state: Cart = initState, action: CartAction): Cart => {
  switch (action.type) {
    case UPDATE_QUANTITY_CART:
      return {
        ...state,
        quantity: action.data
      };
    default:
      return {
        ...state
      };
  }
};
