import { CHECK_LOGIN } from 'redux/actions';

export interface LoginState {
  isLogin: boolean;
  screen: string
}

interface LoginAction {
  type: string;
  data: LoginState;
}

const initState = {
  isLogin: false,
  screen:''
};

export const isLogin = (state: LoginState = initState, action: LoginAction) : LoginState => {
  switch (action.type) {
    case CHECK_LOGIN:
      return {
        ...state,
        ...action.data
      };
    default:
      return {
        ...state
      };
  }
};
