export enum GENDER {
  FEMALE = 'FEMALE',
  MALE = 'MALE',
  OTHER = 'OTHER',
  ALL = 'ALL'
}

export const MESSAGE_STATUS = {
  NOREAD: 0,
  READ: 1,
  SENT: 2,
  RECEIVER: 3,
  PENDING: 4,
  FAILED: 5
};

export const CONVERSATION_TYPE = {
  PRIVATE: 1,
  GROUP: 2
};

export const ONLINE = {
  OFFLINE: 0,
  ONLINE: 1,
  ABSENT: 2
};

export const ONLINE_KEY = {
  [ONLINE.OFFLINE]: 'chat.offline',
  [ONLINE.ONLINE]: 'chat.online',
  [ONLINE.ABSENT]: 'chat.absent'
};

export const MESSAGE_TYPE = {
  TIME: 1,
  TEXT: 2,
  IMAGE: 3,
  ALBUM: 4,
  LINK: 5,
  DELETE: 6
};
export const GENDER_NAME = {
  [GENDER.MALE]: 'user_infor.male',
  [GENDER.FEMALE]: 'user_infor.female',
  [GENDER.OTHER]: 'user_infor.other',
  [GENDER.ALL]: 'profile.all'
};

export const GENDER_TYPE = {
  [GENDER.FEMALE]: 'ic_femalee',
  [GENDER.MALE]: 'ic_male',
  [GENDER.OTHER]: 'ic_other',
  [GENDER.ALL]: 'non_gender'
};

////////////////

export const NOTIFY_STATUS = {
  NOREAD: false,
  READ: true
};

export enum PhotoSize {
  SMALL,
  MEDIUM
}

export const VIDEO_TYPES = [
  'video/x-flv',
  'video/mp4',
  'video/MP2T',
  'video/3gpp',
  'video/quicktime',
  'video/x-msvideo',
  'video/x-ms-wmv',
  'video/mpeg',
  'video/ogg',
  'video/webm',
  'video/x-m4v',
  'video/ms-asf',
  'video',
  'VIDEO',
  'application/javascript',
  'application/vnd.apple.mpegurl',
  'application/x-mpegurl'
];

export const ImageMimeType = {
  gif: 'image/gif',
  jpeg: 'image/jpeg',
  jpg: 'image/jpeg',
  png: 'image/png'
};

export const ImageMime = {
  gif: 'gif',
  jpeg: 'jpeg',
  jpg: 'jpg',
  png: 'png'
};

export const mime = {
  [ImageMime.jpeg]: 'jpeg',
  [ImageMime.png]: 'png',
  [ImageMime.gif]: 'gif',
  [ImageMime.jpg]: 'jpg'
};

export const PASSWORD = {
  MIN: 6,
  MAX: 127
};

export const NAME_LENGTH = {
  MIN: 2,
  MAX: 50
};

export const LIKE_TYPE = {
  LIKE: 0,
  LIKED: 1
};

export const SWITCH_TYPE = {
  OFF: 0,
  ON: 1
};

export const VIDEO_HEIGHT = 248;

export const VIMEO_VERIFY_STATUS = {
  SUCCESS: 200
};

export const COMMENT_LENGTH = {
  MIN: 10,
  MAX: 1000
};

export const POST_DESCRIPTION_LENGTH = {
  MIN: 10,
  MAX: 5000
};
export const TEXT_DESCRIPTION_LENGTH = {
  MIN: 10,
  MAX: 2000
};

export const ABOUT_US_LENGTH = {
  MIN: 50,
  MAX: 5000
};

export const POST_TITLE_LENGTH = {
  MIN: 10,
  MAX: 100
};

export const PAGE_PRODUCT_TITLE_LENGTH = {
  MIN: 10,
  MAX: 100
};

export const PAGE_PRODUCT_PRICE_LENGTH = {
  MIN: 5,
  MAX: 13,
  SPACE: 3,
  PRICE_MAX: 1000000000
};

export const PAGE_PRODUCT_DESCRIPTION_LENGTH = {
  MIN: 10,
  MAX: 5000
};

export const PAGE_NAME_LENGTH = {
  MIN: 2,
  MAX: 50
};

// đã đưa vào cms
export const CONTENT_REVIEW_PAGE_LENGTH = {
  MIN: 25,
  MAX: 256
};

export const NUMBER_OF_LINES = {
  HOME_DESCRIPTION: 6,
  COMMENT: 2,
  VIDEO: 1
};

export enum STATUS_ORDER {
  ALL = 'ALL',
  NEW = 'NEW',
  CANCEL = 'CANCEL',
  PAID = 'PAID',
  NOT_PAID = 'NOT_PAID',
  DELIVERING = 'DELIVERING',
  DELIVERED = 'DELIVERED'
}

export enum MessageType {
  TEXT = 'TEXT',
  PHOTO = 'PHOTO',
  VIDEO = 'VIDEO',
  VOICE = 'VOICE',
  MEDIA = 'MEDIA',
  SYSTEM = 'SYSTEM'
}
export const LIMIT_RECORD = {
  FRIENDS: 10,
  MESSAGES: 20,
  MEDIA: 50,
  VIDEO: 1,
  FRIENDS_PROFILE: 15,
  POST_ON_PAGE: 15,
  PAGE_IMAGES: 10,
  PAGE_LIKED: 20,
  PAGE_PRODUCTS: 15,
  PAGE_REVIEWS: 15,
  ASTRAER: 100, // liên quan đến top điểm astra sửa cần chú ý hỏi trước => cảm ơn
  LIST_VIDEO: 10,
  SUGGESTIONS_FRIEND: 10
};
