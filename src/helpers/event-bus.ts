import { Subject } from 'rxjs';

export interface EventBusType {
  type: string;
  payload?: any;
}

export const EventBusName = {
  SUBTRACT_UNREAD_NOTIFICATION_EVENT: 'SUBTRACT_UNREAD_NOTIFICATION_EVENT',
  UPDATE_CONFERENCE_EVENT: 'UPDATE_CONFERENCE_EVENT',
  REMOVE_ITEM_CART: 'REMOVE_ITEM_CART',
  UPDATE_ITEM_CART: 'UPDATE_ITEM_CART',
  UPDATE_ADRESS: 'UPDATE_ADRESS',
  UPDATE_COUNT_CART: 'UPDATE_COUNT_CART',
  CREATE_PRODUCT: 'CREATE_PRODUCT',
  CREATE_ADDRESS: 'CREATE_ADDRESS',
  UPDATE_ORDER_HISTORY: 'UPDATE_ORDER_HISTORY',
  REMOVE_ADRESS: 'REMOVE_ADRESS',
  REMOVE_ADRESS_DETAIL: 'REMOVE_ADRESS_DETAIL',
  NEW_MESSAGE: 'NEW_MESSAGE',
  PERSONAL: 'PERSONAL'
};

export default class EventBus {
  private static instance: EventBus;

  private eventSubject = new Subject<EventBusType>();

  static getInstance(): EventBus {
    if (!EventBus.instance) {
      EventBus.instance = new EventBus();
    }
    return EventBus.instance;
  }

  get events(): any {
    return this.eventSubject.asObservable();
  }

  post(event: EventBusType): void {
    this.eventSubject.next(event);
  }
}
