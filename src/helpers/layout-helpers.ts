import { Platform, StatusBar, Dimensions } from 'react-native';
import {
  getBottomSpace,
  getStatusBarHeight
} from 'react-native-iphone-x-helper';

export function statusBarHeight(safe: boolean = true) {
  // console.log('safe: ', safe, StatusBar.currentHeight);
  return Platform.OS === 'ios'
    ? getStatusBarHeight(safe)
    : StatusBar.currentHeight || 0;
}

export function bottomSpaceHeight() {
  return Platform.OS === 'ios' ? getBottomSpace() / 2 : 0;
}

export function androidNavigationBarHeight(fromVideoScreen: boolean) {
  if (Platform.OS === 'ios') return 0;
  let deviceH = Dimensions.get('screen').height;
  // the value returned does not include the bottom navigation bar, I am not sure why yours does.
  let windowH = Dimensions.get('window').height;
  let bottomNavBarH = deviceH - windowH;
  if (fromVideoScreen) {
    if (bottomNavBarH > 50) bottomNavBarH = 50;
    return bottomNavBarH;
  } else {
    return bottomNavBarH;
  }
}
