export const URL_API = {
  PRODUCTS: '/product',
  LOGIN: '/user/login',
  LOGOUT: '/user/logout',
  CHECK_PHONE_EXIST: '/user/check-phone',
  REFRESH_TOKEN: '/user/refresh-token',
  REGISTER_ACCOUNT: '/user/register',
  STORE_LIST: '/store/list',
  CITY: '/city',
  DISTRCIT: '/district',
  WARD: '/ward',
  PROFILE: '/user/detail/',
  UPDATE_PROFILE: '/user/',
  AUTOSUGGES: '/autosuggest',
  UPDATE_AVATAR: '/user/avatar',
  CHANGE_PASS: '/user/change-password',
  FORGOT_PASS: '/user/forgot-password',

  //banner
  BANNER: '/general-configurations/banner',

  //category
  CATEGORIES: '/product/category',

  //hot sale
  HOTSALE: '/product/getHotSale/',

  //endow
  ENDOW: '/promotion/app/',
  // DETAIL_ENDOW: '/promotion/',

  //Product
  PRODUCT: '/product/getByStore/',

  //Promotion
  PROMOTION: '/product/promotions/',

  //HotNews
  HOTNEWS: '/news/list/',
  HOTNEWS_DETAIL: '/news/detail/',

  //Support
  SUPPORT: '/questions/listApp/',

  ///TERMS DELIVERY
  // TERMS_DELIVERY: '/terms-delivery',

  //contact
  CONTACT: '/communication/detail/',

  //Condition Delivery
  CONDITION_DELIVERY: '/conditiondelivery',

  //cart
  CART_CREATE: '/cart/create',
  CART_DETAIL: '/cart/detail',
  CART_ADD_REMOVE: '/cart/addOrRemoveItem',
  CART_TOTAL_PRODUCT: '/cart/totalProductItem/',
  ORDER_DETAIL: '/order/',
  ORDER_CREATE: '/order/create',
  ADD_DELIVERY: '/user/addDeliveryAddress',
  LIST_DELIVERY: '/user/delivery-address',
  DETAILT_DELIVERY: '/user/detail-address/',
  UPDATE_DELIVERY: '/user/delivery-address/',
  ORDER_HISTORY: '/order/orderHistory',
  ORDER_UPDATE_STATUS: '/order/updateStatus',
  ORDER_PROMOTION: '/order/promotion',
  //media
  UPLOAD_FILE: '/media',
  UPLOAD_MULTI: '/media/multi',
  //bank
  BANK_LIST: '/bank/list',

  /// notification
  LIST_NOTIFICATION: '/notification/listApp',
  COUNT_NOTIFICATION: '/notification/count',
  RESET_NOTIFICATION: '/notification/markAllAsSeen',
  MAKE_NOTIFICATION: '/notification/markOneAsSeen',

  ///ranking
  RANK_USER: '/user/ranking-detail',

  //general config
  GENERAL_CONFIG: '/general-configurations',
  
  //Message
  MESSAGE: '/message',
  MESSAGE_UNREAD: '/message/messageisnotread/'
};
