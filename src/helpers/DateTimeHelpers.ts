const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = MINUTE * 60;
const DAY = 24 * HOUR;
const MONTH = 30 * DAY;
const YEAR = 12 * MONTH;

const TIME_VALUE = {
  SECOND,
  MINUTE,
  HOUR,
  DAY,
  MONTH,
  YEAR
};

export default {
  TIME_VALUE,
};
