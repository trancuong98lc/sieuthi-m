import { Store_Type } from 'features/store/model/store-entity';
import { ConfigType } from 'types/ConfigType';
import { User } from 'types/user-type';

let idToken: any = null;
let useOTPFirebase: boolean = false;
let countResentPhoneNumber = 1;
let keyboardHeight = 0;
let idUser: any = null;
let currentConverationId = '';
let isInternetConnected = true;
let tokenFirebase = '';
let sensitives: string[] = [];
let activeRouteKey: string = '';
let configs: Map<string, ConfigType> = new Map();
let idStore: any = null;
let store: any = null;
let PlayerId: any = null;

export default {
  idToken,
  keyboardHeight,
  countResentPhoneNumber,
  idUser,
  currentConverationId,
  isInternetConnected,
  tokenFirebase,
  sensitives,
  activeRouteKey,
  configs,
  useOTPFirebase,
  idStore,
  store,
  PlayerId
};
