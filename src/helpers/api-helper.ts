/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import Config from 'react-native-config';
import commons from './Commons';
import { BaseResponse, STATUS } from 'types/BaseResponse';
import { Alert, Platform } from 'react-native';
import { translate } from 'res/languages';
import EventBus, { EventBusName } from './event-bus';
import socketIO from 'socket.io-client';
import { URL_API } from './url-api';
import Commons from './Commons';
const REQ_TIMEOUT = 15 * 1000;

const instance = axios.create({
  baseURL: Config.API_URL,
  timeout: REQ_TIMEOUT
});
const instanceMap = axios.create({
  baseURL: Config.HEAR_API,
  timeout: REQ_TIMEOUT
});

instance.interceptors.request.use((_config) => requestHandler(_config));
instanceMap.interceptors.request.use((_config) => requestHandler(_config));

const requestHandler = (request: AxiosRequestConfig) => {
  if (__DEV__) {
    console.log(`Request API: ${request.url}`, request.params, request.data);
  }

  return request;
};

instance.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error)
);

instanceMap.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error)
);

const errorHandler = (error: any) => {
  if (__DEV__) {
    console.log(error);
  }
  // EventBus.getInstance().post({
  //   type:EventBusName.
  // })
  return Promise.reject({ ...error });
};

const successHandler = (response: AxiosResponse) => {
  if (__DEV__) {
    console.log(`Response API: ${response.config.url}`, response.data);
  }
  return response.data || response.results;
};

async function fetch<ReqType, ResType extends BaseResponse>(
  url: string,
  params?: any,
  isAuth?: boolean
): Promise<ResType> {
  let headers = null;
  if (isAuth) {
    headers = {
      Authorization: `Bearer ${commons.idToken}`,
      Retailer: 'hethongdmart'
    };
  }

  return instance.get(url, { params, headers });
}
async function fetchLocation(url: string, params?: any, isAuth?: boolean) {
  let headers = null;
  headers = {
    Accept:
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Content-Type': 'application/json'
  };

  return instanceMap.get(url, { params });
}

async function post<ReqType, ResType extends BaseResponse>(
  url: string,
  data?: any,
  isAuth?: boolean
): Promise<ResType> {
  let headers = null;
  if (isAuth) {
    headers = {
      Authorization: `Bearer ${commons.idToken}`
    };
  }

  return instance.post(url, { ...data }, { headers });
}
async function deletes<ReqType, ResType extends BaseResponse>(
  url: string,
  data?: any,
  isAuth?: boolean
): Promise<ResType> {
  let headers = null;
  if (isAuth) {
    headers = {
      Authorization: `Bearer ${commons.idToken}`
    };
  }
  return instance.delete(url, { data: { ...data }, headers: { ...headers } });
}

async function postTokenMap(url: string, data?: any, isAuth?: boolean) {
  let headers = null;
  if (isAuth) {
    headers = {
      Authorization: `Bearer ${commons.idToken}`
    };
  }

  return instance.post(url, { ...data }, { headers });
}

// export function checkToken(res: any) {
//   if (res && res.code === STATUS.INVALID_TOKEN) {
//     return new Promise<Object>((resolve, reject) => {
//       Alert.alert(
//         translate('notify'),
//         translate('tokenInvalid'),
//         [{ text: translate('common.yes'), onPress: () => logout(true) }],
//         { cancelable: false }
//       );
//     });
// }

async function postForm<ReqType, ResType extends BaseResponse>(
  url: string,
  data: any,
  isAuth?: boolean
): Promise<ResType> {
  let headers = null;
  if (isAuth) {
    headers = {
      Accept: '*/*',
      Authorization: `Bearer ${commons.idToken}`,
      'Content-Type': 'multipart/form-data'
    };
  }

  return instance.post(url, data, { headers });
}

async function put<ReqType, ResType extends BaseResponse>(
  url: string,
  data?: any,
  isAuth?: boolean
): Promise<ResType> {
  let headers = null;
  if (isAuth) {
    headers = {
      Authorization: `Bearer ${commons.idToken}`
    };
  }

  return instance.put(url, { ...data }, { headers });
}
function SubscribeSocket(): SocketIOClient.Socket {
  const PlayerId = Commons.PlayerId;
  const urlSocket = Config.API_URL + URL_API.MESSAGE;
  return socketIO(urlSocket, {
    transports: ['polling'],
    query: { authorization: `Bearer ${commons.idToken}`, PlayerId: PlayerId }
  });
}

const ApiHelper = {
  fetch,
  post,
  put,
  fetchLocation,
  postTokenMap,
  postForm,
  deletes,
  SubscribeSocket
};
export default ApiHelper;
