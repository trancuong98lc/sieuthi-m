import Config from 'react-native-config';
import { User } from 'types/user-type';
import RNFetchBlob from 'rn-fetch-blob';
import { MediaType, photoType, PHOTO_SIZE } from 'types/ConfigType';
import images from 'res/images';
import R from 'res/R';
import moment from 'moment';
import { translate } from 'res/languages';
import DateTimeHelpers from './DateTimeHelpers';

export const validatePhone = (str: string) => {
  var re = /^[0-9\+]{9,14}$/;
  return re.test(str);
};

export const validateEmoji = (str: string): boolean => {
  try {
    var emojiRegex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;

    if (str.match(emojiRegex)) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};
export const validateName = (str: string) => {
  // var re =  /[$&+,:;=?@#|'<>.^*()%!-\[\]{}$\/\\]/
  let re = /[!$%^&*()+|~=`{}\[\]:\/;<>?,.@#©®™℅•√π÷×¶∆£¢€¥^°₫\[0-9\]]/;
  return re.test(str);
};

export function _formatPrice(num: string | number) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

export const counterFormat = (num: number) => {
  if (!num) return '0';
  if (typeof num === 'string') num = parseInt(num);
  if (num > 9) return '9+';
  return num;
};

export const validateEmail = (str: string) => {
  if (!str) return false;
  let re = /^([^<>()\[\]\\.,;:\s@"]+((?:\.[a-zA-Z0-9_]+)*))@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,3}$/;
  let check = re.test(str.toString());
  return check;
};

export const getOnlyPhoneNumber = (str: string): string => {
  try {
    if (!str) return '';
    return str.replace(/[!$%^&*()+|~=`{}\[\]:\/;<>?,.@#a-zA-Z ]/gi, '');
  } catch (error) {
    return str || '';
  }
};

export const getFileSize = async (uri: string) => {
  try {
    const { size } = await RNFetchBlob.fs.stat(uri);
    return size;
  } catch (error) {
    console.log('getFileSize error: ', error);
    return 0;
  }
};

export const getPhotoUrl = (user?: User) => {
  if (!user || !user.avatar) return null;
  return Config.API_URL + user.avatar;
};
export const getImageUrl = (url: string) => {
  if (!url) return null;
  return Config.API_URL + '/' + url;
};

export const getRotation = (originalRotation?: number): number => {
  let rotation = 0;
  if (originalRotation === 90) {
    rotation = 90;
  } else if (originalRotation === 270) {
    rotation = -90;
  }
  return rotation;
};

export const getMediaUrl = (thumnail?: MediaType | null, size?: string) => {
  if (thumnail) {
    let url = thumnail.thumbnail || thumnail.url;
    if (size) {
      switch (size) {
        case PHOTO_SIZE.Small:
          url = thumnail.thumbnail2x;
          break;
        case PHOTO_SIZE.Medium:
          url = thumnail.thumbnail;
          break;
        case PHOTO_SIZE.Large:
          url = thumnail.url;
          break;
        default:
          url;
          break;
      }
    }
    return { uri: `${Config.API_URL}/${url}` };
  }
  return images.raucu;
};
export const getLogoUrl = (logo?: string) => {
  if (logo) {
    let url = logo;
    return { uri: `${Config.API_URL}/${url}` };
  }
  return R.images.raucu;
};

// Tín khoảng cách theo đường chim bay

const toRad = (value: number) => (value * Math.PI) / 180;
const robustAcos = (value: number): number => {
  if (value > 1) {
    return 1;
  }
  if (value < -1) {
    return -1;
  }

  return value;
};

export const getDistance = (p1: any, p2: any, accuracy: number = 1) => {
  const earthRadius = 6378137;
  accuracy = typeof accuracy !== 'undefined' && !isNaN(accuracy) ? accuracy : 1;

  const fromLat = p1.latitude;
  const fromLon = p1.longitude;
  const toLat = p2.latitude;
  const toLon = p2.longitude;

  const distance =
    Math.acos(
      robustAcos(
        Math.sin(toRad(toLat)) * Math.sin(toRad(fromLat)) +
          Math.cos(toRad(toLat)) *
            Math.cos(toRad(fromLat)) *
            Math.cos(toRad(fromLon) - toRad(toLon))
      )
    ) * earthRadius;

  return Math.round(distance / accuracy) * accuracy || 0.1;
};

export const stripHTMLTag = (html: string) => {
  if (!html || html === '') {
    return '';
  }
  return html
    .replace(/(<([^>]+)>)/gi, ' ')
    .replace(/\&nbsp;/g, '')
    .trim();
};

export function DiffDate(dateCreated: any) {
  let date_from = moment(dateCreated);
  let date_to = moment(new Date());
  let diff = date_to.diff(date_from);
  const diffDuration = moment.duration(diff).asMilliseconds();

  if (diffDuration < DateTimeHelpers.TIME_VALUE.MINUTE) {
    return translate('format_time.just_now');
  }

  if (diffDuration < DateTimeHelpers.TIME_VALUE.HOUR) {
    return `${moment.duration(diff).minutes()} ${translate(
      'format_time.minute_ago'
    )}`;
  }

  if (diffDuration < DateTimeHelpers.TIME_VALUE.DAY) {
    return `${moment.duration(diff).hours()} ${translate(
      'format_time.hour_ago'
    )}`;
  }

  if (diffDuration <= DateTimeHelpers.TIME_VALUE.MONTH) {
    return `${moment.duration(diff).days()} ${translate(
      'format_time.day_ago'
    )}`;
  }
  if (diffDuration < DateTimeHelpers.TIME_VALUE.YEAR) {
    return `${Math.floor(diff / DateTimeHelpers.TIME_VALUE.MONTH)} ${translate(
      'format_time.month_ago'
    )}`;
  }
  return `${moment.duration(diff).years()} ${translate(
    'format_time.year_ago'
  )}`;
}
