import AsyncStorage from '@react-native-community/async-storage';

export enum StorageKey {
	LAST_TIME_SHOW_RATING_DIALOG = 'LAST_TIME_SHOW_RATING_DIALOG',
	CAN_SHOW_RATING_DIALOG = 'CAN_SHOW_RATING_DIALOG',
	ID_TOKEN = 'ID_TOKEN',
	PHONE_NUMBER_SUB = 'PHONE_NUMBER_SUB',
	REFRESH_TOKEN = 'REFRESH_TOKEN',
	LOGIN_SESSION = 'LOGIN_SESSION',
	LOGIN_SESSION_DRAFT = 'LOGIN_SESSION_DRAFT',
	POST_DRAFT = 'POST_DRAFT',
	PAGE_POST_DRAFT = 'PAGE_POST_DRAFT',
	LIST_SEARCH = 'LIST_SEARCH',
	INTERESTED_SCREEN = 'INTERESTED_SCREEN',
	CONFIGS = 'CONFIGS',
	LOCAL_CONTACT_DATAS = 'SYNC_CONTACT_SUCESS',
	CHECK_SCAN_FRIENDS = 'CHECK_SCAN_FRIENDS',
	ID_USER = 'ID_USER',
	STORE = 'STORE'
}

function save(key: StorageKey, value: any) {
	AsyncStorage.setItem(key, value);
}

async function get(key: StorageKey) {
	return AsyncStorage.getItem(key);
}

async function remove(key: StorageKey) {
	return AsyncStorage.removeItem(key);
}

// sử dụng nếu AsyncStorage dữ liệu dạng Json object
async function getObject(key: string) {
	let value: any = await AsyncStorage.getItem(key);
	return JSON.parse(value);
}
async function getArray(key: string) {
	let value: any = await AsyncStorage.getItem(key);
	return JSON.parse(value);
}

// sử dụng nếu AsyncStorage dữ liệu dạng Json object
function saveObject(key: string, value: any) {
	AsyncStorage.setItem(key, JSON.stringify(value));
}

function saveArray(key: string, value: any[]) {
	AsyncStorage.setItem(key, JSON.stringify(value));
}

export default {
	save,
	get,
	remove,
	getObject,
	saveObject,
	saveArray,
	getArray
};
