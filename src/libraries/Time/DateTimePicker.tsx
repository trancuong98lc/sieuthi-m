import * as React from 'react';
import DateTimer, {
  DateTimePickerProps
} from 'react-native-modal-datetime-picker';
import 'moment/locale/vi';
import { translate } from 'res/languages';

export const DateTimePicker = (props: DateTimePickerProps) => {
  return (
    // @ts-ignore
    <DateTimer
      {...props}
      pickerContainerStyleIOS={{ paddingLeft: 35, width: '100%' }}
      headerTextIOS={
        props.headerTextIOS || translate('dateTimePicker.headerTextIOS')
      }
      confirmTextIOS={
        props.confirmTextIOS || translate('dateTimePicker.confirmTextIOS')
      }
      cancelTextIOS={
        props.cancelTextIOS || translate('dateTimePicker.cancelTextIOS')
      }
      locale="vi"
    />
  );
};
