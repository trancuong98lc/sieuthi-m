import * as React from 'react';
import {
  ImageSourcePropType,
  TouchableOpacity,
  StyleProp,
  ViewStyle,
  ImageStyle,
  View
} from 'react-native';
import R from 'res/R';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';
import { styles } from 'features/vegetable/view/vegetable.style';
import { counterFormat } from 'helpers/helper-funtion'

interface BaseIconProps {
  icon?: ImageSourcePropType;
  name?: string | '';
  width?: number;
  height?: number;
  onPress?: () => void;
  style?: StyleProp<ViewStyle>;
  iconStyle?: StyleProp<ImageStyle>;
  color?: string;
  activeOpacity?: number;
  bage?: number;
  disable?: boolean;
}

let iconSize = 24;

export default class BaseIcon extends React.PureComponent<BaseIconProps> {
  render() {
    let {
      width,
      height,
      icon,
      name,
      onPress,
      style,
      iconStyle,
      color,
      bage,
      activeOpacity
    } = this.props;
    if (!width) width = iconSize;
    if (!height) height = width;
    let disabled = false;
    if (this.props.disable) {
      disabled = this.props.disable;
    }
    if (onPress !== undefined) {
      return (
        <TouchableOpacity
          onPress={onPress}
          style={style}
          activeOpacity={activeOpacity}
          disabled={disabled}
        >
          <FastImage
            resizeMode="contain"
            source={icon || R.images[name]}
            style={[{ width, height }, iconStyle || {}]}
            tintColor={color}
          />
          {bage && bage > 0 ? <View style={styles.bage}>
            <DailyMartText style={{ color: 'white', fontSize: 12 }}>{counterFormat(bage)}</DailyMartText>
          </View> : null}
        </TouchableOpacity>
      );
    } else {
      return (
        <FastImage
          resizeMode="contain"
          source={icon || R.images[name]}
          style={[{ width, height }, iconStyle || {}]}
          tintColor={color}
        />
      );
    }
  }
}
