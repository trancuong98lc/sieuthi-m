import React, { PureComponent } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Animated,
  Image,
  Platform
} from 'react-native';
import colors from 'res/colors';
import { translate } from 'res/languages';
import NetInfo, {
  NetInfoState,
  NetInfoSubscription
} from '@react-native-community/netinfo';
import { DimensionHelper } from 'helpers/dimension-helper';
import { statusBarHeight } from 'helpers/layout-helpers';

interface IProps {
  home?: boolean;
  onInternetChange: (isConnected: boolean | null) => void;
}

interface IState {
  fadeAnmin: any;
  fadeAniminImage: any;
  isConnected: boolean;
}

interface IState {
  fadeAnmin: any;
  fadeAniminImage: any;
}

export default class NoInternet extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      fadeAnmin: new Animated.Value(0),
      fadeAniminImage: new Animated.Value(0)
    };
  }
  _subscription: NetInfoSubscription | null = null;

  componentDidMount() {
    this._subscription = NetInfo.addEventListener(
      this._handleConnectivityChange
    );
  }
  componentWillUnmount() {
    this._subscription && this._subscription();
  }

  _handleConnectivityChange = (state: NetInfoState) => {
    if (state.isInternetReachable) {
      this.notification(0);
    } else {
      if (this.props.home) {
        this.aniImage(56);
        this.notification(DimensionHelper.height);
      } else {
        this.notification(40);
      }
    }
    this.props.onInternetChange(state.isInternetReachable);
  };

  notification = (toValue: number) => {
    Animated.timing(this.state.fadeAnmin, {
      toValue,
      duration: 200
    }).start();
  };

  aniImage = (toValue: number) => {
    Animated.timing(this.state.fadeAniminImage, {
      toValue
    }).start();
  };

  render() {
    return (
      <Animated.View
        style={[
          styles.container,
          {
            height: this.state.fadeAnmin,
            zIndex: !this.props.home && 100,
            position: this.props.home ? 'relative' : 'absolute',
            borderTopLeftRadius: this.props.home ? 12 : 0,
            borderTopRightRadius: this.props.home ? 12 : 0,
            marginTop: this.props.home ? 12 : 0
          }
        ]}
      >
        <Animated.Text
          style={[
            styles.text,
            {
              alignSelf: 'center',
              height: this.state.fadeAnmin
            }
          ]}
        >
          {translate('no_internet')}
        </Animated.Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 40,
    position: 'absolute',
    top: 52,
    width: DimensionHelper.width,
    justifyContent: 'center',
    zIndex: 100,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: colors.white100
  },
  textHome: {
    color: '#888888',
    fontSize: 18,
    alignSelf: 'center'
  },
  text: {
    color: colors.redGg,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    marginTop: Platform.OS === 'ios' ? 20 : 0
  }
});
