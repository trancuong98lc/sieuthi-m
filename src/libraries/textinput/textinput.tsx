import BaseIcon from 'libraries/baseicon/BaseIcon';
import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  Image,
  TextStyle,
  ImageStyle,
  ViewStyle,
  KeyboardTypeOptions,
  ImageSourcePropType,
  TouchableOpacity,
  ReturnKeyType
} from 'react-native';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import R from 'res/R';

export interface AppProps {
  styleTextInput?: TextStyle;
  styleTextInputs?: ViewStyle;
  placeholder?: string;
  secureTextEntry?: boolean;
  showDeleteText?: boolean;
  onChangeText?: (text: string) => void;
  keyboardType?: KeyboardTypeOptions;
  autoFocus?: boolean;
  source?: ImageSourcePropType;
  styleImageTextInput?: ImageStyle;
  value?: string;
  maxLength?: number;
  onPress?: () => void;
  onPressRight?: () => void;
  editable?: boolean;
  onTouchStart?: () => void;
  onSubmitEditing?: () => void;
  returnKeyType?: ReturnKeyType;
}

export default class TextInputs extends React.PureComponent<AppProps, any> {
  constructor(props: AppProps) {
    super(props);
  }

  refInput = React.createRef<TextInput>();

  componentDidMount = () => {};

  enableFocus = () => {
    setTimeout(() => {
      this.refInput && this.refInput.current && this.refInput.current!.focus();
    }, 100);
  };

  public render() {
    const {
      source,
      onPress,
      onTouchStart,
      editable,
      showDeleteText,
      onPressRight
    } = this.props;
    return (
      <View style={[styles.style1, this.props.styleTextInputs]}>
        {showDeleteText ? (
          <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <FastImage
              source={source}
              resizeMode={FastImage.resizeMode.contain}
              style={[styles.style2, this.props.styleImageTextInput]}
            />
          </TouchableOpacity>
        ) : null}
        <TextInput
          {...this.props}
          ref={this.refInput}
          style={[this.props.styleTextInput, { fontFamily: R.fonts.regular }]}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          onChangeText={this.props.onChangeText}
          onTouchStart={onTouchStart}
          onSubmitEditing={this.props.onSubmitEditing}
          returnKeyType={this.props.returnKeyType}
          keyboardType={this.props.keyboardType}
          autoFocus={this.props.autoFocus}
          value={this.props.value}
          maxLength={this.props.maxLength}
        />
        {!showDeleteText && source ? (
          <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <FastImage
              source={source}
              resizeMode={FastImage.resizeMode.contain}
              style={[styles.style2, this.props.styleImageTextInput]}
            />
          </TouchableOpacity>
        ) : null}
        {showDeleteText && this.props.value && this.props.value?.length > 0 ? (
          <BaseIcon name={'ic_close'} width={10} onPress={onPressRight} />
        ) : null}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  style1: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#F7F7F8'
  },
  style2: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginRight: 10
  },
  style3: {
    width: 20,
    height: 20,
    resizeMode: 'contain'
  }
});
