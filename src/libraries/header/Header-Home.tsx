import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'res/R';
import { navigate } from 'routing/service-navigation';
import { StoreScreen, SearchScreen, NotifyScreen } from 'routing/screen-name';
import TextInputs from 'libraries/textinput/textinput';
import { translate } from 'res/languages';
import { DimensionHelper } from 'helpers/dimension-helper';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import { SCREEN_NAME } from 'helpers/utils';
import Commons from 'helpers/Commons';

export interface HeaderHomeProps {
  isEdit?: boolean;
  bage: number;
  onResetNotification: () => void;
}

export default class HeaderHome extends React.PureComponent<
  HeaderHomeProps,
  any
> {
  constructor(props: HeaderHomeProps) {
    super(props);
  }

  gotoSearchScreen = () => {
    navigate(SearchScreen);
  };

  gotoStoreScreen = () => {
    navigate(StoreScreen, { screen: SCREEN_NAME.HOME });
  };

  gotoNotification = () => {
    this.props.onResetNotification && this.props.onResetNotification();
    navigate(NotifyScreen);
  };

  public render() {
    const { isEdit, bage } = this.props;
    return (
      <View style={styles.header}>
        <BaseIcon
          name="ic_supermarket"
          width={23}
          iconStyle={{ flex: 1 }}
          onPress={this.gotoStoreScreen}
        />
        {/* <TouchableOpacity onPress={this.gotoSearchScreen} activeOpacity={0.6}> */}
        <TouchableOpacity
          activeOpacity={1}
          style={styles.styleTextInputs}
          onPress={this.gotoSearchScreen}
        >
          <TextInputs
            placeholder={translate('textinput.searchvegetable', {
              name: Commons.store ? Commons.store.name : 'Daily Mart'
            })}
            styleTextInputs={styles.styleTextInputs}
            styleTextInput={styles.styleTextInput}
            source={R.images.ic_search}
            editable={isEdit}
            autoFocus={false}
            onTouchStart={this.gotoSearchScreen}
            styleImageTextInput={styles.styleImageTextInput}
          />
        </TouchableOpacity>
        {/* </TouchableOpacity> */}

        <BaseIcon
          name="ic_notify"
          width={23}
          iconStyle={{ flex: 1 }}
          bage={bage}
          onPress={this.gotoNotification}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: R.colors.primaryColor,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 12
  },
  styleTextInputs: {
    paddingHorizontal: 12,
    height: 35,
    width: '100%',
    flex: 1
  },

  styleTextInput: {
    fontSize: 13,
    color: '#8D8D8D',
    alignItems: 'center',
    flex: 1,
    maxHeight: 35
  },

  styleImageTextInput: {
    height: 15,
    width: 15
  }
});
