import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import colors from 'res/colors';
import images from 'res/images';
import { translate } from 'res/languages';
import R from 'res/R';
import { NotifyScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { DimensionHelper } from 'helpers/dimension-helper';
import { TextInput } from 'react-native-gesture-handler';

export interface HeaderServiceProps {
	title: string;
	headerIconCenter?: any,
	iconRight?: string;
	leftIcon?: string;
	onBackPress?: any;
	sizeIcon?: number;
	onPressRight?: () => void
	isTab?: boolean,
	width?: number,
	searchInput?: any,
	searchInputChangeText?: (search: string) => void
}

export default class HeaderService extends React.PureComponent<HeaderServiceProps, any> {
	constructor(props: HeaderServiceProps) {
		super(props);
	}

	onBackPress = () => {
		const { onBackPress } = this.props;
		if (onBackPress) {
			onBackPress();
		} else {
			goBack();
		}
	};

	onPressIcon = () => {
		const { onPressRight } = this.props;
		if (onPressRight) {
			return onPressRight()
		}
		navigate(NotifyScreen)
	}

	public render() {
		const { title, iconRight, leftIcon, sizeIcon, isTab, onPressRight, width, headerIconCenter, searchInput, searchInputChangeText } = this.props;
		return (
			<View style={styles.header}>
				<BaseIcon
					name={isTab ? '' : leftIcon || 'ic_back2'}
					width={sizeIcon || 30}
					iconStyle={{ flex: 1 }}
					onPress={this.onBackPress}
				/>
				<View style={styles.headerCenter}>
					{
						searchInput ? <><TextInput
							placeholder={translate('textinput.search')}
							onChangeText={searchInputChangeText}
							autoCapitalize={'none'}
							style={styles.searchInputStyle}/>
							<FastImage source={images.ic_search} style={styles.inputIcon} />
						</>
							:
							<>{headerIconCenter ? <FastImage source={headerIconCenter} style={styles.imageHeader} /> : null}
								<DailyMartText style={styles.text}>{translate(title)}</DailyMartText></>
					}
				</View>


				{iconRight ? (
					<BaseIcon
						name={iconRight}
						width={width || 23}
						iconStyle={{ flex: 1 }}
						onPress={this.onPressIcon}
					/>
				) : (
						<View style={{ width: 23 }} />
					)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: R.colors.primaryColor,
		height: 50,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: 12
	},
	headerCenter: {
		flexDirection: "row"
	},
	imageHeader: {
		width: 0.04 * DimensionHelper.getScreenWidth(),
		height: 0.053 * DimensionHelper.getScreenWidth(),
		marginRight: 10,
		marginLeft: -5
	},
	styleTextInputs: {
		paddingHorizontal: 12,
		height: 35,
		marginHorizontal: 12,
		width: '100%',
		flex: 1
	},

	styleTextInput: {
		fontSize: 13,
		color: '#8D8D8D',
		alignItems: 'center',
		flex: 1,
		maxHeight: 35
	},

	styleImageTextInput: {
		height: 15,
		width: 15
	},
	text: {
		color: colors.white100,
		fontSize: 18,
		fontWeight: 'bold'
	},
	searchInputStyle: {
		height: 35,
		width: DimensionHelper.getScreenWidth() - 100,
		backgroundColor: "#F7F7F8",
		borderRadius: 50,
		color: '#8D8D8D',
		paddingLeft: 0.09 * DimensionHelper.getScreenWidth(),
		paddingRight: 10
	},
	inputIcon: {
		position: 'absolute',
		left: 10, top: 10,
		width: 0.04 * DimensionHelper.getScreenWidth(),
		height: 0.04 * DimensionHelper.getScreenWidth(),
	}
});
