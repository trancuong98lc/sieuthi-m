import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import R from '/res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import { navigate } from 'routing/service-navigation';
import { ReceivedDetailScreen } from 'routing/screen-name';
import { Cart } from 'features/cart/model/cart';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import FastImage from 'react-native-fast-image';
import { Store_Type } from 'features/store/model/store-entity';
import BaseIcon from 'libraries/baseicon/BaseIcon';

interface Items {
     orderCode: string,
     nameSupermarket: string,
     newcost: string,
     oldcost: string,
     image: any,
     sold: number,
     product: []
}

export interface Props {
     item?: any
     index: number
}

class ViewProduct extends React.PureComponent<Props, any> {
     constructor(props: Props) {
          super(props);
          this.state = {

          }
     }

     public render() {
          // console.log(a);
          const { item } = this.props
          return (
               <View style={styles.containerProduct}>
                    <View style={styles.hrProduct} />
                    <View style={styles.itemProduct}>
                         <FastImage style={styles.imageProduct} source={getMediaUrl(item && item.medias && item.medias.length > 0 ? item?.medias[0] : null)} />
                         <View>
                              <DailyMartText style={styles.name}>{item?.name || ''}</DailyMartText>
                              <DailyMartText style={styles.cost}>{_formatPrice(item?.price || 0)}</DailyMartText>
                         </View>
                         <DailyMartText style={styles.count}> x{item?.qty || 0}</DailyMartText>
                    </View>
               </View>
          );
     }
}

export default class FlatListReceived extends React.PureComponent<Props, any> {
     constructor(props: Props) {
          super(props);
          this.state = {

          }
     }

     renderItemProduct = ({ item, index }: any) => {
          const newItem: Cart = { ...item, ...item.product! }
          delete newItem.product
          return <ViewProduct item={newItem} index={index} />;
     };
     private keyExtractor = (item: Cart): string => item.productId.toString();
     public render() {
          const { item } = this.props
          const { cart } = item
          const { orderedStore } = cart
          const store: Store_Type = JSON.parse(orderedStore)
          return (
               <View style={styles.container}>
                    <View style={styles.header}>
                         <BaseIcon name="ic_store" width={20} />
                         <DailyMartText style={styles.nameSupermarket}>{store.name}</DailyMartText>
                         {/* <FastImage style={styles.ic_next} source={R.images.ic_nextPerson} /> */}
                    </View>
                    <FlatList
                         data={cart.items}
                         keyExtractor={this.keyExtractor}
                         renderItem={this.renderItemProduct}
                    />
                    <View style={styles.hr}></View>
               </View>

          );
     }
}

const styles = StyleSheet.create({
     container: {
          marginTop: 10,
          backgroundColor: "#FFFFFF",

     },
     header: {
          flexDirection: 'row',
          alignItems: 'center',
     },
     ViewCodeOrder: {
          justifyContent: 'center',
          alignContent: 'center',

          //position: 'absolute',
     },
     imageCodeOrder: {
          height: 35.06,
          width: 99
     },
     textCodeOrder: {
          position: 'absolute',
          fontSize: 15,
          color: R.colors.white100,
          marginLeft: 6
     },
     image: {
          height: 19,
          width: 19,
          marginLeft: 10
     },
     nameSupermarket: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: R.colors.primaryColor,
          marginLeft: 10,
          flex: 1
     },
     ic_next: {
          width: 8,
          height: 14,
          marginRight: 12

     },
     containerProduct: {
          marginTop: 10

     },
     hrProduct: {
          borderTopWidth: 1,
          borderColor: '#F2F2F2'
          // borderColor:'red'
     },
     itemProduct: {
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 10
     },
     imageProduct: {
          height: 70,
          width: 70,
          marginLeft: 10
     },
     name: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#333333',
          width: DimensionHelper.getScreenWidth() * 0.6,
          marginLeft: 10,
          // marginTop:-10
     },
     cost: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#3BBB5A',
          marginLeft: 10,
          marginTop: 40
     },
     count: {
          fontSize: 18,
          color: '#A8A8A8',
          //marginTop:40
     },
     hr: {
          borderTopWidth: 10,
          borderColor: '#F5F5F5',
          marginTop: 10
     }


})