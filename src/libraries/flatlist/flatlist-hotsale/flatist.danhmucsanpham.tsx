import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, ViewStyle, TextStyle } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';

interface Item {
  _id: string,
  name?: string,
  image?: any,

}
export interface FlatListDanhmucsanphamProps {
  item: Item,
  index: number,
  touchableOpacity?: ViewStyle,
  text?: TextStyle,
  selected: number,
  getHotsalesByCategory: (categoryId: string) => void,
  changeCategory: (selected: number) => void
}

export default class FlatListDanhmucsanpham extends React.PureComponent<FlatListDanhmucsanphamProps, any> {
  constructor(props: FlatListDanhmucsanphamProps) {
    super(props);
    this.state = {}
  }

  public render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={[styles.touchableopacity, {backgroundColor: this.props.selected === this.props.index ? R.colors.primaryColor : "transparent"}]} 
          onPress={this.selectCategory}>
          <DailyMartText numberOfLines={1} style={[styles.name, {color: this.props.selected === this.props.index ? "#FFFFFF" : "#6B6B6B"}]}>{this.props.item.name}</DailyMartText>
        </TouchableOpacity>
      </View>
    );
  }

  selectCategory = () => {
    this.props.changeCategory(this.props.index);
    this.props.getHotsalesByCategory(this.props.item._id);
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    height: 30
  },
  touchableopacity: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    paddingHorizontal: 15,
    maxWidth: 0.5 * DimensionHelper.getScreenWidth(),
    height: 0.09 * DimensionHelper.getScreenWidth(),
    borderRadius: 20,
    marginTop: 15,
    marginHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  name: {
    fontSize: 15,
    color: '#6B6B6B',
  }
})