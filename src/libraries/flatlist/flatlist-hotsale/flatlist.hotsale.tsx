import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import colors from '../../../res/colors';
import R from '/res/R';
import { navigate } from 'routing/service-navigation';
import { ProductDetailScreen } from 'routing/screen-name';

import FastImage from 'react-native-fast-image';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';
import images from 'res/images';
import { translate } from 'res/languages';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface FlatListHotsaleProps {
  item?: any;
  index: number;
}

export interface ProductItemSaleStates {
  timeLeft: number,
  timeLeftText: string
}

export default class FlatListHotsale extends React.PureComponent<
  FlatListHotsaleProps,
  any
  > {
    interval: any;
  constructor(props: FlatListHotsaleProps) {
    super(props);
    this.state = {
      timeLeft: Date.parse(this.props.item.attributeProduct.endDate),
      timeLeftText: ''
    };
  }

  componentDidMount = () => {
    this.countdownTime();
  }

  navigateDetailProduct = () => {
    const { item } = this.props;
    navigate(ProductDetailScreen, {
      _id: item._id,
      quantity: item.attributeProduct.qtyDefault
    });
  }

  onCheckPrice(): number {
    const { item } = this.props;
    let b = Number(item.attributeProduct.qtySelled ?? 0);
    let c = Number(item.attributeProduct.qtyDefault ?? 0);
    return c === 0 ? 1 : (b / c);
  }

  text() {
    const { item } = this.props;
    const product = item.attributeProduct;
    if (product.status === 'OUT_PRODUCT' || product.qtyDefault === 0) {
      return translate('product_item.sold_out');
    }
    if (product.status === 'NEAR_OUT_PRODUCT') {
      return translate('product_item.about_to_sell_out');
    }
    if (!product.status || product.status === '') {
      return translate('product_item.sold', { price: product.qtySelled});
    }
  }

  countdown = (time: number) => {
    let seconds = Math.floor(time / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    let days = Math.floor(hours / 24);

    hours = hours - (days * 24);
    minutes = minutes - (days * 24 * 60) - (hours * 60);
    seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

    let daysText = days === 0 ? '' : `${days} ngày `;
    let hoursText = hours === 0 ? '' : hours < 10 ? `0${hours}:`: `${hours}:`;
    let minutesText = minutes === 0 ? '' : minutes < 10 ? `0${minutes}:` : `${minutes}:`;
    let secondsText = `${(seconds < 10 ? `0${seconds}` : seconds)}${((days === 0 && hours === 0 && minutes === 0) ? " giây" : '')}`;

    return `${daysText}${hoursText}${minutesText}${secondsText}`;
  }

  countdownTime = () => {
    const { timeLeft } = this.state;
    this.interval = setInterval(() => {
      if (timeLeft - new Date().getTime() <= 0) {
        clearInterval(this.interval);
        this.setState({
          timeLeft: 0,
          timeLeftText: translate('product_item.out_of_time')
        });
      } else {
        this.setState({
          timeLeft: timeLeft - 1000,
          timeLeftText: this.countdown(timeLeft - new Date().getTime())
        });
      }
    }, 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.interval);
    this.setState = (state, callback)=>{
      return;
    };
  }

  public render() {
    const { timeLeft, timeLeftText } = this.state;
    const { item, index } = this.props;
    let product = item.attributeProduct;
    const soldPercentage = this.onCheckPrice();
    return (
      <TouchableOpacity style={styles.container} onPress={this.navigateDetailProduct}>
        <View>
          <FastImage source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)} style={styles.image} />
          <FastImage source={images.ic_percent} style={styles.imagePercent}>
            <DailyMartText style={styles.percentValue}>
            {`-${product.preferentialAmount >= 100 ? 100 : product.preferentialAmount}%`}
            </DailyMartText>
          </FastImage>
          <DailyMartText numberOfLines={2} style={styles.name}>
            {item.name}
          </DailyMartText>
          {item.unit && <DailyMartText numberOfLines={1} style={styles.unit}>
          {`${translate('unit')} ${item.unit.name}`}
        </DailyMartText>}
          <View style={{ width: '90%' }}>
            <DailyMartText style={styles.newcost}>{_formatPrice(product.totalAll)} đ</DailyMartText>
            <DailyMartText style={styles.oldcost}>{_formatPrice(item.price)} đ</DailyMartText>
            <DailyMartText style={styles.text1}>{`${timeLeft <= 0 ?  '' : 'Còn'} ${timeLeftText}`}</DailyMartText>
          </View>
          <View style={styles.view1}>
            <View
              style={[
                styles.view2,
                {
                  width:
                    soldPercentage * (0.5 * DimensionHelper.getScreenWidth() - 20),
                  backgroundColor:
                    soldPercentage == 1 ? '#E5334B' : '#3BBB5A',
                    borderTopEndRadius: soldPercentage == 1 ? 50 : 0,
                    borderBottomEndRadius: soldPercentage == 1 ? 50 : 0,
                }
              ]}
            ></View>
            <DailyMartText style={[styles.text2]}>{this.text()}</DailyMartText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 0.5 * DimensionHelper.getScreenWidth() - 20,
    marginVertical: 10,
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
  },
  image: {
    // marginTop: 20,
    width: 0.45 * DimensionHelper.getScreenWidth(),
    height: 0.45 * DimensionHelper.getScreenWidth(),
    borderWidth: 1,
    borderColor: '#F5F5F5'
  },
  imagePercent: {
    width: 0.1 * DimensionHelper.getScreenWidth(),
    height: 0.1 * DimensionHelper.getScreenWidth(),
    position: "absolute",
    top: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  percentValue: {
    color: "#FFFFFF",
    fontFamily: R.fonts.medium,
    fontSize: 13,
    marginTop: -5,
  },
  name: {
    fontSize: 15,
    fontWeight: "500",
    marginTop: 3,
    color: '#333333',
    height: 40,
    width: 0.4 * DimensionHelper.getScreenWidth()
  },
  unit: {
    fontSize: 12,
    color: '#555555',
    marginTop: 5,
    fontFamily: R.fonts.light,
    width: 0.34 * DimensionHelper.getScreenWidth()
  },
  newcost: {
    color: '#3BBB5A',
    //marginTop:5,
    //marginLeft: -0.28 * DimensionHelper.getScreenWidth(),
    fontSize: 15
  },
  oldcost: {
    color: '#777777',
    marginTop: 5,
    //marginLeft: -0.28 * DimensionHelper.getScreenWidth(),
    fontSize: 13,
    textDecorationLine: 'line-through'
  },
  text1: {
    color: '#E5334B',
    marginTop: 2,
    //marginLeft: -0.33 * DimensionHelper.getScreenWidth(),
    fontSize: 12
  },
  view1: {
    height: 0.047 * DimensionHelper.getScreenWidth(),
    backgroundColor: '#C2DEC9',
    width: 0.5 * DimensionHelper.getScreenWidth() - 20,
    borderRadius: 50,
    marginTop: 5,
    justifyContent: 'center'
  },
  view2: {
    height: 0.047 * DimensionHelper.getScreenWidth(),
    borderTopStartRadius: 50,
    borderBottomStartRadius: 50,
    position: 'absolute'
  },
  text2: {
    color: '#FFFFFF',
    fontSize: 10,
    textAlign: 'center'
  }
});
