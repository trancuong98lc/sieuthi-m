import { DimensionHelper } from 'helpers/dimension-helper';
import { navigate } from 'routing/service-navigation';
import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ProductDetailScreen } from 'routing/screen-name';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import R from 'res/R';
import images from 'res/images';
import { translate } from 'res/languages';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface FlatListSanphamkhuyenmaiProps {
  item?: any;
  index: number;
}

export default class FlatListSanphamkhuyenmai extends React.PureComponent<
  FlatListSanphamkhuyenmaiProps,
  any
  > {
  constructor(props: FlatListSanphamkhuyenmaiProps) {
    super(props);
    this.state = {};
  }

  gotoDetailProduct = () => {
    const { item } = this.props;
    navigate(ProductDetailScreen, {
      _id: item._id,
      quantity: item.preferentialQuantity
    })
  }

  public render() {
    const { item, index } = this.props;
    const promotionApp = item.promotion;
    const precentValue = promotionApp.applyingType === 'CASH' ? Math.round((item.priceDiscounted ?? 0) * 100 / item.price) : promotionApp.preferentialAmount ?? 0;

    return (
      <TouchableOpacity onPress={this.gotoDetailProduct} style={styles.container}>
        {item.promotion && item.promotion.preferentialAmount && <FastImage source={images.ic_percent} style={styles.imagePercent}>
          <DailyMartText style={styles.percentValue}>
            {`-${precentValue >= 100 ? 100 : precentValue === 0 ? 1 : precentValue}%`}
          </DailyMartText>
        </FastImage>}
        <FastImage resizeMode={FastImage.resizeMode.contain} source={getMediaUrl(item.medias && item.medias.length > 0 && item.medias[0] || undefined)} style={styles.image} />
        <DailyMartText numberOfLines={2} style={styles.name}>
          {item.name}
        </DailyMartText>
        {item.unit && <DailyMartText numberOfLines={1} style={styles.unit}>
          {`${translate('unit')} ${item.unit.name}`}
        </DailyMartText>}
        <View style={styles.cost}>
          <DailyMartText style={[styles.newcost, { flex: String(item.totalPriceDiscounted || item.price).length > 8 ? 1 : 0 }]} numberOfLines={2} > {`${_formatPrice(item.totalPriceDiscounted)}₫`}</DailyMartText>
          {item.promotion && item.promotion.applyingType && <DailyMartText style={[styles.oldcost, { flex: String(item.price).length > 8 ? 1 : 0 }]} numberOfLines={2}>{`${`${_formatPrice(item.price || 0)}₫`}`}</DailyMartText>}
        </View>
      </TouchableOpacity >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 0.5 * DimensionHelper.getScreenWidth() - 20,
    marginHorizontal: 10,
    marginTop: 10,
    backgroundColor: '#FFFFFF',
  },
  imagePercent: {
    width: 0.1 * DimensionHelper.getScreenWidth(),
    height: 0.1 * DimensionHelper.getScreenWidth(),
    position: "absolute",
    top: 0,
    right: 0,
    zIndex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  percentValue: {
    color: "#FFFFFF",
    fontFamily: R.fonts.medium,
    fontSize: 12,
    marginTop: -5,
  },
  image: {
    // marginTop: 10,
    width: 0.45 * DimensionHelper.getScreenWidth(),
    height: 0.45 * DimensionHelper.getScreenWidth(),
  },
  name: {
    fontSize: 15,
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
    fontWeight: '600',
  },
  unit: {
    fontSize: 12,
    color: '#555555',
    marginTop: 5,
    marginHorizontal: 10,
    fontFamily: R.fonts.light,
  },
  cost: {
    flexDirection: 'row',
    marginVertical: 5,
    flex: 1,
    paddingHorizontal: 5,
    alignItems: "flex-end"
  },
  newcost: {
    color: '#3BBB5A',
    fontSize: 15,
    fontWeight: '600'
  },
  oldcost: {
    color: '#777777',
    fontSize: 13,
    marginLeft: 10,
    textDecorationLine: 'line-through'
  }
});
