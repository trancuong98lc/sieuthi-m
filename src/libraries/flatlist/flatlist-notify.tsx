import { Notify, TYPE_NOTY } from 'features/notify/model/notify';
import { DimensionHelper } from 'helpers/dimension-helper';
import { getLogoUrl, getMediaUrl } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from '/res/R';
import moment from 'moment'
import ParsedText from 'libraries/parsed-text/ParsedText';
import { goBack, navigate, resetStack } from 'routing/service-navigation';
import { HomeScreen, PromotionScreen, QuesAndAnsScreen, RankBronzeScreen, ReceivedDetailScreen } from 'routing/screen-name';
import { showAlertByType } from 'libraries/BaseAlert';
import { connect } from 'react-redux';
import { updateCountNoti } from 'redux/actions';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import EventBus, { EventBusName } from 'helpers/event-bus';
import { reset } from 'i18n-js';

interface Items {
  codeOrders?: string;
  date?: string;
  deliveryDate?: string;
  image?: any;
}

export interface FlatListNotifyProps {
  item: Notify;
  index: number;
  updateCountNoty?: (quantity: number) => void
}

class FlatListNotify extends React.PureComponent<
  FlatListNotifyProps,
  any
  > {
  constructor(props: FlatListNotifyProps) {
    super(props);
    this.state = {};
  }

  private patternRegex = (item: Notify) => {
    try {
      const pattern = new RegExp(`${(item.payload && item.payload.codeOrder?.replace(/\((.+?)\)/g, '\\($1\\)'))}`)
      return pattern;
    } catch (error) {
      return '';
    }
  };

  viewNotice = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.MAKE_NOTIFICATION, null, true)
      if (res.status === STATUS.SUCCESS) {
        this.props.updateCountNoty && this.props.updateCountNoty(res.data)
      }
    } catch (error) {
      console.log("viewNotice -> error", error)

    }
  }


  onPressItem = () => {
    const { item } = this.props
    console.log("onPressItem -> item", item)
    if (!item) return
    if (!item.payload) return
    switch (item.payload.type) {
      case TYPE_NOTY.RECEIVE:
      case TYPE_NOTY.RECEIVED:
      case TYPE_NOTY.CANCEL:
        this.viewNotice()
        navigate(ReceivedDetailScreen, { orderId: item.payload.orderId, type: item.payload.type })
        break;
      case TYPE_NOTY.PROMOTION:
        this.viewNotice()
        navigate(PromotionScreen)
        break;
      case TYPE_NOTY.HAPPY_BIRTHDAY:
        this.viewNotice()
        showAlertByType({
          title: item.title,
          message: item.content || '',
          options: [{ text: 'OK', onPress: () => { } }],
          hideIcon: true
        });
        break;
      case TYPE_NOTY.USE_POINT:
        this.viewNotice()
        navigate(RankBronzeScreen)
        break;
      case TYPE_NOTY.MESSAGE_CMS:
        EventBus.getInstance().post({
          type: EventBusName.NEW_MESSAGE
        })
        navigate(QuesAndAnsScreen)
        break;
      case TYPE_NOTY.GENERAL:
        this.viewNotice()
        showAlertByType({
          title: translate('notify.header'),
          message: item.content || '',
          options: [{ text: 'OK', onPress: () => { } }],
          hideIcon: true
        });
        break;

      default:
        this.viewNotice()
        break;
    }
  }

  public render() {
    const { item } = this.props
    return (
      <TouchableOpacity style={styles.container} onPress={this.onPressItem} activeOpacity={0.7}>
        <FastImage resizeMode={FastImage.resizeMode.cover} source={getLogoUrl(item.image)} style={styles.image} />
        <View style={{ flex: 1 }}>
          <DailyMartText style={styles.successful}>
            {item.title}
          </DailyMartText>
          <ParsedText
            style={styles.confimPlease}
            parse={[{
              pattern: this.patternRegex(item),
              style: [styles.confimPlease, { fontWeight: 'bold' }],
            }]}
            childrenProps={{ allowFontScaling: false }}
          >
            {item.content}
          </ParsedText>
          <DailyMartText style={styles.deliveryDate}>
            {moment(item.createdAt).format('HH:mm DD/MM/yyyy')}
          </DailyMartText>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 12,
  },
  image: {
    width: 45,
    height: 45
  },
  successful: {
    marginLeft: 20,
    flex: 1,
    fontFamily: R.fonts.bold,
    fontSize: 15
  },
  confimPlease: {
    marginLeft: 20,
    fontSize: 12,
    color: '#555555',
    flex: 1
  },
  orderCode: {
    fontFamily: R.fonts.bold,
    fontSize: 12
  },
  deliveryDate: {
    marginLeft: 20,
    marginTop: 6,
    fontSize: 12,
    color: '#999999'
  },
  hr: {
    marginTop: 7,
    borderColor: '#E5E5E5',
    borderTopWidth: 1
  }
});

const mapStateToProps = (state: any) => {
  const { countNoti } = state.notificationReducer;
  return {
    countNoti
  };
};
const mapDispatchToProps = (dispatch: any) => ({
  updateCountNoty: (quantity: number) => dispatch(updateCountNoti(quantity)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FlatListNotify)
