import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import R from '/res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import FastImage from 'react-native-fast-image';
import { Cart } from 'features/cart/model/cart';
import { getMediaUrl } from 'helpers/helper-funtion';
import images from 'res/images';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface FlatListProductPayProps {
  item: Cart;
  index: number;
  unavailable_products: string[];
}

export default class FlatListProductPay extends React.PureComponent<
  FlatListProductPayProps,
  any
> {
  constructor(props: FlatListProductPayProps) {
    super(props);
    this.state = {};
  }

  public render() {
    const { item, unavailable_products } = this.props;
    return (
      <View style={styles.container}>
        <FastImage
          style={[
            styles.image,
            {
              opacity:
                unavailable_products.includes(item._id) || item.deletedAt
                  ? 0.6
                  : 1
            }
          ]}
          source={getMediaUrl(
            (item.medias && item.medias.length > 0 && item.medias[0]) ||
              undefined
          )}
        />
        {unavailable_products.includes(item._id) ||
          (item.deletedAt && (
            <FastImage
              source={images.ic_modal_waring}
              resizeMode={FastImage.resizeMode.contain}
              style={styles.iamgewr}
            />
          ))}
        <DailyMartText
          style={[
            styles.text,
            {
              opacity:
                unavailable_products.includes(item._id) || item.deletedAt
                  ? 0.6
                  : 1
            }
          ]}
        >
          {item.qty}
        </DailyMartText>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 80,
    height: 80,
    marginBottom: 10,
    marginLeft: 10,
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#F5F5F5'
  },
  iamgewr: {
    opacity: 0.6,
    position: 'absolute',
    width: 50,
    height: 50,
    alignSelf: 'center',
    // left: 8,
    top: 10
  },
  image: {
    height: '100%',
    width: '100%'
  },
  text: {
    position: 'absolute',
    backgroundColor: '#F2F2F2',
    right: 0,
    width: 20,
    height: 20,
    textAlign: 'center',
    marginLeft: 50,
    fontFamily: R.fonts.medium,
    fontSize: 12,
    color: '#A4A4A4'
  }
});
