import cart from 'data/cart';
import { DimensionHelper } from 'helpers/dimension-helper';
import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { Alert, Image, StyleSheet, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Swipeout from 'react-native-swipeout';
import R from 'res/R';

interface Items {
  name?: string;
  newcost?: string;
  oldcost?: string;
  image?: any;
  sold?: number;
  key?: number;
  cost?: number;
}

export interface FlatListCartProps {
  item: any;
  index: number;
  parentFlatList: any;
}
interface State {
  count: number;
  activeRowKey: any;
}
export default class FlatListCart extends React.PureComponent<
  FlatListCartProps,
  State
> {
  constructor(props: FlatListCartProps) {
    super(props);
    this.state = {
      count: 0,
      activeRowKey: null
    };
  }
  decrease = () => {
    if (this.state.count > 0) {
      this.setState({ count: this.state.count - 1 });
      return;
    }
    if (this.state.count == 0) {
      //Alert.alert('Hello')
      const deletingRow = this.state.activeRowKey;
      Alert.alert(
        'Alert',
        'Are you sure you want to delete ?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          },
          {
            text: 'Yes',
            onPress: () => {
              cart.splice(this.props.index, 1);
              //Refresh FlatList !
              this.props.parentFlatList.refreshFlatList(deletingRow);
            }
          }
        ],
        { cancelable: true }
      );
    }
  };

  increase = () => {
    this.setState({ count: this.state.count + 1 });
  };

  public render() {
    var swipeoutBtns = [
      {
        text: 'Button'
      }
    ];
    const SwipeoutSettings = {
      autoClose: true,
      onClose: (secId: any, rowId: any, direction: any) => {
        if (this.state.activeRowKey != null) {
          this.setState({ activeRowKey: null });
        }
      },
      onOpen: (secId: any, rowId: any, direction: any) => {
        this.setState({ activeRowKey: this.props.item.name });
      },
      right: [
        {
          onPress: () => {
            const deletingRow = this.state.activeRowKey;
            Alert.alert(
              'Alert',
              'Are you sure you want to delete ?',
              [
                {
                  text: 'No',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
                },
                {
                  text: 'Yes',
                  onPress: () => {
                    cart.splice(this.props.index, 1);
                    //Refresh FlatList !
                    this.props.parentFlatList.refreshFlatList(deletingRow);
                  }
                }
              ],
              { cancelable: true }
            );
          },
          text: 'Delete',
          type: 'delete'
        }
      ],
      rowId: this.props.index,
      sectionId: 1
    };
    return (
      <Swipeout {...SwipeoutSettings}>
        <View style={styles.container}>
          <View style={{ height: 1, backgroundColor: '#F2F2F2' }}></View>
          <View style={styles.flatListCart}>
            <FastImage source={this.props.item.image} style={[styles.image, {}]} />
            <View>
              <Text numberOfLines={2} style={styles.name}>
                {this.props.item.name}
              </Text>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.cost}>{this.props.item.cost}</Text>
                <Buttons
                  onPress={this.decrease}
                  styleImageButton={styles.imageButtonDe}
                  source={R.images.ic_decrease}
                />
                <DailyMartText style={styles.number}>
                  {this.state.count}
                </DailyMartText>
                <Buttons
                  onPress={this.increase}
                  styleImageButton={styles.imageButtonIn}
                  source={R.images.ic_increase}
                />
              </View>
            </View>
          </View>
        </View>
      </Swipeout>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100
  },
  flatListCart: {
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 10,
    paddingBottom: 15
  },
  image: {
    height: 65,
    width: 65
  },
  name: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    marginLeft: 20,
    width: DimensionHelper.getScreenWidth() * 0.68,
    height: 40
    //borderWidth:1
  },
  cost: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    marginLeft: 20,
    color: R.colors.primaryColor,
    marginTop: 10
  },
  imageButtonDe: {
    height: 30,
    width: 30,
    marginLeft: DimensionHelper.getScreenWidth() * 0.22
  },
  number: {
    width: 50,
    height: 30,
    backgroundColor: '#F7F7F8',
    marginHorizontal: 5,
    textAlign: 'center',
    paddingVertical: 5
  },
  imageButtonIn: {
    height: 30,
    width: 30
  }
});
