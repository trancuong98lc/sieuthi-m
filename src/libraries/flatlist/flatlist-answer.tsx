import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import DailyMartText from 'libraries/text/text-daily-mart';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';

interface Support {
    id?: number;
    question?: string;
    answer?: string;
}
interface Props {
    item: Support;
    index: number;
}

export default class FlatListAnswer extends React.PureComponent<Props, any> {
    constructor(props: Props) {
        super(props);
    }

    public render() {
      return (
        
           <View style={ styles.container } >
                <DailyMartText>{this.props.item.question}</DailyMartText>
          <DailyMartText style={styles.question}>{this.props.item.answer}</DailyMartText>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
   
    alignItems: 'center',
    paddingLeft: 12,
    paddingVertical:13
  },
  question: {
    flex: 1,
    fontSize: 15,
    color:'#333333'
  },
 
});
