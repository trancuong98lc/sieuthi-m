import { DimensionHelper } from 'helpers/dimension-helper';
import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import R from 'res/R';
import { HomeScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';
export interface FlatListStoreProps {
	item?: any;
	index: number;
}

export default class FlatListStore extends React.PureComponent<FlatListStoreProps, any> {
	constructor(props: FlatListStoreProps) {
		super(props);
	}

	gotoHomeScren = () => {
		navigate(HomeScreen);
	};

	public render() {
		return (
			<TouchableOpacity style={styles.container} onPress={this.gotoHomeScren}>
				<View style={styles.name}>
					<FastImage source={this.props.item.image1} style={styles.nameImage} />
					<DailyMartText style={styles.nameText}>{this.props.item.name}</DailyMartText>
				</View>
				<View style={styles.add}>
					<FastImage source={this.props.item.image2} style={styles.addImage} />
					<DailyMartText style={styles.addText}>{this.props.item.add}</DailyMartText>
				</View>
			</TouchableOpacity>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		backgroundColor: R.colors.white100,
		marginTop: 10,
		marginHorizontal: 10,
		borderRadius: 6,
		paddingVertical: 10
		// height: DimensionHelper.getScreenHeight() * 0.1
	},
	name: {
		flexDirection: 'row',
		padding: DimensionHelper.getScreenHeight() * 0.1 / 6
	},
	nameImage: {
		width: 20,
		height: 20
	},
	nameText: {
		marginLeft: 10
	},
	add: {
		flexDirection: 'row',
		paddingLeft: DimensionHelper.getScreenHeight() * 0.1 / 5
	},
	addImage: {
		width: 12,
		height: 17,
		marginTop: 5
	},
	addText: {
		marginTop: 5,
		marginLeft: 10
	}
});
