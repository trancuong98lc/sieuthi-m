import * as React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import R from 'res/R';
import FastImage from 'react-native-fast-image';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface ItemPromotionProps {
  item?: any;
  index: number;
}

export default class ItemPromotion extends React.PureComponent<
  ItemPromotionProps,
  any
> {
  constructor(props: ItemPromotionProps) {
    super(props);
    this.state = {};
  }

  public render() {
    // console.log(a);
    return (
      <TouchableOpacity>
        <View style={styles.container}>
          <FastImage source={this.props.item.image} style={styles.image} />

          <DailyMartText numberOfLines={2} style={styles.name}>
            {this.props.item.name}
          </DailyMartText>
          <View style={styles.cost}>
            <DailyMartText style={styles.newcost}>
              {this.props.item.newcost}
            </DailyMartText>
            <DailyMartText style={styles.oldcost}>
              {this.props.item.oldcost}
            </DailyMartText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: 0.41 * DimensionHelper.getScreenWidth(),
    backgroundColor: '#FFFFFF',
    //borderWidth: 1,
    marginVertical: 10,
    marginLeft: 7,
    paddingBottom: 10,
    borderRadius: 10
    //flexDirection:'row',
    // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
    ///backgroundColor: 'mediumseagreen',
  },
  image: {
    marginTop: 10,
    width: 0.35 * DimensionHelper.getScreenWidth(),
    height: 0.4 * DimensionHelper.getScreenWidth()
  },
  name: {
    fontSize: 13,
    color: '#333333',
    height: 40,
    marginHorizontal: 10,
    fontFamily: R.fonts.bold
  },
  cost: {
    flexDirection: 'row'
  },
  newcost: {
    color: '#3BBB5A',
    fontSize: 13,
    marginLeft: -5,
    fontFamily: R.fonts.bold
  },
  oldcost: {
    color: '#777777',
    fontSize: 11,
    marginLeft: 20,
    fontFamily: R.fonts.medium,
    textDecorationLine: 'line-through'
  }
});
