import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import R from 'res/R';
import FastImage from 'react-native-fast-image';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface FlatListHotsaleProps {
  item?: any;
  index: number;
}

export default class FlatListHotsale extends React.PureComponent<
  FlatListHotsaleProps,
  any
> {
  constructor(props: FlatListHotsaleProps) {
    super(props);
    this.state = {};
  }

  onCheckPrice(): number {
    const { item } = this.props;
    //let b = {this.props.item.sold}
    let b = item.sold;
    let c = item.all;
    return b / c;
  }
  text() {
    const { item } = this.props;
    if (0 <= this.onCheckPrice() && this.onCheckPrice() < 0.9) {
      return 'ĐÃ BÁN ' + item.sold;
    }
    if (this.onCheckPrice() >= 0.9 && this.onCheckPrice() < 1.0) {
      return 'SẮP BÁN HẾT';
    }
    if (this.onCheckPrice() == 1) {
      return 'ĐÃ BÁN HẾT';
    }
  }
  public render() {
    const { item } = this.props;
    // console.log(a);
    return (
      <TouchableOpacity>
        <View style={styles.container}>
          <FastImage source={item.image} style={styles.image} />
          <DailyMartText numberOfLines={2} style={styles.name}>
            {item.name}
          </DailyMartText>
          <DailyMartText style={styles.newcost}>{item.newcost}</DailyMartText>
          <DailyMartText style={styles.oldcost}>{item.oldcost}</DailyMartText>
          <DailyMartText style={styles.text1}>Con</DailyMartText>
          <View style={styles.view1}>
            <View
              style={[
                styles.view2,
                {
                  width: this.onCheckPrice() * 130,
                  backgroundColor:
                    this.onCheckPrice() == 1 ? '#E5334B' : '#3BBB5A'
                }
              ]}
            ></View>
            <DailyMartText style={[styles.text2]}> {this.text()}</DailyMartText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: 0.38 * DimensionHelper.getScreenWidth(),
    marginBottom: 10,
    backgroundColor: '#FFFFFF'
    //flexDirection:'row',
    // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
    ///backgroundColor: 'mediumseagreen',
  },
  image: {
    marginTop: 20,
    width: 0.35 * DimensionHelper.getScreenWidth(),
    height: 0.35 * DimensionHelper.getScreenWidth(),
    borderWidth: 1,
    borderColor: '#F5F5F5'
  },
  name: {
    fontSize: 13,
    color: '#333333',
    height: 40,
    fontFamily: R.fonts.medium,
    width: 0.34 * DimensionHelper.getScreenWidth()
  },
  newcost: {
    color: '#3BBB5A',
    marginLeft: -70,
    fontSize: 15,
    fontFamily: R.fonts.medium
  },
  oldcost: {
    color: '#777777',
    marginLeft: -70,
    fontSize: 13,
    fontFamily: R.fonts.medium,
    textDecorationLine: 'line-through'
  },
  text1: {
    color: '#E5334B',
    marginLeft: -90,
    fontSize: 11
  },
  view1: {
    height: 15,
    backgroundColor: '#C2DEC9',
    width: 130,
    borderRadius: 50,
    marginTop: 5,
    alignContent: 'center',
    justifyContent: 'center'
  },
  view2: {
    height: 15,
    //backgroundColor: '#3BBB5A',

    borderRadius: 50,
    position: 'absolute'
  },
  text2: {
    color: '#FFFFFF',
    fontSize: 8,
    textAlign: 'center'
    //marginVertical:2
  }
});
