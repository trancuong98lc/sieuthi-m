import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';
import FastImage from 'react-native-fast-image';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface ItemEndowProps {
  item?: any;
  index: number;
}

export default class ItemEndow extends React.PureComponent<
  ItemEndowProps,
  any
> {
  constructor(props: ItemEndowProps) {
    super(props);
    this.state = {};
  }

  onCheckPrice(): number {
    //let b = {this.props.item.sold}
    let b = this.props.item.sold;
    let c = this.props.item.all;
    return b / c;
  }
  text() {
    if (0 <= this.onCheckPrice() && this.onCheckPrice() < 0.9) {
      return 'ĐÃ BÁN ' + this.props.item.sold;
    }
    if (this.onCheckPrice() >= 0.9) {
      return 'SẮP BÁN HẾT';
    }
  }
  public render() {
    // console.log(a);
    return (
      <TouchableOpacity>
        <View style={styles.container}>
          <FastImage source={this.props.item.image} style={styles.image} />
          <View style={styles.text}>
            <DailyMartText numberOfLines={2} style={styles.name}>
              {this.props.item.name}
            </DailyMartText>
            <DailyMartText style={styles.date}>
              {this.props.item.date}
            </DailyMartText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    //width: 0.38 * DimensionHelper.getScreenWidth(),
    marginBottom: 10,
    paddingVertical: 10,
    // borderWidth: 1,
    // borderColor: 'red',
    marginLeft: 15,
    borderRadius: 15
    //flexDirection:'row',
    // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
    ///backgroundColor: 'mediumseagreen',
  },
  image: {
    width: 0.73 * DimensionHelper.getScreenWidth(),
    height: 0.31 * DimensionHelper.getScreenWidth()
  },
  text: {
    backgroundColor: '#FFFFFF',
    width: 0.73 * DimensionHelper.getScreenWidth(),
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    paddingBottom: 10,
    paddingHorizontal: 10
  },
  name: {
    fontSize: 13,
    color: '#444444',
    height: 50,
    marginTop: 10,
    fontFamily: R.fonts.medium

    //marginLeft:10,
    //width: 0.68 * DimensionHelper.getScreenWidth(),
  },
  date: {
    fontSize: 10,
    color: '#818181',
    marginTop: -10
  }
});
