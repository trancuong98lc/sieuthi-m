import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import { Category } from 'types/category';
import R from 'res/R';
import { navigate } from 'routing/service-navigation';
import { VegetableScreen } from 'routing/screen-name';
import FastImage from 'react-native-fast-image';
export interface ListCateGoryProps {
  item: Category;
  index: number;
}

export default class ItemListCateGory extends React.PureComponent<
  ListCateGoryProps,
  any
> {
  constructor(props: ListCateGoryProps) {
    super(props);
  }
  onNavigate = () => {
    navigate(VegetableScreen, {
      categoryId: this.props.item.categoryId
    });
  };
  public render() {
    const { item } = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.container}
          onPress={() => this.onNavigate()}
        >
          <FastImage
            source={item.image ? { uri: item.image } : R.images.raucu}
            style={styles.image}
          />
          <Text style={styles.name}>{item.categoryName}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginLeft: 10
    //justifyContent: 'center',
    // alignContent:'center'
    //flexDirection:'row',
    // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
    ///backgroundColor: 'mediumseagreen',
  },
  image: {
    width: 0.196 * DimensionHelper.getScreenWidth(),
    height: 0.196 * DimensionHelper.getScreenWidth()
  },
  name: {
    fontSize: 13,
    color: '#666666'
  }
});
