import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import FastImage from 'react-native-fast-image';
import { getMediaUrl, stripHTMLTag } from 'helpers/helper-funtion';
import moment from 'moment';
import { navigate } from 'routing/service-navigation';
import { NewDetailScreen } from 'routing/screen-name';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface ItemHotNewsProps {
  item?: any;
  index: number;
}

export default class ItemHotNews extends React.PureComponent<
  ItemHotNewsProps,
  any
  > {
  constructor(props: ItemHotNewsProps) {
    super(props);
    this.state = {};
  }

  gotoNewsDetail = () => {
    const { item } = this.props;
    navigate(NewDetailScreen, {
      _id: item._id
    })
  }

  public render() {
    const { item } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={this.gotoNewsDetail}>
        <FastImage source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)} style={styles.image} />
        <View style={styles.text}>
          <DailyMartText numberOfLines={2} style={styles.name}>
            {item.title}
          </DailyMartText>
          <DailyMartText style={styles.date}>
            {moment(item.createdAt).format('l')}
          </DailyMartText>
          <DailyMartText numberOfLines={2} style={styles.content}>
            {stripHTMLTag(item.content)}
          </DailyMartText>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DimensionHelper.getScreenWidth() - 20,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    // borderWidth: 1,
    // // borderColor: 'red',
    marginBottom: 8,
    marginHorizontal: 10
  },
  image: {
    width: 0.365 * DimensionHelper.getScreenWidth(),
    height: 0.22 * DimensionHelper.getScreenWidth(),
  },
  text: {
    marginLeft: 10
  },
  name: {
    fontSize: 13,
    width: 0.55 * DimensionHelper.getScreenWidth(),
    fontWeight: 'bold',

    color: '#444444',
    height: 40
  },
  date: {
    fontSize: 10,
    width: 0.55 * DimensionHelper.getScreenWidth(),
    color: '#818181',
    marginTop: -5
  },
  content: {
    fontSize: 13,
    width: 0.55 * DimensionHelper.getScreenWidth(),
    color: '#444444',
    marginTop: 10
  }
});
