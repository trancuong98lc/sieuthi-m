import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import R from '/res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import { navigate } from 'routing/service-navigation';
import { DeliveredDetailScreen } from 'routing/screen-name';
import FastImage from 'react-native-fast-image';

interface Items {
     orderCode: string,
     nameSupermarket: string,
     newcost: string,
     oldcost: string,
     image: any,
     sold: number,
     product: []
}

export interface Props {
     item?: any
     index: number
}

class ViewProduct extends React.PureComponent<Props, any> {
     constructor (props: Props) {
          super(props);
          this.state = {

          }
     }

     public render() {
          // console.log(a);
          return (
               <View style={ styles.containerProduct }>
                    <View style={styles.hrProduct}></View>
                    <View style={styles.itemProduct}>
                         <FastImage style={ styles.imageProduct } source={ this.props.item.image }></FastImage>
                         <View>
                              <DailyMartText style={ styles.name }>{ this.props.item.name }</DailyMartText>
                              <DailyMartText style={ styles.cost }>{ this.props.item.cost }</DailyMartText>
                         </View>
                         <DailyMartText style={ styles.count }> x{ this.props.item.count }</DailyMartText>
                    </View>

               </View>

          );
     }
}

export default class FlatListDelivered extends React.PureComponent<Props, any> {
     constructor (props: Props) {
          super(props);
          this.state = {

          }
     }
     public render() {
          // console.log(a);
          return (
               <View style={ styles.container }>
                    <TouchableOpacity
                    onPress={()=>navigate(DeliveredDetailScreen)}>
                    <View style={ styles.header }>
                         <View style={ styles.ViewCodeOrder }>
                              <FastImage style={ styles.imageCodeOrder } source={ R.images.grounmasieuthi } />
                              <DailyMartText style={ styles.textCodeOrder }>{ this.props.item.codeOrder }</DailyMartText>
                         </View>
                         <FastImage style={ styles.image } source={ R.images.ic_store } />
                         <DailyMartText style={ styles.nameSupermarket }>{ this.props.item.nameSupermarket }</DailyMartText>
                         <FastImage style={ styles.ic_next } source={ R.images.ic_nextPerson } />

                    </View>
                    </TouchableOpacity>
                   
                    <View >
                         <FlatList
                              data={ this.props.item.product }

                              renderItem={ ({ item, index }) => {
                                   return (
                                        <ViewProduct item={ item } index={ index }>
                                        </ViewProduct>);
                              } }
                         >
                         </FlatList>
                    </View>
                    <View style={ styles.hr }></View>
               </View>

          );
     }
}

const styles = StyleSheet.create({
     container: {
          marginTop: 10,
          backgroundColor: "#FFFFFF",
         
     },
     header: {
          flexDirection: 'row',
          alignItems: 'center',
     },
     ViewCodeOrder: {
          justifyContent: 'center',
          alignContent: 'center',

          //position: 'absolute',
     },
     imageCodeOrder: {
          height: 35.06,
          width: 99
     },
     textCodeOrder: {
          position: 'absolute',
          fontSize: 15,
          color: R.colors.white100,
          marginLeft: 6
     },
     image: {
          height: 19,
          width: 19,
          marginLeft: 10
     },
     nameSupermarket: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: R.colors.primaryColor,
          marginLeft: 10,
          flex: 1
     },
     ic_next: {
          width: 8,
          height: 14,
          marginRight: 12

     },
     containerProduct: { 
          marginTop:10  
          
     },
     hrProduct: {
          borderTopWidth: 1,
          borderColor:'#F2F2F2'
        // borderColor:'red'
     },
     itemProduct: {
          flexDirection: 'row',
          alignItems: 'center',
          marginTop:10
     },
     imageProduct: {
          height: 70,
          width: 70,
          marginLeft: 10
     },
     name: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#333333',
          width: DimensionHelper.getScreenWidth() * 0.6,
          marginLeft: 10,
         // marginTop:-10
     },
     cost: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#3BBB5A',
          marginLeft: 10,
          marginTop: 40
     },
     count: {
          fontSize: 18,
          color: '#A8A8A8',
          //marginTop:40
     },
     hr: {
          borderTopWidth: 10,
          borderColor: '#F5F5F5',
          marginTop:10
     }


})