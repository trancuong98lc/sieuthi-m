import { DimensionHelper } from 'helpers/dimension-helper';
import { getMediaUrl } from 'helpers/helper-funtion';
import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import R from 'res/R';
import { EndowDetailScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import moment from 'moment';
import { translate } from 'res/languages';

interface Items {
  name?: string;
  date?: string;
  oldcost?: string;
  image?: any;
  sold?: number;
}

export interface FlatListEndowProps {
  item: any;
  index: number;
}

export default class FlatListEndow extends React.PureComponent<
  FlatListEndowProps,
  any
> {
  constructor(props: FlatListEndowProps) {
    super(props);
    this.state = {};
  }

  navigateDetailEndow = () => {
    const { item } = this.props;
    navigate(EndowDetailScreen, {
      _id: item._id
    });
  }

  public render() {
    const { item } = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.navigateDetailEndow} activeOpacity={0.6}>
          <Buttons
            styleButton={styles.button}
            source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)}
            styleImageButton={styles.image}
            resizeMode={"cover"}
            activeOpacity={0.6}
            onPress={this.navigateDetailEndow}
          />
          <View style={styles.flatlist}>
            <DailyMartText style={styles.name}>
              {item.name}
            </DailyMartText>
            <DailyMartText style={styles.date}>
              {translate('endow.since')} {moment(item.applyingTime.startDate).format("l")} {''}
              {translate('endow.to')} {moment(item.applyingTime.endDate).format("l")}
            </DailyMartText>
          </View>
        </TouchableOpacity>

        <View style={styles.hr}></View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
  },
  button: {
    marginTop: 12,
    marginHorizontal: 12,
    borderWidth: 1,
    borderColor: R.colors.grey200,
    borderRadius: 15
  },
  flatlist: {
    marginHorizontal: (DimensionHelper.getScreenWidth() * 0.063) / 2,
    paddingBottom: 10
  },
  image: {
    width: DimensionHelper.getScreenWidth() * 0.937,
    height: DimensionHelper.getScreenWidth() * 0.4,
    borderRadius: 15
  },
  name: {
    fontFamily: R.fonts.medium,
    color: '#444444',
    fontSize: 13,
    marginTop: 5
  },
  date: {
    fontSize: 12,
    color: '#818181',
    marginTop: 5
  },
  hr: {
    borderColor: '#E5E5E5',
    borderTopWidth: 1
  }
});
