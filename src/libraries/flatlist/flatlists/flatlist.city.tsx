import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Image, TouchableOpacity ,Text} from 'react-native';
export interface FlatListCityProps {
     item?: any
     index: number
}

export default class FlatListCity extends React.PureComponent<FlatListCityProps, any> {
     constructor (props: FlatListCityProps) {
          super(props);
     }

     public render() {
          return (
               <View style={ styles.container }>
                    <TouchableOpacity>
                         <DailyMartText style={ styles.flatListItem }>{ this.props.item.province }</DailyMartText>
                    </TouchableOpacity>
                    <View style={{marginLeft:25,borderTopWidth:1,borderColor:'#686E7F'}}></View>
               </View>
               
          );
     }
}
const styles = StyleSheet.create({
     container: {
          //flex: 1,
          //flexDirection:'row',
          // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'                
          ///backgroundColor: 'mediumseagreen',

     },
     flatListItem: {
          marginTop:10,
          fontSize: 15,
          
          borderColor: "#686E7F",
          height: 40,
          marginLeft: 25,
          width: 400,
          color:"#686E7F",


     },

})