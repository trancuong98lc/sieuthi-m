import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { goBack } from 'routing/service-navigation';
import DailyMartText from 'libraries/text/text-daily-mart';
export interface FlatListDistrictProps {
	item?: any;
	index: number;
}

export default class FlatListDistrict extends React.PureComponent<FlatListDistrictProps, any> {
	constructor(props: FlatListDistrictProps) {
		super(props);
	}

	public render() {
		const { item } = this.props;
		return (
			<TouchableOpacity onPress={goBack} style={styles.container}>
				<DailyMartText style={styles.flatListItem}>{item.district}</DailyMartText>
			</TouchableOpacity>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		borderBottomColor: '#686E7F',
		borderBottomWidth: 1,
		paddingVertical: 10,
		marginHorizontal: 12
		//flexDirection:'row',
		// backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
		///backgroundColor: 'mediumseagreen',
	},
	flatListItem: {
		marginTop: 10,
		fontSize: 18,
		color: '#686E7F'
	}
});
