import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import R from '/res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import Buttons from 'libraries/button/buttons';
import OtpScreen from '../../authen/otp/view/otp.screen';
import { goBack, navigate } from 'routing/service-navigation';
import { AddAddrPayScreen, ConfimPaymentScreen } from 'routing/screen-name';
import { AdressPay } from 'features/address/address-pay/model/address-pay';
interface Items {
  name?: string;
  phone?: string;
  addr?: any;
}

export interface FlatListAddrPayProps {
  item: AdressPay;
  index: number;
  UserPhone?: any;
  screen?: any;
  setAdress: (item: AdressPay) => void;
}
export interface State {
  //key?: string,
  checked?: boolean;
}
export default class FlatListAddrPay extends React.Component<
  FlatListAddrPayProps,
  State
> {
  constructor(props: FlatListAddrPayProps) {
    super(props);
    this.state = {
      checked: props.item.isDefault || false
      //key:props.key,
    };
  }
  checked = () => {
    const { item } = this.props;
    this.props.setAdress(item);
  };

  gotoEdit = () => {
    const { item } = this.props;
    navigate(AddAddrPayScreen, {
      isEdit: true,
      _id: item._id,
      isOrder: !this.props.screen ? true : false
    });
  };

  public render() {
    const { item, screen, UserPhone } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.flatList}>
          <TouchableOpacity
            activeOpacity={!screen ? 0.5 : 1}
            style={styles.action}
            onPress={!screen ? this.checked : () => {}}
          >
            {!screen ? (
              <Buttons
                styleButton={[
                  styles.button,
                  {
                    borderWidth: item.checked ? 5 : 1,
                    borderColor: item.checked ? '#3BBB5A' : '#C4C4C4'
                  }
                ]}
              />
            ) : null}
            <View style={{ flex: 1 }}>
              <DailyMartText
                fontStyle="semibold"
                numberOfLines={2}
                style={styles.infor}
              >
                {item.receiverName} - {item.receiverPhone}
                {`${
                  UserPhone && UserPhone[item._id]
                    ? '/' + UserPhone[item._id]
                    : ''
                }`}
              </DailyMartText>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {item.isDefault && (
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: '#3BBB5A',
                      borderRadius: 5,
                      height: 25,
                      marginLeft: 8
                    }}
                  >
                    <DailyMartText style={{ color: 'white' }}>
                      Mặc định
                    </DailyMartText>
                  </View>
                )}
                <DailyMartText
                  numberOfLines={2}
                  style={styles.addr}
                >{`${item.detail}, ${item.ward.name}, ${item.district.name}, ${item.city.name}`}</DailyMartText>
              </View>
            </View>
          </TouchableOpacity>

          <Buttons
            source={R.images.ic_edit}
            styleButton={styles.buttonEdit}
            styleImageButton={styles.imageButtonEdit}
            onPress={this.gotoEdit}
          />
        </View>

        <View
          style={{ borderWidth: 0.5, width: '100%', borderColor: '#C4C4C4' }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  flatList: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    padding: 12
  },
  action: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  button: {
    width: 19,
    height: 19,
    borderRadius: 19
  },
  infor: {
    marginLeft: 10,
    fontSize: 13,
    flex: 1,
    fontWeight: '600',
    color: '#333333'
  },
  addr: {
    marginTop: 10,
    marginLeft: 10,
    fontSize: 13,
    flex: 1,
    color: '#333333'
  },
  buttonEdit: {
    marginLeft: 30
  },
  imageButtonEdit: {
    width: 19,
    height: 17
  }
});
