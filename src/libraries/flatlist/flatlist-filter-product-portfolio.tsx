import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, ViewStyle, TextStyle } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import { Category } from 'features/home/model/home';
export interface FlatListDanhmucsanphamProps {
     item: Category
     index: number
     touchableOpacity?: ViewStyle
     text?: TextStyle
}

export default class FlatListDanhmucsanpham extends React.PureComponent<FlatListDanhmucsanphamProps, any> {
     constructor(props: FlatListDanhmucsanphamProps) {
          super(props);
     }

     public render() {
          const { item, index } = this.props
          return (

               <TouchableOpacity activeOpacity={0.6} style={{ ...styles.touchableopacity, ...styles.container, marginLeft: index % 2 == 0 ? 0 : 10 }}>
                    <Text style={styles.name}>{item.name}</Text>
               </TouchableOpacity >

          );
     }
}
const styles = StyleSheet.create({
     container: {
          flex: 1,
          height: 40,
          alignItems: 'center',

     },
     touchableopacity: {
          borderRadius: 20,
          backgroundColor: '#F1F1F1',
          marginTop: 10,
          // marginLeft:10,
          alignItems: 'center',
          justifyContent: 'center',

     },
     name: {
          fontSize: 13,
          color: '#4A4A4A',

     }

})