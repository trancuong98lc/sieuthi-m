import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import R from '/res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import FastImage from 'react-native-fast-image';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import { Cart } from 'features/cart/model/cart';
import images from 'res/images';

interface Items {
  name?: string;
  cost?: string;
  image?: any;
  count?: number;
}

export interface FlatListViewProductProps {
  item: Cart;
  unavailable_products: string[];
  index: number;
}

export default class FlatListViewProduct extends React.PureComponent<
  FlatListViewProductProps,
  any
> {
  constructor(props: FlatListViewProductProps) {
    super(props);
    this.state = {};
  }

  public render() {
    // console.log(a);
    const { item, unavailable_products } = this.props;
    return (
      <View style={[styles.container]}>
        <FastImage
          style={styles.image}
          source={getMediaUrl(
            (item.medias && item.medias.length > 0 && item.medias[0]) ||
              undefined
          )}
        />
        {((unavailable_products && unavailable_products.includes(item._id)) ||
          item.deletedAt) && (
          <FastImage
            source={images.ic_modal_waring}
            resizeMode={FastImage.resizeMode.contain}
            style={styles.iamgewr}
          />
        )}
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            marginLeft: 10,
            opacity:
              unavailable_products.includes(item._id) || item.deletedAt
                ? 0.6
                : 1
          }}
        >
          <DailyMartText numberOfLines={2} style={styles.name}>
            {(item && item.name) || ''}
          </DailyMartText>
          <DailyMartText numberOfLines={2} style={styles.cost}>
            {_formatPrice(
              item.promoFor ? (item && item.priceAfterPromo) || '' : item.price
            ) + ' đ'}
          </DailyMartText>
          {item.promoFor ? (
            <DailyMartText
              numberOfLines={2}
              style={[
                styles.cost,
                {
                  color: '#9C9C9C',
                  textDecorationLine: 'line-through'
                }
              ]}
            >
              {_formatPrice((item && item.price) || '') + ' đ'}
            </DailyMartText>
          ) : null}
        </View>
        <DailyMartText style={styles.count}> x{item.qty}</DailyMartText>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 40,
    alignItems: 'center',
    flex: 1
    // marginBottom: 10,
    // marginLeft:10,
    // backgroundColor:"#FFFFFF",
    // borderWidth: 1,
    // borderColor:'#F5F5F5'
  },
  iamgewr: {
    opacity: 0.8,
    position: 'absolute',
    width: 50,
    height: 50,
    alignSelf: 'center',
    left: 8,
    top: 10
  },
  image: {
    height: 70,
    width: 70
  },
  name: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    color: '#333333'
  },
  cost: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    flex: 1,
    color: '#3BBB5A'
  },
  count: {
    fontSize: 18,
    color: '#A8A8A8'
  }
});
