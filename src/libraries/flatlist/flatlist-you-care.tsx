import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import DailyMartText from 'libraries/text/text-daily-mart';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import colors from 'res/colors';
import { getMediaUrl } from 'helpers/helper-funtion';
import FastImage from 'react-native-fast-image';

export interface FlatListYouCareProps {
  item: any,
  index: number,
  getOtherNewsDetail: (newsID: string) => void
}

export default class FlatListYouCare extends React.PureComponent<FlatListYouCareProps, any> {
  constructor(props: FlatListYouCareProps) {
    super(props);
    this.state = {
    }
  }

  gotoNewsDetail = () => {
    const { item } = this.props;
    this.props.getOtherNewsDetail(item._id);
  }

  public render() {
    const { item } = this.props;
    return (
        <TouchableOpacity style={styles.container} onPress={this.gotoNewsDetail}>
          <FastImage source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)} style={styles.image} />
          <DailyMartText numberOfLines={2} style={styles.name}>{item.title}</DailyMartText>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DimensionHelper.getScreenWidth() - 20,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginBottom: 8,
    marginHorizontal: 10
  },
  image: {
    width: 0.207 * DimensionHelper.getScreenWidth(),
    height: 0.123 * DimensionHelper.getScreenWidth(),
  },

  name: {
    fontSize: 13,
    width: 0.68 * DimensionHelper.getScreenWidth(),
    fontFamily: R.fonts.medium,
    // color: '#444444',
    height: 40,
    marginLeft: 10
  },
})