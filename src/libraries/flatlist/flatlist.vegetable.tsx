import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import { Products } from 'types/category';
import { createConfigItem } from '@babel/core';
import FastImage from 'react-native-fast-image';
import { navigate } from 'routing/service-navigation';
import { ProductDetailScreen } from 'routing/screen-name';
import { getMediaUrl } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';

export interface FlatlistVegetableProps {
    item: Products;
    index: number;
}

export default class FlatlistVegetable extends React.PureComponent<
    FlatlistVegetableProps,
    any
    > {
    constructor(props: FlatlistVegetableProps) {
        super(props);
        this.state = {};
    }
    onGotoDetail = () => {
        const { item } = this.props;
        navigate(ProductDetailScreen, {
            _id: item._id
        })
    }
    public render() {
        const { item } = this.props;
        return (
            <View
                style={[
                    styles.container,
                    { marginRight: this.props.index % 2 == 0 ? 0 : 10 }
                ]}
            >
                <TouchableOpacity
                    onPress={this.onGotoDetail}
                >
                    <FastImage
                        source={getMediaUrl(item.medias[0])}
                        style={[styles.image, {}]}
                    />
                    <DailyMartText numberOfLines={2} style={styles.name}>
                        {item.name}
                    </DailyMartText>
                    <View style={styles.cost}>
                        <DailyMartText style={styles.newcost}>{item.price}</DailyMartText>
                        <DailyMartText style={styles.oldcost}>{item.price}</DailyMartText>
                    </View>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        borderRadius: 5,
        marginLeft: 10,
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        width: 40
    },
    image: {
        marginTop: 10,
        width: (DimensionHelper.getScreenWidth() - 30) / 2,
        height: 0.46 * DimensionHelper.getScreenWidth()
    },
    name: {
        fontSize: 15,
        color: '#333333',
        height: 40,
        marginHorizontal: 10,
        marginTop: 5,
        fontFamily: R.fonts.bold
    },
    cost: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        alignItems: 'center',
    },
    newcost: {
        color: '#3BBB5A',
        fontSize: 15,
        flex: 1,
        fontFamily: R.fonts.bold
    },
    oldcost: {
        color: '#777777',
        fontSize: 13,
        marginLeft: 20,
        flex: 1,
        fontFamily: R.fonts.medium,
        textDecorationLine: 'line-through'
    }
});
