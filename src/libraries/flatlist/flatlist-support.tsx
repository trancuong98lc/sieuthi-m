import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import DailyMartText from 'libraries/text/text-daily-mart';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import { navigate } from 'routing/service-navigation';
import { AnswerScreen } from 'routing/screen-name';
import HTMLRender from 'features/component/HTMLRender';
import { styles } from 'features/support-center/answer/view/answer.style';
import { translate } from 'res/languages';
import FastImage from 'react-native-fast-image';

interface Support {
  id?: number;
  question?: string;
  description?: string;
}
interface Props {
  item: Support;
  index: number;
  selected: number[];
  onShow?: () => void;
  onHide?: () => void;
}

interface State {
}

export default class FlatListSupport extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
    }
  }

  onNavigate = () => {
    navigate(AnswerScreen, {
      id: this.props.item.id
    })
  }

  public render() {
    const { item, index, selected } = this.props;
    if (selected.indexOf(index) === -1) {
      return (
        <TouchableOpacity style={stylesSelf.container} onPress={this.props.onShow}>
          <DailyMartText style={stylesSelf.question}>{item.question}</DailyMartText>
          <FastImage style={stylesSelf.image} source={R.images.ic_nextPerson} />
        </TouchableOpacity>
       
      )
    } else {
      return (
        <View style={styles.answer}>
        <TouchableOpacity onPress={this.props.onHide}>
          <DailyMartText style={styles.questionDetail}>
            {item.question}
          </DailyMartText>
        </TouchableOpacity>
        <View style={styles.hrAnswer}></View>
        <DailyMartText style={styles.answerText}>{translate('support.answer')}:</DailyMartText>
        <HTMLRender content={item.description} containerStyle={{paddingLeft: 0}} baseFontStyle={{ color: '#333333', lineHeight: 21, fontSize: 14 }} />
        <View style={styles.HRAnswer}></View>
      </View>
      );
    }
  }
}

const stylesSelf = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    marginTop: 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 12,
    paddingVertical: 13,
    borderBottomColor: "#E5E5E5",
    borderBottomWidth: 1
  },
  question: {
    flex: 1,
    fontSize: 15,
    color: '#333333'
  },
  image: {
    height: 12,
    width: 7,
    marginRight: 12,
    marginLeft: 20
  }
});
