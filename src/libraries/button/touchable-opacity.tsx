import * as React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity as Button,
    StyleProp,
    ViewStyle
} from 'react-native';

interface Props {
    activeOpacity?: number;
    onPress?: any;
    style?: StyleProp<ViewStyle>;
}

interface State {}

export default class TouchableOpacity extends React.PureComponent<Props> {
    public render(): React.ReactNode {
        return (
            <Button
                onPress={this.props.onPress}
                activeOpacity={this.props.activeOpacity || 0.8}
                style={this.props.style}
            >
                {this.props.children}
            </Button>
        );
    }
}
