import * as React from 'react';
import {
	View,
	StyleSheet,
	Text,
	TouchableOpacity,
	Image,
	ViewStyle,
	ImageSourcePropType,
	TextStyle,
	ImageStyle
} from 'react-native';
import DailyMartText from 'libraries/text/text-daily-mart';
import FastImage, { FastImageProps } from 'react-native-fast-image';

export interface AppProps {
	styleButton?: any;
	source?: any;
	onPress?: () => void;
	styleImageButton?: ImageStyle;
	styleTextButton?: TextStyle;
	textButton?: string;
	disabled?: boolean;
	testID?: string;
	activeOpacity?: number,
	resizeMode?: any
}

export default class Buttons extends React.PureComponent<AppProps, any> {
	constructor(props: AppProps) {
		super(props);
	}
	public render() {
		const { source, onPress, activeOpacity } = this.props;
		return (
			<TouchableOpacity
				style={[styles.style, this.props.styleButton]}
				onPress={onPress}
				disabled={this.props.disabled}
				activeOpacity={activeOpacity || 0.7}
				testID={this.props.testID}
			>
				<DailyMartText style={this.props.styleTextButton}>{this.props.textButton}</DailyMartText>
				{source ? <FastImage source={source} resizeMode={this.props.resizeMode} style={this.props.styleImageButton} /> : null}
			</TouchableOpacity>
		);
	}
}
const styles = StyleSheet.create({
	style: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 30,
		backgroundColor: '#FFFFFF'
	}
});
