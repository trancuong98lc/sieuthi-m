import * as React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import Modal from 'react-native-modal'
import { translate } from 'res/languages';
import { PopupChooseMediasView } from './popup-choose-medias.view';
import { PopupChooseMediasPresenter } from './popup-choose-medias.presenter';
import DailyMartText from 'libraries/text/text-daily-mart';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import { MediaSelectedType } from 'types/ConfigType';

export interface PopupChooseMediasProps {
  onImagesSelected?: (images: MediaSelectedType[]) => void;
  onImageSelected?: (image: MediaSelectedType) => void;
  multiple?: boolean;
}

interface States {
  isVisible: boolean;
}

const TIME_OUT_MODAL = 500;

interface ButtonProps {
  onPress: () => void;
  text: string;
  noBorderBottom?: boolean;
}

const Button = (props: ButtonProps) => (
  <View style={props.noBorderBottom ? styles.wrapperButtonNoBorder : styles.wrapperButton}>
    <TouchableOpacity onPress={props.onPress} activeOpacity={0.8}>
      <DailyMartText style={styles.colorButton}>{props.text && translate(props.text)}</DailyMartText>
    </TouchableOpacity>
  </View>
);

export default class PopupChooseMedias extends React.PureComponent<PopupChooseMediasProps, States>
  implements PopupChooseMediasView {
  private presenter: PopupChooseMediasPresenter;

  constructor(props: PopupChooseMediasProps) {
    super(props);
    this.state = {
      isVisible: false
    };
    this.presenter = new PopupChooseMediasPresenter(this);
  }

  public onShow = (): void => {
    this.setState({ isVisible: true });
  };

  private onHide = (): void => {
    this.setState({ isVisible: false });
  };

  onUploadImagesSuccess(images: MediaSelectedType[]): void {
    const { onImagesSelected, onImageSelected } = this.props;
    onImagesSelected && onImagesSelected(images);
    onImageSelected && onImageSelected(images[0]);
  }

  private onChoosePhoto = (): void => {
    this.setState({ isVisible: false }, () => {
      setTimeout(() => {
        this.presenter.onChoosePhoto(this.props);
      }, TIME_OUT_MODAL);
    });
  };

  private onTakePhoto = (): void => {
    this.setState({ isVisible: false }, () => {
      setTimeout(() => {
        this.presenter.onTakePhoto(this.props);
      }, TIME_OUT_MODAL);
    });
  };

  private renderModalContent = (): React.ReactChild => (
    <>
      <View style={styles.modalContent}>
        {/* Tiêu đề */}
        <View style={styles.wrapperButton}>
          <DailyMartText style={styles.titleStyle}>{translate('modalChooseImage.hintTitle')}</DailyMartText>
        </View>

        {/* Chụp ảnh từ camera */}
        <Button onPress={this.onTakePhoto} text="modalChooseImage.hintCamera" />

        {/* Chọn video từ thư viện */}
        {/* <Button onPress={this.onVideo} text="modalChooseImage.hintVideo" /> */}

        {/* Chọn ảnh từ thư viện */}
        <Button onPress={this.onChoosePhoto} text="modalChooseImage.hintImage" noBorderBottom />
      </View>
      {/* Button tắt modal chọn ảnh */}
      <TouchableOpacity style={styles.cancelStyle} onPress={this.onHide} activeOpacity={0.8}>
        <DailyMartText style={styles.textCancel}>{translate('modalChooseImage.hintCancel')}</DailyMartText>
      </TouchableOpacity>
    </>
  );

  public render(): React.ReactNode {
    return (
      <Modal
        isVisible={this.state.isVisible}
        onBackdropPress={this.onHide}
        onBackButtonPress={this.onHide}
      >
        <View style={styles.modalWrap}>
          <TouchableOpacity activeOpacity={1} onPress={this.onHide} style={styles.btnClose} />
          {this.renderModalContent()}
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  modalWrap: {
    flex: 1,
    // backgroundColor: 'rgba(0, 0, 0, 0.7)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'flex-end',
  },
  modalContent: {
    backgroundColor: 'white',
    borderRadius: 15,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  btnClose: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  wrapperButton: {
    width: '100%',
    height: 50,
    borderBottomWidth: 0.4,
    borderBottomColor: '#CCCCCC',
    justifyContent: 'center'
  },
  wrapperButtonNoBorder: {
    width: '100%',
    height: 50,
    justifyContent: 'center'
  },
  colorButton: {
    textAlign: 'center',
    fontSize: 19,
    color: '#1976d2'
  },
  cancelStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 15,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    height: 55,
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: getBottomSpace() + 10
  },
  textCancel: {
    textAlign: 'center',
    fontSize: 20,
    color: '#F50808'
  },
  titleStyle: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 15
  }
});
