import { MediaSelectedType } from 'types/ConfigType';
export interface PopupChooseMediasView {
  onUploadImagesSuccess(images: MediaSelectedType[]): void;
}
