import { zIndexAlert } from './../BaseAlert/BaseAlert';
import ImagePicker, { Image } from 'react-native-image-crop-picker';
import { PopupChooseMediasView } from './popup-choose-medias.view';
import { PopupChooseMediasProps } from './popup-choose-medias';
import { translate } from 'res/languages';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import { showAlert } from 'libraries/BaseAlert';
import { getFileSize } from 'helpers/helper-funtion';

const MAX_SIZE_IMAGE = 5000000;
export class PopupChooseMediasPresenter {
  private view: PopupChooseMediasView;

  constructor(view: PopupChooseMediasView) {
    this.view = view;
  }
  MAX_WIDTH_HEIGHT = 900;

  onChoosePhoto = (props: PopupChooseMediasProps) => {
    ImagePicker.openPicker({
      multiple: true,
      mediaType: 'photo',
      compressImageQuality: 1
    }).then(async (images: any) => {
      let isUpload = true;
      if (images.length > 5) {
        return showAlert('max_image');
      }
      for (let index = 0; index < images.length; index++) {
        const element = images[index];

        const fileSize = await getFileSize(element.path);
        if (fileSize > MAX_SIZE_IMAGE) {
          isUpload = false;
          showAlert(translate('max_size_image_index', { index: index + 1 }));
          break;
        }
      }
      if (isUpload) {
        this.onUploadImageCache(images);
      }
    });
  };

  onTakePhoto = (props: PopupChooseMediasProps): void => {
    ImagePicker.openCamera({
      multiple: false,
      mediaType: 'photo',
      compressImageQuality: 1
    }).then(async (images: any) => {
      console.log('PopupChooseMediasPresenter -> images', images);
      const fileSize = await getFileSize(images.path);
      if (fileSize > MAX_SIZE_IMAGE) {
        return showAlert('max_size_image');
      }
      this.onUploadImageCache([images]);
    });
  };

  onUploadImageCache = async (images: Image[]) => {
    try {
      showLoading();
      let formData = new FormData();
      images.map((item: any, index: number) => {
        const file = {
          uri: item.path,
          name:
            item.filename ||
            Math.floor(Math.random() * Math.floor(999999999)) + '.jpg',
          type: item.mime || 'image/jpeg'
        };
        formData.append(`files`, file);
        formData.append(`width`, item.width);
        formData.append(`height`, item.height);
      });
      const res = await ApiHelper.postForm(
        URL_API.UPLOAD_MULTI,
        formData,
        true
      );
      hideLoading();
      if (res.status === STATUS.SUCCESS) {
        this.view.onUploadImagesSuccess(res.data || []);
      } else {
        showAlert(translate('upload_image_error'), { success: false });
      }
    } catch (error) {
      hideLoading();
      console.log('error: ', error);
      showAlert(error.toString());
    }
  };
}
