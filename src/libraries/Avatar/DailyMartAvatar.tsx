import { GENDER } from 'global/constants';
import Commons from 'helpers/Commons';
import * as React from 'react';
import {
  ImageSourcePropType,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle
} from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'res/R';
import images from 'res/images';
import DailyMartText from 'libraries/text/text-daily-mart';
import { User } from 'types/user-type';

export interface AvatarParams {
  firstCharacter?: string;
  showText?: boolean;
  fontSize?: number;
  borderColor?: string;
  borderWidth?: number;
  textColor?: string;
}
export interface DailyAvatarProps {
  size: number;
  url?: any;
  renderSubView?: () => React.ReactNode;
  source?: ImageSourcePropType;
  onPress?: () => void;
  user?: User;
  style?: StyleProp<ViewStyle>;
  viewSubStyle?: StyleProp<ViewStyle>;
  onHideCommentModal?: any;
  showOnlineStatus?: boolean;
  isOnline?: boolean;
  num?: number;
  params?: AvatarParams;
}

export interface DailyAvatarState {
  defaultImage?: any;
}

export function getAvatarDefault(user?: User) {
  if (!user || !user.gender) return R.images.avataImage;

  if (user.gender == GENDER.MALE) return R.images.avataImage;
  if (user.gender == GENDER.FEMALE) return R.images.avataImage;

  return R.images.avataImage;
}

export default class DailyAvatar extends React.PureComponent<
  DailyAvatarProps,
  DailyAvatarState
> {
  constructor(props: DailyAvatarProps) {
    super(props);
    this.state = {
      defaultImage: null
    };
  }

  onPressDefault = () => {
    const { user, onHideCommentModal } = this.props;
    if (!user) return;
    onHideCommentModal && onHideCommentModal();
    // onPressAvatar(user);
  };

  renderSource = () => {
    let img;
    const { url, source } = this.props;
    if (url === '' || typeof url !== 'string') {
      img = source || getAvatarDefault(this.props.user);
    } else {
      let resizeUrl = url;

      img = { uri: resizeUrl };
    }

    return img;
  };

  renderOnlineStatus() {
    if (!this.props.showOnlineStatus) return null;

    if (!this.props.user && !this.props.isOnline) return null;

    if (
      (this.props.user && this.props.user.online) ||
      (this.props.user && this.props.user._id == Commons.idUser) ||
      this.props.isOnline
    )
      return <View style={styles.onlineStyle} />;

    return null;
  }

  public render() {
    const {
      size,
      url,
      source,
      onPress,
      style,
      user,
      viewSubStyle,
      params,
      num = 17
    } = this.props;
    let {
      firstCharacter,
      showText,
      fontSize,
      borderWidth,
      borderColor,
      textColor
    } = params || {};
    return (
      <TouchableOpacity
        {...this.props}
        onPress={onPress || this.onPressDefault}
        activeOpacity={1}
        style={[
          {
            width: size,
            height: size,
            justifyContent: 'center',
            alignItems: 'center'
          },
          style || {}
        ]}
      >
        {showText ? (
          <View
            style={{
              width: size,
              height: size,
              borderRadius: size / 2,
              alignSelf: 'center',
              backgroundColor: '#EFF2F4',
              justifyContent: 'center',
              borderWidth: borderWidth || 1.5,
              borderColor: borderColor || 'white'
            }}
          >
            <DailyMartText
              fontStyle="bold"
              style={[
                styles.text,
                { fontSize: fontSize || 24 },
                textColor && { color: textColor }
              ]}
            >
              {firstCharacter}
            </DailyMartText>
          </View>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <FastImage
              style={{
                width: size,
                height: size,
                borderRadius: size / 2,
                borderWidth: 1,
                borderColor: 'white',
                position: 'absolute',
                zIndex: 1
              }}
              onError={() => {
                this.setState({
                  defaultImage: getAvatarDefault(this.props.user)
                });
              }}
              source={this.renderSource()}
              resizeMode="cover"
            />
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  onlineStyle: {
    width: 13,
    height: 13,
    borderRadius: 13 / 2,
    borderWidth: 2,
    borderColor: 'white',
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: '#33D156',
    zIndex: 10
  },
  viewSubStyle: {
    position: 'absolute',
    bottom: -3,
    right: -2,
    zIndex: 2
  },
  text: {
    textAlign: 'center',
    color: R.colors.primaryColor
  }
});
