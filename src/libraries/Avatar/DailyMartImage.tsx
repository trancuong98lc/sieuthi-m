import * as React from 'react';
import {
  View,
  StyleSheet,
  Image,
  ImageSourcePropType,
  TouchableOpacity,
  StyleProp,
  ViewStyle,
} from 'react-native';
import R from 'res/R';
import FastImage from 'react-native-fast-image';
import { getImageUrl, getMediaUrl } from 'helpers/helper-funtion';

export interface DailyMartImageProps {
  url?: any;
  source?: ImageSourcePropType;
  useImage?: boolean;
  imageStyle?: StyleProp<ViewStyle>;
}

export interface DailyMartImageState {
  defaultImage?: any;
}

export default class DailyMartImage extends React.PureComponent<
  DailyMartImageProps,
  DailyMartImageState
  > {
  constructor(props: DailyMartImageProps) {
    super(props);
    this.state = {
      defaultImage: null,
    };
  }

  public render() {
    const { url, source, imageStyle, useImage } = this.props;
    if (useImage) {
      return (
        <FastImage
          style={[
            {
              borderWidth: 1,
              borderColor: 'rgba(0,0,0,0.03)',
            },
            imageStyle,
          ]}
          onError={() => {
            this.setState({ defaultImage: R.images.logoperson });
          }}
          source={
            source ||
            (this.state.defaultImage || url
              ? { uri: getImageUrl(url) }
              : R.images.logoperson)
          }
          resizeMode="cover"
        />
      );
    }

    return (
      <FastImage
        style={[
          {
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,0.03)',
          },
          imageStyle,
        ]}
        onError={() => {
          this.setState({ defaultImage: R.images.logoperson });
        }}
        source={
          source ||
          (this.state.defaultImage || url ? { uri: getImageUrl(url) } : R.images.logoperson)
        }
        resizeMode="cover"
      />
    );
  }
}
