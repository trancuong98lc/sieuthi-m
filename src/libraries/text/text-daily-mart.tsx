import * as React from 'react';
import { StyleSheet, Text, StyleProp, TextStyle } from 'react-native';
import R from 'res/R';

type FontType = 'bold' | 'light' | 'semibold' | 'medium' | 'normal';

export interface DailyMartTextProps {
     style?: StyleProp<TextStyle>;
     fontStyle?: FontType;
     numberOfLines?: number;
     ellipsizeMode?: 'head' | 'middle' | 'tail' | 'clip';
     onPress?: any;
     onLongPress?: () => void;
     onTextLayout?: any;
     
}

export default class DailyMartText extends React.PureComponent<DailyMartTextProps> {
     static defaultProps = {
          fontStyle: 'normal',
     };

     constructor (props: DailyMartTextProps) {
          super(props);
          this.state = {};
     }

     public render() {
          let {
               style, fontStyle, ellipsizeMode, children
          } = this.props;

          let textFontStyle = styles.fontRegular;
          switch (fontStyle) {
               case 'bold':
                    textFontStyle = styles.fontBold;
                    break;

               case 'light':
                    textFontStyle = styles.fontLight;
                    break;

               case 'semibold':
                    textFontStyle = styles.fontSemibold;
                    break;
               
               case 'medium':
                    textFontStyle = styles.fontMedium;
                    break;
               
               default:
                    textFontStyle = styles.fontRegular;
                    break;
          }

          return (
               <Text
                    { ...this.props }
                    style={ [textFontStyle, style] }
                    ellipsizeMode={ ellipsizeMode || 'tail' }
               >
                    { children }
               </Text>
          );
     }
}

const styles = StyleSheet.create({
     fontBold: {
          fontFamily: 'Quicksand-Bold',
          color: R.colors.grey900
     },
     fontRegular: {
          fontFamily: 'Quicksand-Regular',
          color: R.colors.grey900
     },
     fontLight: {
          fontFamily: 'Quicksand-ExtraLight',
          color: R.colors.grey900
     },
     fontSemibold: {
          fontFamily: 'Quicksand-SemiBold',
          color: R.colors.grey900
     },
     fontMedium: {
          fontFamily: 'Quicksand-Medium',
          color: R.colors.grey900
          
     }
});
