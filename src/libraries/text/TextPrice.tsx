import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import NumberFormat from 'react-number-format';
import DailyMartText from './text-daily-mart';

export interface TextPriceProps {
    total: any,
    style?: any
}

export default class TextPrice extends React.PureComponent<TextPriceProps, any> {
    constructor(props: TextPriceProps) {
        super(props);
    }

    public render() {
        const { total, style } = this.props
        return (
            <NumberFormat
                value={total}
                displayType="text"
                thousandSeparator="."
                decimalSeparator=","
                renderText={(value: any) => (
                    <DailyMartText style={style}> {value + ' đ'}</DailyMartText>
                )}
            />
        );
    }
}
