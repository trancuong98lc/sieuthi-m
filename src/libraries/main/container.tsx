/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  StatusBar,
  ViewStyle,
  StyleProp
} from 'react-native';
import { statusBarHeight, bottomSpaceHeight } from 'helpers/layout-helpers';
import R from 'res/R';

interface IProps {
  style?: StyleProp<ViewStyle>;
  statusBarColor?: string;
  barStyle?: any;
  containerStyle?: StyleProp<ViewStyle>;
  home?: boolean;
  colorBack?: string
}

class Container extends Component<IProps> {
  static defaultProps = {
    statusBarColor: R.colors.primaryColor
  };

  render() {
    const {
      style,
      statusBarColor,
      barStyle,
      containerStyle,
      children,
      home,
      colorBack
    } = this.props;
    return (
      <View
        style={[styles.container, { backgroundColor: statusBarColor }, style]}
      >
        <StatusBar
          translucent
          backgroundColor={statusBarColor}
          barStyle={
            this.props.statusBarColor !== R.colors.primaryColor
              ? 'dark-content'
              : 'light-content'
          }
        />
        <View
          style={[
            styles.subContainer,
            containerStyle,
            {
              backgroundColor: !home ? colorBack || R.colors.white100 : R.colors.primaryColor
            }
          ]}
        >
          {children}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: statusBarHeight()
    // paddingBottom: bottomSpaceHeight()
    // backgroundColor: R.colors.primaryColor
  },

  subContainer: {
    flex: 1
  }
});

export default Container;
