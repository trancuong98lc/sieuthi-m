import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import { translate } from 'res/languages';

export interface EmptylistComponentProps {
    name?: string,
    souce?: any,
    width?: number,
    height?: numbre
}

export default class EmptylistComponent extends React.PureComponent<EmptylistComponentProps, any> {
    constructor(props: EmptylistComponentProps) {
        super(props);
    }

    public render() {
        const { name, souce, width, height } = this.props
        return (
            <View style={{ width: '100%', marginTop: 20, alignItems: 'center' }}>
                <FastImage source={souce || images.ic_empty} resizeMode={FastImage.resizeMode.contain} style={{ width: width || DimensionHelper.width / 2.5, height: height || DimensionHelper.width / 2.5, marginBottom: 20 }} />
                <DailyMartText>{translate(name || "nodata")}</DailyMartText>
            </View>
        );
    }
}
