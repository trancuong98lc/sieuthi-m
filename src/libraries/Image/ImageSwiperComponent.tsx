import * as React from 'react';
import {
    Linking,
    Platform,
    StyleProp,
    StyleSheet,
    TouchableOpacity,
    View,
    ViewStyle,
    Dimensions
} from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'res/R';
import { PhotoSize } from 'global/constants';
import SwiperLibraries from 'libraries/SwiperLibraries';
import { DimensionHelper } from 'helpers/dimension-helper';
import { MediaType, PHOTO_SIZE } from 'types/ConfigType';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import { getMediaUrl, getPhotoUrl } from 'helpers/helper-funtion';

export enum typeResize {
    contain = 'contain',
    cover = 'cover'
}

export interface ImageSwiperComponentProps {
    resizeModeIMG?: typeResize | string;
    height?: number;
    data: MediaType[];
    showsPagination?: boolean;
    autoplayTimeout?: number;
    imgStyle?: StyleProp<ViewStyle>;
    style?: StyleProp<ViewStyle>;
    imgStyleTouch?: StyleProp<ViewStyle>;
    imgDefault?: string;
    banner?: boolean;
    index?: number | boolean;
}

// const HEIGHT_IMG = DimensionHelper.width;

export default class ImageSwiperComponent extends React.PureComponent<
    ImageSwiperComponentProps,
    any
    > {
    static defaultProps = {
        resizeModeIMG: 'cover',
        showsPagination: true,
        autoplayTimeout: 3
    };

    constructor(props: ImageSwiperComponentProps) {
        super(props);
    }

    onPress = (index: number) => () => {
        const { data } = this.props;
    };

    onPressBanner = (item: MediaType) => () => {
    };

    getThumbnail(media: MediaType) {
        return getMediaUrl(media, PHOTO_SIZE.Large);
    }

    renderIconPlay = (media: MediaType) => {
        if (media.type !== 'VIDEO') return null;
        return (
            <View
                style={{
                    position: 'absolute',
                    zIndex: 10
                }}
            >
                <BaseIcon
                    name="ic_video_pause"
                    width={48}
                    iconStyle={{ alignSelf: 'center' }}
                />
            </View>
        );
    };

    _renderListSwiper = () => {
        const { data, imgStyle, resizeModeIMG, height, banner } = this.props;
        return data.map((item: any, index: number) => (
            <TouchableOpacity
                key={item._id}
                style={{ justifyContent: 'center', alignItems: 'center', height: height }}
                onPress={
                    banner ? this.onPressBanner(item.banner) : this.onPress(index)
                }
                activeOpacity={0.9}
            >
                <FastImage
                    style={[
                        styles.imgStyle,
                        imgStyle
                    ]}
                    source={banner ? this.getThumbnail(item.banner) : this.getThumbnail(item)}
                    resizeMode={'cover'}
                />
            </TouchableOpacity>
        ));
    };

    public render() {
        const { data, imgStyle, imgDefault, style, index, banner, autoplayTimeout, showsPagination } = this.props;

        const { width, height } = Dimensions.get('window');

        if (data && data.length === 0 && !imgDefault)
            return (
                <View
                    style={[
                        styles.imgStyle,
                        {
                            height: this.props.height || 300,
                            backgroundColor: '#AAAAAA'
                        },
                        imgStyle
                    ]}
                />
            );
        if (data && data.length === 0) return null;
        return (
            <View
                style={[
                    styles.container,

                    style,
                    {
                        height: this.props.height || 300,
                        width: banner ? width - 44 : width
                    }
                ]}
            >
                <SwiperLibraries
                    autoplay
                    index={index ? index : 0}
                    autoplayTimeout={autoplayTimeout}
                    height={height / 2}
                    paginationStyle={{ position: "absolute", bottom: -25 }}
                    width={Platform.OS === 'android' ? width : undefined}
                    showsButtons={false}
                    showsPagination={showsPagination || true}

                // activeDotColor="#000000"
                // dotColor={R.colors.grey400}
                >
                    {this._renderListSwiper()}
                </SwiperLibraries>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        width: DimensionHelper.width,
        alignItems: 'center',
        borderRadius: 5,
        alignSelf: 'center',
        marginBottom: 10,
        // flex: 1,
        minHeight: 250,
        backgroundColor: '#fff'
    },
    imgStyle: {
        width: '100%',
        height: '100%',
    }
});
