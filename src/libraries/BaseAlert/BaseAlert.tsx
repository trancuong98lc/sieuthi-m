import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Modal,
  Keyboard,
  ScrollView
} from 'react-native';
import { translate, missingTranslation } from '../../res/languages';
import { ALERT_TYPE } from '.';
import DailyMartText from 'libraries/text/text-daily-mart';
import { resetStack } from 'routing/service-navigation';
import { LoginScreen } from 'routing/screen-name';
import R from 'res/R';
import FastImage from 'react-native-fast-image';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import { color } from 'react-native-reanimated';
import colors from 'res/colors';
import { DimensionHelper } from 'helpers/dimension-helper';

interface LProps {
  onClose?: () => void;
  style?: any;
  visible?: boolean;
}

interface AlertStyle {
  color?: any;
  backgroundColor?: any;
}
export interface AstraAlertOptions {
  text: string;
  buttonStyle?: AlertStyle;
  onPress?: () => void;
  preventHideModal?: boolean;
}

export const zIndexAlert = 100000000;

const TIMEOUT = 300;

interface LState {
  visible: boolean;
  title?: String;
  message?: String;
  image?: any;
  success: boolean;
  accountExists: boolean;
  type: string;
  options?: AstraAlertOptions[];
  onClose?: () => void;
  callback?: () => void;
  callBackConfirm?: () => void;
  hideIcon?: boolean;
}

export interface ShowModalOptions {
  callback?: () => void;
  callBackConfirm?: () => void;
  success?: boolean;
  timeout?: number;
  hideKeyboard?: boolean;
}

const showModalOptionsDefault = {
  callback: undefined,
  callBackConfirm: undefined,
  success: false,
  timeout: 100,
  hideKeyboard: false
};

export interface ShowAlertByTypeParams {
  type?: string;
  title: any;
  message?: any;
  success?: boolean;
  options?: AstraAlertOptions[] | [];
  onClose?: () => void;
  callBackConfirm?: () => void;
  hideIcon?: boolean;
  timeout?: number;
}

export const ShowAlertByTypeDefaltParams = {
  type: ALERT_TYPE.BASE_ALERT,
  title: '',
  message: '',
  options: [],
  onClose: undefined,
  hideIcon: false,
  timeout: 0
};

export default class BaseAlert extends React.PureComponent<LProps, LState> {
  state = {
    visible: false,
    title: '',
    message: '',
    success: false,
    accountExists: false,
    type: ALERT_TYPE.DEFAULT,
    options: [],
    callback: () => {},
    callBackConfirm: () => {},
    hideIcon: false
  };

  timeoutShowModal: any;
  timeoutCloseModal: any;

  showModal = (
    message: string,
    options: ShowModalOptions = showModalOptionsDefault
  ) => {
    if (options.hideKeyboard) {
      Keyboard.dismiss();
    }

    if (this.timeoutShowModal) {
      clearTimeout(this.timeoutShowModal);
    }

    this.timeoutShowModal = setTimeout(() => {
      this.setState({
        message,
        success: options!.success || false,
        visible: true,
        callback: options!.callback
      });
    }, TIMEOUT);
  };

  onClose = () => {
    let { callback } = this.state;
    if (callback) {
      if (this.timeoutCloseModal) {
        clearTimeout(this.timeoutCloseModal);
      }
      callback();
      this.timeoutCloseModal = setTimeout(() => {
        this.setState({
          visible: false,
          type: ALERT_TYPE.DEFAULT,
          options: undefined,
          callback: () => {}
        });
      }, TIMEOUT);
    } else {
      this.setState({
        visible: false,
        type: ALERT_TYPE.DEFAULT,
        options: undefined
      });
    }
  };

  onConfirm = () => {
    let { callBackConfirm } = this.state;
    if (callBackConfirm) {
      if (this.timeoutCloseModal) {
        clearTimeout(this.timeoutCloseModal);
      }
      callBackConfirm();
      this.timeoutCloseModal = setTimeout(() => {
        this.setState({
          visible: false,
          type: ALERT_TYPE.DEFAULT,
          options: undefined,
          callBackConfirm: () => {}
        });
      }, TIMEOUT);
    } else {
      this.setState({
        visible: false,
        type: ALERT_TYPE.DEFAULT,
        options: undefined
      });
    }
  };

  onShow = () => this.setState({ visible: true });

  timeoutShowByType: any;

  onShowByType = (
    params: ShowAlertByTypeParams = ShowAlertByTypeDefaltParams
  ) => {
    if (this.timeoutShowByType) {
      clearTimeout(this.timeoutShowByType);
    }
    this.timeoutShowByType = setTimeout(() => {
      this.setState(
        {
          type: params.type || ALERT_TYPE.BASE_ALERT,
          title: params.title,
          message: params.message,
          success: params.success || false,
          options: params.options,
          onClose: params.onClose,
          callBackConfirm: params!.callBackConfirm,
          hideIcon: params.hideIcon
        },
        this.onShow
      );
    }, params.timeout);
  };

  onShowModalAccountExists = () => {
    this.setState({ type: ALERT_TYPE.ACCOUNT_EXISTS }, this.onShow);
  };

  render() {
    let { visible } = this.state;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={this.nop}
      >
        <ScrollView
          style={styles.modalWrap}
          alwaysBounceVertical={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ justifyContent: 'center', flexGrow: 1 }}
        >
          <StatusBar
            translucent
            backgroundColor={'transparent'}
            barStyle="dark-content"
          />
          <TouchableOpacity
            onPress={this.state.onClose || this.onClose}
            style={styles.modalOverlay}
            activeOpacity={1}
          />
          {this.checkRenderContent()}
        </ScrollView>
      </Modal>
    );
  }

  checkRenderContent = () => {
    let { type } = this.state;
    switch (type) {
      case ALERT_TYPE.ACCOUNT_EXISTS:
      case ALERT_TYPE.BASE_ALERT:
        return this.renderModalAccountExists();
      case ALERT_TYPE.CONTACT_MANAGER:
        return this.renderModalContactManager();
      case ALERT_TYPE.CONFIRM:
        return this.renderModalConfirm();
      default:
        return this.renderModalContent();
    }
  };

  renderModalConfirm = () => {
    let { message = 'Thông báo', success, title } = this.state;
    return (
      <View
        style={[
          styles.modalView,
          {
            borderRadius: 15,
            paddingTop: 0,
            paddingHorizontal: 0,
            paddingBottom: 0
          }
        ]}
      >
        <View style={styles.headerConfirm}>
          <DailyMartText
            fontStyle="semibold"
            style={{
              flex: 1,
              textAlign: 'center',
              color: 'white',
              fontSize: 20
            }}
          >
            {title || translate('notify.header')}
          </DailyMartText>
          <BaseIcon name="ic_close_white" width={25} onPress={this.onClose} />
        </View>
        <View style={{ paddingVertical: 20 }}>
          <FastImage
            source={
              success ? R.images.ic_modal_success : R.images.ic_modal_waring
            }
            style={success ? styles.icon : styles.iconWaring}
            resizeMode={FastImage.resizeMode.contain}
          />
          {/* <DailyMartText fontStyle='semibold' style={styles.message}>{translate('otp.trouble')}</DailyMartText> */}
          <DailyMartText style={styles.message}>{message}</DailyMartText>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={[
              styles.btnOk2,
              { borderBottomLeftRadius: 15, backgroundColor: 'white' }
            ]}
            onPress={this.onClose}
          >
            <DailyMartText
              fontStyle="semibold"
              style={[styles.txtOk, { color: colors.primaryColor }]}
            >
              {translate('cancel')}
            </DailyMartText>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={[styles.btnOk2, { borderBottomRightRadius: 15 }]}
            onPress={this.onConfirm}
          >
            <DailyMartText fontStyle="semibold" style={styles.txtOk}>
              {translate('accept')}
            </DailyMartText>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderModalContent = () => {
    let { message, success } = this.state;
    let missing = missingTranslation(message);
    return (
      <View style={[styles.modalView]}>
        <FastImage
          source={
            success ? R.images.ic_modal_success : R.images.ic_modal_waring
          }
          style={success ? styles.icon : styles.iconWaring}
          resizeMode={FastImage.resizeMode.contain}
        />
        <DailyMartText fontStyle="semibold" style={styles.message}>
          {missing ? message : translate(message)}
        </DailyMartText>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.btnOk}
          onPress={this.onClose}
        >
          <DailyMartText fontStyle="semibold" style={styles.txtOk}>
            OK
          </DailyMartText>
        </TouchableOpacity>
      </View>
    );
  };
  // renderModalContentWarring = () => {
  // 	let { message, success } = this.state;
  // 	let missing = missingTranslation(message);
  // 	return (
  // 		<View style={[ styles.modalView, { alignItems: 'center', justifyContent: 'center' } ]}>
  // 			<Image source={R.images.waring_deploy} style={{ width: '50%', height: 90 }} />
  // 			<DailyMartText fontStyle="normal" style={styles.message}>
  // 				{missing ? message : translate(message)}
  // 			</DailyMartText>
  // 			<TouchableOpacity activeOpacity={0.8} style={styles.btnOk} onPress={this.onClose}>
  // 				<DailyMartText fontStyle="semibold" style={styles.txtOk}>
  // 					OK
  // 				</DailyMartText>
  // 			</TouchableOpacity>
  // 		</View>
  // 	);
  // };

  renderCallToAction() {
    if (!this.state.options || this.state.options.length == 0) return null;

    let children = this.state.options!.map(
      (item: AstraAlertOptions, index: number) => {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            key={`${index}`}
            style={[styles.callToActionStyle, item.buttonStyle || null]}
            onPress={() => {
              if (!item.preventHideModal) this.onClose();
              item.onPress && item.onPress();
            }}
          >
            <DailyMartText
              fontStyle="semibold"
              style={[styles.txtOk, item.buttonStyle || null]}
            >
              {item.text}
            </DailyMartText>
          </TouchableOpacity>
        );
      }
    );

    return (
      <View style={{ width: '90%', marginTop: 12, alignSelf: 'center' }}>
        {children}
      </View>
    );
  }

  renderModalAccountExists = () => {
    const { hideIcon } = this.state;
    return (
      <View style={styles.modalView}>
        {!hideIcon ? (
          <FastImage
            source={R.images.ic_modal_waring}
            style={styles.iconWaring}
            resizeMode={FastImage.resizeMode.contain}
          />
        ) : null}
        <DailyMartText fontStyle="semibold" style={styles.message}>
          {this.state.title}
        </DailyMartText>
        {!!this.state.message && (
          <DailyMartText style={styles.subMessage}>
            {this.state.message}
          </DailyMartText>
        )}

        {this.renderCallToAction()}
      </View>
    );
  };

  renderModalContactManager = () => {
    return (
      <View style={styles.modalView}>
        <FastImage
          source={R.images.ic_modal_waring}
          resizeMode={FastImage.resizeMode.contain}
          style={styles.iconWaring}
        />
        {/* <DailyMartText fontStyle='semibold' style={styles.message}>{translate('otp.trouble')}</DailyMartText> */}
        <DailyMartText style={styles.message}>
          {translate('otp.trouble')}? .{translate('otp.contact_qtv')}
        </DailyMartText>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.btnOk}
          onPress={this.onLogin}
        >
          <DailyMartText fontStyle="semibold" style={styles.txtOk}>
            {translate('otp.contact')}
          </DailyMartText>
        </TouchableOpacity>
      </View>
    );
  };

  onLogin = () => {
    this.setState({ visible: false, type: ALERT_TYPE.DEFAULT }, () => {
      resetStack(LoginScreen);
    });
  };

  nop = () => {};
}

const styles = StyleSheet.create({
  modalWrap: {
    flex: 1,
    backgroundColor: 'rgba(12, 12, 12, 0.8)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: zIndexAlert + 20
  },
  modalOverlay: {
    // backgroundColor: R.colors.black60p,
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  modalView: {
    backgroundColor: 'rgba(248, 248, 248, 1)',
    borderRadius: 12,
    paddingTop: 20,
    paddingBottom: 14,
    paddingHorizontal: 10,
    width: '80%',
    marginVertical: DimensionHelper.getStatusBarHeight(),
    alignSelf: 'center',
    zIndex: 10
  },
  nonPadding: {
    paddingHorizontal: 0,
    paddingBottom: 0
  },
  title: {
    fontSize: 16,
    color: R.colors.primaryColor,
    textAlign: 'center',
    paddingBottom: 10,
    width: '85%',
    alignSelf: 'center'
  },
  titleView: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#CBCACA'
  },
  messageView: {
    // paddingVertical: 20
  },
  message: {
    fontSize: 17,
    color: R.colors.grey900,
    textAlign: 'center',
    width: '80%',
    alignSelf: 'center',
    paddingTop: 12,
    paddingBottom: 5
  },
  icon: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  iconWaring: {
    width: 40,
    height: 43.96,
    resizeMode: 'contain',
    tintColor: colors.primaryColor,
    alignSelf: 'center'
  },
  btnOk: {
    backgroundColor: R.colors.primaryColor,
    paddingVertical: 10,
    width: '50%',
    alignSelf: 'center',
    borderRadius: 6,
    marginTop: 20
  },
  btnOk2: {
    backgroundColor: R.colors.primaryColor,
    borderTopColor: R.colors.primaryColor,
    borderTopWidth: 1,
    paddingVertical: 20,
    width: '50%',
    alignSelf: 'center'
  },

  callToActionStyle: {
    backgroundColor: R.colors.primaryColor,
    paddingVertical: 10,
    width: '100%',
    alignSelf: 'center',
    borderRadius: 6,
    marginBottom: 8
  },

  txtOk: {
    color: R.colors.white100,
    fontSize: 16,
    textAlign: 'center'
  },
  subMessage: {
    fontSize: 14,
    // paddingBottom: 16,
    textAlign: 'center'
  },
  headerConfirm: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingHorizontal: 12
  }
});
