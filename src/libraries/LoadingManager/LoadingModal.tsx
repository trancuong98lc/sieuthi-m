import React, { PureComponent } from 'react';
import { View, Modal, StyleSheet } from 'react-native';
import Spinner from 'react-native-spinkit';
import R from '../../res/R';
import LoadingManager from './LoadingManager';
import PropTypes from 'prop-types';
import colors from 'res/colors';
import { zIndexAlert } from 'libraries/BaseAlert/BaseAlert';

const TIME_OUT = 5 * 1000;


export function showLoading() {
  const ref = LoadingManager.getDefault();
  if (!!ref) {
    ref.showLoading();
  }
}

export function hideLoading() {
  const ref = LoadingManager.getDefault();
  if (!!ref) {
    ref.hideLoading();
  }
}

interface IProps {
  spinnerSize?: number;
  spinnerType?: string;
  spinnerColor?: string;
}

class LoadingModal extends PureComponent<IProps, any> {

  static defaultProps = {
    spinnerSize: 40,
    spinnerType: 'Circle',
    spinnerColor: R.colors.primaryColor,
  };

  static propTypes = {
    spinnerSize: PropTypes.number,
    spinnerType: PropTypes.string,
    spinnerColor: PropTypes.string,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      isVisible: false
    }
  }

  componentWillUnmount() { }

  hideLoading = () => {
    setTimeout(() => {
      if (this.state.isVisible) {
        this.setState({
          isVisible: false
        })
      }
    }, 200);
  };

  showLoading = () => {
    if (!this.state.isVisible) {
      this.setState({
        isVisible: true
      })
    }
  };

  render() {
    return (
      <Modal transparent animationType="fade" visible={this.state.isVisible}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.25)',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Spinner
            isVisible
            size={this.props.spinnerSize}
            type={this.props.spinnerType}
            color={colors.primaryColor}
          />
        </View>
      </Modal>
    );
  }
}

export default LoadingModal;
