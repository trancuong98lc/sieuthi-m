import React from 'react';
import { View, StyleSheet, StatusBar, TouchableOpacity, Animated, PanResponder, Keyboard } from 'react-native';
import { zIndexAlert } from 'libraries/BaseAlert/BaseAlert';
import R from 'res/R';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import { missingTranslation, translate } from 'res/languages';
import DailyMartText from 'libraries/text/text-daily-mart';
import { color } from 'react-native-reanimated';
import colors from 'res/colors';
export const ToastType = {
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
  WARNING: 'WARNING'
};

const TimeOut = 3000;

interface BaseToastProps { }

export interface BaseToastState {
  visible?: boolean;
  type?: string;
  message?: string;
  fadeAnim?: Animated.Value;
  timeout?: number;
  hideKeyboard?: boolean;
}

const BackgroundColor = {
  [ToastType.SUCCESS]: colors.primaryColor,
  [ToastType.WARNING]: 'rgba(255, 193, 7, 1)',
  [ToastType.ERROR]: 'rgba(220, 53, 69, 0.9)'
};

const ToastIcon = {
  [ToastType.SUCCESS]: 'ic_modal_success',
  [ToastType.WARNING]: 'ic_modal_waring',
  [ToastType.ERROR]: 'ic_modal_waring'
};

class BaseToast extends React.PureComponent<BaseToastProps, BaseToastState> {
  constructor(props: BaseToastProps) {
    super(props);
    this.state = {
      visible: true,
      type: ToastType.SUCCESS,
      message: 'Success',
      fadeAnim: new Animated.Value(1)
    };
  }

  componentDidMount() {
    // this.test()
  }

  test() {
    if (this._timeOut) clearTimeout(this._timeOut);
    this.fadeIn();
    this._timeOut = setTimeout(() => {
      this.fadeOut();
    }, TimeOut);
  }

  _timeOut: any;

  onShow = (message?: string, params: BaseToastState = {}) => {
    if (this._timeOut) clearTimeout(this._timeOut);

    if (params!.hideKeyboard) Keyboard.dismiss();
    this.setState({ message, visible: true, ...params }, () => {
      this.fadeIn();
      this._timeOut = setTimeout(() => {
        this.fadeOut();
      }, params!.timeout || TimeOut);
    });
  };

  onClose = () => this.setState({ message: '', type: ToastType.SUCCESS, visible: false });

  fadeOut() {
    this.setState({ fadeAnim: new Animated.Value(0) }, () => {
      Animated.timing(
        // Animate over time
        this.state.fadeAnim!, // The animated value to drive
        {
          toValue: 1, // Animate to opacity: 1 (opaque)
          duration: 0, // ms,
          useNativeDriver: true
        }
      ).start();
      this.onClose();
    });
  }

  fadeIn() {
    this.setState({ fadeAnim: new Animated.Value(0) });
    Animated.timing(
      // Animate over time
      this.state.fadeAnim!, // The animated value to drive
      {
        toValue: 1, // Animate to opacity: 1 (opaque)
        duration: 300, // ms,
        useNativeDriver: true
      }
    ).start(); // Starts the animation
  }

  render() {
    let { visible, type, message, fadeAnim } = this.state;
    // if (!visible) return null
    let missing = missingTranslation(message!);
    const opacity = fadeAnim!.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0]
    });
    return (
      <View pointerEvents="none" style={styles.modalContainer}>
        <Animated.View
          style={[
            styles.modalView,
            { backgroundColor: BackgroundColor[type!], opacity },
            ToastIcon[type!] && { flexDirection: 'row', alignItems: 'center' }
          ]}
        >
          {ToastIcon[type!] && (
            <BaseIcon name={ToastIcon[type!]} width={20} iconStyle={styles.icon} color="#fff" />
          )}
          <DailyMartText style={styles.message}>{missing ? message : translate(message)}</DailyMartText>
        </Animated.View>
      </View>
    );
  }

  nop = () => { };
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 12,
    right: 12,
    bottom: 100,
    zIndex: zIndexAlert + 2
  },
  modalView: {
    backgroundColor: 'transparent',
    width: '80%',
    alignSelf: 'center',
    zIndex: 10,
    borderRadius: 6,
    padding: 10,
    justifyContent: 'center'
  },
  message: {
    textAlign: 'center',
    color: 'white',
    fontSize: 14,
    maxWidth: '80%'
  },
  icon: {
    marginRight: 12
  }
});

export default BaseToast;
