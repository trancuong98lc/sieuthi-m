class Manager {
  _BaseToast: any = null;

  register(_ref: any) {
    if (!this._BaseToast) {
      this._BaseToast = _ref;
    }
  }

  unregister(_ref: any) {
    if (this._BaseToast && this._BaseToast._id === _ref._id) {
      this._BaseToast = null;
    }
  }

  getDefault() {
    return this._BaseToast;
  }
}

export default new Manager();
