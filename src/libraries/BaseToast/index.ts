import Manager from './Manager';
import { BaseToastState } from './BaseToast';

export function showToast(message?: string, params?: BaseToastState) {
  const ref = Manager.getDefault();

  if (!!ref) {
    ref.onShow(message, params);
  }
}

export function hideToast() {
  const ref = Manager.getDefault();

  if (!!ref) {
    ref.onClose();
  }
}
