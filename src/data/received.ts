import R from "res/R"

const received = [
     {
          codeOrder: "#MM2506",
          nameSupermarket: "Siêu thị Mega Market Việt Nam",
          product: [
               {
               name: "Dứa ngọt MD2 Global Gap (1Kg)",
               image: R.images.duaMD2,
               cost: "205.000",
               count: 0
               },
               {
                    name: "Dưa leo hưu cơ USDA Vitamin Oganic(300g)",
                    image: R.images.dualeo,
                  cost: "205.000",
                  count:0
                  
               },
               {
                    name: "Dưa leo hưu cơ USDA Vitamin Oganic(300g)",
                    image: R.images.dualeo,
                  cost: "205.000",
                  count:0
                  
               },
          ]
         
     },
     {
          codeOrder: "#CM2507",
          nameSupermarket: "Siêu thị Mega Market Việt Nam",
          product: [
               {
                    name: "Dưa leo hưu cơ USDA Vitamin Oganic(300g)",
                    image: R.images.dualeo,
                  cost: "205.000",
                  count:0
                  
               },
               {
                    name: "Nho đỏ Red Global-Úc (1Kg)",
                    image: R.images.nhodo,
                    cost: "205.000",
                    count:0
                    },
               {
                    name: "Chanh dây tươi mát",
                    image: R.images.chanh,
                    cost: "205.000",
                    count:0
               },
               {
                    name: "Cải thìa hữu cơ USDA Food King (250g)",
                    image: R.images.caithia,
                    cost: "205.000",
                    count:0
               },
          ] 
     },
     {
          codeOrder: "#MM2508",
          nameSupermarket: "Siêu thị Mega Market Việt Nam",
          product: [
               {
                    name: "Ớt chuông USA Food King (250g)",
                    image: R.images.otchuong,
                    cost: "205.000",
                    count:0
               },
               {
                    name: "Ớt chuông USA Food King (250g)",
                    image: R.images.otchuong,
                    cost: "205.000",
                    count:0
               },
               {
                    name: "Ớt chuông USA Food King (250g)",
                    image: R.images.otchuong,
                    cost: "205.000",
                    count:0
               },
               {
                    name: "Ớt chuông USA Food King (250g)",
                    image: R.images.otchuong,
                    cost: "205.000",
                    count:0
               },
          ] 
     },
     
]
export default received