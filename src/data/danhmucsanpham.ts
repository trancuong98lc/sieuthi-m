import R from "res/R"

const danhmucsanpham = [
     {
          name: "Tất cả"
     },
     {
          name: "Rau củ",
          image:R.images.raucu
     },
     {
          name: "Trái cây",
          image:R.images.traicay
     },
     {
          name: "Thịt",
          image:R.images.thit
     },
     {
          name: "Hải sản",
          image:R.images.haisan
     },
     {
          name: "Đồ uống",
          image:R.images.douong
     },
     {
          name: "Bánh kẹo",
          image:R.images.banhkeo
     },
     {
          name: "Gia vị",
          image:R.images.giavi
     },
     {
          name: "TP Khác",
          image:R.images.tpkhac
     }
]
export default danhmucsanpham