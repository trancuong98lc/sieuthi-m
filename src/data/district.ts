const district = [
     {
       district: 'Ba Đình',
       area: '1,408.9',
       population: '1,248,000',
     },
     {
       district: 'Bắc Từ Liêm',
       area: '1,285.4',
       population: '1,028,000',
     },
     {
     district: 'Cầu Giấy	',
       area: '1,527.4',
       population: '1,963,300',
     },
     {
          district: 'Đống Đa',
       area: '3,324.5',
       population: '7,216,000',
     },
     {
          district: 'Hà Đông',
       area: '2,095.5',
       population: '8,146,300',
     },
     {
          district: 'Hai Bà Trưng',
       area: '91.46',
       population: '122,424',
     },
     {
          district: 'Hoàn Kiếm',
       area: '175.4',
       population: '188,863',
     },
     {
          district: 'Hoàng Mai',
       area: '32.21',
       population: '126,810',
     },
     {
          district: 'Long Biên',
       province: 'Bắc Ninh',
       area: '80.28',
       population: '272,634',
     },
     {
          district: 'Nam Từ Liêm',
       area: '232.56',
       population: '153,362',
     },
     {
          district: 'Tây Hồ',
       area: '264.07',
       population: '1,104,495',
     },
     {
          district: 'Thanh Xuân',
       area: '67.48',
       population: '143,312',
     },
]
export default district;