import R from 'res/R';

const product = [
  {
    name: 'Dứa ngọt MD2 Global Gap (1Kg)',
    image: R.images.duaMD2,
    newcost: '205.000',
    oldcost: '325.000',
    sold: 100,
    all: 100
  },
  {
    name: 'Dưa leo hưu cơ USDA Vitamin Oganic(300g)',
    image: R.images.dualeo,
    newcost: '205.000',
    oldcost: '325.000',
    sold: 90,
    all: 100
  },
  {
    name: 'Nho đỏ Red Global-Úc (1Kg)',
    image: R.images.nhodo,
    newcost: '205.000',
    oldcost: '325.000',
    sold: 30,
    all: 100
  },
  {
    name: 'Chanh dây tươi mát',
    image: R.images.chanh,
    newcost: '205.000',
    oldcost: '325.000',
    sold: 0,
    all: 100
  },
  {
    name: 'Cải thìa hữu cơ USDA Food King (250g)',
    image: R.images.caithia,
    newcost: '205.000',
    oldcost: '325.000',
    sold: 60,
    all: 100
  },
  {
    name: 'Ớt chuông USA Food King (250g)',
    image: R.images.otchuong,
    newcost: '205.000',
    oldcost: '325.000',
    sold: 80,
    all: 100
  }
];
export default product;
