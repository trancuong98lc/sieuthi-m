const fonts = {
     bold: "Quicksand-Bold",
     light: "Quicksand-Light",
     medium: "Quicksand-Medium",
     regular: "Quicksand-Regular",
     semibold:"Quicksand-Semibold",
};

export default fonts;
