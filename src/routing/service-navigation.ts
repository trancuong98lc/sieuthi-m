import { NavigationContainerRef, StackActions } from '@react-navigation/core';
import * as React from 'react';
import { NavigationInjectedProps } from 'react-navigation';
type Props = NavigationInjectedProps;
export const navigationRef = React.createRef<NavigationContainerRef>();

export function makeNavigationAction(
  this: any,
  name: string,
  params?: object
): void {
  this.props.navigation.navigate(name);
}

export function navigate(name: string, params?: object): void {
  if (!navigationRef.current) return;
  navigationRef.current.navigate({
    name,
    params,
    key: name
  });
}
export function Push(name: any, params: any = {}): void {
  if (!navigationRef.current) return;
  navigationRef.current.dispatch(StackActions.push(name, params));
}

export function goBack(): void {
  if (!navigationRef.current) return;
  navigationRef.current.goBack();
}

export function popMultipleScreen(number: number) {
  if (!navigationRef.current) return;
  navigationRef.current.dispatch(StackActions.pop(number));
}

export function resetStack(name: string, params?: object): void {
  if (!navigationRef.current) return;
  navigationRef.current.reset({
    index: 0,
    routes: [{ name }]
  });
}
