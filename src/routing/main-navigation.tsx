import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import * as ScreenName from 'routing/screen-name';
import VegetableScreen from 'features/vegetable/view/vegetable.screen';
import TabNavigation from './tab-navigation';
import SplashScreen from 'features/splash/view/splash.screen';
import LoginScreen from 'features/authen/login/view/login.screen';
import RegistrationScreen from 'features/authen/registration/view/registration.screen';
import OtpScreen from 'features/authen/otp/view/otp.screen';
import Complete_registrationScreen from 'features/authen/complete_registration/view/complete_registration.screen';
import ForgetpasswordScreen from 'features/authen/forgetpassword/view/forgetpassword.screen';
import NewpasswordScreen from 'features/authen/newpassword/view/newpassword.screen';
import RepasswordScreen from 'features/authen/repassword/view/repassword.screen';
import TrueregistrationScreen from 'features/authen/trueregistration/trueregistration';
import HotsaleScreen from 'features/hotsale/view/hotsale.screen';
import PromotionScreen from 'features/spkhuyenmai/view/Promotion.screen';
import ProductDetailScreen from 'features/product-detail/view/product-detail.screen';
import CartScreen from 'features/cart/view/cart.screen';
import ConfimPaymentScreen from 'features/confim-payment/view/confim-payment.screen';
import AddrPayScreen from 'features/address/address-pay/view/address-pay.screen';
import AddAddrPayScreen from 'features/address/add-address-pay/view/add-address-pay.screen';
import EndowScreen from 'features/endow/endow/view/endow.screen';
import EndowDetailScreen from 'features/endow/endow-detail/view/endow-detail.screen';
import SearchScreen from 'features/search/view/search.screen';
import CartViewScreen from 'features/cart-view/view/cart-view.screen';
import NotifyScreen from 'features/notify/view/notify.screen';
import QuesAndAnsScreen from 'features/ques-and-ans/view/ques-and-ans.screen';
import PersonalNotLoginScreen from 'features/personal/personal-not-login/view/personal-not-login.screen';
import PersonalLoginScreen from 'features/personal/personal-login/view/personal-login.screen';
import RuleScreen from 'features/rules/view/rule.screen';
import NewsScreen from 'features/news/new/view/news.screen';
import NewDetailScreen from 'features/news/new-detail/view/new-detail.screen';
import ReceivedScreen from 'features/history-order/received/view/received.screent';
import ReceivedDetailScreen from 'features/history-order/received-detail/view/received-detail.screen';
import DeliveredDetailScreen from 'features/history-order/delivered-detail/view/delivered-detail.screen';
import DeliveredScreen from 'features/history-order/delivered/view/delivered.screen';
import DeletedDetailScreen from 'features/history-order/deleted-detail/view/deleted-detail.screen';
import DeletedScreen from 'features/history-order/deleted/view/deleted.screen';
import RankBronzeScreen from 'features/membership-points/rank-bronze/view/rank-bronze.screen';
import RankSliverScreen from 'features/membership-points/rank-silver/view/rank-sliver.screen';
import RankGoldScreen from 'features/membership-points/rank-gold/view/rank-gold.screen';
import QuestionScreen from 'features/support-center/question/view/question.screen';
import AnswerScreen from 'features/support-center/answer/view/answer.screen';
import ContactScreen from 'features/contact/view/contact.screen';
import CityScreen from 'features/store/view/city.screen';
import StoreScreen from 'features/store/store.screen';
import DistrictScreen from 'features/store/view/district.screen';
import AccountInforEdit from 'features/personal/account-infor/view/account-infor.screen';
import CompleteSuccess from 'features/authen/complete_registration/view/complete_success';
import SearchLocation from 'features/personal/account-infor/model/components/SearchLocation';
import WardScreen from 'features/store/view/ward.screen';
import CreatePayContainer from 'features/create-pay/view/create-pay.screen';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();


function MainStack(): JSX.Element {
	return (
		<Stack.Navigator
			screenOptions={{
				headerShown: false
			}}
			initialRouteName={ScreenName.SplashScreen}
		>
			<Stack.Screen name={ScreenName.SplashScreen} component={SplashScreen} />
			<Stack.Screen name={ScreenName.LoginScreen} component={LoginScreen} />
			<Stack.Screen name={ScreenName.RegistrationScreen} component={RegistrationScreen} />
			<Stack.Screen name={ScreenName.OtpScreen} component={OtpScreen} />
			<Stack.Screen name={ScreenName.Complete_registrationScreen} component={Complete_registrationScreen} />
			<Stack.Screen name={ScreenName.CompleteSuccess} component={CompleteSuccess} />
			<Stack.Screen name={ScreenName.ForgetpasswordScreen} component={ForgetpasswordScreen} />
			<Stack.Screen name={ScreenName.NewpasswordScreen} component={NewpasswordScreen} />
			<Stack.Screen name={ScreenName.RepasswordScreen} component={RepasswordScreen} />
			<Stack.Screen name={ScreenName.TrueregistrationScreen} component={TrueregistrationScreen} />
			<Stack.Screen name={ScreenName.SearchLocation} component={SearchLocation} />
			<Stack.Screen name={ScreenName.CreatePayScreen} component={CreatePayContainer} />
			<Stack.Screen name={ScreenName.StoreScreen} component={StoreScreen} />
			<Stack.Screen name={ScreenName.CityScreen} component={CityScreen} />
			<Stack.Screen name={ScreenName.DistrictScreen} component={DistrictScreen} />
			<Stack.Screen name={ScreenName.HomeScreen} component={TabNavigation} />
			<Stack.Screen name={ScreenName.HotsaleScreen} component={HotsaleScreen} />
			<Stack.Screen name={ScreenName.PromotionScreen} component={PromotionScreen} />
			<Stack.Screen name={ScreenName.VegetableScreen} component={VegetableScreen} />
			<Stack.Screen name={ScreenName.ProductDetailScreen} component={ProductDetailScreen} />
			<Stack.Screen name={ScreenName.CartScreen} component={CartScreen} />
			<Stack.Screen name={ScreenName.ConfimPaymentScreen} component={ConfimPaymentScreen} />
			<Stack.Screen name={ScreenName.AddrPayScreen} component={AddrPayScreen} />
			<Stack.Screen name={ScreenName.AddAddrPayScreen} component={AddAddrPayScreen} />
			<Stack.Screen name={ScreenName.EndowScreen} component={EndowScreen} />
			<Stack.Screen name={ScreenName.EndowDetailScreen} component={EndowDetailScreen} />
			<Stack.Screen name={ScreenName.SearchScreen} component={SearchScreen} />
			<Stack.Screen name={ScreenName.CartViewScreen} component={CartViewScreen} />
			<Stack.Screen name={ScreenName.NotifyScreen} component={NotifyScreen} />
			<Stack.Screen name={ScreenName.QuesAndAnsScreen} component={QuesAndAnsScreen} />
			<Stack.Screen name={ScreenName.PersonalNotLoginScreen} component={PersonalNotLoginScreen} />
			<Stack.Screen name={ScreenName.PersonalLoginScreen} component={PersonalLoginScreen} />
			<Stack.Screen name={ScreenName.AccountInforScreen} component={AccountInforEdit} />
			<Stack.Screen name={ScreenName.RuleScreen} component={RuleScreen} />
			<Stack.Screen name={ScreenName.NewsScreen} component={NewsScreen} />
			<Stack.Screen name={ScreenName.NewDetailScreen} component={NewDetailScreen} />
			<Stack.Screen name={ScreenName.ReceivedScreen} component={ReceivedScreen} />
			<Stack.Screen name={ScreenName.ReceivedDetailScreen} component={ReceivedDetailScreen} />
			<Stack.Screen name={ScreenName.DeliveredDetailScreen} component={DeliveredDetailScreen} />
			<Stack.Screen name={ScreenName.DeliveredScreen} component={DeliveredScreen} />
			<Stack.Screen name={ScreenName.DeletedDetailScreen} component={DeletedDetailScreen} />
			<Stack.Screen name={ScreenName.DeletedScreen} component={DeletedScreen} />
			<Stack.Screen name={ScreenName.RankBronzeScreen} component={RankBronzeScreen} />
			<Stack.Screen name={ScreenName.RankSliverScreen} component={RankSliverScreen} />
			<Stack.Screen name={ScreenName.RankGoldScreen} component={RankGoldScreen} />
			<Stack.Screen name={ScreenName.QuestionScreen} component={QuestionScreen} />
			<Stack.Screen name={ScreenName.AnswerScreen} component={AnswerScreen} />
			<Stack.Screen name={ScreenName.WardScreen} component={WardScreen} />
			<Stack.Screen name={ScreenName.ContactScreen} component={ContactScreen} />
		</Stack.Navigator>
	);
}

export default MainStack;
