import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CartViewScreen from 'features/cart-view/view/cart-view.screen';
import EndowScreen from 'features/endow/endow/view/endow.screen';
import HomeScreen from 'features/home/view/home.screen';
import PersonalNotLoginScreen from 'features/personal/personal-not-login/view/personal-not-login.screen';
import QuesAndAnsScreen from 'features/ques-and-ans/view/ques-and-ans.screen';
import React, { useEffect, useState } from 'react';
import { Image, ShadowPropTypesIOS } from 'react-native';
import R from 'res/R';
import FastImage from 'react-native-fast-image';
import PersonalLoginScreen from 'features/personal/personal-login/view/personal-login.screen';
import CartScreen from 'features/cart/view/cart.screen';
import { useSelector } from 'react-redux';
import { counterFormat } from 'helpers/helper-funtion';
import ApiHelper from 'helpers/api-helper';
import { Subscription } from 'rxjs';
import EventBus, { EventBusName, EventBusType } from 'helpers/event-bus';
const Tab = createBottomTabNavigator();

function TabNavigation(props: any): JSX.Element {
  const [initTab, setInitTab] = useState('');
  const subScription = new Subscription();
  useEffect(() => {
    subScription.add(
      EventBus.getInstance().events.subscribe((data: EventBusType) => {
        if (data.type == EventBusName.NEW_MESSAGE) {
          return;
        }
      })
    );
    return () => {
      // setInitTab('')
      subScription && subScription.unsubscribe();
    };
  }, []);

  const totalProduct = useSelector((state: any) => {
    const totalProduct = state.cart.quantity;
    return totalProduct;
  });

  const totalMessage = useSelector((state: any) => {
    const totalMessage = state.chat.messageCount;
    return totalMessage;
  });

  return (
    <Tab.Navigator
      screenOptions={({ route, navigation }) => ({
        tabBarIcon: ({ focused }) => {
          let iconName;
          if (route.name === 'Trang chủ') {
            iconName = focused
              ? R.images.ic_home_focus
              : R.images.ic_home_notfocus;
          } else if (route.name === 'Ưu đãi') {
            iconName = focused
              ? R.images.ic_category_focus
              : R.images.ic_category_notfocus;
          } else if (route.name === 'Giỏ hàng') {
            iconName = focused
              ? R.images.ic_cart_focus
              : R.images.ic_cart_notfocus;
          } else if (route.name === 'Hỏi đáp') {
            iconName = focused
              ? R.images.ic_chat_focus
              : R.images.ic_chat_notfocus;
          } else if (route.name === 'Cá nhân') {
            iconName = focused
              ? R.images.ic_personal_focus
              : R.images.ic_personal_notfocus;
          }
          return (
            <FastImage source={iconName} style={{ width: 24, height: 24 }} />
          );
        }
      })}
      initialRouteName={initTab}
      tabBarOptions={{
        activeTintColor: R.colors.primaryColor,
        inactiveTintColor: 'gray',
        keyboardHidesTabBar: true,
        style: {
          shadowColor: 'black',
          shadowOffset: { width: 2, height: 0 },
          shadowOpacity: 0.5,
          borderTopWidth: 0.5,
          elevation: 8
        }
      }}
    >
      <Tab.Screen name="Trang chủ" component={HomeScreen} />
      <Tab.Screen name="Ưu đãi" children={() => <EndowScreen isTab />} />
      <Tab.Screen
        name="Giỏ hàng"
        options={{
          tabBarBadge: totalProduct
            ? String(counterFormat(totalProduct))
            : undefined,
          unmountOnBlur: true
        }}
        children={() => <CartScreen navigation={props.navigation} isTab />}
      />
      <Tab.Screen
        name="Hỏi đáp"
        options={{
          tabBarBadge: totalMessage
            ? String(counterFormat(totalMessage))
            : totalMessage,
          unmountOnBlur: true
        }}
        children={() => <QuesAndAnsScreen isTab />}
      />
      <Tab.Screen
        name="Cá nhân"
        options={{ unmountOnBlur: true }}
        component={PersonalLoginScreen}
      />
    </Tab.Navigator>
  );
}

export default TabNavigation;
