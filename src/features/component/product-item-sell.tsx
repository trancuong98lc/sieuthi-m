import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import R from 'res/R';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import { navigate } from 'routing/service-navigation';
import { ProductDetailScreen } from 'routing/screen-name';
import images from 'res/images';
import colors from 'res/colors';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface ProductItemSaleProps {
  item?: any;
  index: number;
}

export interface ProductItemSaleStates {
  timeLeft: number,
  timeLeftText: string
}

export default class ProductItemSale extends React.PureComponent<
  ProductItemSaleProps,
  any
  > {
  interval: any;
  constructor(props: ProductItemSaleProps) {
    super(props);
    const endDate = Date.parse(this.props.item.attributeProduct.endDate);
    this.state = {
      timeLeft: endDate >= new Date().getTime() ? endDate : 0,
      timeLeftText: ''
    };
  }

  onCheckPrice(): number {
    const { item } = this.props;
    let b = Number(item.attributeProduct.qtySelled ?? 0);
    let c = Number(item.attributeProduct.qtyDefault ?? 0);
    return c === 0 ? 1 : (b / c);
  }

  text() {
    const { item } = this.props;
    const product = item.attributeProduct;
    if (product.status === 'OUT_PRODUCT' || product.qtyDefault === 0) {
      return translate('product_item.sold_out');
    }
    if (product.status === 'NEAR_OUT_PRODUCT') {
      return translate('product_item.about_to_sell_out');
    }
    if (!product.status || product.status === '') {
      return translate('product_item.sold', { price: product.qtySelled });
    }
  }

  navigateDetailProduct = () => {
    const { item } = this.props;
    navigate(ProductDetailScreen, {
      _id: item._id,
      quantity: item.attributeProduct.qtyDefault - item.attributeProduct.qtySelled
    });
  }

  componentDidMount = () => {
    this.countdownTime();
  }

  countdown = (time: number) => {
    let seconds = Math.floor(time / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    let days = Math.floor(hours / 24);

    hours = hours - (days * 24);
    minutes = minutes - (days * 24 * 60) - (hours * 60);
    seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

    let daysText = days === 0 ? '' : `${days} ngày `;
    let hoursText = hours === 0 ? '' : hours < 10 ? `0${hours}:` : `${hours}:`;
    let minutesText = minutes === 0 ? '' : minutes < 10 ? `0${minutes}:` : `${minutes}:`;
    let secondsText = `${(seconds < 10 ? `0${seconds}` : seconds)}${((days === 0 && hours === 0 && minutes === 0) ? " giây" : '')}`;

    return daysText + "" + hoursText + minutesText + secondsText;
  }

  countdownTime = () => {
    const { timeLeft } = this.state;

    this.interval = setInterval(() => {
      if (timeLeft - new Date().getTime() <= 0) {
        clearInterval(this.interval);
        this.setState({
          timeLeft: 0,
          timeLeftText: translate('product_item.out_of_time')
        });
      } else {
        this.setState({
          timeLeft: timeLeft - 1000,
          timeLeftText: this.countdown(timeLeft - new Date().getTime())
        });
      }
    }, 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.interval);
    this.setState = (state, callback) => {
      return;
    };
  }

  public render() {
    const { timeLeft, timeLeftText } = this.state;
    const { item } = this.props;
    let product = item.attributeProduct;
    const soldPercentage = this.onCheckPrice();

    return (
      <TouchableOpacity activeOpacity={0.6} style={styles.container} onPress={() => this.navigateDetailProduct()}>
        <FastImage source={getMediaUrl(item.medias[0])} resizeMode={FastImage.resizeMode.cover} style={styles.image} />
        <FastImage source={images.ic_percent} style={styles.imagePercent}>
          <DailyMartText style={styles.percentValue}>
            {`-${product.preferentialAmount >= 100 ? 100 : product.preferentialAmount}%`}
          </DailyMartText>
        </FastImage>
        <DailyMartText numberOfLines={2} style={styles.name}>
          {item.name}
        </DailyMartText>
        {item.unit && <DailyMartText numberOfLines={1} style={styles.unit}>
          {`${translate('unit')} ${item.unit.name}`}
        </DailyMartText>}
        <View style={{ width: '100%' }}>
          <DailyMartText style={styles.newcost}>{_formatPrice(product.totalAll)} đ</DailyMartText>
          <DailyMartText style={styles.oldcost}>{_formatPrice(item.price)} đ</DailyMartText>
          <DailyMartText style={styles.text1}>{`${timeLeft <= 0 ? '' : 'Còn'} ${timeLeftText}`}</DailyMartText>
        </View>
        <View style={styles.view1}>
          <View
            style={[
              styles.view2,
              {
                width: soldPercentage * 0.35 * DimensionHelper.getScreenWidth(),
                backgroundColor:
                  soldPercentage == 1 ? '#E5334B' : '#3BBB5A',
                borderTopEndRadius: soldPercentage == 1 ? 50 : 0,
                borderBottomEndRadius: soldPercentage == 1 ? 50 : 0,
              }
            ]}
          />
          <DailyMartText style={styles.text2}>{this.text()}</DailyMartText>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: 0.35 * DimensionHelper.getScreenWidth(),
    marginBottom: 5,
    marginHorizontal: 10,
    backgroundColor: '#FFFFFF'
  },
  image: {
    marginTop: 10,
    borderRadius: 5,
    width: 0.35 * DimensionHelper.getScreenWidth(),
    height: 0.3 * DimensionHelper.getScreenWidth(),
  },
  imagePercent: {
    width: 0.1 * DimensionHelper.getScreenWidth(),
    height: 0.1 * DimensionHelper.getScreenWidth(),
    position: "absolute",
    top: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  percentValue: {
    color: "#FFFFFF",
    fontFamily: R.fonts.medium,
    fontSize: 12,
    marginTop: -5,
  },
  name: {
    fontSize: 13,
    color: '#333333',
    height: 40,
    marginTop: 5,
    fontFamily: R.fonts.medium,
    width: 0.34 * DimensionHelper.getScreenWidth()
  },
  unit: {
    fontSize: 12,
    color: '#555555',
    fontFamily: R.fonts.light,
    width: 0.34 * DimensionHelper.getScreenWidth()
  },
  newcost: {
    color: '#3BBB5A',
    fontSize: 14,
    fontFamily: R.fonts.medium,
    marginBottom: 2
  },
  oldcost: {
    color: '#777777',
    fontSize: 12,
    fontFamily: R.fonts.medium,
    textDecorationLine: 'line-through',
    marginBottom: 2
  },
  text1: {
    color: '#E5334B',
    fontSize: 11
  },
  view1: {
    height: 15,
    backgroundColor: '#C2DEC9',
    width: 0.35 * DimensionHelper.getScreenWidth(),
    borderRadius: 50,
    marginTop: 5,
    alignContent: 'center',
    justifyContent: 'center'
  },
  view2: {
    height: 15,
    borderTopStartRadius: 50,
    borderBottomStartRadius: 50,
    position: 'absolute'
  },
  text2: {
    color: "#FFFFFF",
    fontSize: 8,
    textAlign: 'center',
    textTransform: "uppercase"
  }
});
