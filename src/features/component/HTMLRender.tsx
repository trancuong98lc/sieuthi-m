import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import HTML from "react-native-render-html";
import R from 'res/R';

export default class HTMLRender extends React.PureComponent<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  render = () => {
    return <HTML html={this.props.content} containerStyle={[styles.contentStyle, this.props.containerStyle]}
      tagsStyles={{
        p: { fontFamily: 'Quicksand-Regular', marginTop: 10 },
        strong: { fontFamily: 'Quicksand-Bold' },
      }} baseFontStyle={this.props.baseFontStyle} />
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    paddingHorizontal: 10,
    paddingBottom: 10
  }
});