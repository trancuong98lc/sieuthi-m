import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, StyleProp, ViewProps, ViewStyle } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import { Products } from 'types/category';
import { createConfigItem } from '@babel/core';
import FastImage from 'react-native-fast-image';
import { navigate } from 'routing/service-navigation';
import { ProductDetailScreen } from 'routing/screen-name';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import images from 'res/images';
import NumberFormat from 'react-number-format';
import { translate } from 'res/languages';

export interface ProductItemProps {
  item: Products;
  index: number;
  promotion?: boolean
  style?: StyleProp<ViewStyle>
}

export default class ProductItem extends React.PureComponent<
  ProductItemProps,
  any
  > {
  constructor(props: ProductItemProps) {
    super(props);
    this.state = {};
  }

  onGotoDetail = () => {
    const { item } = this.props;
    navigate(ProductDetailScreen, {
      _id: item._id,
      quantity: item.preferentialQuantity
    })
  }


  public render() {
    const { item, index, style, promotion } = this.props;
    const promotionApp = promotion ? item.promotion : null;
    const precentValue = promotion ? (promotionApp?.applyingType === 'CASH' ? Math.round((item.priceDiscounted ?? 0) * 100 / item.price) : promotionApp?.preferentialAmount ?? 0) : item.preferentialAmount;

    return (
      // <View style={[styles.container, style]}>
      <TouchableOpacity
        onPress={this.onGotoDetail}
        activeOpacity={0.7}
        style={styles.childContainer}>
        <FastImage
          source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)}
          resizeMode={FastImage.resizeMode.cover}
          style={styles.image}
        />
        {(promotion || item.promoFor == "PERCENT") &&
          <FastImage source={images.ic_percent} style={styles.imagePercent}>
            <DailyMartText style={styles.percentValue}>
              {`-${precentValue >= 100 ? 100 : precentValue === 0 ? 1 : precentValue}%`}
            </DailyMartText>
          </FastImage>}
        <DailyMartText numberOfLines={2} style={styles.name}>
          {item.name}
        </DailyMartText>
        {item.unit && <DailyMartText numberOfLines={1} style={styles.unit}>
          {`${translate('unit')} ${item.unit.name}`}
        </DailyMartText>}
        <View style={!promotion ? styles.cost : styles.promotionStyle}>
          <NumberFormat
            value={item.promotion ? item.totalPriceDiscounted || 0 : item.promoFor ? item.totalAll || 0 : item.price || 0}
            displayType="text"
            thousandSeparator="."
            decimalSeparator=","
            renderText={(value: any) => (
              <DailyMartText numberOfLines={2} style={[styles.newcost, { flex: String(item.totalPriceDiscounted || item.totalAll || item.price).length > 8 ? 0.6 : 0 }]}>{`${value}₫`}</DailyMartText>
            )}
          />
          {item.promoFor || item.promotion && item.promotion.applyingType ?
            <NumberFormat
              value={item.price}
              displayType="text"
              thousandSeparator="."
              decimalSeparator=","
              renderText={(value: any) => (
                <DailyMartText numberOfLines={2} style={[styles.oldcost, { marginLeft: promotion ? 0 : 13, flex: String(item.price).length > 6 ? 1 : 0 }]}>{`${value} đ`}</DailyMartText>
              )}
            />
            : null}
        </View>
      </TouchableOpacity>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    marginTop: 10,
    paddingLeft: 10,
  },
  imagePercent: {
    width: 0.1 * DimensionHelper.getScreenWidth(),
    height: 0.1 * DimensionHelper.getScreenWidth(),
    position: "absolute",
    top: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  percentValue: {
    color: "#FFFFFF",
    fontFamily: R.fonts.medium,
    fontSize: 12,
    marginTop: -5,
  },
  costHorizontal: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  promotionStyle: {
    flexDirection: 'column',
    paddingHorizontal: 10,
    flex: 1
  },
  costVertical: {
    flexDirection: "column",
    alignItems: "flex-start"
  },
  childContainer: {
    width: DimensionHelper.width / 2 - 15,
    height: 250,
    margin: 5,
    shadowColor: 'black',
    shadowOpacity: 0.2,
    borderRadius: 5,
    shadowOffset: {
      width: 2,
      height: 2
    },
    elevation: 2,
    backgroundColor: '#FFFFFF',
  },
  image: {
    // marginTop: 10
    alignSelf: 'center',
    width: '90%',
    height: 120,
    borderRadius: 5,
    marginVertical: 10
  },
  name: {
    fontSize: 15,
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
    fontFamily: R.fonts.bold
  },
  unit: {
    fontSize: 12,
    color: '#555555',
    marginVertical: 5,
    marginHorizontal: 10,
    fontFamily: R.fonts.light
  },
  cost: {
    flexDirection: 'row',
    paddingHorizontal: 12,
    marginVertical: 5,
    alignItems: 'center',
  },
  newcost: {
    color: '#3BBB5A',
    fontSize: 15,
    fontFamily: R.fonts.bold
  },
  oldcost: {
    color: '#777777',
    fontSize: 13,
    fontFamily: R.fonts.medium,
    textDecorationLine: 'line-through'
  }
});
