import { PromotionType } from 'features/confim-payment/model/confim-payment';
import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import { CartDetail } from '../model/cart';
import CartScreen from '../view/cart.screen';
import { CartView } from '../view/cart.view';

export class CartPresenter {
  private cartView: CartView;

  constructor(cartView: CartView) {
    this.cartView = cartView;
  }

  onFetchProductsDetail = async () => {
    try {
      const res = await ApiHelper.fetch(
        URL_API.CART_DETAIL + `/${Commons.idStore}/PENDING`,
        null,
        true
      );
      if (res.status == STATUS.SUCCESS && res.data && res.data.items) {
        this.cartView.onFetchDataSuccess(
          res.data.items || [],
          res.data._id,
          res.data
        );
      }
    } catch (error) {
      console.log('error: ', error);
      this.cartView.onFetchDataFail();
    }
  };

  onCheckData = (
    data: CartDetail[],
    valuePromotion: PromotionType,
    unavailable_products: string[]
  ) => {
    let price: number = 0;
    data.forEach((obj: CartDetail) => {
      if (
        !unavailable_products.includes(obj.productId) ||
        (obj.product && !obj.product.deletedAt)
      ) {
        if (obj.promoFor) {
          price += (Number(obj.priceAfterPromo) || 0) * obj.qty;
        } else {
          price += (obj.product!.price || 0) * obj.qty;
        }
      }
    });
    // if (valuePromotion) {
    //     price -= valuePromotion.pointUser + valuePromotion.promotionPriceProduct
    // }
    console.log('CartPresenter -> onCheckData -> price', price);
    return price;
  };
}
