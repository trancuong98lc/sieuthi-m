import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  StyleProp,
  ViewProps,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import images from 'res/images';
import { translate } from 'res/languages';
import { ConfimPaymentScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../cart.style';
import NumberFormat from 'react-number-format';
import FastImage from 'react-native-fast-image';
export interface BtnCartProps {
  total: string;
  name?: string;
  totalProduct: number;
  _id: string;
  onPress?: () => void;
  style?: StyleProp<ViewProps>;
}

export default class BtnCart extends React.PureComponent<BtnCartProps, any> {
  constructor(props: BtnCartProps) {
    super(props);
  }

  onGotoConfirmPay = () => {
    navigate(ConfimPaymentScreen, { cartId: this.props._id });
  };

  public render() {
    const { total, totalProduct, _id, style, onPress, name } = this.props;
    return (
      <View style={[styles.cartFooter, style]}>
        <View style={styles.sumCost}>
          <DailyMartText style={styles.text}>
            {totalProduct} {translate('cart.product')}
          </DailyMartText>
          <NumberFormat
            value={total}
            displayType="text"
            thousandSeparator="."
            decimalSeparator=","
            renderText={(value: any) => (
              <DailyMartText style={styles.sum}> {value + ' đ'}</DailyMartText>
            )}
          />
        </View>
        <FastImage
          source={images.btn_cart}
          style={styles.pay}
          resizeMode="stretch"
        >
          <TouchableOpacity
            onPress={onPress || this.onGotoConfirmPay}
            activeOpacity={0.6}
            disabled={_id && Number(totalProduct) > 0 ? false : true}
            style={{
              alignItems: 'center',
              height: '100%',
              justifyContent: 'center'
            }}
          >
            <DailyMartText style={styles.textPay}>
              {translate(name || 'button.pay')}
            </DailyMartText>
          </TouchableOpacity>
        </FastImage>
      </View>
    );
  }
}
