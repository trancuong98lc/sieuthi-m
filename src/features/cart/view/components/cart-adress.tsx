import Commons from 'helpers/Commons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import { translate } from 'res/languages';
import { styles } from '../cart.style';
export interface AdressHeadProps {
  isShow?: boolean;
}

export default class AdressHead extends React.PureComponent<
  AdressHeadProps,
  any
> {
  constructor(props: AdressHeadProps) {
    super(props);
  }

  public render() {
    const { isShow } = this.props;
    return (
      <View>
        <View style={styles.name}>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={images.ic_store}
            style={styles.nameImage}
          />
          <DailyMartText style={styles.nameText}>
            {Commons.store.name}
          </DailyMartText>
        </View>
        <View style={styles.add}>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={images.ic_add}
            style={styles.addImage}
          />
          <DailyMartText style={styles.addText}>
            {' '}
            {Commons.store.address}
          </DailyMartText>
        </View>
        {!isShow && (
          <View style={{ height: 11, backgroundColor: '#E5E5E5' }}></View>
        )}
      </View>
    );
  }
}
