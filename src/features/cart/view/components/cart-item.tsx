import { Cart } from 'features/cart/model/cart';
import ApiHelper from 'helpers/api-helper';
import { DimensionHelper } from 'helpers/dimension-helper';
import EventBus, { EventBusName } from 'helpers/event-bus';
import { getMediaUrl } from 'helpers/helper-funtion';
import { URL_API } from 'helpers/url-api';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import _ from 'lodash';
import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import NumberFormat from 'react-number-format';
import images from 'res/images';
import { translate } from 'res/languages';
import R from 'res/R';
import { STATUS } from 'types/BaseResponse';

interface Items {
  name?: string;
  newcost?: string;
  oldcost?: string;
  image?: any;
  sold?: number;
  key?: number;
  cost?: number;
}

export interface CartItemProps {
  item: Cart;
  key: number;
  index: number;
  unavailable_products?: string[];
  parentFlatList: any;
  _idcart: string;
  onSetCart: (data: any) => void;
  dataCart: any;
}
interface State {
  count: number;
  activeRowKey: any;
}
export default class CartItem extends React.PureComponent<
  CartItemProps,
  State
> {
  constructor(props: CartItemProps) {
    super(props);
    this.state = {
      count: props.item.qty,
      activeRowKey: null
    };
    this.logCount = _.debounce(this.logCount, 500, { maxWait: 1000 });
  }

  logCount = (type: string) => {
    const { item, _idcart } = this.props;
    try {
      const body = {
        cartId: _idcart,
        action: type,
        productId: item.productId,
        qty: this.state.count
      };
      EventBus.getInstance().post({
        type: EventBusName.UPDATE_ITEM_CART,
        payload: body
      });
      ApiHelper.post(URL_API.CART_ADD_REMOVE, body, true).then((res) => {
        if (body.qty == 1 && type == 'REMOVE') {
          EventBus.getInstance().post({
            type: EventBusName.REMOVE_ITEM_CART,
            payload: body
          });
        }
        if (res.status == STATUS.PRODUCT_IS_OUT_OF_STOCK) {
          EventBus.getInstance().post({
            type: EventBusName.UPDATE_ITEM_CART,
            payload: { ...body, qty: this.state.count - 1 }
          });
          showAlert(translate(`status.${res.status}`));
          this.setState({ count: this.state.count - 1 });
        }
      });
    } catch (error) {}
  };

  decrease = () => {
    const { item } = this.props;
    if (item.deletedAt) return;
    if (this.state.count > 1) {
      this.setState({ count: this.state.count - 1 });
      this.logCount('ADD');
      return;
    }
    if (this.state.count == 1) {
      const deletingRow = this.state.activeRowKey;
      showAlertByType({
        title: translate('notify.header'),
        message: translate('cart.remove_product'),
        options: [
          {
            text: 'OK',
            onPress: () => {
              this.logCount('REMOVE');
            }
          }
        ]
      });
    }
  };

  increase = () => {
    const { item } = this.props;
    if (item.deletedAt) return;
    this.setState({ count: this.state.count + 1 });
    this.logCount('ADD');
  };

  public render() {
    const { item, unavailable_products } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ height: 1, backgroundColor: '#F2F2F2' }} />
        <View style={styles.flatListCart}>
          <FastImage
            source={getMediaUrl(
              (item.medias && item.medias.length > 0 && item.medias[0]) ||
                undefined
            )}
            style={[styles.image, { opacity: item.deletedAt ? 0.6 : 1 }]}
          />
          {((unavailable_products && unavailable_products.includes(item._id)) ||
            item.deletedAt) && (
            <FastImage
              source={images.ic_modal_waring}
              resizeMode={FastImage.resizeMode.contain}
              style={styles.iamgewr}
            />
          )}
          <View>
            <DailyMartText numberOfLines={2} style={styles.name}>
              {item.name || ''}
            </DailyMartText>
            <View
              style={{
                flexDirection: 'row',
                opacity:
                  (unavailable_products &&
                    unavailable_products.includes(item._id)) ||
                  item.deletedAt
                    ? 0.5
                    : 1,
                alignItems: 'flex-end',
                justifyContent: 'space-between'
              }}
            >
              <View>
                <NumberFormat
                  value={
                    item.promoFor ? item.priceAfterPromo || 0 : item.price || 0
                  }
                  displayType="text"
                  thousandSeparator="."
                  decimalSeparator=","
                  renderText={(value: any) => (
                    <DailyMartText style={[styles.cost, { flex: 1 }]}>
                      {value + ' đ'}
                    </DailyMartText>
                  )}
                />

                {item.promoFor !== '' && (
                  <NumberFormat
                    value={item.price || 0}
                    displayType="text"
                    thousandSeparator="."
                    decimalSeparator=","
                    renderText={(value: any) => (
                      <DailyMartText
                        style={[
                          styles.cost,
                          {
                            flex: 1,
                            textDecorationLine: 'line-through',
                            color: '#9C9C9C'
                          }
                        ]}
                      >
                        {value + ' đ'}
                      </DailyMartText>
                    )}
                  />
                )}
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Buttons
                  onPress={this.decrease}
                  styleImageButton={styles.imageButtonDe}
                  source={R.images.ic_decrease}
                />
                <DailyMartText style={styles.number}>
                  {this.state.count}
                </DailyMartText>
                <Buttons
                  onPress={this.increase}
                  styleImageButton={styles.imageButtonIn}
                  source={R.images.ic_increase}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100
  },
  flatListCart: {
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 10,
    paddingBottom: 15
  },
  iamgewr: {
    opacity: 0.6,
    position: 'absolute',
    width: 50,
    height: 50,
    alignSelf: 'center',
    left: 8,
    top: 10
  },
  image: {
    height: 65,
    width: 65
  },
  name: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    marginLeft: 20,
    width: DimensionHelper.getScreenWidth() * 0.68,
    height: 40
    //borderWidth:1
  },
  cost: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    marginLeft: 20,
    color: R.colors.primaryColor,
    marginTop: 10
  },
  imageButtonDe: {
    height: 30,
    width: 30
    // marginLeft: DimensionHelper.getScreenWidth() * 0.22
  },
  number: {
    width: 50,
    height: 30,
    backgroundColor: '#F7F7F8',
    marginHorizontal: 5,
    textAlign: 'center',
    paddingVertical: 5
  },
  imageButtonIn: {
    height: 30,
    width: 30
  }
});
