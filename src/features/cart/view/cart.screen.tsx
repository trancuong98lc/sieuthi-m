import { AdressPay } from 'features/address/address-pay/model/address-pay';
import { PromotionType } from 'features/confim-payment/model/confim-payment';
import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import EventBus, { EventBusName, EventBusType } from 'helpers/event-bus';
import { _formatPrice } from 'helpers/helper-funtion';
import { URL_API } from 'helpers/url-api';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import * as React from 'react';
import { FlatList, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Swipeout from 'react-native-swipeout';
import { connect } from 'react-redux';
import { updateQuantityCart } from 'redux/actions';
import { translate } from 'res/languages';
import R from 'res/R';
import { Subscription } from 'rxjs';
import { STATUS } from 'types/BaseResponse';
import { Cart, CartAll, CartDetail } from '../model/cart';
import { CartPresenter } from '../presenter/cart-presenter';
import { styles } from './cart.style';
import { CartView } from './cart.view';
import AdressHead from './components/cart-adress';
import BtnCart from './components/cart-btn';
import CartItem from './components/cart-item';

export interface Props {
  isTab?: boolean;
  navigation: any;
  updateQuantityCart: (quantity: number) => void;
}
interface State {
  deletedRowKey: any;
  cartAll: CartAll | null;
  valueCart: AdressPay | null;
  dataCart: CartDetail[];
  _idCart: string;
  valuePromotion: PromotionType;
}
class CartScreen extends React.Component<Props, State> implements CartView {
  presenter: CartPresenter;
  constructor(props: Props) {
    super(props);
    this.state = {
      deletedRowKey: null,
      valueCart: null,
      dataCart: [],
      valuePromotion: {
        promotionPriceProduct: 0,
        pointUser: 0,
        total: 0
      },
      _idCart: '',
      cartAll: null
    };
    this.presenter = new CartPresenter(this);
  }

  subScription = new Subscription();

  focusListener: any;
  componentDidMount() {
    const { navigation } = this.props;
    const { valueCart } = this.state;
    this.reCallTabProduct();
    this.subScription.add(
      EventBus.getInstance().events.subscribe((data: EventBusType) => {
        if (data.type == EventBusName.REMOVE_ITEM_CART) {
          this.reMoveItemCart(data.payload);
        }
        if (data.type == EventBusName.REMOVE_ADRESS && valueCart) {
          if (valueCart._id == data.payload._id) {
            this.setState({
              valueCart: null
            });
          }
          return;
        }
        if (data.type == EventBusName.UPDATE_ITEM_CART) {
          this.UpdateItemCart(data.payload);
        }
        if (
          data.type == EventBusName.CREATE_PRODUCT &&
          data.payload &&
          data.payload.length > 0
        ) {
          this.UpdateItemCart(data.payload[0]);
        }
      })
    );
  }

  reCallTabProduct = () => {
    this.presenter.onFetchProductsDetail();
  };

  logCount = (item: CartDetail) => {
    try {
      const body = {
        cartId: this.state._idCart,
        action: 'REMOVE',
        productId: item.productId,
        qty: 0
      };
      ApiHelper.post(URL_API.CART_ADD_REMOVE, body, true).then((res) => {
        if (res.status == STATUS.PRODUCT_UNAVAILABLE) {
          return showAlert(`status.${res.status}`);
        }
        this.reMoveItemCart(item);
      });
    } catch (error) {}
  };

  createItemCart = (data: any) => {
    this.setState({
      dataCart: [...data, ...this.state.dataCart]
    });
  };

  reMoveItemCart = (data: any) => {
    const { cartAll } = this.state;
    const newData = [...this.state.dataCart];
    const newDataE: any = newData.filter((e) => e.productId !== data.productId);
    const newCardAll: any = {
      ...cartAll,
      totalItems: this.state.cartAll?.totalItems! - 1,
      unavailableProducts: this.state.cartAll?.unavailableProducts.filter(
        (e) => e !== data.productId
      )
    };
    this.props.updateQuantityCart(newCardAll.totalItems || 0);
    this.setState({
      dataCart: newDataE,
      cartAll: newCardAll
    });
  };

  componentWillUnmount = () => {
    // this.subScription && this.subScription.unsubscribe()
  };

  UpdateItemCart = (data: any) => {
    const newData = [...this.state.dataCart];
    const index = newData.findIndex((e) => e.productId == data.productId);
    if (index >= 0) {
      newData.map((e) => {
        if (e.productId == data.productId) {
          e.qty = data.qty;
        }
        return e;
      });
    } else {
      this.setState(
        {
          dataCart: [data, ...newData]
        },
        () => {
          this.props.updateQuantityCart([data, ...newData].length || 0);
        }
      );
    }

    this.setState({
      dataCart: newData
    });
  };

  onFetchDataSuccess(
    cart: CartDetail[],
    _idCart: string,
    cartAll: CartAll
  ): void {
    this.setState(
      {
        dataCart: cart,
        _idCart,
        cartAll
      },
      () => {
        const itemss: any[] = [];
        cart.map((e: CartDetail) => {
          if (e.product && !cartAll.unavailableProducts.includes(e.productId)) {
            itemss.push({
              productId: e.productId,
              qty: e.qty
            });
          }
        });
        this.onGetPromotion(itemss);
      }
    );
  }

  onGetPromotion = async (itemss: any[]) => {
    try {
      const params = {
        cartId: this.state._idCart,
        itemss,
        storeId: Commons.idStore
      };
      const res = await ApiHelper.post(URL_API.ORDER_PROMOTION, params, true);
      if (res.status == STATUS.SUCCESS) {
        this.setState({
          valuePromotion: res.data
        });
      } else {
        this.setState({
          valuePromotion: null
        });
      }
    } catch (error) {
      console.log('onGetPromotion -> error', error);
    }
  };

  onSetCart = (cart: CartDetail[]) => {
    this.setState({
      dataCart: cart
    });
  };

  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  refreshFlatList = (deletedKey: any) => {
    this.setState({ deletedRowKey: deletedKey });
  };

  renderDelete = () => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <View
          style={{
            width: '100%',
            aspectRatio: 1 / 1,
            backgroundColor: '#E5334B',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={R.images.ic_trash_white}
            style={{ width: 25, height: 25 }}
          />
        </View>
      </View>
    );
  };

  renderItem = ({ item, index }: { item: CartDetail; index: number }) => {
    const newItem: Cart = { ...item, ...item.product! };
    delete newItem.product;
    return (
      <Swipeout
        autoClose
        sectionId={index}
        right={[
          {
            text: 'delete',
            color: 'black',
            backgroundColor: 'white',
            type: 'delete',
            onPress: () => {
              showAlertByType({
                title: translate('notify.header'),
                message: translate('cart.remove_product'),
                options: [
                  {
                    text: 'OK',
                    onPress: () => {
                      this.logCount(item);
                    }
                  }
                ]
              });
            },
            component: this.renderDelete()
          }
        ]}
        backgroundColor="white"
      >
        <CartItem
          item={newItem}
          key={newItem.qty}
          onSetCart={this.onSetCart}
          dataCart={this.state.dataCart}
          unavailableProducts={this.state.cartAll?.unavailableProducts}
          _idcart={this.state._idCart}
          index={index}
          parentFlatList={this}
        />
      </Swipeout>
    );
  };

  keyExtractor = (item: CartDetail, index: number) => {
    return index.toString();
  };

  public render() {
    const { isTab } = this.props;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService isTab={isTab} title={'cart.header'} />
        <FlatList
          style={styles.flatList}
          data={this.state.dataCart}
          ListHeaderComponent={<AdressHead />}
          showsVerticalScrollIndicator={false}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
        {this.state.cartAll && (
          <BtnCart
            _id={(this.state.cartAll! && this.state.cartAll!._id) || ''}
            // totalProduct={this.state.cartAll?.totalItems || 0}
            totalProduct={
              Number(this.state.cartAll?.totalItems) -
                this.state.cartAll.unavailableProducts.length || 0
            }
            total={this.presenter.onCheckData(
              this.state.dataCart,
              this.state.valuePromotion,
              this.state.cartAll?.unavailableProducts
            )}
          />
        )}
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateQuantityCart: (quantity: number) =>
      dispatch(updateQuantityCart(quantity))
  };
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  CartScreen
);
