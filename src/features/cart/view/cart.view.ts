import { Cart } from 'cart/model/cart';
import { CartAll, CartDetail } from '../model/cart';

export interface CartView {
  onFetchDataSuccess(cart: Cart, _idCart: string, cartAll: CartAll): void;
  onFetchDataFail(error?: any): void;
  onSetCart(cart: CartDetail[]): void;
}
