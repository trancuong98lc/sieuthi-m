import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom: -DimensionHelper.getBottomSpace()
     },
     cartHeader: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          paddingBottom: 10
     },
     cartHeaderImage: {
          width: 35,
          height: 35,
          marginTop: 10,
          marginLeft: 10
     },
     cartHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginLeft: DimensionHelper.getScreenWidth() * 0.3
     },
     name: {
          flexDirection: 'row',
          paddingTop: 10,
          paddingHorizontal: 10,
     },
     nameImage: {
          width: 17,
          height: 17,
     },
     nameText: {
          marginLeft: 10,
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: R.colors.primaryColor
     },
     add: {
          flexDirection: 'row',
          paddingHorizontal: 10,
          paddingVertical: 10
     },
     addImage: {
          width: 13,
          height: 18,
     },
     addText: {
          marginLeft: 10,
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: "#7A7A7A"
     },
     flatList: {
          backgroundColor: R.colors.white100,
          height: DimensionHelper.getScreenHeight() * 0.7
     },
     cartFooter: {
          flexDirection: 'row',
          elevation: 1,
          shadowOpacity: 0.2,
          backgroundColor: 'white',
          shadowColor: 'black',
          shadowOffset: {
               width: 3,
               height: 1
          },
          height: 70 + (DimensionHelper.isIphoneX() ? 0 : DimensionHelper.getBottomSpace()),
     },
     sumCost: {
          width: DimensionHelper.getScreenWidth() / 2,
          paddingLeft: 20

     },
     text: {
          fontSize: 13,
          color: '#979797',
          marginTop: 10
     },
     sum: {
          fontFamily: R.fonts.bold,
          color: '#E5334B',
          fontSize: 20,
          marginTop: 8,
          flex: 1
     },
     textPay: {
          fontFamily: R.fonts.bold,
          color: R.colors.white100,
          fontSize: 20
     },
     pay: {
          width: DimensionHelper.getScreenWidth() / 2,
          // backgroundColor: R.colors.primaryColor,
          borderRadius: 0
     }


})