import { Store_Type } from 'features/store/model/store-entity';
import { UnitType } from 'types/category';
import { MediaType } from 'types/ConfigType';

export interface Cart {
  categoryId: string;
  code: string;
  createdAt: string;
  deletedAt?: string;
  description?: string;
  featured: string;
  mediaIds: string[];
  medias?: MediaType[];
  name: string;
  new: string;
  price: number;
  productStore: Store_Type[];
  productStores: Store_Type;
  priceAfterPromo: number;
  priceBeforePromo: number;
  pricePromoted: number;
  preferentialAmount: number;
  totalApreferentialAmountll: number;
  promoFor: string;
  totalAll: number;
  status: string;
  unit: UnitType;
  unitId: string;
  _id: string;
  productId: String;
  qty: number;
  product?: any;
}

export interface CartDetail {
  product?: Cart;
  productId: string;
  qty: number;
  promoFor: string;
  totalPreferentialAmountll: number | string;
  priceBeforePromo: number | string;
  priceAfterPromo: number | string;
  pricePromoted: number | string;
}

export interface CartAll {
  code: string,
  createdAt: string;
  createdBy: string;
  deletedAt?: string;
  items: CartDetail[];
  unavailableProducts: string[];
  orderedStore: string;
  status: string;
  storeId: string;
  totalItems: number;
  _id: string;
}
