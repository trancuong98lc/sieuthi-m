export const Constants = {
    EN: 'en',
    VI: 'vn',
    DATE_FORMAT: 'MMM. DD',
    DATE_HOUR_FORMAT: 'MMM. DD - hh:mm a',
    DATE_TIME_NOTIFICATION: 'YYYY-MM-DD HH:mm',
    FOLDER_NAME: 'Conference'
};
