export interface ConfimPayment {}

export interface PromotionType {
  promotionPriceProduct: number;
  pointUser: number;
  total: number;
  listProduct?: number;
}
