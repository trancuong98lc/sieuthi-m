import { ConfimPayment } from 'confim-payment/model/confim-payment';
import { Cart, CartAll, CartDetail } from 'features/cart/model/cart';

export interface ConfimPaymentView {
  onFetchDataSuccess(
    cart: CartDetail[],
    _idCart: string,
    cartAll: CartAll
  ): void;
  onFetchDetailView(Confirm: ConfimPaymentView): void;
  onFetchDataFail(error?: any, notCity?: boolean): void;
}
