import { AdressPay } from 'features/address/address-pay/model/address-pay';
import { CartAll, CartDetail } from 'features/cart/model/cart';
import BtnCart from 'features/cart/view/components/cart-btn';
import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { DimensionHelper } from 'helpers/dimension-helper';
import { getDistance, _formatPrice } from 'helpers/helper-funtion';
import { URL_API } from 'helpers/url-api';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextPrice from 'libraries/text/TextPrice';
import * as React from 'react';
import { ImageBackground, View, ScrollView } from 'react-native';
import colors from 'res/colors';
import images from 'res/images';
import { translate } from 'res/languages';
import R from 'res/R';
import { ReceivedScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { STATUS } from 'types/BaseResponse';
import { PromotionType } from '../model/confim-payment';
import { ConfirmPresenter } from '../presenters/confirm-presenter';
import Address from './components/confirm-address';
import ListProductPayment from './components/confirm-listProduct';
import ConfirmPayMethod from './components/confirm-methodPay';
import ConfirmSum from './components/confirm-sum-price';
import { styles } from './confim-payment.style';
import { ConfimPaymentView } from './confim-payment.view';
import { connect } from 'react-redux';
import { updateConfig } from 'redux/actions';
import moment from 'moment';
import { Config } from 'types/ConfigType';
import FastImage from 'react-native-fast-image';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import { Subscription } from 'rxjs';
import EventBus, { EventBusName, EventBusType } from 'helpers/event-bus';

export interface Props {
  route: any;
  estimatedDeliveryTime: number;
  updateConfig: (config: Config) => void;
}
export interface State {
  isModalVisible: boolean;
  isUsePoint: boolean;
  cartId: string;
  cartAll: CartAll | null;
  dataCart: CartDetail[];
  _idCart: string;
  valueAddress: AdressPay | null;
  valuePay: any | null;
  valuePromotion: PromotionType | null;
  UserPhone: any;
  // estimatedDeliveryTime: string,
  conditionDelivery: {
    outOfRange: boolean;
    description: string;
  };
  addressHeight: number;
}
class ConfimPaymentScreen extends React.PureComponent<Props, State>
  implements ConfimPaymentView {
  presenter: ConfirmPresenter;
  interval: any;
  conditionDelivery: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      isModalVisible: false,
      cartId: props.route?.params?.cartId ?? '',
      isUsePoint: true,
      cartAll: null,
      valueAddress: null,
      UserPhone: null,
      valuePay: null,
      valuePromotion: {
        promotionPriceProduct: 0,
        pointUser: 0,
        total: 0
      },
      dataCart: [],
      _idCart: '',
      // estimatedDeliveryTime: `${this.getNow()} + ${this.timeConvert(this.props.estimatedDeliveryTime)}`,
      conditionDelivery: {
        outOfRange: false,
        description: ''
      },
      addressHeight: 0
    };
    this.presenter = new ConfirmPresenter(this);
  }
  refConfirmPay = React.createRef<ConfirmPayMethod>();

  subScription = new Subscription();
  componentDidMount = async () => {
    this.subScription.add(
      EventBus.getInstance().events.subscribe(async (data: EventBusType) => {
        if (data.type == EventBusName.UPDATE_ADRESS && data.payload) {
          await this.getUserSubPhone();
          this.setValueCart(data.payload);
          return;
        }
        if (data.type == EventBusName.CREATE_ADDRESS && data.payload) {
          await this.getUserSubPhone();
          return;
        }
      })
    );
    await this.getUserSubPhone();
    this.onGetCart();
    this.onGetListAddress();
    this.getConfig();
  };

  componentWillUnmount = () => {
    this.subScription && this.subScription.unsubscribe();
  };

  onGetCart = () => {
    this.presenter.onFetchProductsDetail();
  };

  getUserSubPhone = async () => {
    let UserPhone = await AsyncStorageHelpers.getObject(
      StorageKey.PHONE_NUMBER_SUB
    );
    this.setState({
      UserPhone
    });
  };

  onFetchDetailView(data: any): void {}

  onFetchDataSuccess(
    cart: CartDetail[],
    _idCart: string,
    cartAll: CartAll
  ): void {
    this.setState(
      {
        dataCart: cart,
        _idCart,
        cartAll
      },
      () => {
        let itemss: any[] = [];
        cart.map((e: CartDetail) => {
          if (e.product && !e.product.deletedAt) {
            itemss.push({
              productId: e.productId,
              qty: e.qty
            });
          }
        });
        console.log(
          '🚀 ~ file: confim-payment.screen.tsx ~ line 167 ~ itemss',
          itemss
        );
        this.onGetPromotion(itemss);
      }
    );
  }
  onFetchDataFail(error?: any, notCity?: boolean): void {
    if (notCity) {
      this.setState({
        valueAddress: null
      });
    }

    throw new Error('Method not implemented.');
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  usePoint = () => {
    this.setState({ isUsePoint: !this.state.isUsePoint });
  };

  onGetListAddress = async () => {
    try {
      const res = await ApiHelper.fetch(
        URL_API.LIST_DELIVERY,
        { isDefault: true },
        true
      );
      if (res.status === STATUS.SUCCESS && res.data) {
        if (res.data.length > 0) {
          this.setValueCart(res.data[0]);
          this.getConditionDelivery();
        } else {
          this.setValueCart(null);
        }
      }
    } catch (error) {
      console.log('onGetListAddress -> error', error);
    }
  };

  onGetPromotion = async (itemss: any[]) => {
    try {
      const params = {
        cartId: this.state._idCart,
        itemss,
        storeId: Commons.idStore
      };
      const res = await ApiHelper.post(URL_API.ORDER_PROMOTION, params, true);
      if (res.status == STATUS.SUCCESS) {
        this.setState({
          valuePromotion: res.data
        });
      } else {
        this.setState({
          valuePromotion: null
        });
      }
    } catch (error) {
      console.log('onGetPromotion -> error', error);
    }
  };

  setValueCart = (valueAddress: AdressPay | null) => {
    this.setState(
      {
        valueAddress: valueAddress
      },
      () => {
        if (this.conditionDelivery) {
          this.checkConditionDelivery(this.conditionDelivery);
        }
      }
    );
  };

  goToHistories = () => {
    const { UserPhone } = this.state;
    if (
      this.conditionDelivery &&
      this.checkDistance() > this.conditionDelivery.deliveryCondition
    ) {
      return showAlert('confimPay.deliveryOutOfRange');
    }
    if (!this.state.valueAddress) return showAlert('no_select_adress');
    const body: any = {
      cartId: this.state._idCart,
      deliveryAddressId: this.state.valueAddress?._id,
      payment: {
        method: this.refConfirmPay.current!.state.valuePay.type
      },
      totalPrice: this.state.valuePromotion?.total || 0,
      promotionPointUser: this.state.valuePromotion?.pointUser || 0,
      promotionProduct: this.state.valuePromotion?.promotionPriceProduct || 0,
      deliveryTime: moment(
        new Date().getTime() + this.props.estimatedDeliveryTime * 60 * 1000
      ).format('YYYY/MM/DD HH:mm')
    };
    if (UserPhone && UserPhone[this.state.valueAddress?._id]) {
      body.subreceiverPhone = UserPhone[this.state.valueAddress?._id];
    }
    if (this.refConfirmPay.current!.state.valuePay.type == 'BANKING')
      body.payment.mediaIds = [
        this.refConfirmPay.current!.state.valuePay.image._id
      ];
    this.presenter.onCreateOder(body);
  };

  // timeConvert = (time: number) => {
  //   if (time <= 60) {
  //     return `${time}p`
  //   }
  //   let hours = Math.floor(time / 60);
  //   let days = Math.floor(hours / 24);
  //   hours = hours - (days * 24);
  //   let minutes = time - (days * 24 * 60) - (hours * 60);

  //   let daysText = days === 0 ? '' : `${days} ${translate('time_unit.day')} `;
  //   let hoursText = hours === 0 ? '' : ((hours < 10 ? '0' : '') + `${hours}${translate('time_unit.hour')}`);
  //   let minutesText = minutes === 0 ? '00p' : ((minutes < 10 ? '0' : '') + `${minutes}${translate('time_unit.minute')}`);

  //   return `${daysText}${hoursText}${minutesText}`;
  // }

  // getNow = () => {
  //   const now = new Date();
  //   let minutes = now.getMinutes();
  //   let hours = now.getHours();
  //   return (hours < 10 ? `0${hours}:` : `${hours}:`) + (minutes < 10 ? `0${minutes}` : `${minutes}`)
  // }

  // caculateDeliveryTime = () => {
  //   const estimatedDeliveryTime = this.timeConvert(this.props.estimatedDeliveryTime);
  //   let time = this.getNow();
  //   this.setState({
  //     estimatedDeliveryTime: `${time} + ${estimatedDeliveryTime}`
  //   });
  //   this.interval = setInterval(() => {
  //     time = this.getNow();
  //     this.setState({
  //       estimatedDeliveryTime: `${time} + ${estimatedDeliveryTime}`
  //     });
  //   }, 1000);
  // }

  getConfig = () => {
    this.presenter.getGeneralConfig();
  };

  getConditionDelivery = () => {
    this.presenter.getConditionDelivery();
  };

  checkConditionDelivery = (conditionDelivery: any) => {
    this.conditionDelivery = conditionDelivery;
    this.setState({
      conditionDelivery: {
        outOfRange:
          this.checkDistance() > conditionDelivery.deliveryCondition
            ? true
            : false,
        description: conditionDelivery.description
      }
    });
  };

  checkDistance = () => {
    const store = Commons.store;
    const location = store.location.split(',');
    const storeCoordinate = {
      latitude: Number(location[0]),
      longitude: Number(location[1])
    };
    const { valueAddress } = this.state;
    const addressCoordinate = {
      latitude: valueAddress?.lat,
      longitude: valueAddress?.lon
    };
    return getDistance(storeCoordinate, addressCoordinate, 100) / 1000;
  };

  onAddressLayout = (event: any) => {
    const { width, height } = event.nativeEvent.layout;
    this.setState({ addressHeight: height + 30 });
  };

  componentWillUnmount = () => {
    clearInterval(this.interval);
  };

  public render() {
    const {
      valueAddress,
      valuePromotion,
      cartAll,
      estimatedDeliveryTime,
      conditionDelivery,
      UserPhone
    } = this.state;
    const unavailableProducts = this.state.cartAll?.unavailableProducts || [];
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService title="confimPay.header" />

        <View style={{ height: 8, backgroundColor: '#E5E5E5' }} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Address
            UserPhone={UserPhone}
            valueAddress={valueAddress}
            conditionDelivery={conditionDelivery}
            setValueCart={this.setValueCart}
            onAddressLayout={this.onAddressLayout}
          />
          {conditionDelivery.outOfRange && (
            <View
              style={[
                styles.tooltipOutOfRange,
                { top: this.state.addressHeight }
              ]}
            >
              <FastImage
                style={styles.tooltipArrowOutOfRange}
                source={R.images.ic_tooltip_arrow}
              />
              <DailyMartText
                style={{ color: '#FFFFFF', fontFamily: R.fonts.medium }}
              >
                {translate('confimPay.messageOutOfRange', {
                  distance: this.conditionDelivery.deliveryCondition
                })}
              </DailyMartText>
            </View>
          )}
          {/* <ImageBackground style={{ width: '100%', flex: 1, borderRadius: 10 }} imageStyle={{ borderRadius: 10 }} resizeMode="contain" source={images.ic_tip} /> */}
          <View style={{ height: 8, backgroundColor: '#E5E5E5' }} />
          <ListProductPayment
            unavailable_products={unavailableProducts}
            totalProduct={this.state.cartAll?.totalItems || 0}
            dataCart={this.state.dataCart}
          />
          <View style={styles.hr} />
          <ConfirmPayMethod
            ref={this.refConfirmPay}
            code={cartAll ? cartAll.code : ''}
          />
          <View style={{ height: 8, backgroundColor: '#E5E5E5' }} />
          <ConfirmSum
            nameIcon="sumMoney"
            title="confimPay.sumMoney"
            price={this.presenter.onCheckData(
              this.state.dataCart,
              this.state.valuePromotion,
              unavailableProducts
            )}
          />
          {valuePromotion && valuePromotion.promotionPriceProduct > 0 ? (
            <ConfirmSum
              nameIcon="sale"
              title="confimPay.sale"
              price={
                '- ' + String(valuePromotion.promotionPriceProduct.toFixed(0))
              }
              color={colors.primaryColor}
            />
          ) : null}
          {valuePromotion && valuePromotion.pointUser > 0 ? (
            <ConfirmSum
              nameIcon="saleMember"
              title="confimPay.saleMember"
              price={'- ' + String(valuePromotion.pointUser.toFixed(0))}
              color={colors.primaryColor}
            />
          ) : null}

          <View
            style={{ height: 1, backgroundColor: '#E5E5E5', marginTop: 10 }}
          />
          <View style={styles.sumPay}>
            <DailyMartText style={styles.sumPayText}>
              {translate('confimPay.sumPay')}
            </DailyMartText>
            <TextPrice
              total={
                (this.state.valuePromotion &&
                  this.state.valuePromotion.total) ||
                0
              }
              style={styles.sumPayCost}
            />
          </View>
          {/* <View style={{ height: 150 }} >
            < DailyMartText style={styles.deliveryTimeText}>
              {`${translate('confimPay.deliveryTime')} ${estimatedDeliveryTime}`}
            </DailyMartText>
          </View> */}
        </ScrollView>
        {/* 

          <DailyMartText style={styles.deliveryTimeText}>
            {translate('confimPay.deliveryTime')}: 17:12+30p
          </DailyMartText>

          <View style={{ height: 1, backgroundColor: '#E5E5E5' }}></View>

          <View style={styles.usePoint}>
            <FastImage
              style={styles.usePointImage}
              source={R.images.usePoint}
            />
            <DailyMartText style={styles.usePointText}>
              {translate('confimPay.use')}
            </DailyMartText>
            <DailyMartText style={styles.usePointText}> 1000 </DailyMartText>
            <DailyMartText style={styles.usePointText}>
              {translate('confimPay.point')}
            </DailyMartText>
            <DailyMartText
              style={[
                styles.usePointNum,
                {
                  color: this.state.isUsePoint ? '#E5334B' : '#F2F2F2'
                }
              ]}
            >
              [-10.000 đ]
            </DailyMartText>

            <View
              style={[
                styles.usePointViewButton,
                {
                  backgroundColor: this.state.isUsePoint
                    ? R.colors.primaryColor
                    : '#F2F2F2'
                }
              ]}
            >
              <Buttons
                styleButton={[
                  styles.button,
                  {
                    marginLeft: this.state.isUsePoint ? 22 : 1
                  }
                ]}
                onPress={this.usePoint}
              ></Buttons>
            </View>
          </View> */}
        <BtnCart
          style={styles.confimPayFooter}
          onPress={this.goToHistories}
          _id={(cartAll! && cartAll!._id) || ''}
          totalProduct={
            Number(cartAll?.totalItems) - unavailableProducts.length || 0
          }
          name="continue"
          total={
            (this.state.valuePromotion && this.state.valuePromotion.total) || 0
          }
        />
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { estimatedDeliveryTime } = state.config;
  return {
    estimatedDeliveryTime
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateConfig: (config: Config) => dispatch(updateConfig(config))
  };
};

export default connect(
  mapStatesToProps,
  mapDispatchToProps
)(ConfimPaymentScreen);
