import { Platform, StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";
import { getBottomSpace } from "react-native-iphone-x-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom: -DimensionHelper.getBottomSpace()
     },
     confimPayHeader: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          paddingBottom: 10
     },
     confimPayHeaderImage: {
          width: 35,
          height: 35,
          marginTop: 10,
          marginLeft: 10,

     },
     confimPayHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginLeft: DimensionHelper.getScreenWidth() * 0.18
     },
     deliveryAddr: {
          marginVertical: DimensionHelper.getScreenHeight() * 0.016,
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.036,
          paddingHorizontal: DimensionHelper.getScreenWidth() * 0.036,
          paddingVertical: DimensionHelper.getScreenHeight() * 0.016,
          borderWidth: 1,
          borderRadius: 1,
          borderStyle: 'dashed'
     },
     deliveryAddrOutOfRange: {
          borderColor: "#E5334B",
          backgroundColor: "#FFF3F5"
     },
     deliveryAddrInRange: {
          borderColor: '#3BBB5A',
          backgroundColor: "transparent"
     },
     textColorOutOfRange: {
          color: "#E5334B"
     },
     deliveryAddrImage: {
          width: 13,
          height: 17
     },
     warningImage: {
          width: 18,
          height: 17
     },
     deliveryAddrTittle: {
          fontFamily: R.fonts.medium,
          flexWrap: 'wrap',
          // flex: 1,
          fontSize: 17,
          color: '#3BBB5A',
          marginHorizontal: 10
     },
     deliveryAddrName: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: '#3BBB5A',
          marginTop: 10
     },
     deliveryAddrAddr: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: '#3BBB5A',
     },
     tooltipOutOfRange: {
          position: "absolute", 
          right: DimensionHelper.getScreenWidth() * 0.036, 
          left: DimensionHelper.getScreenWidth() * 0.036, 
          justifyContent: "center", 
          padding: 10, 
          borderRadius: 5, 
          alignItems: "center", 
          backgroundColor: R.colors.primaryColor, 
          maxWidth: DimensionHelper.getScreenWidth() * 0.928, 
          maxHeight: DimensionHelper.getScreenWidth(), 
          zIndex: 100 
     },
     tooltipArrowOutOfRange: {
          position: "absolute", 
          top: -15, 
          left: DimensionHelper.getScreenWidth() * 0.464 - 24, 
          zIndex: 100, 
          width: 48, 
          height: 24
     },
     name: {
          flexDirection: 'row',
          padding: DimensionHelper.getScreenHeight() * 0.1 / 6,
     },
     nameImage: {
          width: 17,
          height: 17,
     },
     nameText: {
          marginLeft: 10,
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: R.colors.primaryColor
     },
     addr: {
          flexDirection: 'row',
          paddingLeft: DimensionHelper.getScreenHeight() * 0.1 / 5,
     },
     addrImage: {
          width: 13,
          height: 18,
     },
     addrText: {
          marginLeft: 10,
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: "#7A7A7A"
     },
     flatList: {
          marginTop: 10,
          backgroundColor: R.colors.white100
     },
     buttonAll2: {
          marginLeft: DimensionHelper.getScreenWidth() * 0.7,
          marginTop: DimensionHelper.getScreenHeight() * 0.015,
     },
     textButtonAll2: {
          color: "#333333",
          fontSize: 13,
          fontFamily: R.fonts.medium
     },
     imageButtonAll2: {
          height: 12,
          width: 7,
          marginLeft: 5
     },
     view: {
          justifyContent: 'flex-end',
          margin: 0,
     },
     modal: {
          backgroundColor: 'white',
          padding: 20,
          height: DimensionHelper.getScreenHeight() * 0.866,
          borderTopLeftRadius: 10
     },
     modalTitle: {

     },
     flatListModal: {
          height: DimensionHelper.getScreenHeight() * 0.5,

     },
     modalButton: {
          width: 140,
          height: 45,
          backgroundColor: '#3BBB5A',
          marginHorizontal: (DimensionHelper.getScreenWidth() - 180) / 2
     },
     modalTextButton: {
          color: R.colors.white100,
          fontSize: 13,
          fontFamily: R.fonts.bold
     },
     hr: {
          height: 8,
          backgroundColor: '#E5E5E5',
          marginTop: DimensionHelper.getScreenHeight() * 0.015,
     },
     methodPay: {
          flexDirection: 'row',
          height: 35,
          marginVertical: 11,
          alignItems: 'center'
     },
     methodPayImage: {
          width: 19,
          height: 14,
          marginLeft: 15
     },
     methodPayText: {
          marginLeft: 10,
          fontSize: 13
     },
     methodPayCod: {
          width: 90,
          fontFamily: R.fonts.bold,
          fontSize: 13,
          color: "#333333",
          marginLeft: DimensionHelper.getScreenWidth() * 0.22
     },
     methodPayImageButton: {
          width: 7,
          height: 12,
          marginLeft: 15
     },
     sumMoney: {
          flexDirection: 'row',
          marginTop: DimensionHelper.getScreenHeight() * 0.015,
          marginLeft: 15
     },
     sumMoneyImage: {
          width: 15,
          height: 15
     },
     sumMoneyText: {
          fontSize: 13,
          color: "#333333",
          marginLeft: 10,
          flex: 1
     },
     sumMoneyCost: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: "#444444",
          marginRight: 20
     },
     sale: {
          flexDirection: 'row',
          marginTop: DimensionHelper.getScreenHeight() * 0.015,
          marginLeft: 15
     },
     saleImage: {
          width: 18,
          height: 14
     },
     saleText: {
          fontSize: 13,
          color: "#333333",
          marginLeft: 10,
          flex: 1
     },
     saleCost: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: "#3BBB5A",
          marginRight: 20
     },
     saleMember: {
          flexDirection: 'row',
          marginTop: DimensionHelper.getScreenHeight() * 0.015,
          marginBottom: DimensionHelper.getScreenHeight() * 0.01,
          marginLeft: 15
     },
     saleMemberImage: {
          width: 17,
          height: 17
     },
     saleMemberText: {
          fontSize: 13,
          color: "#333333",
          marginLeft: 10,
          flex: 1
     },
     saleMemberCost: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: "#3BBB5A",
          marginRight: 20
     },
     sumPay: {
          flexDirection: 'row',
          marginTop: DimensionHelper.getScreenHeight() * 0.01,
     },
     sumPayText: {
          fontFamily: R.fonts.bold,
          fontSize: 16,
          color: "#444444",
          marginLeft: 15,
          flex: 1
     },
     sumPayCost: {
          fontFamily: R.fonts.bold,
          fontSize: 19,
          color: "#3BBB5A",
          marginRight: 20
     },
     deliveryTimeText: {
          textAlign: 'center',
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: '#E5334B',
          marginTop: DimensionHelper.getScreenHeight() * 0.01,
          marginBottom: 17
     },
     usePoint: {
          flexDirection: 'row',
          //marginVertical: DimensionHelper.getScreenHeight() * 0.012,
          alignItems: 'center',
          marginLeft: 15,
          flex: 1
     },
     usePointImage: {
          width: 20,
          height: 20,
          marginRight: 10
     },
     usePointText: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: '#333333'
     },
     usePointNum: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          //color: '#E5334B',
          marginLeft: 0.15 * DimensionHelper.getScreenWidth(),
          flex: 1
     },
     usePointViewButton: {
          width: 52,
          height: 31,
          //backgroundColor: '#3BBB5A',
          borderRadius: 30,
          justifyContent: 'center',
          marginRight: 20
     },
     button: {
          width: 29,
          height: 29,
          //marginLeft: 22
     },
     confimPayFooter: {
          flexDirection: 'row',
          marginBottom: 0,
          position: 'absolute',
          bottom: 0,
          height: 70,

     },
     sumCost: {
          borderTopWidth: 1,
          borderColor: '#979797',
          width: DimensionHelper.getScreenWidth() / 2,
          paddingLeft: 20

     },
     text: {
          fontSize: 13,
          color: '#979797',
          marginTop: DimensionHelper.getScreenHeight() * 0.01,
     },
     sum: {
          fontFamily: R.fonts.bold,
          color: '#E5334B',
          marginTop: DimensionHelper.getScreenHeight() * 0.006,
          fontSize: 20
     },
     textPContinue: {
          fontFamily: R.fonts.bold,
          color: R.colors.white100,
          fontSize: 20
     },
     continue: {
          width: DimensionHelper.getScreenWidth() / 2,
          backgroundColor: R.colors.primaryColor,
          borderRadius: 0
     }


});