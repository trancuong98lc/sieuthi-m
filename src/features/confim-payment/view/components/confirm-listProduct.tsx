import cart from 'data/cart';
import FlatListProductPay from 'libraries/flatlist/flatlist.product-pay';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, FlatList, ScrollView } from 'react-native';
import Modal from 'react-native-modal';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { AddrPayScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../confim-payment.style';
import FlatListViewProduct from 'libraries/flatlist/flatlist-view-product';
import Buttons from 'libraries/button/buttons';
import { Cart, CartDetail } from 'features/cart/model/cart';
import AdressHead from 'features/cart/view/components/cart-adress';
import BaseIcon from 'libraries/baseicon/BaseIcon';
export interface ListProductPaymentProps {
    dataCart: any[]
    unavailable_products: string[]
    totalProduct: any
}

export default class ListProductPayment extends React.PureComponent<ListProductPaymentProps, any> {
    constructor(props: ListProductPaymentProps) {
        super(props);
        this.state = {
            isModalVisible: false,
        }
    }


    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    renderItem = ({ item, index }: { item: CartDetail, index: number }) => {
        const newItem: Cart = { ...item, ...item.product! }
        delete newItem.product
        return (
            <FlatListProductPay
                unavailable_products={this.props.unavailable_products}
                item={newItem}
                index={index}
            />
        );
    }

    renderItemModal = ({ item, index }: any) => {
        const newItem: Cart = { ...item, ...item.product! }
        delete newItem.product
        return (
            <FlatListViewProduct
                unavailable_products={this.props.unavailable_products}
                item={newItem}
                index={index}
            />
        );
    }

    keyExtractor = (item: any, index: number) => {
        return index.toString();
    }

    public render() {
        const { dataCart, totalProduct } = this.props
        return (
            <View>
                <AdressHead isShow />
                <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        style={styles.flatList}
                        data={dataCart}
                        keyExtractor={this.keyExtractor}
                        scrollEnabled={false}
                        key={Math.round(cart.length / 2)}
                        numColumns={Math.round(cart.length / 2)}
                        renderItem={this.renderItem}
                    />
                </ScrollView>
                <Buttons
                    textButton={`${totalProduct} ${translate('confimPay.product')}`}
                    source={R.images.ic_all2}
                    styleButton={styles.buttonAll2}
                    styleTextButton={styles.textButtonAll2}
                    styleImageButton={styles.imageButtonAll2}
                    onPress={this.toggleModal}
                />
                <Modal
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.2}
                    onBackdropPress={this.toggleModal}
                    style={styles.view}
                >
                    <View style={styles.modal}>
                        <BaseIcon name="close_black" width={12} onPress={this.toggleModal} style={{ position: 'absolute', top: 10, right: 10 }} />
                        <DailyMartText style={styles.modalTitle}>
                            {translate('confimPay.listProduct')}
                        </DailyMartText>
                        <FlatList
                            style={styles.flatListModal}
                            data={dataCart}
                            renderItem={this.renderItemModal}
                            keyExtractor={this.keyExtractor}
                        />
                        <Buttons
                            styleButton={styles.modalButton}
                            styleTextButton={styles.modalTextButton}
                            textButton={translate('button.ok')}
                            onPress={this.toggleModal}
                        />
                    </View>
                </Modal>
            </View>
        );
    }
}
