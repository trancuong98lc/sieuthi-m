import { AdressPay } from 'features/address/address-pay/model/address-pay';
import Commons from 'helpers/Commons';
import { DimensionHelper } from 'helpers/dimension-helper';
import { getDistance } from 'helpers/helper-funtion';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ImageBackground } from 'react-native';
import FastImage from 'react-native-fast-image';
import { color } from 'react-native-reanimated';
import colors from 'res/colors';
import images from 'res/images';
import { translate } from 'res/languages';
import R from 'res/R';
import { AddrPayScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../confim-payment.style';
export interface AddressProps {
    valueAddress?: AdressPay | null;
    setValueCart?: (data: AdressPay) => void | null;
    UserPhone?: any;
    conditionDelivery: {
        outOfRange: boolean,
        description: string
    },
    onAddressLayout: (event: any) => void
}

export default class Address extends React.PureComponent<AddressProps, any> {
    constructor(props: AddressProps) {
        super(props);
    }

    navigateAddrPay = () => {
        const { valueAddress } = this.props;
        navigate(AddrPayScreen, {
            key: 'comfimPayAddr',
            valueAddress: valueAddress,
            setValueCart: this.props.setValueCart
        });
    };

    renderAddress = () => {
        const { valueAddress, conditionDelivery, UserPhone } = this.props;
        let textColor, iconColor, addressIcon, warningIcon;
        if (conditionDelivery.outOfRange) {
            addressIcon = R.images.ic_addr3;
            warningIcon = R.images.ic_warning;
            textColor = styles.textColorOutOfRange;
            iconColor = "#E5334B";
        } else {
            addressIcon = R.images.ic_addr2;
            iconColor = colors.primaryColor;

        }
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ flex: 1, paddingRight: 5 }}>
                    <View style={stylesw.viewTop}>
                        <FastImage style={styles.deliveryAddrImage} source={addressIcon} />
                        <DailyMartText style={[styles.deliveryAddrTittle, textColor]}>{translate('confimPay.deliveryAddress')}</DailyMartText>
                        {warningIcon && <FastImage style={styles.warningImage} source={warningIcon} />}
                    </View>
                    {valueAddress && <DailyMartText style={[styles.deliveryAddrName, textColor]}>{`${valueAddress.receiverName} - ${valueAddress.receiverPhone}${UserPhone && UserPhone[valueAddress._id] ? '/' + UserPhone[valueAddress._id] : ''}`}</DailyMartText>}
                    {valueAddress && <DailyMartText numberOfLines={2} style={[styles.deliveryAddrAddr, textColor]}>
                        {`${valueAddress.detail.trim()}, ${valueAddress.ward && valueAddress.ward.name + ',' || ''} ${valueAddress.district && valueAddress.district.name + ',' || ''} ${valueAddress.city && valueAddress.city.name || ''}`}
                    </DailyMartText>}
                </View>
                <BaseIcon style={{}} name="ic_arrow_right" width={15} color={iconColor} iconStyle={{ tintColor: colors.primaryColor }} />
            </View>
        );
    };

    renderChoiceAddress = () => <DailyMartText style={{ color: colors.primaryColor }}>{translate('confimPay.chooseAddress')}</DailyMartText>;

    public render() {
        const { valueAddress, conditionDelivery } = this.props;
        let deliveryAddressStyle;
        if (conditionDelivery.outOfRange) {
            deliveryAddressStyle = styles.deliveryAddrOutOfRange;
        } else {
            deliveryAddressStyle = styles.deliveryAddrInRange;
        }
        return (
            <View style={[styles.deliveryAddr, deliveryAddressStyle]} onLayout={this.props.onAddressLayout}>
                <TouchableOpacity onPress={this.navigateAddrPay}>
                    {valueAddress ? this.renderAddress() : this.renderChoiceAddress()}
                </TouchableOpacity>

            </View>
        );
    }
}

const stylesw = StyleSheet.create({
    viewTop: {
        flexDirection: 'row',
        alignItems: 'center'
    }
});
