import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { AddrPayScreen, CreatePayScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../confim-payment.style';
export interface ConfirmPayMethodProps {
  code: string;
}
export interface ConfirmPayMethodStates {
  valuePay: any;
}

export default class ConfirmPayMethod extends React.PureComponent<
  ConfirmPayMethodProps,
  any
> {
  constructor(props: ConfirmPayMethodProps) {
    super(props);
    this.state = {
      valuePay: { type: 'CASHING' }
    };
  }

  gotoGetPayment = () => {
    navigate(CreatePayScreen, {
      valuePay: this.state.valuePay,
      setIspayment: this.setIspayment,
      code: this.props.code
    });
  };

  setIspayment = (value: any) => {
    this.setState({
      valuePay: value
    });
  };

  public render() {
    const { valuePay } = this.state;
    return (
      <View style={styles.methodPay}>
        <View style={styles.methodPay}>
          <FastImage
            source={R.images.methodPay}
            style={styles.methodPayImage}
          />
          <DailyMartText style={styles.methodPayText}>
            {translate('confimPay.methodPay')}
          </DailyMartText>
        </View>
        <TouchableOpacity
          onPress={this.gotoGetPayment}
          activeOpacity={0.8}
          style={{ flexDirection: 'row' }}
        >
          <DailyMartText style={styles.methodPayCod}>
            {valuePay.type == 'CASHING'
              ? translate('create_pay.pay1')
              : translate('create_pay.pay2')}
          </DailyMartText>
          <Buttons
            styleImageButton={styles.methodPayImageButton}
            source={R.images.ic_all2}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
