import { _formatPrice } from 'helpers/helper-funtion';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import NumberFormat from 'react-number-format';
import { translate } from 'res/languages';
import R from 'res/R';
import { AddrPayScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../confim-payment.style';
export interface ConfirmSumProps {
    nameIcon: string,
    color?: string,
    title: string,
    price: any
}

export default class ConfirmSum extends React.PureComponent<ConfirmSumProps, any> {
    constructor(props: ConfirmSumProps) {
        super(props);
    }

    public render() {
        const { color, title, price } = this.props
        return (
            <View style={styles.sumMoney}>
                <BaseIcon width={20} name={this.props.nameIcon} />
                <DailyMartText style={styles.sumMoneyText}>{translate(title)} :</DailyMartText>
                <NumberFormat
                    value={price}
                    displayType="text"
                    thousandSeparator="."
                    decimalSeparator=","
                    renderText={(value: any) => (
                        <DailyMartText style={[styles.sumMoneyCost, { color }]}>{`${value} đ`}</DailyMartText>
                    )}
                />

            </View>
        );
    }
}
