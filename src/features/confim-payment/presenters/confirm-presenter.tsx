import { CartDetail } from 'features/cart/model/cart';
import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import EventBus, { EventBusName } from 'helpers/event-bus';
import { URL_API } from 'helpers/url-api';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import { translate } from 'res/languages';
import { ReceivedScreen } from 'routing/screen-name';
import { navigate, resetStack } from 'routing/service-navigation';
import { STATUS } from 'types/BaseResponse';
import { PromotionType } from '../model/confim-payment';
import { ConfimPaymentView } from '../view/confim-payment.view';
import moment from 'moment';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';

export class ConfirmPresenter {
  private ConfirmView: ConfimPaymentView;

  constructor(ConfirmView: ConfimPaymentView) {
    this.ConfirmView = ConfirmView;
  }

  async onFetchCartDetail(idCart: String): Promise<any> {
    try {
      const res = await ApiHelper.fetch(
        URL_API.ORDER_DETAIL + idCart,
        null,
        true
      );
      if (res.status == STATUS.SUCCESS && res.data) {
        this.ConfirmView.onFetchDetailView(res.data);
      }
    } catch (error) {
      console.log('error: ', error);
      this.ConfirmView.onFetchDataFail();
    }
  }

  async onFetchProductsDetail(): Promise<any> {
    try {
      const res = await ApiHelper.fetch(
        URL_API.CART_DETAIL + `/${Commons.idStore}/PENDING`,
        null,
        true
      );
      if (res.status == STATUS.SUCCESS && res.data && res.data.items) {
        this.ConfirmView.onFetchDataSuccess(
          res.data.items || [],
          res.data._id,
          res.data
        );
      }
    } catch (error) {
      console.log('error: ', error);
      this.ConfirmView.onFetchDataFail();
    }
  }

  onCheckData = (
    data: CartDetail[],
    promotion?: PromotionType | null,
    unavailable_products: string[]
  ) => {
    let price: number = 0;
    data.forEach((obj: CartDetail) => {
      if (
        !unavailable_products.includes(obj.productId) ||
        (obj.product && !obj.product.deletedAt)
      ) {
        if (obj.promoFor) {
          price += (Number(obj.priceBeforePromo) || 0) * obj.qty;
        } else {
          price += (obj.product!.price || 0) * obj.qty;
        }
      }
    });
    return price;
  };

  onCreateOder = async (data: any) => {
    showLoading();
    try {
      const res = await ApiHelper.post(URL_API.ORDER_CREATE, data, true);
      if (res.status == STATUS.SUCCESS) {
        setTimeout(() => {
          showAlert(
            `${translate('create_pay.pay_success')}.\n${translate(
              'confimPay.deliveryTime'
            )} ${moment(res.data.deliveryTime).format('HH:mm')}`,
            { success: true }
          );
        }, 500);
        hideLoading();
        AsyncStorageHelpers.remove(StorageKey.PHONE_NUMBER_SUB);
        resetStack(ReceivedScreen);
        EventBus.getInstance().post({
          type: EventBusName.UPDATE_COUNT_CART,
          payload: 0
        });
      } else {
        hideLoading();
        if (res.status === STATUS.CART_INVALID) {
          hideLoading();
          setTimeout(() => {
            showAlert(`status.${res.status}`);
          }, 300);
        }
        if (res.status === STATUS.PRODUCT_IS_OUT_OF_STOCK) {
          hideLoading();
          setTimeout(() => {
            showAlert(`status.PRODUCT_IS_OUT_OF_STOCK2`);
          }, 300);
        }
        if (res.status === STATUS.PRODUCT_UNAVAILABLE) {
          hideLoading();
          setTimeout(() => {
            this.onFetchProductsDetail();
            showAlert(`status.PRODUCT_UNAVAILABLE`);
          }, 300);
        }
        if (res.status === STATUS.PRODUCT_DO_NOT_EXIST) {
          hideLoading();
          setTimeout(() => {
            this.onFetchProductsDetail();
            showAlert(`status.PRODUCT_DO_NOT_EXIST`);
          }, 300);
        }

        if (res.status === STATUS.DELIVERY_ADDRESS_DO_NOT_EXISTS) {
          hideLoading();
          this.ConfirmView.onFetchDataFail('err', true);
          setTimeout(() => {
            showAlert(`status.DELIVERY_ADDRESS_DO_NOT_EXISTS`);
          }, 300);
        }
      }
    } catch (error) {
      hideLoading();
      setTimeout(() => {
        showAlert(`status.FAILURE`);
      }, 300);
      console.log('error: ', error);
      this.ConfirmView.onFetchDataFail();
    }
  };

  getGeneralConfig = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.GENERAL_CONFIG, {}, true);
      if (res.status == STATUS.SUCCESS) {
        this.ConfirmView.props.updateConfig(res.data);
        // this.ConfirmView.caculateDeliveryTime();
      } else {
        // this.ConfirmView.caculateDeliveryTime();
      }
    } catch (error) {
      console.log('getGeneralConfig error: ', error);
      //   this.ConfirmView.caculateDeliveryTime();
    }
  };

  getConditionDelivery = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.CONDITION_DELIVERY, {}, true);
      if (res.status == STATUS.SUCCESS) {
        this.ConfirmView.checkConditionDelivery(res.data);
      } else {
      }
    } catch (error) {
      console.log('getGeneralConfig error: ', error);
    }
  };
}
