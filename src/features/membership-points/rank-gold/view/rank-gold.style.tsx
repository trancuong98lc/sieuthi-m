import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          //backgroundColor:R.colors.white100
     },
     header: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          alignItems: 'center',
          paddingBottom:DimensionHelper.getScreenHeight()*0.123
          
     },
     buttonBack: {
          backgroundColor: R.colors.primaryColor,
          
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop: 10,
          marginBottom: 13,
          marginLeft:12
     },
     new: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginBottom:10,
          marginLeft:DimensionHelper.getScreenWidth()*0.18
     },
     viewMemberPoint: {
          alignItems: 'center',
          marginTop: -DimensionHelper.getScreenWidth() * 0.15,
          height:DimensionHelper.getScreenWidth() * 0.352,
          
     },
     viewAvata: {
          width: 88,
          height: 88,
          backgroundColor: R.colors.white100,
          borderRadius: 88,
          alignItems: 'center',
          justifyContent: 'center',
          position: 'absolute',
          marginTop:-44
     },
     avataImage: {
          height: 86,
          width: 86,
     },
     imagePersonal: {
          width: DimensionHelper.getScreenWidth() * 0.92,
          height: DimensionHelper.getScreenWidth() * 0.352,
         //marginTop:DimensionHelper.getScreenHeight() * 0.021,
          borderRadius:5,
          
     },
     rank: {
          marginTop: -DimensionHelper.getScreenWidth() * 0.15,
          fontSize: 11,
          color:"#78674F"
     },
     point: {
          marginTop: -DimensionHelper.getScreenWidth() * 0.13,
          fontFamily:R.fonts.bold,
          fontSize: 30,
          color:"#78674F"
     },
     infoEarnPoints: {
          alignItems: 'center',
          borderWidth: 1,
          backgroundColor: R.colors.white100,
          marginTop: 12,
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.04,
          paddingTop: 20,
          paddingBottom: 8,
          borderStyle: 'dashed',
          borderColor:'#EFEFEF'
     },
     textAccumulated: {
          fontFamily: R.fonts.medium,
          fontSize: 11,
          color:'#A2A2A2'
     },
     moneyAccumulated: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#202020',
          
     },
     statusBar: {
          justifyContent: 'center',
          marginTop:30
     },
     viewStatusBar1: {
          height: 6,
          backgroundColor: '#EDEDED',
          width: DimensionHelper.getScreenWidth() * 0.76,
          borderRadius:6
          //marginLeft:DimensionHelper.getScreenWidth() * 0.12,
          
     },
     viewStatusBar2: {
          height: 6,
          backgroundColor: '#3BBB5A',
          borderRadius:6,
          //marginLeft:DimensionHelper.getScreenWidth() * 0.12,
          position: 'absolute'
     },
     rankIcon: {
          flexDirection: 'row',
          position: 'absolute',
          //marginLeft:DimensionHelper.getScreenWidth() * 0.12,
     },
     icRankCu: {
          width: 30,
          height: 30,
          borderRadius:30,
          marginLeft:-15
     },
     icRankAg: {
          width: 30,
          height: 30,
          borderRadius: 30,
          marginLeft:DimensionHelper.getScreenWidth() * 0.38-30,
     },
     icRankAu: {
          width: 30,
          height: 30,
          borderRadius: 30,
          marginLeft:DimensionHelper.getScreenWidth() * 0.38-30,
     },
     rankText: {
          flexDirection: 'row',
          position: 'absolute',
     },
     textRankCu: {
          width: 70,
          fontSize: 12,
          color: '#767676',
          marginTop: 60,
          //borderWidth:1,
          marginLeft: -33,
          textAlign:'center'
     },
     textRankAg: {
          width: 70,
          fontSize: 12,
          color: '#767676',
          marginTop: 60,
          marginLeft: DimensionHelper.getScreenWidth() * 0.38 - 70,
          textAlign:'center'
     },
     textRankAu: {
          width: 70,
          fontSize: 12,
          color: '#767676',
          marginTop: 60,
          marginLeft: DimensionHelper.getScreenWidth() * 0.38 - 75,
          textAlign:'center'
     },
     textSpend: {
          marginTop: 65,
          fontSize: 14,
          color:'#767676'
     },
     moneySpend: {
          fontFamily: R.fonts.bold,
          fontSize: 14,
          color:R.colors.primaryColor
     },
     viewEndowPolicy: {
          flexDirection: 'row',
          height: 60,
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          //borderWidth:1
     },
     hrEndowPolicy: {
          borderTopWidth: 1,
          borderColor: '#707070',
          width: DimensionHelper.getScreenWidth() * 0.25,
          alignItems:'center'
          
     },
     textEndowPolicy: {
          width: DimensionHelper.getScreenWidth() * 0.41,
          textAlign: 'center',
          fontFamily: R.fonts.bold,
          fontSize: 17,
          color:'#202020'
     },
     endowDetail: {
          backgroundColor: R.colors.white100,
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.04,
          borderWidth: 1,
          borderColor:"#F2F2F2"
     },
     endowDetailHead: {
          flexDirection:'row'
     },
     itemClick: {
          width: DimensionHelper.getScreenWidth() * 0.92 / 3-3,
          justifyContent: 'center',
          alignItems: 'center',
          height: 60,
          borderLeftWidth: 1,
          borderLeftColor:'#F2F2F2',
          borderBottomWidth: 2,
          borderBottomColor:R.colors.primaryColor
     },
     itemNotClick: {
          width: DimensionHelper.getScreenWidth() * 0.92 / 3,
          justifyContent: 'center',
          alignItems: 'center',
          height: 60,
          borderLeftWidth: 1,
          backgroundColor: '#FBFBFB',
          borderBottomWidth: 2,
          borderBottomColor: '#F2F2F2',
          borderLeftColor:'#F2F2F2',
     },
     itemIc: {
          width: 20,
          height: 20,
          borderRadius:20
     },
     itemText: {
          marginTop:5,
          fontSize: 12,
          color:'#767676'
     },
     endowDetailContent: {
          height: DimensionHelper.getScreenHeight()/4,
          width: DimensionHelper.getScreenWidth() * 0.92,
          paddingTop: 10,
          paddingLeft:10
     },
     textEndowDetailContent: {
          fontSize: 13,
          color: '#3B3B3B',
          marginTop:5
     }
})