import { RankGold } from "../model/rank-gold";

export interface RankGoldView {
     onFetchDataSuccess(rankGold: RankGold): void;
     onFetchDataFail(error?: any): void;
 }