import { User } from 'types/user-type';

export interface dataRank {
  createdAt: string;
  name: string;
  preferential: number;
  description: string;
  status: string;
  listPoint: number[];
  targetPoint: string;
  _id: string;
}

export interface RankDetail {
  _id: string;
  ratio: number;
  user: User;
  price_amount_accumulated: number;
  listRank: dataRank[];
}
