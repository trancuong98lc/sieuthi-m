import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View } from 'react-native';
import { translate } from 'res/languages';
import { styles } from '../rank-bronze.style';
export interface ContentRankProps {
    description: string
}

export default class ContentRank extends React.PureComponent<ContentRankProps, any> {
    constructor(props: ContentRankProps) {
        super(props);
    }

    public render() {
        const { description } = this.props
        return (
            <View style={styles.endowDetail}>
                <View style={styles.endowDetailContent}>
                    <DailyMartText
                        style={styles.textEndowDetailContent}
                    >
                        {description}
                    </DailyMartText>
                </View>
            </View>
        );
    }
}
