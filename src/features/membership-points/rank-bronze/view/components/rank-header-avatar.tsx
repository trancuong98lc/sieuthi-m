import { DimensionHelper } from 'helpers/dimension-helper';
import { getPhotoUrl } from 'helpers/helper-funtion';
import DailyAvatar from 'libraries/Avatar/DailyMartAvatar';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { User } from 'types/user-type';
import { styles } from '../rank-bronze.style';
export interface HeaderRankAvatarProps {
    user: User
}

export default class HeaderRankAvatar extends React.PureComponent<HeaderRankAvatarProps, any> {
    constructor(props: HeaderRankAvatarProps) {
        super(props);
    }

    public render() {
        const { user } = this.props
        const pointAccumulated = user.pointAccumulated || 0;
        const point = user.point || 0;
        return (
            <View style={styles.viewMemberPoint}>
                <FastImage
                    style={styles.imagePersonal}
                    source={R.images.groundMembershipPoint}
                />
                <View style={{flexDirection: "row", marginTop: -DimensionHelper.getScreenWidth() * 0.15,  }}>
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <DailyMartText style={styles.pointTitle}>
                            {translate('membershipPoints.pointAccumulated')}
                        </DailyMartText>
                        <DailyMartText style={[styles.point, {fontSize: pointAccumulated.toString().length <= 5 ? 30 : (30 - (pointAccumulated.toString().length - 5) * 3)}]}>
                            {pointAccumulated} {translate('personalLogin.point')}
                        </DailyMartText>
                    </View>
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <DailyMartText style={styles.pointTitle}>
                                {translate('membershipPoints.pointAvaiable')}
                            </DailyMartText>
                            <DailyMartText style={[styles.point, {fontSize: point.toString().length <= 5 ? 30 : (30 - (point.toString().length - 5) * 3)}]}>
                                {point} {translate('personalLogin.point')}
                            </DailyMartText>
                        </View>
                </View>
    
                <DailyMartText style={styles.rank}>
                    {user.pointRanking ? user.pointRanking.name.toUpperCase() : translate('membershipPoints.noRank').toUpperCase()}
                </DailyMartText>
                <DailyAvatar style={styles.viewAvata} url={getPhotoUrl(user)} size={87} />
            </View>
        );
    }
}
