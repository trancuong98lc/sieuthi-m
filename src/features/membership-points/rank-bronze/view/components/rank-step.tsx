import { DimensionHelper } from 'helpers/dimension-helper';
import { _formatPrice } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextPrice from 'libraries/text/TextPrice';
import * as React from 'react';
import { ScrollView, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import colors from 'res/colors';
import { translate } from 'res/languages';
import R from 'res/R';
import { User } from 'types/user-type';
import { dataRank, RankDetail } from '../../model/rank-bronze';
import { styles } from '../rank-bronze.style';
export interface RankStepProps {
  user: User;
  dataRank: RankDetail | null;
}
export interface RankStepStates {}

export default class RankStep extends React.PureComponent<
  RankStepProps,
  RankStepStates
> {
  constructor(props: RankStepProps) {
    super(props);
  }

  renderStep = (listRank: dataRank[]) => {
    const { user } = this.props;
    if (listRank.length == 0) return;
    return (
      listRank &&
      listRank.length > 0 &&
      listRank.map((item: dataRank, indexe: number) => {
        // let dataDot: any[] = [];
        // // for (let index = 1; index < 7; index++) {
        // //     dataDot.push({
        // //         point: (index - 1) * (Number(item.targetPoint || 0) / 6) + Number(item.targetPoint || 0),
        // //         isEnd: user.point <= (index - 1) * (Number(item.targetPoint || 0) / 6) + Number(item.targetPoint || 0)
        // //     });
        // //     console.log("RankStep -> renderStep -> dataDot", dataDot)
        // // }
        const widthBar =
          listRank.length <= 2
            ? DimensionHelper.width / 2
            : listRank.length < 4
            ? DimensionHelper.width / 4
            : DimensionHelper.width / 4;
        const pointRange = this.checkPointRange(user.point, listRank);
        const widthProgress =
          pointRange >= listRank.length
            ? widthBar
            : pointRange - 1 > indexe
            ? widthBar
            : pointRange - 1 < indexe
            ? 0
            : ((user.point -
                Number((item.targetPoint && item.targetPoint) || 0)) *
                widthBar) /
              (Number(
                (listRank[indexe + 1] && listRank[indexe + 1].targetPoint) || 0
              ) -
                Number((item.targetPoint && item.targetPoint) || 0));

        return (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              flex: 1,
              marginBottom: 10
            }}
          >
            <FastImage source={R.images.ic_rankCu} style={styles.icRankCu} />
            <DailyMartText style={styles.text} numberOfLines={2}>
              {item.name}
            </DailyMartText>

            {listRank.length - 1 > indexe && (
              // dataDot.length > 0 &&
              // dataDot.map((e, i) => {
              //     return (
              <View
                style={[
                  styles.viewStatusBar1,
                  {
                    width: widthBar,
                    backgroundColor: '#EDEDED'
                    // user.point >= Number(item.targetPoint || 0)
                    //     ? colors.primaryColor
                    //     :

                    // borderTopRightRadius:
                    //     !dataDot[i].isEnd &&
                    //         dataDot[i + 1] &&
                    //         dataDot[i + 1].isEnd &&
                    //         user.pointRanking
                    //         ? 5
                    //         : 0,
                    // borderBottomRightRadius:
                    //     !dataDot[i].isEnd &&
                    //         dataDot[i + 1] &&
                    //         dataDot[i + 1].isEnd &&
                    //         user.pointRanking
                    //         ? 5
                    //         : 0
                  }
                ]}
              >
                <View
                  style={{
                    position: 'absolute',
                    height: 6,
                    backgroundColor: colors.primaryColor,
                    width: widthProgress,
                    borderTopRightRadius: widthProgress < widthBar ? 5 : 0,
                    borderBottomRightRadius: widthProgress < widthBar ? 5 : 0
                  }}
                />
              </View>
            )}
            {/* //     );
                            // })} */}
          </View>
        );
      })
    );
  };

  checkPointRange = (point: number, listRank: dataRank[]) => {
    if (listRank.length == 0) return null;
    for (let i = 0; i <= listRank.length - 1; i++) {
      if (point < Number(listRank[i].targetPoint || 0 || 0)) {
        return i;
      }
    }
    return listRank.length;
  };

  private onCheckPrice = () => {
    const { dataRank, user } = this.props;
    if (!dataRank)
      return {
        pointUp: 0,
        nameVip: 0
      };
    let data = dataRank.listRank;
    let pointUp = 0;
    let nameVip = '';
    if (
      data[0] &&
      data[0].targetPoint &&
      dataRank.price_amount_accumulated <
        Number(data[0].targetPoint) * dataRank.ratio
    ) {
      pointUp =
        Number(data[0].targetPoint) * dataRank.ratio -
        Number(user.point) * dataRank.ratio;
      nameVip = data[0].name;
      return {
        pointUp,
        nameVip
      };
    }

    for (let index = 1; index < data.length; index++) {
      const element = data[index];
      if (
        element &&
        element.targetPoint &&
        dataRank.price_amount_accumulated <
          Number(Number(element.targetPoint) * dataRank.ratio)
      ) {
        pointUp =
          Number(Number(element.targetPoint) * dataRank.ratio) -
          Number(dataRank.price_amount_accumulated);
        nameVip = data[index].name;
        break;
      }
    }

    return {
      pointUp,
      nameVip
    };
  };

  public render() {
    const { dataRank, user } = this.props;
    return (
      <View style={styles.infoEarnPoints}>
        <DailyMartText style={styles.textAccumulated}>
          {translate('membershipPoints.moneyAccumulated')}
          {dataRank && (
            // <TextPrice total={`${(dataRank.price_amount_accumulated)} đ`} style={styles.moneyAccumulated} />
            <TextPrice
              total={`${user.priceAccumulated || 0} đ`}
              style={styles.moneyAccumulated}
            />
          )}
        </DailyMartText>
        <ScrollView
          horizontal
          style={{ paddingBottom: 20 }}
          contentContainerStyle={{ paddingHorizontal: 20 }}
          showsHorizontalScrollIndicator={false}
        >
          {dataRank && dataRank.listRank && (
            <View style={styles.statusBar}>
              {this.renderStep(dataRank.listRank || [])}
            </View>
          )}
        </ScrollView>
        {this.onCheckPrice().nameVip ? (
          <DailyMartText style={styles.textSpend}>
            {translate('membershipPoints.youSpend')}
            <DailyMartText style={styles.moneySpend}>
              {` ${_formatPrice(this.onCheckPrice()!.pointUp)}đ ${translate(
                'membershipPoints.become'
              )} ${this.onCheckPrice()!.nameVip}`}
            </DailyMartText>
          </DailyMartText>
        ) : !user.pointRanking ? (
          <DailyMartText style={styles.textSpend}>
            {translate('membershipPoints.noRank')}
          </DailyMartText>
        ) : (
          <DailyMartText style={styles.textSpend}>
            {translate('membershipPoints.amount_success')}
          </DailyMartText>
        )}
      </View>
    );
  }
}
