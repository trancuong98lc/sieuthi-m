import ApiHelper from 'helpers/api-helper';
import { DimensionHelper } from 'helpers/dimension-helper';
import { URL_API } from 'helpers/url-api';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import {
    Alert,
    FlatList, ScrollView, TouchableOpacity, View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import colors from 'res/colors';
import { translate } from 'res/languages';
import R from 'res/R';
import { STATUS } from 'types/BaseResponse';
import { User } from 'types/user-type';
import { dataRank, RankBronze, RankDetail } from '../model/rank-bronze';
import ContentRank from './components/rank-content-detail';
import HeaderRankAvatar from './components/rank-header-avatar';
import RankStep from './components/rank-step';
import { styles } from './rank-bronze.style';
import { RankBronzeView } from './rank-bronze.view';

export interface Props {
    user: User;
}
export interface States {
    dataRank: RankDetail | null;
    valueRank: dataRank | null;
}

class RankBronzeScreen extends React.PureComponent<Props, States>
    implements RankBronzeView {
    constructor(props: Props) {
        super(props);
        this.state = {
            dataRank: null,
            valueRank: null
        };
    }

    refFlatlist = React.createRef<FlatList>()
    onFetchDataSuccess(rankBronze: RankBronze): void {
        throw new Error('Method not implemented.');
    }
    onFetchDataFail(error?: any): void {
        throw new Error('Method not implemented.');
    }

    componentDidMount = () => {
        this.getRanking();
    };

    getRanking = async () => {
        try {
            const res = await ApiHelper.fetch(URL_API.RANK_USER, null, true);
            if (res.status == STATUS.SUCCESS) {
                this.setState(
                    {
                        dataRank: res.data
                    },
                    () => {
                        this.setState({
                            valueRank: res.data.listRank[0]
                        });
                    }
                );
            }
        } catch (error) { }
    };

    onPressItem = (item: dataRank, index: number) => () => {
        this.setState({
            valueRank: item
        }, () => {
            setTimeout(() => {
                this.refFlatlist.current!.scrollToIndex({ animated: true, index })
            }, 300);
        });
    };

    _renderItem = ({ item, index }: { item: dataRank, index: number }) => {
        return (
            <TouchableOpacity
                style={
                    item._id === this.state.valueRank?._id
                        ? styles.itemClick
                        : styles.itemNotClick
                }
                activeOpacity={0.6}
                onPress={this.onPressItem(item, index)}
            >
                <FastImage source={R.images.ic_rankCu} style={styles.itemIc} />
                <DailyMartText style={styles.itemText}>{item.name}</DailyMartText>
            </TouchableOpacity>
        );
    };

    public render() {
        const { user } = this.props;
        const { dataRank, valueRank } = this.state;
        return (
            <Container
                statusBarColor={R.colors.primaryColor}
                colorBack={colors.white101}
            >
                <HeaderService title="membershipPoints.header" />
                <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                    <View style={styles.viewBack} />
                    <View style={{ marginTop: - DimensionHelper.width / 3 }}>
                        <HeaderRankAvatar user={dataRank ? dataRank.user : user} />
                        <RankStep
                            user={dataRank ? dataRank.user : user}
                            dataRank={dataRank}
                        />
                        <View style={styles.viewEndowPolicy}>
                            <View style={styles.hrEndowPolicy} />
                            <DailyMartText style={styles.textEndowPolicy}>
                                {translate('membershipPoints.endowPolicy')}
                            </DailyMartText>
                            <View style={styles.hrEndowPolicy}></View>
                        </View>
                        <FlatList
                            horizontal
                            ref={this.refFlatlist}
                            showsHorizontalScrollIndicator={false}
                            data={dataRank?.listRank}
                            style={{ marginHorizontal: 17, }}
                            keyExtractor={(item: dataRank) => item._id}
                            extraData={this.state}
                            renderItem={this._renderItem}
                        />
                        <ContentRank
                            description={
                                valueRank &&
                                    valueRank.description &&
                                    valueRank.description.length > 0
                                    ? valueRank.description
                                    : translate('membershipPoints.noDes')
                            }
                        />
                        <View style={{ height: 150 }} />
                    </View>
                </ScrollView>
            </Container>
        );
    }
}
const mapStatesToProps = (state: any) => {
    const { user } = state.userReducers;
    const { isLogin, screen } = state.isLogin;
    return {
        user,
        isLogin,
        screen
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {};
};

export default connect(mapStatesToProps, mapDispatchToProps)(RankBronzeScreen);
