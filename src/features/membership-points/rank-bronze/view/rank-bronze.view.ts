import { RankBronze } from "../model/rank-bronze";

export interface RankBronzeView {
     onFetchDataSuccess(rankBronze: RankBronze): void;
     onFetchDataFail(error?: any): void;
 }