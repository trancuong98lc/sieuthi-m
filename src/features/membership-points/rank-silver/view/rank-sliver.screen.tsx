import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import Buttons from 'libraries/button/buttons';
import { goBack, navigate } from 'routing/service-navigation';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import { DimensionHelper } from 'helpers/dimension-helper';
import { RankSliverView } from './rank-sliver.view';
import { RankSliver } from '../model/rank-sliver';
import { styles } from './rank-sliver.style';
import { RankBronzeScreen, RankGoldScreen, PersonalLoginScreen } from 'routing/screen-name';
import Container from 'libraries/main/container';
import FastImage from 'react-native-fast-image';

export interface Props {}

export default class RankSliverScreen extends React.PureComponent<Props, any>
    implements RankSliverView {
    constructor(props: Props) {
        super(props);
    }
    onFetchDataSuccess(rankSliver: RankSliver): void {
        throw new Error('Method not implemented.');
    }
    onFetchDataFail(error?: any): void {
        throw new Error('Method not implemented.');
    }

    StatusBar = () => {
        let a = 1000;
        let b = 3000;
        if (a / b > 0 && a / b <= 0.5) {
            return (
                ((DimensionHelper.getScreenWidth() * 0.76 - 60) * a) / b + 15
            );
        }
        if (a / b > 0.5 && a / b <= 1) {
            return (
                ((DimensionHelper.getScreenWidth() * 0.76 - 60) * a) / b + 45
            );
        }
    };
    public render() {
        return (
            <Container statusBarColor={R.colors.primaryColor}>
                <View style={styles.container}>
                <View style={styles.header}>
                    <Buttons
                        onPress={ ()=>navigate(PersonalLoginScreen) }
                        source={R.images.ic_back2}
                        styleButton={styles.buttonBack}
                        styleImageButton={styles.imageButtonBack}
                    />
                    <DailyMartText style={styles.new}>
                        {translate('membershipPoints.header')}
                    </DailyMartText>
                </View>
                <View style={styles.viewMemberPoint}>
                    <FastImage
                        style={styles.imagePersonal}
                        source={R.images.groundMembershipPoint}
                    />
                    <DailyMartText style={styles.point}>
                        {' '}
                        1000 {translate('personalLogin.point')}
                    </DailyMartText>
                    <DailyMartText style={styles.rank}>
                        {translate('personalLogin.member')}
                        {translate('personalLogin.rank')}
                    </DailyMartText>
                    <View style={styles.viewAvata}>
                        <FastImage
                            source={R.images.avataImage}
                            style={styles.avataImage}
                        />
                    </View>
                </View>
                <View style={styles.infoEarnPoints}>
                    <DailyMartText style={styles.textAccumulated}>
                        {translate('membershipPoints.moneyAccumulated')}
                        <DailyMartText style={styles.moneyAccumulated}>
                            {'     1.000.000 đ'}
                        </DailyMartText>
                    </DailyMartText>
                    <View style={styles.statusBar}>
                        <View style={styles.viewStatusBar1}></View>
                        <View
                            style={[
                                styles.viewStatusBar2,
                                {
                                    width: this.StatusBar()
                                }
                            ]}
                        ></View>
                        <View style={styles.rankIcon}>
                            <FastImage
                                source={R.images.ic_rankCu}
                                style={styles.icRankCu}
                            />
                            <FastImage
                                source={R.images.ic_rankAg}
                                style={styles.icRankAg}
                            />
                            <FastImage
                                source={R.images.ic_rankAu}
                                style={styles.icRankAu}
                            />
                        </View>
                        <View style={styles.rankText}>
                            <DailyMartText style={styles.textRankCu}>
                                {translate('membershipPoints.memberBronze')}{' '}
                            </DailyMartText>
                            <DailyMartText style={styles.textRankAg}>
                                {translate('membershipPoints.memberSliver')}
                            </DailyMartText>
                            <DailyMartText style={styles.textRankAu}>
                                {translate('membershipPoints.memberGold')}{' '}
                            </DailyMartText>
                        </View>
                    </View>

                    <DailyMartText style={styles.textSpend}>
                        {translate('membershipPoints.youSpend')}
                        <DailyMartText style={styles.moneySpend}>
                            {' '}
                            500.000 đ{' '}
                        </DailyMartText>
                        {translate('membershipPoints.become')}{' '}
                        <DailyMartText style={styles.moneySpend}>
                            {translate('membershipPoints.memberSliver')}
                        </DailyMartText>
                    </DailyMartText>
                </View>
                <View style={styles.viewEndowPolicy}>
                    <View style={styles.hrEndowPolicy}></View>
                    <DailyMartText style={styles.textEndowPolicy}>
                        {translate('membershipPoints.endowPolicy')}
                    </DailyMartText>
                    <View style={styles.hrEndowPolicy}></View>
                </View>
                <View style={styles.endowDetail}>
                    <View style={styles.endowDetailHead}>
                        <TouchableOpacity
                            onPress={() => navigate(RankBronzeScreen)}
                        >
                            <View style={styles.itemNotClick}>
                                <FastImage
                                    source={R.images.ic_rankCu}
                                    style={styles.itemIc}
                                />
                                <DailyMartText style={styles.itemText}>
                                    {' '}
                                    {translate('membershipPoints.memberBronze')}
                                </DailyMartText>
                            </View>
                        </TouchableOpacity>

                        <View style={styles.itemClick}>
                            <FastImage
                                source={R.images.ic_rankAg}
                                style={styles.itemIc}
                            />
                            <DailyMartText style={styles.itemText}>
                                {translate('membershipPoints.memberSliver')}
                            </DailyMartText>
                        </View>
                        <TouchableOpacity
                        onPress={() => navigate(RankGoldScreen)}>
                            <View style={styles.itemNotClick}>
                                <FastImage
                                    source={R.images.ic_rankAu}
                                    style={styles.itemIc}
                                />
                                <DailyMartText style={styles.itemText}>
                                    {translate('membershipPoints.memberGold')}
                                </DailyMartText>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.endowDetailContent}>
                        <DailyMartText style={styles.textEndowDetailContent}>
                            {translate('membershipPoints.endowRankAg1')}
                        </DailyMartText>
                        <DailyMartText style={styles.textEndowDetailContent}>
                            {translate('membershipPoints.endowRankAg2')}
                        </DailyMartText>
                        <DailyMartText style={styles.textEndowDetailContent}>
                            {translate('membershipPoints.endowRankAg3')}
                        </DailyMartText>
                    </View>
                </View>
            </View>
            </Container>
        );
    }
}
