import { RankSliver } from "../model/rank-sliver";


export interface RankSliverView {
     onFetchDataSuccess(rankSliver: RankSliver): void;
     onFetchDataFail(error?: any): void;
 }