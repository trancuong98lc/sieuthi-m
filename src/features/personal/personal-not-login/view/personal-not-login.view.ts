import PersonalNotLogin from "./personal-not-login.screen";

export interface PersonalNotLoginView {
     onFetchDataSuccess(personalNotLogin: PersonalNotLogin): void;
     onFetchDataFail(error?: any): void;
 }