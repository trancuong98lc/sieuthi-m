import Buttons from 'libraries/button/buttons';
import * as React from 'react';
import { Image, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import {
  ContactScreen,
  LoginScreen,
  QuestionScreen,
  RegistrationScreen,
  RuleScreen
} from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { PersonalNotLogin } from '../model/personal-not-login';
import { styles } from './personal-not-login.style';
import { PersonalNotLoginView } from './personal-not-login.view';
import FastImage from 'react-native-fast-image';

export interface Props {}

export default class PersonalNotLoginScreen
  extends React.PureComponent<Props, any>
  implements PersonalNotLoginView {
  constructor(props: Props) {
    super(props);
  }
  onFetchDataSuccess(personalNotLogin: PersonalNotLogin): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  public render() {
    return (
      <View style={styles.container}>
        <FastImage
          source={R.images.groundHeaderPersonal}
          style={styles.groundHeader}
        ></FastImage>
        

        <View style={styles.content}>
          <View style={styles.item}>
            <Buttons
              source={R.images.ic_myOrder}
              textButton={translate('personalNotLogin.myOrder')}
              styleButton={styles.button}
              styleImageButton={styles.image}
              styleTextButton={styles.textButton}
            />
            <Buttons
              source={R.images.ic_nextPerson}
              styleButton={styles.buttonNextPerson}
              styleImageButton={styles.imageNextPerson}
            />
          </View>
          <View style={styles.hr}></View>

          <View style={styles.item}>
            <Buttons
              onPress={() => navigate(QuestionScreen)}
              source={R.images.ic_question}
              textButton={translate('personalNotLogin.question')}
              styleButton={styles.button}
              styleImageButton={styles.image}
              styleTextButton={styles.textButton}
            />
            <Buttons
              source={R.images.ic_nextPerson}
              styleButton={styles.buttonNextPerson}
              styleImageButton={styles.imageNextPerson}
            />
          </View>
          <View style={styles.hr}></View>

          <View style={styles.item}>
            <Buttons
              onPress={() => navigate(ContactScreen)}
              source={R.images.ic_contact}
              textButton={translate('personalNotLogin.contact')}
              styleButton={styles.button}
              styleImageButton={styles.image}
              styleTextButton={styles.textButton}
            />
            <Buttons
              source={R.images.ic_nextPerson}
              styleButton={styles.buttonNextPerson}
              styleImageButton={styles.imageNextPerson}
            />
          </View>
          <View style={styles.hr}></View>

          <View style={styles.item}>
            <Buttons
              source={R.images.ic_addrDelivery}
              textButton={translate('personalNotLogin.addr')}
              styleButton={styles.button}
              styleImageButton={styles.image}
              styleTextButton={styles.textButton}
            />
            <Buttons
              source={R.images.ic_nextPerson}
              styleButton={styles.buttonNextPerson}
              styleImageButton={styles.imageNextPerson}
            />
          </View>
          <View style={styles.hr}></View>

          <View style={styles.item}>
            <Buttons
              onPress={() => navigate(RuleScreen)}
              source={R.images.ic_rule}
              textButton={translate('personalNotLogin.rule')}
              styleButton={styles.button}
              styleImageButton={styles.image}
              styleTextButton={styles.textButton}
            />
            <Buttons
              source={R.images.ic_nextPerson}
              styleButton={styles.buttonNextPerson}
              styleImageButton={styles.imageNextPerson}
            />
          </View>
          <View style={styles.hr}></View>
        </View>
      </View>
    );
  }
}
