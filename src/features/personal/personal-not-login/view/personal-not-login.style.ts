import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FAFAFA',
    flex: 1
  },
  groundHeader: {
    width: DimensionHelper.getScreenWidth(),
    height: DimensionHelper.getScreenWidth() * 0.403
  },
  header: {
    flexDirection: 'row'
  },
  viewLogo: {
    width: 94,
    height: 94,
    borderRadius: 94,
    backgroundColor: R.colors.white100,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -DimensionHelper.getScreenWidth() * 0.12,
    marginLeft: DimensionHelper.getScreenWidth() * 0.04
  },
  logo: {
    width: 82,
    height: 28
  },
  login: {
    width: DimensionHelper.getScreenWidth() * 0.29,
    height: DimensionHelper.getScreenWidth() * 0.11,
    marginTop: -DimensionHelper.getScreenWidth() * 0.055,
    marginLeft: DimensionHelper.getScreenWidth() * 0.06
  },
  textLogin: {
    fontFamily: R.fonts.medium,
    fontSize: 13,
    color: R.colors.primaryColor
  },
  registration: {
    width: DimensionHelper.getScreenWidth() * 0.29,
    height: DimensionHelper.getScreenWidth() * 0.11,
    marginTop: -DimensionHelper.getScreenWidth() * 0.055,
    marginLeft: DimensionHelper.getScreenWidth() * 0.02
  },
  textregistration: {
    fontFamily: R.fonts.medium,
    fontSize: 13,
    color: R.colors.primaryColor
  },
  content: {
    marginTop: DimensionHelper.getScreenHeight() * 0.07
  },
  item: {
    flexDirection: 'row'
  },
  button: {
    // backgroundColor: '#FAFAFA',
    width: 300,
    marginTop: 15
  },
  image: {
    height: 27,
    width: 27,
    marginLeft: -250
  },

  textButton: {
    width: 200,
    marginLeft: -190,
    fontFamily: R.fonts.medium,
    fontSize: 15,
    color: '#333333'
  },
  buttonNextPerson: {
    //     backgroundColor: '#FAFAFA',
    marginLeft: DimensionHelper.getScreenWidth() * 0.18
  },
  imageNextPerson: {
    width: 8,
    height: 14,
    marginTop: 15
  },
  hr: {
    borderTopWidth: 1,
    borderColor: '#E5E5E5',
    marginTop: 15,
    marginLeft: 60
  }
});
