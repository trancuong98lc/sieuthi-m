import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  groundHeader: {
    width: DimensionHelper.getScreenWidth(),
    height: DimensionHelper.getScreenWidth() * 0.403,
    position: 'absolute'
  },
  headerPersonal: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 10
  },
  viewAvata: {
    width: 57,
    height: 57,
    backgroundColor: R.colors.white100,
    borderRadius: 57,
    alignItems: 'center',
    justifyContent: 'center'
  },
  avataImage: {
    height: 55,
    width: 55
  },
  namePersonal: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: R.colors.white100,
    marginLeft: 10,
    marginTop: 5
  },
  addrHeaderPersonal: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 5,
    alignItems: 'center'
  },
  icaddrHeaderPersonal: {
    width: 11,
    height: 12
  },
  textaddrHeaderPersonal: {
    fontSize: 12,
    color: R.colors.white100,
    marginLeft: 10
  },
  textLogin: {
    fontFamily: R.fonts.medium,
    fontSize: 13,
    color: R.colors.primaryColor
  },
  imagePersonal: {
    width: DimensionHelper.getScreenWidth() * 0.92,
    height: DimensionHelper.getScreenWidth() * 0.176,
    marginTop: DimensionHelper.getScreenHeight() * 0.021,
    marginLeft: DimensionHelper.getScreenWidth() * 0.04,
    borderRadius: 5
  },
  rank: {
    marginTop: -DimensionHelper.getScreenWidth() * 0.088,
    marginLeft: DimensionHelper.getScreenWidth() * 0.08,
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: '#78674F'
  },
  point: {
    marginTop: -DimensionHelper.getScreenWidth() * 0.088,
    marginLeft: DimensionHelper.getScreenWidth() * 0.08,
    fontSize: 13,
    color: '#78674F'
  },
  content: {
    marginTop: DimensionHelper.getScreenHeight() * 0.12
  },
  item: {
    flexDirection: 'row'
  },
  button: {
    // backgroundColor: '#FAFAFA',
    width: 300,
    marginTop: 15
  },
  image: {
    height: 27,
    width: 27,
    marginLeft: -250
  },

  textButton: {
    width: 200,
    marginLeft: -190,
    fontFamily: R.fonts.medium,
    fontSize: 15,
    color: '#333333'
  },
  buttonNextPerson: {
    // backgroundColor: '#FAFAFA',
    marginLeft: DimensionHelper.getScreenWidth() * 0.18
  },
  imageNextPerson: {
    width: 8,
    height: 14,
    marginTop: 15
  },
  hr: {
    borderTopWidth: 1,
    borderColor: '#E5E5E5',
    marginLeft: 60
  }
});
