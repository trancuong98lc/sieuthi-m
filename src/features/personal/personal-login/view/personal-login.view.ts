import { PersonalLogin } from "../model/personal-login";

export interface PersonalLoginView {
     onFetchDataSuccess(personalLogin: PersonalLogin): void;
     onFetchDataFail(error?: any): void;
 }