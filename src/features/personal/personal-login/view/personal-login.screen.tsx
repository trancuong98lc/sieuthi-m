import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { Image, ScrollView, TouchableOpacity, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import {
  AccountInforScreen,
  AddAddrPayScreen,
  AddrPayScreen,
  ContactScreen,
  QuestionScreen,
  RankBronzeScreen,
  ReceivedScreen,
  RuleScreen,
  LoginScreen
} from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { PersonalLogin } from '../model/personal-login';
import { styles } from './personal-login.style';
import { PersonalLoginView } from './personal-login.view';
import { connect } from 'react-redux';
import complete_registrationScreen from 'features/authen/complete_registration/view/complete_registration.screen';
import HeaderPersonal from '../model/components/HeaderPersonal';
import RankUser from '../model/components/RankUser';
import { User } from 'types/user-type';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import { onUpdateUserInfo, updateCheckLogin } from 'redux/actions';
import CodePushVerion from 'helpers/code-push-ver';
import BtnPersonal from '../model/components/BtnPersonal';
import { LoginState } from 'redux/reducers/islogin-reducer';
import { async } from 'rxjs';
import AsyncStorageHelpers from 'helpers/AsyncStorageHelpers';
import Commons from 'helpers/Commons';
import { ALERT_TYPE, showAlert, showAlertByType } from 'libraries/BaseAlert';

export interface Props {
  user: User;
  isLogin: boolean
  onUpdateUser: (data: User) => void;
  updateCheckLogin: (data: LoginState) => void;
}

class PersonalLoginScreen extends React.PureComponent<Props, any> {
  constructor(props: Props) {
    super(props);
  }

  componentDidMount = async () => {
    try {
      const { user } = this.props;
      if (user && user._id) {
        const res = await ApiHelper.fetch(URL_API.PROFILE + user._id, null, true);
        if (res.status === STATUS.SUCCESS && res.user) {
          this.props.onUpdateUser(res.user!);
        }
      }
    } catch (error) { }
  };

  navigateAddrDelivery = () => {
    const { user } = this.props;
    const isLogin = Commons.idToken
    if (!isLogin) return showAlertByType({
      title: translate('notify.header'),
      type: ALERT_TYPE.CONFIRM,
      message: translate('want_login'),
      callBackConfirm: () => {
        navigate(LoginScreen)
      }
    })
    navigate(AddrPayScreen, { screen: 'MENU' })
  }

  onCheckLogin = (data: LoginState) => {
    this.props.updateCheckLogin(data)
    navigate(data.screen)
  }

  navigateToAuth = () => {
    const isLogin = Commons.idToken
    if (!isLogin) return showAlertByType({
      title: translate('notify.header'),
      type: ALERT_TYPE.CONFIRM,
      message: translate('want_login'),
      callBackConfirm: () => {
        navigate(LoginScreen)
      }
    })
    navigate(ReceivedScreen, { screen: 'menu' })
  }


  public render() {
    const { user, isLogin } = this.props;
    return (
      <Container statusBarColor={R.colors.primaryColor} style={styles.container}>
        <HeaderPersonal user={user} onCheckLogin={this.onCheckLogin} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.content}>
            <BtnPersonal
              nameIcon="ic_myOrder"
              title="personalNotLogin.myOrder"
              onPress={this.navigateToAuth}
            />
            <View style={styles.hr} />
            <BtnPersonal
              nameIcon="ic_question"
              title="personalNotLogin.question"
              onPress={() => navigate(QuestionScreen)}
            />

            <View style={styles.hr} />
            <BtnPersonal
              nameIcon="ic_contact"
              title="personalNotLogin.contact"
              onPress={() => navigate(ContactScreen)}
            />
            <View style={styles.hr} />
            <BtnPersonal
              nameIcon="ic_addrDelivery"
              title="personalNotLogin.addr"
              onPress={this.navigateAddrDelivery}
            />
            <View style={styles.hr} />
            <BtnPersonal
              nameIcon="ic_rule"
              title="personalNotLogin.rule"
              onPress={() => navigate(RuleScreen)}
            />
            <View style={styles.hr} />
            <CodePushVerion />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  const { isLogin, screen } = state.isLogin;
  return {
    user, isLogin, screen
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onUpdateUser: (data: User) => dispatch(onUpdateUserInfo(data)),
    updateCheckLogin: (data: LoginState) => dispatch((updateCheckLogin(data)))
  };
};

export default connect(mapStatesToProps, mapDispatchToProps)(PersonalLoginScreen);
