import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import { translate } from 'res/languages';

export interface BtnPersonalProps {
    onPress: any, nameIcon: string, title: string
}

export default class BtnPersonal extends React.PureComponent<BtnPersonalProps, any> {
    constructor(props: BtnPersonalProps) {
        super(props);
    }

    public render() {
        const { onPress, nameIcon, title } = this.props
        return (
            <TouchableOpacity
                style={[styles.container]}
                onPress={onPress}
                activeOpacity={0.7}
            >
                <BaseIcon name={nameIcon} width={28} />
                <DailyMartText fontStyle="normal" style={styles.text}>{translate(title)}</DailyMartText>
                <BaseIcon name="ic_nextPerson" width={15} />

            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: 40,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 15,
        marginVertical: 10
    },
    text: {
        flex: 1,
        marginLeft: 15,
        fontSize: 16,
        color: 'black'
    }
});