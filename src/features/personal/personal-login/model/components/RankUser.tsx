import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import { User } from 'types/user-type';
import { _formatPrice } from 'helpers/helper-funtion';
import { navigate } from 'routing/service-navigation';
import { RankBronzeScreen } from 'routing/screen-name';

export interface RankUserProps {
  user: User;
}

export default class RankUser extends React.PureComponent<RankUserProps, any> {
  constructor(props: RankUserProps) {
    super(props);
  }

  gotoTankUser = () => {
    navigate(RankBronzeScreen);
  };

  public render() {
    const { user } = this.props;
    return (
      <TouchableOpacity onPress={this.gotoTankUser} style={styles.container}>
        <FastImage
          style={styles.imagePersonal}
          source={R.images.imagePersonal}
        />
        <View style={styles.viewInimage}>
          <DailyMartText style={styles.rank}>
            {user.pointRanking
              ? user.pointRanking.name.toUpperCase()
              : translate('membershipPoints.noRank').toUpperCase()}
          </DailyMartText>
          <DailyMartText style={styles.point}>
            {translate('personalLogin.youHave')} {_formatPrice(user.point || 0)}
            {translate('personalLogin.point')}
          </DailyMartText>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: DimensionHelper.getScreenHeight() * 0.021,
    width: DimensionHelper.getScreenWidth() - 24,
    height: DimensionHelper.getScreenWidth() * 0.176
  },
  viewInimage: {
    position: 'absolute',
    zIndex: 55,
    top: 0,
    width: '100%',
    height: '100%',
    paddingHorizontal: 12,
    justifyContent: 'center'
  },
  imagePersonal: {
    width: '100%',
    height: '100%',
    borderRadius: 5
  },

  rank: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: '#78674F'
  },

  point: {
    fontSize: 13,
    color: '#78674F'
  }
});
