import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import DailyAvatar from 'libraries/Avatar/DailyMartAvatar';
import images from 'res/images';
import colors from 'res/colors';
import RankUser from './RankUser';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import Buttons from 'libraries/button/buttons';
import { navigate } from 'routing/service-navigation';
import {
  LoginScreen,
  RegistrationScreen,
  AccountInforScreen
} from 'routing/screen-name';
import { User } from 'types/user-type';
import { getPhotoUrl } from 'helpers/helper-funtion';
import Commons from 'helpers/Commons';
import { LoginState } from 'redux/reducers/islogin-reducer';

export interface HeaderPersonalProps {
  user: User;
  onCheckLogin: (data: LoginState) => void
}

export default class HeaderPersonal extends React.PureComponent<
  HeaderPersonalProps,
  any
  > {
  constructor(props: HeaderPersonalProps) {
    super(props);
  }

  gotoAuth = (screen: string) => () => {
    this.props.onCheckLogin({ isLogin: true, screen: screen })
  }

  renderNotLogin = () => {
    return (
      <View style={styles.header}>
        <View style={styles.viewLogo}>
          <FastImage source={R.images.logoperson} style={styles.logo} />
        </View>
        <Buttons
          styleButton={styles.login}
          textButton={translate('button.login')}
          styleTextButton={styles.textLogin}
          onPress={this.gotoAuth(LoginScreen)}
        />
        <Buttons
          styleButton={styles.registration}
          textButton={translate('button.registration')}
          styleTextButton={styles.textregistration}
          onPress={this.gotoAuth(RegistrationScreen)}
        />
      </View>
    );
  };

  gotoProfile = () => {
    navigate(AccountInforScreen);
  };

  renderLogin = () => {
    const { user } = this.props;
    return (
      <View style={styles.headerPersonal}>
        <TouchableOpacity
          style={styles.headerName}
          onPress={this.gotoProfile}
          activeOpacity={0.9}
        >
          <DailyAvatar url={getPhotoUrl(user)} size={80} />
          <View style={styles.viewCenter}>
            <DailyMartText style={styles.namePersonal} numberOfLines={2}>
              {user && user.name && user.name.trim() || ""}
            </DailyMartText>
            {user && user.location ? <View style={styles.addrHeaderPersonal}>
              <BaseIcon name="ic_addrHeaderPersonal" width={15} />
              <DailyMartText style={styles.textaddrHeaderPersonal}>
                {user.location.trim() || ""}
              </DailyMartText>
            </View> : null}
          </View>
          <BaseIcon name="ic_next" width={15} />
        </TouchableOpacity>
        <RankUser user={user} />
      </View >
    );
  };

  public render() {
    const { user } = this.props;
    const isLogin = user && user._id ? true : false;
    return (
      <View style={styles.container}>
        <FastImage
          source={R.images.groundHeaderPersonal}
          style={[styles.groundHeader, {height: isLogin ? 209 : 169}]}
        />
        {isLogin ? this.renderLogin() : this.renderNotLogin()}
        {isLogin && <View style={{width: '100%', height: 40, position: "absolute", zIndex: 1, backgroundColor: R.colors.white100, bottom: 0}} />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  groundHeader: {
    width: DimensionHelper.getScreenWidth(),
    height: 169
  },
  addrHeaderPersonal: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 5,
    alignItems: 'center'
  },
  icaddrHeaderPersonal: {
    width: 11,
    height: 12
  },
  textaddrHeaderPersonal: {
    fontSize: 12,
    color: R.colors.white100,
    marginLeft: 10
  },
  container: {
    justifyContent: 'center'
  },
  namePersonal: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: R.colors.white100,
    marginLeft: 10,
    marginTop: 5
  },
  headerPersonal: {
    marginHorizontal: 12,
    flexDirection: 'column',
    justifyContent: 'center',
    position: 'absolute',
    top: DimensionHelper.getScreenWidth() * 0.08,
    zIndex: 2
  },
  headerName: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  viewCenter: {
    flex: 1
  },
  login: {
    width: DimensionHelper.getScreenWidth() * 0.29,
    height: DimensionHelper.getScreenWidth() * 0.11,
    marginTop: -DimensionHelper.getScreenWidth() * 0.055,
    marginLeft: DimensionHelper.getScreenWidth() * 0.06,
    shadowOpacity: 0.1,
    elevation: 1,
    shadowColor: 'grey',
    shadowOffset: {
      width: 2,
      height: 5
    }
  },
  textLogin: {
    fontFamily: R.fonts.medium,
    fontSize: 13,
    color: R.colors.primaryColor
  },
  header: {
    flexDirection: 'row'
  },
  viewLogo: {
    width: 94,
    height: 94,
    borderRadius: 94,
    backgroundColor: R.colors.white100,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -DimensionHelper.getScreenWidth() * 0.12,
    marginLeft: DimensionHelper.getScreenWidth() * 0.04,
    shadowOpacity: 0.1,
    elevation: 1,
    shadowColor: 'grey',
    shadowOffset: {
      width: 2,
      height: 5
    }
  },
  logo: {
    width: 82,
    height: 28
  },
  registration: {
    width: DimensionHelper.getScreenWidth() * 0.29,
    height: DimensionHelper.getScreenWidth() * 0.11,
    marginTop: -DimensionHelper.getScreenWidth() * 0.055,
    marginLeft: DimensionHelper.getScreenWidth() * 0.02,
    shadowOpacity: 0.1,
    elevation: 1,
    shadowColor: 'grey',
    shadowOffset: {
      width: 2,
      height: 5
    }
  },
  textregistration: {
    fontFamily: R.fonts.medium,
    fontSize: 13,
    color: R.colors.primaryColor
  }
});
