import { DimensionHelper } from 'helpers/dimension-helper';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'res/R';

export interface InputProfileProps {
  value: string;
  source: any;
  placeholder: string;
  editable: boolean;
  onPress?: any;
}
interface state {
  value: string;
}

export default class InputProfile extends React.PureComponent<
  InputProfileProps,
  state
> {
  constructor(props: InputProfileProps) {
    super(props);
    this.state = {
      value: props.value || ''
    };
  }

  textInputCom = React.createRef<TextInputs>();

  onChangeText = (value: string) => {
    this.setState({
      value
    });
  };

  public render() {
    const { source, placeholder, editable, onPress } = this.props;
    const { value } = this.state;
    return (
      <>
        <TouchableOpacity
          style={styles.item}
          activeOpacity={0.9}
          disabled={!onPress}
          onPress={onPress}
        >
          <BaseIcon name={source} width={20} iconStyle={styles.itemIcon} />
          <TextInputs
            ref={this.textInputCom}
            styleTextInputs={{ flex: 1 }}
            onTouchStart={onPress}
            editable={editable}
            styleTextInput={styles.text}
            placeholder={placeholder}
            value={onPress ? this.props.value : value}
            onChangeText={this.onChangeText}
          />
        </TouchableOpacity>
        <View style={styles.hr} />
      </>
    );
  }
}
const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    backgroundColor: R.colors.white100,
    height: 60,
    width: '100%',
    flex: 1,
    alignItems: 'center'
  },
  itemIcon: {
    marginHorizontal: 20
  },
  text: {
    fontFamily: R.fonts.medium,
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    fontSize: 15,
    color: '#686E7F'
  },
  hr: {
    borderTopWidth: 1,
    borderColor: '#F7F7F8',
    width: 2520
  }
});
