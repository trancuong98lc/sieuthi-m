import { City } from 'features/store/model/store-entity';
import ItemCityDistrict from 'features/store/view/components/store-item-district-city';
import ApiHelper from 'helpers/api-helper';
import { DimensionHelper } from 'helpers/dimension-helper';
import { URL_API } from 'helpers/url-api';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import * as React from 'react';
import _ from 'lodash'
import {
    FlatList,
    Platform,
    StyleSheet,
    TextInput,
    View
} from 'react-native';
import Config from 'react-native-config';
import { color } from 'react-native-reanimated';
import colors from 'res/colors';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import Axios from 'axios';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
export interface Props {
    route: any;
}
interface states {
    DataCity: any[];
    search: string
}

export default class SearchLocation extends React.PureComponent<Props, states> {

    constructor(props: Props) {
        super(props);
        this.state = {
            DataCity: this.props.route && this.props.route.params && this.props.route.params.setGender ? [{ _id: 1, name: "MALE" }, { _id: 2, name: "FEMALE" }, { _id: 2, name: "OTHER" }] : [],
            search: ''
        };
        // this.onChangetext = _.debounce(this.onChangetext)
    }

    componentDidMount = () => {
        const isGender = this.props.route && this.props.route.params && this.props.route.params.setGender ? true : false
        if (!isGender) {
            this.getCity('a');
        }
    };

    getCity = async (text: string) => {
        // Platform.select()
        const params = {
            q: "số 8 Phan Văn Trường",
            in: "countryCode: VNM",
            at: "21.02888,105.85464",
            lang: "vi-VN",
            apiKey: 'x-dm4cwU2RrEq9cdJmg5QNuzPvTLKJFKpChgtkIBQDo',
        };

        Axios.get('https://autosuggest.search.hereapi.com/v1/autosuggest?q="số 8 Phan văn trường,Dịch Vọng Hậu "&in=countryCode:VNM&at=21.02888,105.85464&lang=vi-VN&apiKey=x-dm4cwU2RrEq9cdJmg5QNuzPvTLKJFKpChgtkIBQDo&limit=40')
            .then(res => {
                console.log("SearchLocation -> getCity -> res", res)

            })
        // const res = await ApiHelper.fetchLocation(URL_API.AUTOSUGGES, params);
        // console.log("SearchLocation -> getCity -> res", res)
        // if (res.results) {
        //     this.setState({
        //         DataCity: res.results
        //     })
        // }
    };

    onPressItem = (item: City) => () => {
        const { onSetLocation, setGender } = this.props.route.params;
        const isGender = this.props.route && this.props.route.params && this.props.route.params.setGender ? true : false
        if (isGender) {
            goBack();
            setGender(item.name)
            return
        }
        onSetLocation && onSetLocation(item);
        goBack();
    };

    renderItem = ({ item, index }: any) => {
        const isGender = this.props.route && this.props.route.params && this.props.route.params.setGender ? true : false
        return (
            <ItemCityDistrict
                onPress={this.onPressItem(item)}
                isGender={isGender}
                item={item}
                index={index}
            />
        );
    };

    keyExtra = (item: City) => item._id;

    onEndReached = () => { };

    renderFooter = () => <View style={{ height: 200 }} />;

    onChangetext = (value: string) => {
        this.setState({
            search: value
        })
        this.getCity(value)
    }

    public render() {
        const isGender = this.props.route && this.props.route.params && this.props.route.params.setGender ? true : false
        return (
            <Container
                statusBarColor={R.colors.primaryColor}
                containerStyle={{ backgroundColor: 'white' }}
            >
                <HeaderService
                    title={isGender ? 'account.gender' : 'listStore.selectCity'}
                    leftIcon={'ic_close'}
                    sizeIcon={20}
                />
                {!isGender && <TextInput
                    style={styles.textInput}
                    value={this.state.search}
                    onChangeText={this.onChangetext}
                    placeholder="Nhập địa chỉ cần tìm" />}
                <View style={styles.flatlist}>
                    <FlatList
                        data={this.state.DataCity}
                        extraData={this.state}
                        keyExtractor={this.keyExtra}
                        scrollEventThrottle={0.5}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={() => {
                            return (
                                <EmptylistComponent />
                            );
                        }}
                    />
                </View>
            </Container>
        );
    }
}
const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        marginTop: DimensionHelper.getStatusBarHeight() + 20,
        marginLeft: 25
    },
    ic_close: {
        width: 20,
        height: 20
    },
    text: {
        fontSize: 20,
        marginLeft: 70
    },
    flatlist: {
        marginTop: 10
    },
    textInput: { height: 40, padding: 12, width: '90%', marginTop: 10, alignSelf: 'center', backgroundColor: colors.cyan50, borderRadius: 30 }
});
