/* 
  Created by dungnt at 08-10-2020 09:32:34
  Màn hình cá nhân app khách hàng
*/

import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { BaseResponse } from 'types/BaseResponse';
import { RequestUpdateUser } from './accoynt-type';

export default class AccountServices {
  private static instance: AccountServices;

  static getInstance(): AccountServices {
    if (!AccountServices.instance) {
      AccountServices.instance = new AccountServices();
    }
    return AccountServices.instance;
  }

  get(id: string): Promise<BaseResponse> {
    return ApiHelper.fetch(URL_API.PROFILE + id, null, true);
  }
  put(data: RequestUpdateUser): Promise<BaseResponse> {
    return ApiHelper.put(URL_API.UPDATE_PROFILE, data, true);
  }
}
