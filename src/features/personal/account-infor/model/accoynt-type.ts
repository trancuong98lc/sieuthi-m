export interface RequestUpdateUser {
  _id: string;
  name?: string;
  email?: string;
  location?: string;
  dateOfBirth?: string;
  gender: string;
}
