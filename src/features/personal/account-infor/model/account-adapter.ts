import AccountInforEdit from 'features/personal/account-infor/view/account-infor.screen';
import ImagePicker, { Image } from 'react-native-image-crop-picker';
/* 
Created by dungnt at 08-10-2020 09:32:34
Màn hình cá nhân app khách hàng
*/

import { BaseResponse, BaseResponseLogIn, STATUS } from 'types/BaseResponse';
import { showAlert } from 'libraries/BaseAlert';
import { translate } from 'res/languages';
import AccountServices from './account-services';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { RequestUpdateUser } from './accoynt-type';
import { goBack } from 'routing/service-navigation';
import { showToast } from 'libraries/BaseToast';
import { getFileSize } from 'helpers/helper-funtion';

export const MAX_SIZE_IMAGE = 5000000;
export class AccountAdapter {
  view: typeof AccountInforEdit;
  constructor(view: typeof AccountInforEdit) {
    this.view = view;
  }

  onChoosePhoto = async () => {
    await ImagePicker.openPicker({
      multiple: false,
      mediaType: 'photo',
      compressImageQuality: 1
    }).then(async (images: any) => {
      const fileSize = await getFileSize(images.path);
      if (fileSize > MAX_SIZE_IMAGE) {
        return showAlert('max_size_image');
      }
      this.onUploadImageCache([images], this.view.props.user._id);
    });
  };

  onTakePhoto = (): void => {
    ImagePicker.openCamera({}).then((images: any) => {
      this.onUploadImageCache([images], this.view.props.user._id);
    });
  };

  onUploadImageCache = async (images: Image[], _id?: string) => {
    try {
      showLoading();
      const data: any = new FormData();
      images.map((item: any, index: number) => {
        data.append('avatar', {
          uri: item.path,
          type: 'image/jpeg',
          name: 'album.jpg'
        });
        data.append('_id', _id);
      });
      const res = await ApiHelper.postForm(URL_API.UPDATE_AVATAR, data, true);
      if (res.status === STATUS.SUCCESS) {
        hideLoading();
        this.view.props.onUpdateUser(res.user);
      } else {
        hideLoading();
        showAlert(translate('warning.warning'));
      }
    } catch (error) {
      hideLoading();
      console.log('error: ', error);
    }
  };

  getProfile = (id: string) => {
    try {
      AccountServices.getInstance()
        .get(id)
        .then((result) => {
          if (result.status == STATUS.SUCCESS) {
            console.log('getProfile -> result', result);
            this.view.props.onUpdateUser(result.user);
          }
        })
        .catch((errr) => {
          console.log('getProfile -> res', errr);
        });
    } catch (error) {}
  };

  updateUser = (data: RequestUpdateUser) => {
    showLoading();
    AccountServices.getInstance()
      .put(data)
      .then(async (res) => {
        if (res.status == STATUS.SUCCESS) {
          this.view.props.onUpdateUser(res.user);
          this.view.UpdateProfile();
          await hideLoading();
          setTimeout(() => {
            showAlert('account.update_susscess', { success: true });
            goBack();
          }, 300);
        } else {
          hideLoading();
          setTimeout(() => {
            showAlert(translate(`status.${res.status}`));
            goBack();
          }, 300);
        }
      })
      .catch((err) => {
        hideLoading();
        setTimeout(() => {
          showAlert(translate(`${err}`));
          goBack();
        }, 300);
        console.log('updateUser -> err', err);
      });
  };
}
