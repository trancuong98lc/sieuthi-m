import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import {
  getPhotoUrl,
  validateEmail,
  validateEmoji,
  validateName
} from 'helpers/helper-funtion';
import DailyAvatar from 'libraries/Avatar/DailyMartAvatar';
import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import { DateTimePicker } from 'libraries/Time/DateTimePicker';
import * as React from 'react';
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity,
  View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { onLogout, onUpdateUserInfo, updateMessageCount } from 'redux/actions';
import { translate } from 'res/languages';
import R from 'res/R';
import OneSignal from 'react-native-onesignal';
import {
  HomeScreen,
  RepasswordScreen,
  SearchLocation
} from 'routing/screen-name';
import { goBack, navigate, resetStack } from 'routing/service-navigation';
import { GENDER_NAME, User } from 'types/user-type';
import { AccountAdapter } from '../model/account-adapter';
import InputProfile from '../model/components/InputProfile';
import { styles } from './account-infor.style';
import moment from 'moment';
import { RequestUpdateUser } from '../model/accoynt-type';
import { ALERT_TYPE, showAlert, showAlertByType } from 'libraries/BaseAlert';
import { NAME_LENGTH } from 'global/constants';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export interface Props {
  user: User;
  onUpdateUser: (data?: User) => void;
  onLogout: (data?: any) => void;
  updateMessageCount: (messageCount: number | undefined) => void;
}

class AccountInforEdit extends React.PureComponent<Props, any> {
  adapter: AccountAdapter;

  refName = React.createRef<InputProfile>();
  refEmail = React.createRef<InputProfile>();
  refGender = React.createRef<InputProfile>();
  refLocation = React.createRef<InputProfile>();

  constructor(props: Props) {
    super(props);
    this.adapter = new AccountAdapter(this);
    this.state = {
      isVisible: false,
      time: new Date(),
      adress: null,
      gender: props.user.gender || '',
      isEdit: false,
      dateOfBirth: props.user.dateOfBirth || ''
    };
  }

  open = () => {
    if (!this.state.isEdit) return;
    this.setState({
      isVisible: true
    });
  };

  hide = () => {
    this.setState({
      isVisible: false
    });
  };

  componentDidMount = () => {
    const { user } = this.props;
    this.adapter.getProfile(user._id);
  };

  componentWillUnmount = () => {
    Keyboard.dismiss();
  };

  resetUser = async () => {
    const token = await AsyncStorageHelpers.getObject(StorageKey.ID_TOKEN);
    if (token) {
      showAlertByType({
        title: translate('notify.header'),
        type: ALERT_TYPE.CONFIRM,
        message: translate('want_logout'),
        callBackConfirm: () => {
          OneSignal.deleteTag('user_id');
          this.props.updateMessageCount(undefined);
          this.props.onLogout({
            token: token
          });
        }
      });
    } else {
      resetStack(HomeScreen);
    }
  };

  onSetLocation = (adress: any) => {
    this.setState({
      adress
    });
  };

  setGender = (gender: string) => {
    this.setState({ gender });
  };

  gotoGender = () => {
    if (!this.state.isEdit) return;
    navigate(SearchLocation, { setGender: this.setGender });
  };

  onConfirm = (dateOfBirth: Date) => {
    this.setState(
      {
        dateOfBirth
      },
      () => {
        this.hide();
      }
    );
  };

  EditProfile = () => {
    this.setState({
      isEdit: true
    });
    this.refName &&
      this.refName.current &&
      this.refName.current.textInputCom.current!.enableFocus();
  };
  UpdateProfile = () => {
    this.setState({
      isEdit: false
    });
  };

  UpdateAvatar = () => {
    this.adapter.onChoosePhoto();
  };

  onUpdateUser = () => {
    if (!this.state.isEdit) {
      return this.EditProfile();
    }
    const { user } = this.props;
    const name = this.refName.current!.state.value;
    const email = this.refEmail.current!.state.value;
    const location = this.refLocation.current!.state.value;
    const checkDate =
      new Date(moment.utc(this.state.dateOfBirth).format()).getTime() >
      new Date(moment.utc().format()).getTime();
    if (name.length == 0) {
      return showAlert('account.name_blank');
    }
    if (name.trim().length < NAME_LENGTH.MIN || name.length == 0) {
      return showAlert('account.fullname_from_2character');
    }

    if (name.trim().length > NAME_LENGTH.MAX) {
      return showAlert('account.fullname_max');
    }

    if (validateName(name) || validateEmoji(name)) {
      return showAlert('account.fullname_valid');
    }
    if (email.length == 0 || email.trim() == '') {
      showAlert(translate('account.emply_email'));
      return;
    }
    if (!validateEmail(email)) {
      showAlert(translate('alert.emailNotValid'));
      return;
    }
    if (location.trim().length == 0 || name.length == 0) {
      return showAlert('account.location_not_blank');
    }
    if (checkDate) {
      showAlert(translate('account.birh_err'));
      return;
    }
    const body: RequestUpdateUser = {
      _id: user._id,
      gender: this.state.gender,
      location: location.trim()
    };
    if (name) body.name = this.refName.current!.state.value.trim();
    if (email) body.email = this.refEmail.current!.state.value.trim();
    if (this.state.dateOfBirth) body.dateOfBirth = this.state.dateOfBirth;
    this.adapter.updateUser(body);
  };

  public render() {
    const { user } = this.props;
    const { isVisible, time, isEdit, dateOfBirth } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.accountInfor}>
              <Buttons
                onPress={() => goBack()}
                source={R.images.ic_back2}
                styleButton={styles.accountInforButtonBack}
                styleImageButton={styles.accountInforImageButtonBack}
              />
              <DailyMartText style={styles.accountInforText}>
                {translate('personalLogin.accountInfor')}
              </DailyMartText>
              <Buttons
                styleButton={styles.accountInforButtonEdit}
                textButton={
                  isEdit ? translate('button.update') : translate('button.edit')
                }
                onPress={this.onUpdateUser}
                styleTextButton={styles.accountInforTextButtonEdit}
              />
            </View>
            <View style={styles.viewAvataImage}>
              <DailyAvatar url={getPhotoUrl(user)} size={87} />
              <Buttons
                styleButton={styles.accountInforButtonChang}
                textButton={translate('button.changeImage')}
                onPress={this.UpdateAvatar}
                styleTextButton={styles.accountInforTextButtonChang}
              />
            </View>
          </View>
          <KeyboardAwareScrollView enableOnAndroid enableAutomaticScroll>
            <DailyMartText style={styles.inforAccount}>
              {translate('personalLogin.accountInfor')}
            </DailyMartText>
            <InputProfile
              editable={isEdit}
              ref={this.refName}
              placeholder="Tên"
              source={'ic_personalName'}
              value={(user && user.name && user.name.trim()) || ''}
            />
            <InputProfile
              editable={false}
              placeholder="Số điện thoại"
              source={'ic_personalPhone'}
              value={(user && user.phoneNumber && user.phoneNumber) || ''}
            />
            <InputProfile
              editable={isEdit}
              ref={this.refEmail}
              placeholder="Email"
              source={'ic_personalEmail'}
              value={user && user.email && user.email.trim()}
            />
            <View style={styles.HR} />
            <InputProfile
              editable={false}
              onPress={this.open}
              placeholder="Ngày sinh"
              source={'ic_personalBirth'}
              value={
                dateOfBirth ? moment(dateOfBirth).format('DD/MM/YYYY') : ''
              }
            />
            <InputProfile
              editable={false}
              onPress={this.gotoGender}
              ref={this.refGender}
              placeholder="Giới tính"
              source={'ic_personalgender'}
              value={
                user && translate(GENDER_NAME[this.state.gender || user.gender])
              }
            />
            <InputProfile
              editable={isEdit}
              ref={this.refLocation}
              placeholder="Địa chỉ"
              source={'ic_personalAddr'}
              value={(user && user.location && user.location.trim()) || ''}
            />

            <View style={styles.HR}></View>

            <View style={styles.item}>
              <FastImage
                style={styles.itemIconRePass}
                source={R.images.ic_personalRePass}
              />
              <DailyMartText
                onPress={() => navigate(RepasswordScreen)}
                style={styles.text}
              >
                {translate('personalLogin.rePass')}
              </DailyMartText>
            </View>

            <Buttons
              onPress={this.resetUser}
              styleButton={styles.buttonLogOut}
              textButton={translate('button.logout')}
              styleTextButton={styles.textButtonLogOut}
            />
          </KeyboardAwareScrollView>
        </View>
        <DateTimePicker
          headerTextIOS={translate('dateTimePicker.title')}
          titleIOS={translate('dateTimePicker.title')}
          confirmTextIOS={translate('dateTimePicker.ok')}
          cancelTextIOS={translate('dateTimePicker.cancel')}
          isVisible={isVisible}
          onConfirm={this.onConfirm}
          onCancel={this.hide}
          date={time}
          // maximumDate={timeEnd}
          mode="date"
        />
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  return {
    user
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    onLogout: (data?: User) => dispatch(onLogout(data)),
    onUpdateUser: (data: User) => dispatch(onUpdateUserInfo(data)),
    updateMessageCount: (messageCount: number) =>
      dispatch(updateMessageCount(messageCount))
  };
};

export default connect(
  mapStatesToProps,
  mapDispatchToProps,
  null,
  AccountInforEdit
)(AccountInforEdit);
