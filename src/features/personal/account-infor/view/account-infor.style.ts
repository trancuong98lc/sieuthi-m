import { StyleSheet } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import { MaterialIndicator } from 'react-native-indicators';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F7F7F8',
    flex: 1,
    marginBottom: -DimensionHelper.getBottomSpace()
  },
  header: {
    backgroundColor: R.colors.primaryColor,
    paddingBottom: 10
    // height: DimensionHelper.getScreenHeight() * 0.227
  },
  accountInfor: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  accountInforButtonBack: {
    backgroundColor: R.colors.primaryColor,
    marginLeft: 10
  },
  accountInforImageButtonBack: {
    width: 35,
    height: 35
  },
  accountInforText: {
    fontFamily: R.fonts.bold,
    fontSize: 18,
    color: R.colors.white100,
    textAlign: 'center',
    flex: 1
  },
  accountInforButtonEdit: {
    backgroundColor: R.colors.primaryColor,
    marginRight: 20
  },
  accountInforTextButtonEdit: {
    fontSize: 15,
    color: R.colors.white100
  },
  viewAvataImage: {
    // backgroundColor: R.colors.white100,
    justifyContent: 'center',
    alignItems: 'center',
    // marginLeft: DimensionHelper.getScreenWidth() * 0.38,
    marginRight: 12
    // marginTop: DimensionHelper.getScreenHeight() * 0.02
  },
  avataImage: {
    width: 85,
    height: 85
  },
  accountInforButtonChang: {
    backgroundColor: R.colors.primaryColor,
    marginVertical: DimensionHelper.getScreenHeight() * 0.01
  },
  accountInforTextButtonChang: {
    fontSize: 13,
    color: R.colors.white100
  },
  inforAccount: {
    fontFamily: R.fonts.medium,
    fontSize: 15,
    color: '#333333',
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 30
  },
  item: {
    flexDirection: 'row',
    backgroundColor: R.colors.white100,
    height: DimensionHelper.getScreenHeight() * 0.06,
    alignItems: 'center'
  },
  itemIcon: {
    width: 13,
    height: 15,
    marginLeft: 30
  },
  text: {
    fontFamily: R.fonts.medium,
    fontSize: 15,
    color: '#686E7F',
    marginLeft: 30
  },
  hr: {
    borderTopWidth: 1,
    borderColor: '#F7F7F8',
    width: 2520
  },
  HR: {
    borderTopWidth: 10,
    borderColor: '#F7F7F8',
    width: 2520
  },
  itemIconPhone: {
    width: 13,
    height: 13,
    marginLeft: 30
  },
  itemIconEmail: {
    width: 13,
    height: 10,
    marginLeft: 30
  },
  itemIconBirth: {
    width: 12,
    height: 11,
    marginLeft: 30,
    resizeMode: 'contain'
  },
  itemIconGender: {
    width: 14,
    height: 13,
    marginLeft: 30
  },
  itemIconAddr: {
    width: 10,
    height: 13,
    marginLeft: 30,
    resizeMode: 'contain'
  },
  itemIconRePass: {
    width: 11,
    height: 13,
    marginLeft: 30
  },

  buttonLogOut: {
    marginTop: 50,
    backgroundColor: R.colors.white100,
    //marginBottom:50
    padding: 20,
    borderRadius: 0
  },
  textButtonLogOut: {
    fontSize: 13,
    fontFamily: R.fonts.medium,
    color: '#FF0023'
  }
});
