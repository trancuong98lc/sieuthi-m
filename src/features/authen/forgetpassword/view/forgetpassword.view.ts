import { Forgetpassword } from "../model/forgetpassword";

export interface  ForgetpasswordView {
     onFetchDataSuccess(forgetpassword: Forgetpassword): void;
     onFetchDataFail(error?: any): void;
 }
 