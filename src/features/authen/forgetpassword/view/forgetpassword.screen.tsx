import { validatePhone } from 'helpers/helper-funtion';
import { SCREEN_NAME } from 'helpers/utils';
import { showAlert } from 'libraries/BaseAlert';
import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import { Alert, Image, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { onCheckPhoneExists, PhoneExistType } from 'redux/actions';
import { translate } from 'res/languages';
import R from 'res/R';
import { OtpScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { Forgetpassword } from '../model/forgetpassword';
import { styles } from './forgetpassword.style';
import { ForgetpasswordView } from './forgetpassword.view';

interface Props {
  route: any;
  onCheckPhoneExists: (data: PhoneExistType) => void;
}
interface State {
  forgetpassword: Forgetpassword;
  phoneNumber: string;
}
class ForgetpasswordScreen extends React.PureComponent<Props, State>
  implements ForgetpasswordView {
  constructor(props: Props) {
    super(props);
    this.state = {
      forgetpassword: {},
      phoneNumber: props.route?.params?.phoneNumber ?? undefined
    };
  }
  onFetchDataSuccess(forgetpassword: Forgetpassword): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }
  send = () => {
    let { phoneNumber } = this.state;
    phoneNumber = phoneNumber.trim();

    if (phoneNumber.length == 0) {
      return showAlert('account.empty_phone');
    }
    if (!phoneNumber.startsWith('0')) {
      return showAlert('alert.phoneNotValid');
    }
    if (phoneNumber.trim() == '') {
      return showAlert('account.empty_phone');
    }
    if (phoneNumber.length > 10 || phoneNumber.length < 10) {
      return showAlert('account.max_Phone');
    }
    if (!validatePhone(phoneNumber)) {
      return showAlert('alert.phoneNotValid');
    }
    this.props.onCheckPhoneExists({
      phoneNumber,
      screen: SCREEN_NAME.FORGET_PASS
    });
  };
  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={goBack}>
              <FastImage
                resizeMode={FastImage.resizeMode.contain}
                source={R.images.ic_back}
                style={styles.styleImageButton}
              />
            </TouchableOpacity>
          </View>
          <DailyMartText style={styles.styleTextHeader}>
            {translate('button.forgetpass')}
          </DailyMartText>
          <View style={{ flex: 1 }} />
        </View>
        <DailyMartText style={styles.textforgetpasswordscreen}>
          {translate('textforgetpassscreen')}
        </DailyMartText>
        <TextInputs
          styleTextInputs={styles.styleTextInputs1}
          placeholder={translate('textinput.phone')}
          styleTextInput={styles.styleTextInput1}
          source={R.images.ic_phone}
          keyboardType="numeric"
          styleImageTextInput={styles.styleImageTextInput}
          value={this.state.phoneNumber}
          onChangeText={(text) => this.setState({ phoneNumber: text.trim() })}
        />

        <Buttons
          styleButton={styles.styleButton}
          textButton={translate('button.sent')}
          styleTextButton={styles.styleTextButton}
          onPress={this.send}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onCheckPhoneExists: (data: PhoneExistType) =>
      dispatch(onCheckPhoneExists(data))
  };
};

export default connect(null, mapDispatchToProps)(ForgetpasswordScreen);
