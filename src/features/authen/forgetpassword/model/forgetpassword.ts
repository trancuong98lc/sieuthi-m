export interface Forgetpassword {
     country?: string;
     city?: string;
     lat?: number;
     lon?: number;
     query?: string;
 }
 