import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import { RegisterBodyRequest } from 'redux/sagas/auth';
import images from 'res/images';
import { translate } from 'res/languages';
import { HomeScreen } from 'routing/screen-name';
import { popMultipleScreen, resetStack } from 'routing/service-navigation';
import colors from 'res/colors';
import { DimensionHelper } from 'helpers/dimension-helper';

interface Props {
  onRegisterUser: (data: RegisterBodyRequest) => void;
  route: any;
}
interface State {
  routerIndex: number;
}
export default class CompleteSuccess extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    console.log('CompleteSuccess -> constructor -> props', props);
    this.state = {
      routerIndex: props.navigation.dangerouslyGetState().index
    };
  }

  componentDidMount = () => {};

  onContinue = () => {
    if (this.state.routerIndex > 6) {
      popMultipleScreen(this.state.routerIndex);
    } else {
      resetStack(HomeScreen);
    }
  };

  public render() {
    return (
      <Container
        style={{
          padding: 0,
          alignItems: 'center'
        }}
        statusBarColor={'white'}
      >
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white'
          }}
        >
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={images.ic_success}
            style={{ width: 80, height: 80, marginBottom: 20 }}
          />
          <DailyMartText style={{ fontSize: 15 }}>
            {translate('account.register_succes')}
          </DailyMartText>
        </View>
        <Buttons
          onPress={this.onContinue}
          styleButton={styles.styleButton}
          textButton={translate('continue')}
          styleTextButton={styles.styleTextButton}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  styleButton: {
    backgroundColor: colors.primaryColor,
    padding: 10,
    width: DimensionHelper.getScreenWidth() / 2,
    marginBottom: 20
  },
  styleTextButton: {
    color: 'white'
  }
});
