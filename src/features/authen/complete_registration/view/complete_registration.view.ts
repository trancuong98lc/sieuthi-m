import { Complete_registration } from "../model/complete_registration";

export interface Complete_registrationView {
     onFetchDataSuccess(complete_registration: Complete_registration): void;
     onFetchDataFail(error?: any): void;
 }
 