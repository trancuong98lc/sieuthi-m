import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import { Alert, Image, TouchableOpacity, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { LoginScreen, RegistrationScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { Complete_registration } from '../model/complete_registration';
import { styles } from './complete_registration.style';
import { Complete_registrationView } from './complete_registration.view';
import { showAlert } from 'libraries/BaseAlert';
import {
  validateName,
  validateEmail,
  validateEmoji
} from 'helpers/helper-funtion';
import { RegisterBodyRequest } from 'redux/sagas/auth';
import { onRegisterUser } from 'redux/actions';
import { connect } from 'react-redux';
import { NAME_LENGTH, PASSWORD } from 'global/constants';
import FastImage from 'react-native-fast-image';

interface Props {
  onRegisterUser: (data: RegisterBodyRequest) => void;
  route: any;
}
interface State {
  passWord: string;
  isPassWord: boolean;
  rePassWord: string;
  isRePassWord: boolean;
  name: string;
  phoneNumber: string;
  email: string;
}
class Complete_registrationScreen extends React.PureComponent<Props, State>
  implements Complete_registrationView {
  constructor(props: Props) {
    super(props);
    this.state = {
      passWord: '',
      rePassWord: '',
      isPassWord: true,
      phoneNumber: props.route.params.phoneNumber || '',
      isRePassWord: true,
      name: '',
      email: ''
    };
  }

  onFetchDataSuccess(complete_registration: Complete_registration): void {
    throw new Error('Method not implemented.');
  }

  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  _changeIcon = () => {
    this.setState({ isPassWord: !this.state.isPassWord });
  };

  _changeIconRePassWord = () => {
    this.setState({ isRePassWord: !this.state.isRePassWord });
  };

  registration = () => {
    const { name, email, passWord, rePassWord, phoneNumber } = this.state;
    const { onRegisterUser } = this.props;

    if (name.trim().length < NAME_LENGTH.MIN || name.length == 0) {
      return showAlert('account.fullname_from_2character');
    }

    if (name.trim().length > NAME_LENGTH.MAX) {
      return showAlert('account.fullname_max');
    }
    if (validateName(name) || validateEmoji(name)) {
      return showAlert('account.fullname_valid');
    }
    if (email.length == 0 || email.trim() == '') {
      showAlert(translate('account.emply_email'));
      return;
    }
    if (!validateEmail(email)) {
      showAlert(translate('alert.emailNotValid'));
      return;
    }
    if (passWord.trim().length == 0 || rePassWord.trim().length == 0) {
      return showAlert('account.trim_pass');
    }
    if (passWord.length < PASSWORD.MIN) {
      return showAlert('account.password_invalid');
    }

    if (passWord.length > PASSWORD.MAX) {
      return showAlert('account.password_max');
    }
    if (passWord != rePassWord) {
      showAlert(translate('alert.notComfimPass'));
      return;
    }
    let body: RegisterBodyRequest = {
      email: email,
      phoneNumber: phoneNumber,
      password: passWord,
      name: name
    };
    onRegisterUser(body);
  };
  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => navigate(RegistrationScreen)}>
              <FastImage
                resizeMode={FastImage.resizeMode.contain}
                source={R.images.ic_back}
                style={styles.styleImageButton}
              />
            </TouchableOpacity>
          </View>
          <DailyMartText style={styles.styleTextHeader}>
            {translate('button.registration')}
          </DailyMartText>
          <View style={{ flex: 1 }} />
        </View>
        <TextInputs
          styleTextInputs={styles.styleTextInputs1}
          placeholder={translate('textinput.name')}
          styleTextInput={styles.styleTextInput1}
          source={R.images.ic_name}
          styleImageTextInput={styles.styleImageTextInput}
          onChangeText={(text) => this.setState({ name: text })}
          value={this.state.name}
        />
        <TextInputs
          styleTextInputs={styles.styleTextInputs2}
          placeholder={translate('textinput.email')}
          styleTextInput={styles.styleTextInput1}
          source={R.images.ic_email}
          styleImageTextInput={styles.styleImageTextInput}
          onChangeText={(text) => this.setState({ email: text })}
          value={this.state.email}
        />
        <TextInputs
          styleTextInputs={styles.styleTextInputs2}
          placeholder={translate('textinput.pass')}
          styleTextInput={styles.styleTextInput1}
          source={this.state.isPassWord ? R.images.ic_eyeoff : R.images.ic_eye}
          onPress={this._changeIcon}
          onChangeText={(text: string) => this.setState({ passWord: text })}
          value={this.state.passWord}
          secureTextEntry={this.state.isPassWord}
          styleImageTextInput={styles.styleImageTextInput}
        />
        <TextInputs
          styleTextInputs={styles.styleTextInputs2}
          placeholder={translate('textinput.verifypass')}
          styleTextInput={styles.styleTextInput1}
          source={
            this.state.isRePassWord ? R.images.ic_eyeoff : R.images.ic_eye
          }
          onPress={this._changeIconRePassWord}
          onChangeText={(text: string) => this.setState({ rePassWord: text })}
          value={this.state.rePassWord}
          secureTextEntry={this.state.isRePassWord}
          styleImageTextInput={styles.styleImageTextInput}
        />
        <Buttons
          onPress={this.registration}
          styleButton={styles.styleButton}
          textButton={translate('button.registration')}
          styleTextButton={styles.styleTextButton}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onRegisterUser: (data: RegisterBodyRequest) =>
      dispatch(onRegisterUser(data))
  };
};

export default connect(null, mapDispatchToProps)(Complete_registrationScreen);
