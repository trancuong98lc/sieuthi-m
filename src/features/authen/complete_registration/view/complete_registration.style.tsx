import { StyleSheet } from "react-native";
import { DimensionHelper } from 'helpers/dimension-helper';
import R from "res/R";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: "#FFFFFF",
          alignItems: 'center',
          flex:1
     },
     header:{
          flexDirection: "row",
          alignItems: "center",
          width: DimensionHelper.getScreenWidth(),
          height: 50,
          marginTop:DimensionHelper.getStatusBarHeight()+50
     },
     styleImageButton :{
          width: 45,
          height: 45,
          resizeMode: "contain",
          marginLeft:15
     },
     styleTextHeader: {
          fontSize: 20,
          textAlign: "center",
          fontFamily:R.fonts.bold
     },
     styleTextInputs1: {
          width: 0.85 * DimensionHelper.getScreenWidth(),
          height: 0.109 * DimensionHelper.getScreenWidth(),
          marginTop: 0.07* DimensionHelper.getScreenHeight(),
     },
     styleImageTextInput: {
          width: 13,
          height: 15,
          marginLeft: 40,
        },
        styleTextInput1: {
          marginLeft: 25,
          width: DimensionHelper.getScreenWidth()*0.6,
             fontSize: 13,
          fontFamily:R.fonts.regular
     },
     styleTextInputs2: {
          width: 0.85 * DimensionHelper.getScreenWidth(),
          height: 0.109 * DimensionHelper.getScreenWidth(),
          marginTop: 0.03 * DimensionHelper.getScreenHeight(),
     },
     styleButton: {
          width: 0.85 * DimensionHelper.getScreenWidth(),
          height: 0.109 * DimensionHelper.getScreenWidth(),
          marginTop: 0.04 * DimensionHelper.getScreenHeight(),
          backgroundColor: "#3BBB5A",
        },
     styleTextButton: {
             color: R.colors.white100,
             fontSize: 15,
             fontFamily:R.fonts.medium
        },
})