export interface Complete_registration {
     country?: string;
     city?: string;
     lat?: number;
     lon?: number;
     query?: string;
 }
 