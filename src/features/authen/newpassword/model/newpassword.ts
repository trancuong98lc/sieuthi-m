export interface Newpassword {
     country?: string;
     city?: string;
     lat?: number;
     lon?: number;
     query?: string;
 }
 