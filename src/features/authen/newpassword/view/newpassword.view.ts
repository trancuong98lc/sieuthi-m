import { Newpassword } from "../model/newpassword";

export interface  NewpasswordView {
     onFetchDataSuccess(forgetpassword:Newpassword): void;
     onFetchDataFail(error?: any): void;
 }