import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Alert
} from 'react-native';
import { Newpassword } from '../model/newpassword';
import { NewpasswordView } from './newpassword.view';
import { styles } from './newpassword.style';
import { translate } from 'res/languages';
import TextInputs from 'libraries/textinput/textinput';
import Buttons from 'libraries/button/buttons';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';
import { ForgetpasswordScreen, LoginScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { showAlert } from 'libraries/BaseAlert';
import { PASSWORD } from 'global/constants';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import FastImage from 'react-native-fast-image';

interface Props {
  route: any;
}
interface State {
  passWord: string;
  isPassWord: boolean;
  rePassWord: string;
  isRePassWord: boolean;
  phoneNumber: string;
}
export default class NewpasswordScreen extends React.PureComponent<Props, State>
  implements NewpasswordView {
  constructor(props: Props) {
    super(props);
    this.state = {
      passWord: '',
      rePassWord: '',
      phoneNumber: props.route.params.phoneNumber || '',
      isPassWord: true,
      isRePassWord: true
    };
  }
  onFetchDataSuccess(forgetpassword: Newpassword): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }
  _changeIcon = () => {
    this.setState({ isPassWord: !this.state.isPassWord });
  };
  _changeIconRePassWord = () => {
    this.setState({ isRePassWord: !this.state.isRePassWord });
  };
  rePassWord = async () => {
    const { passWord, rePassWord, phoneNumber } = this.state;
    if (passWord.trim().length == 0) {
      return showAlert('account.trim_pass');
    }
    if (rePassWord.trim().length == 0) {
      return showAlert('account.trim_pass_re');
    }
    if (passWord.length < PASSWORD.MIN) {
      return showAlert('account.password_invalid');
    }
    if (rePassWord.length < PASSWORD.MIN) {
      return showAlert('account.min_re_pass');
    }
    if (passWord !== rePassWord) {
      return showAlert('alert.notComfimPass');
    }
    const body = {
      phoneNumber: phoneNumber,
      password: passWord
    };
    try {
      showLoading();
      const res = await ApiHelper.put(URL_API.FORGOT_PASS, body);
      if (res.status == STATUS.SUCCESS) {
        showAlert(`changePass.${res.status}`, { success: true });
        navigate(LoginScreen);
        hideLoading();
      } else {
        hideLoading();
        showAlert(`changePass.${res.status}`);
      }
    } catch (error) {
      hideLoading();
    }
  };
  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => navigate(ForgetpasswordScreen)}>
              <FastImage
                resizeMode={FastImage.resizeMode.contain}
                source={R.images.ic_back}
                style={styles.styleImageButton}
              />
            </TouchableOpacity>
          </View>
          <DailyMartText style={styles.styleTextHeader}>
            {translate('newpass')}
          </DailyMartText>
          <View style={{ flex: 1 }} />
        </View>
        <TextInputs
          styleTextInputs={styles.styleTextInputs1}
          placeholder={translate('textinput.pass')}
          styleTextInput={styles.styleTextInput1}
          styleImageTextInput={styles.styleImageTextInput}
          source={this.state.isPassWord ? R.images.ic_eyeoff : R.images.ic_eye}
          onPress={this._changeIcon}
          onChangeText={(text: string) => this.setState({ passWord: text })}
          value={this.state.passWord}
          secureTextEntry={this.state.isPassWord}
        />
        <TextInputs
          styleTextInputs={styles.styleTextInputs2}
          placeholder={translate('textinput.verifypass')}
          styleTextInput={styles.styleTextInput1}
          source={
            this.state.isRePassWord ? R.images.ic_eyeoff : R.images.ic_eye
          }
          onPress={this._changeIconRePassWord}
          onChangeText={(text: string) => this.setState({ rePassWord: text })}
          value={this.state.rePassWord}
          secureTextEntry={this.state.isRePassWord}
          styleImageTextInput={styles.styleImageTextInput}
        />

        <Buttons
          onPress={this.rePassWord}
          styleButton={styles.styleButton}
          textButton={translate('button.repass')}
          styleTextButton={styles.styleTextButton}
        />
      </View>
    );
  }
}
