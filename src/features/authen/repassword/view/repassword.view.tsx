import { Repassword } from "../model/repassword";

export interface  RepasswordView {
     onFetchDataSuccess(forgetpassword:Repassword): void;
     onFetchDataFail(error?: any): void;
 }