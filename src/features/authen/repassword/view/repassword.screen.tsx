import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { Repassword } from '../model/repassword';
import { RepasswordView } from './repassword.view';
import { styles } from './repassword.style';
import { translate } from 'res/languages';
import TextInputs from 'libraries/textinput/textinput';
import R from 'res/R';
import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import { goBack, navigate } from 'routing/service-navigation';
import { AccountInforScreen } from 'routing/screen-name';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import { showAlert } from 'libraries/BaseAlert';
import { PASSWORD } from 'global/constants';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FastImage from 'react-native-fast-image';

interface Props {}
interface State {
  isPassWord: boolean;
  isPassWordNew: boolean;
  isPassWordRE: boolean;
  valuePass: string;
  valueNewPass: string;
  valueRePass: string;
}
export default class RepasswordScreen extends React.PureComponent<Props, State>
  implements RepasswordView {
  constructor(props: Props) {
    super(props);
    this.state = {
      isPassWord: true,
      isPassWordNew: true,
      isPassWordRE: true,
      valuePass: '',
      valueNewPass: '',
      valueRePass: ''
    };
  }
  onFetchDataSuccess(forgetpassword: Repassword): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  _changeIcon = () => {
    this.setState({ isPassWord: !this.state.isPassWord });
  };
  _changeIconNew = () => {
    this.setState({ isPassWordNew: !this.state.isPassWordNew });
  };
  _changeIconRE = () => {
    this.setState({ isPassWordRE: !this.state.isPassWordRE });
  };
  _changePass = (value: string) => {
    this.setState({ valuePass: value });
  };
  _changeNewPass = (value: string) => {
    this.setState({ valueNewPass: value });
  };
  _changeREPass = (value: string) => {
    this.setState({ valueRePass: value });
  };

  genderText = () => {
    const { valuePass, valueNewPass, valueRePass } = this.state;
  };

  onChangePass = async () => {
    const { valuePass, valueNewPass, valueRePass } = this.state;
    if (valuePass.trim().length == 0) {
      return showAlert('account.trim_pass');
    }
    if (valueNewPass.trim().length == 0) {
      return showAlert('account.trim_pass_new');
    }
    if (valueRePass.trim().length == 0) {
      return showAlert('account.trim_pass_re');
    }
    if (valuePass.length < PASSWORD.MIN) {
      return showAlert('account.password_invalid');
    }
    if (valueNewPass.length < PASSWORD.MIN) {
      return showAlert('account.min_new_pass');
    }
    if (valueRePass.length < PASSWORD.MIN) {
      return showAlert('account.min_re_pass');
    }
    if (valueNewPass != valueRePass) {
      showAlert(translate('alert.notComfimPass'));
      return;
    }
    const body = {
      password: valuePass,
      newPassword: valueNewPass,
      passwordConfirmed: valueRePass
    };
    try {
      showLoading();
      const res = await ApiHelper.post(URL_API.CHANGE_PASS, body, true);
      if (res.status == STATUS.SUCCESS) {
        goBack();
        showAlert(`changePass.${res.status}`, { success: true });
        hideLoading();
      } else {
        hideLoading();
        showAlert(`changePass.${res.status}`);
      }
    } catch (error) {
      hideLoading();
    }
  };

  public render() {
    const {
      isPassWord,
      isPassWordNew,
      isPassWordRE,
      valuePass,
      valueNewPass,
      valueRePass
    } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => navigate(AccountInforScreen)}>
              <FastImage
                resizeMode={FastImage.resizeMode.contain}
                source={R.images.ic_back}
                style={styles.styleImageButton}
              />
            </TouchableOpacity>
          </View>
          <DailyMartText style={styles.styleTextHeader}>
            {translate('button.repass')}
          </DailyMartText>
          <View style={{ flex: 1 }} />
        </View>
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          enableOnAndroid
          enableAutomaticScroll
        >
          <TextInputs
            styleTextInputs={styles.styleTextInputs1}
            placeholder={translate('textinput.oldpass')}
            styleTextInput={styles.styleTextInput1}
            source={isPassWord ? R.images.ic_eyeoff : R.images.ic_eye}
            autoFocus
            value={valuePass}
            styleImageTextInput={styles.styleImageTextInput}
            secureTextEntry={isPassWord}
            onPress={this._changeIcon}
            onChangeText={this._changePass}
          />
          <TextInputs
            styleTextInputs={styles.styleTextInputs2}
            placeholder={translate('textinput.newpass')}
            styleTextInput={styles.styleTextInput1}
            source={isPassWordNew ? R.images.ic_eyeoff : R.images.ic_eye}
            secureTextEntry={isPassWordNew}
            value={valueNewPass}
            styleImageTextInput={styles.styleImageTextInput}
            onPress={this._changeIconNew}
            onChangeText={this._changeNewPass}
          />
          <TextInputs
            styleTextInputs={styles.styleTextInputs2}
            placeholder={translate('textinput.verifypass')}
            styleTextInput={styles.styleTextInput1}
            source={isPassWordRE ? R.images.ic_eyeoff : R.images.ic_eye}
            secureTextEntry={isPassWordRE}
            value={valueRePass}
            styleImageTextInput={styles.styleImageTextInput}
            onPress={this._changeIconRE}
            onChangeText={this._changeREPass}
          />

          <Buttons
            onPress={this.onChangePass}
            styleButton={styles.styleButton}
            textButton={translate('button.repass')}
            styleTextButton={styles.styleTextButton}
          />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
