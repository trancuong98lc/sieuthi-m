export interface Repassword {
     country?: string;
     city?: string;
     lat?: number;
     lon?: number;
     query?: string;
 }
 