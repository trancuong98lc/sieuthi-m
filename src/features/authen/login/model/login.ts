export interface Login {
    country?: string;
    city?: string;
    lat?: number;
    lon?: number;
    query?: string;
 }
 