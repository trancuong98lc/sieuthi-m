import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
export const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor:R.colors.white100
     },
     header: {
          flexDirection:"row"
     },
     styleImageMask: {
          width: DimensionHelper.getScreenWidth()* 0.309,
          height: DimensionHelper.getScreenHeight() * 0.16,
          marginLeft: DimensionHelper.getScreenWidth() * 0.691,
          //marginTop: DimensionHelper.getStatusBarHeight() + 4,
          resizeMode:"contain"
     },
     buttonclose: {
          position: 'absolute',
          marginLeft: DimensionHelper.getScreenWidth() * 0.92,
     },

     ic_close: {
          width: 18,
          height: 18,
          //marginTop: -250,
         
         
     },
     logo: {
          width: 0.71 * DimensionHelper.getScreenWidth(),
          height: 0.125 * DimensionHelper.getScreenHeight(),
          marginTop: - 0.09 * DimensionHelper.getScreenHeight(),
          resizeMode:"contain"
     },
     styleTextInputs1: {
          width: 0.85 * DimensionHelper.getScreenWidth(),
          height: 0.109 * DimensionHelper.getScreenWidth(),
          marginTop: 0.045 * DimensionHelper.getScreenHeight()


     },
     styleTextInput1: {
          marginLeft: 25,
          width: DimensionHelper.getScreenWidth()*0.6,
          fontSize: 13,
          fontFamily:R.fonts.regular

          //flex:0.9
     },
     styleImageTextInput1: {
          width: 13,
          height: 15,
          marginLeft: 40

     },
     styleImageTextInput2: {
          width: 15,
          height: 17,
          marginLeft: 40

     },
     styleTextInputs2: {
          width: 0.85 * DimensionHelper.getScreenWidth(),
          height: 0.109 * DimensionHelper.getScreenWidth(),
          marginTop: 0.029 * DimensionHelper.getScreenHeight(),
     },
     styleTextInput2: {
          marginLeft: 25,
          width: DimensionHelper.getScreenWidth()*0.6,
          fontSize: 13,
          fontFamily:R.fonts.regular

          //flex:0.9
     },
     forgetPW: {
          alignItems: "center",
          marginTop: 0.029 * DimensionHelper.getScreenWidth(),
          fontSize: 12,
          fontFamily:R.fonts.medium
     },
     styleButton: {
          width: 0.85 * DimensionHelper.getScreenWidth(),
          height: 0.109 * DimensionHelper.getScreenWidth(),
          marginTop: 0.05 * DimensionHelper.getScreenHeight(),
          backgroundColor: '#3BBB5A',
     },
     styleTextButton: {
          color: R.colors.white100,
     },
     textFooter: {
          color: '#3BBB5A',
          fontSize: 13,
          fontFamily:R.fonts.bold
     }
   });
   