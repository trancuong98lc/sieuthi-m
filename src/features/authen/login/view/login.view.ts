import { Location } from 'location/model/location';

import { Login } from '../model/login';

export interface LoginView {
    onFetchDataSuccess(location: Login): void;
    onFetchDataFail(error?: any): void;
}
