import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import {
  Image,
  Keyboard,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import {
  ForgetpasswordScreen,
  PersonalLoginScreen,
  RegistrationScreen,
  HomeScreen
} from 'routing/screen-name';
import {
  goBack,
  navigate,
  navigationRef,
  popMultipleScreen,
  resetStack
} from 'routing/service-navigation';
import { Login } from '../model/login';
import { styles } from './login.style';
import { LoginView } from './login.view';
import FastImage from 'react-native-fast-image';
import { onLoginUser, LoginInput } from 'redux/actions';
import { connect } from 'react-redux';
import { validatePhone } from 'helpers/helper-funtion';
import { showAlert } from 'libraries/BaseAlert';

interface Props {
  onLogin: (data: any) => void;
}

interface State {
  login?: Login;
  phoneNumber: string;
  passWord: string;
  isPassWord?: boolean;
  showKeyborad?: boolean;
  routerIndex: number;
}

class LoginScreen extends React.Component<Props, State> implements LoginView {
  constructor(props: Props) {
    super(props);
    console.log(
      'LoginScreen -> constructor -> props.navigation.dangerouslyGetState().index',
      props.navigation.dangerouslyGetState()
    );
    this.state = {
      login: {},
      routerIndex:
        props.navigation.dangerouslyGetState().routes.length >= 8
          ? props.navigation.dangerouslyGetState().index - 2
          : props.navigation.dangerouslyGetState().index,
      phoneNumber: '',
      passWord: '',
      isPassWord: true,
      showKeyborad: true
    };
  }
  onFetchDataSuccess(location: Login): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  componentDidMount(): void {
    Keyboard.addListener('keyboardDidShow', this.onShowKeyBoard);
    Keyboard.addListener('keyboardDidHide', this.onHideKeyBoard);

    StatusBar.setBackgroundColor('white');
  }
  onShowKeyBoard = () => {
    this.setState({ showKeyborad: false });
  };

  onHideKeyBoard = () => {
    this.setState({ showKeyborad: true });
  };

  onDissmiss = () => {
    Keyboard.dismiss();
  };

  loginScreen = () => {
    const { phoneNumber, passWord } = this.state;
    if (phoneNumber.length == 0) {
      return showAlert('account.empty_phone');
    }
    if (phoneNumber.trim() == '') {
      return showAlert('account.empty_phone');
    }
    if (!phoneNumber.startsWith('0')) {
      return showAlert('alert.phoneNotValid');
    }
    if (phoneNumber.length > 10 || phoneNumber.length < 10) {
      return showAlert('account.max_Phone');
    }
    if (!validatePhone(phoneNumber)) {
      return showAlert('alert.phoneNotValid');
    }
    if (passWord.length === 0) {
      return showAlert('account.password_empty');
    }
    if (passWord.length < 6) {
      return showAlert('account.passCharacter');
    }
    if (passWord.trim() === '') {
      return showAlert('account.password_empty');
    }
    const body = {
      phoneNumber,
      password: passWord
    };
    this.props.onLogin({ body, routerIndex: this.state.routerIndex });
  };

  _changeIcon = () => {
    this.setState({ isPassWord: !this.state.isPassWord });
  };

  backPress = () => {
    if (this.state.routerIndex == 0) {
      resetStack(HomeScreen);
    } else {
      popMultipleScreen(this.state.routerIndex);
    }
  };
  public render() {
    const { login, isPassWord } = this.state;
    return (
      <Container
        statusBarColor={R.colors.white100}
        containerStyle={{ backgroundColor: 'white' }}
      >
        <TouchableWithoutFeedback
          onPress={this.onDissmiss}
          onPressIn={this.onDissmiss}
        >
          <View style={{ flex: 1 }}>
            <View style={styles.header}>
              <FastImage
                source={R.images.mask}
                resizeMode={FastImage.resizeMode.contain}
                style={styles.styleImageMask}
              />
              <Buttons
                source={R.images.ic_close}
                styleButton={styles.buttonclose}
                styleImageButton={styles.ic_close}
                onPress={this.backPress}
              />
            </View>
            <View style={{ alignItems: 'center' }}>
              <FastImage
                resizeMode={FastImage.resizeMode.contain}
                source={R.images.logo}
                style={styles.logo}
              />
            </View>

            <View style={{ alignItems: 'center' }}>
              <TextInputs
                styleTextInputs={styles.styleTextInputs1}
                placeholder={translate('textinput.phone')}
                styleTextInput={styles.styleTextInput1}
                source={R.images.ic_phone}
                styleImageTextInput={styles.styleImageTextInput1}
                keyboardType="numeric"
                autoFocus
                onChangeText={(text: string) =>
                  this.setState({ phoneNumber: text })
                }
                value={this.state.phoneNumber}
              />
              <TextInputs
                styleTextInputs={styles.styleTextInputs2}
                placeholder={translate('textinput.pass')}
                styleTextInput={styles.styleTextInput2}
                source={isPassWord ? R.images.ic_eyeoff : R.images.ic_eye}
                styleImageTextInput={styles.styleImageTextInput2}
                onChangeText={(text: string) =>
                  this.setState({ passWord: text })
                }
                value={this.state.passWord}
                secureTextEntry={isPassWord}
                onPress={this._changeIcon}
              />
            </View>
            <TouchableOpacity
              style={styles.forgetPW}
              onPress={() =>
                navigate(ForgetpasswordScreen, {
                  phoneNumber: this.state.phoneNumber,
                  key: 'forgetPassWord'
                })
              }
            >
              <DailyMartText>{translate('logscreen.forgetpass')}</DailyMartText>
            </TouchableOpacity>
            <View style={{ alignItems: 'center' }}>
              <Buttons
                styleButton={styles.styleButton}
                textButton={translate('button.login')}
                styleTextButton={styles.styleTextButton}
                onPress={this.loginScreen}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
        {this.state.showKeyborad ? (
          <View
            style={{
              alignItems: 'center',
              marginBottom: 30
            }}
          >
            <DailyMartText style={{ fontSize: 13 }}>
              {`${translate('logscreen.newuser')}? `}
              <DailyMartText
                style={styles.textFooter}
                onPress={() => navigate(RegistrationScreen)}
              >
                {translate('button.registration')}
              </DailyMartText>
            </DailyMartText>
          </View>
        ) : null}
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onLogin: (data: any) => dispatch(onLoginUser(data))
  };
};

export default connect(null, mapDispatchToProps)(LoginScreen);
