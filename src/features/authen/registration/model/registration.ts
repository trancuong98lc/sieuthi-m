export interface Registration {
        country?: string;
        city?: string;
        lat?: number;
        lon?: number;
        query?: string;
 }