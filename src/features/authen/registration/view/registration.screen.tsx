import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import {
  Alert,
  Image,
  View,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { OtpScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { Registration } from '../model/registration';
import { styles } from './registration.style';
import { RegistrationView } from './registration.view';
import { showAlert } from 'libraries/BaseAlert';
import FastImage from 'react-native-fast-image';
import { validatePhone } from 'helpers/helper-funtion';
import { PASSWORD } from 'global/constants';
import { PhoneExistType, onCheckPhoneExists } from 'redux/actions';
import { connect } from 'react-redux';
import { SCREEN_NAME } from 'helpers/utils';

interface Props {
  onCheckPhoneExists: (data: PhoneExistType) => void;
}

interface State {
  registration: Registration;
  phoneNumber: string;
}

class RegistrationScreen extends React.Component<Props, State>
  implements RegistrationView {
  constructor(props: Props) {
    super(props);
    this.state = {
      registration: {},
      phoneNumber: ''
    };
  }
  onFetchDataSuccess(location: Registration): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  componentDidMount(): void {
    // this.presenter.onStart();
    // ApiHelper.fetch(Config.API_URL).then((res) => {});
  }

  onHideKeyBroad = () => {
    Keyboard.dismiss();
  };

  continue = () => {
    let { phoneNumber } = this.state;
    phoneNumber = phoneNumber.trim();

    if (phoneNumber.length == 0) {
      return showAlert('account.empty_phone');
    }
    if (!phoneNumber.startsWith('0')) {
      return showAlert('alert.phoneNotValid');
    }
    if (phoneNumber.trim() == '') {
      return showAlert('account.empty_phone');
    }
    if (phoneNumber.length > 10 || phoneNumber.length < 10) {
      return showAlert('account.max_Phone');
    }
    if (!validatePhone(phoneNumber)) {
      return showAlert('alert.phoneNotValid');
    }
    this.props.onCheckPhoneExists({
      phoneNumber,
      screen: SCREEN_NAME.REGISTER
    });
    // navigate(OtpScreen, {
    // 	phoneNumber: phoneNumber,
    // 	key: 'registration'
    // });
  };

  public render(): React.ReactNode {
    return (
      <Container style={styles.container}>
        <TouchableWithoutFeedback onPress={this.onHideKeyBroad}>
          <View style={{ flex: 1 }}>
            <View style={styles.header}>
              <FastImage
                resizeMode={FastImage.resizeMode.contain}
                source={R.images.mask}
                style={styles.styleImageMask}
              />
              <Buttons
                source={R.images.ic_close}
                styleImageButton={styles.ic_close}
                styleButton={styles.buttonClose}
                onPress={goBack}
              />
            </View>
            <DailyMartText style={styles.textHeader}>
              {translate('button.registration')}
            </DailyMartText>
            <View style={{ alignItems: 'center' }}>
              <TextInputs
                styleTextInputs={styles.styleTextInputs1}
                placeholder={translate('textinput.phone')}
                styleTextInput={styles.styleTextInput1}
                source={R.images.ic_phone}
                autoFocus={true}
                styleImageTextInput={styles.styleImageTextInput}
                keyboardType="numeric"
                onChangeText={(text: string) =>
                  this.setState({ phoneNumber: text })
                }
                value={this.state.phoneNumber}
              />
            </View>
            {/* </View> */}
            <View style={{ alignItems: 'center' }}>
              <Buttons
                styleButton={styles.styleButton}
                textButton={translate('button.continue')}
                styleTextButton={styles.styleTextButton}
                // onPress={ () => navigate(OtpScreen, {
                //   phoneNumber: this.state.phoneNumber,
                //   key: 'registration'
                // }) }
                onPress={this.continue}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onCheckPhoneExists: (data: PhoneExistType) =>
      dispatch(onCheckPhoneExists(data))
  };
};

export default connect(null, mapDispatchToProps)(RegistrationScreen);
