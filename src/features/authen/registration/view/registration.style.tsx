import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: R.colors.white100
    },
    header: {
        flexDirection:'row'
    },
    textHeader: {
        fontSize: 21,
        color: '#686E7F',
        textAlign: 'center',
        marginTop: -0.08 * DimensionHelper.getScreenHeight(),
        fontFamily: R.fonts.bold
    },
    styleImageMask: {
        width: DimensionHelper.getScreenWidth() * 0.309,
        height: DimensionHelper.getScreenHeight() * 0.16,
      //marginTop: DimensionHelper.getStatusBarHeight(),
      marginLeft: DimensionHelper.getScreenWidth() * 0.691,
    },
  buttonClose: {
    position: 'absolute',
    //marginTop: DimensionHelper.getStatusBarHeight() ,
    backgroundColor:R.colors.white100,
    marginLeft:DimensionHelper.getScreenWidth() * 0.93
    },
    ic_close: {
        width: 18,
        height: 18,
        marginRight: 20
    },
    logo: {
        width: 0.71 * DimensionHelper.getScreenWidth(),
        height: 0.12 * DimensionHelper.getScreenHeight(),
        marginTop: -0.09 * DimensionHelper.getScreenHeight()
    },
    styleTextInputs1: {
        width: 0.85 * DimensionHelper.getScreenWidth(),
        height: 0.109 * DimensionHelper.getScreenWidth(),
        marginTop: 0.17 * DimensionHelper.getScreenHeight()
    },
    styleTextInput1: {
        marginLeft: 25,
        width: DimensionHelper.getScreenWidth() * 0.6,
        fontSize: 13,
        //color: '#ACACAC'

        //flex:0.9
    },
    styleImageTextInput: {
        width: 13,
        height: 15,
        marginLeft: 40
    },
    styleButton: {
        width: 0.85 * DimensionHelper.getScreenWidth(),
        height: 0.109 * DimensionHelper.getScreenWidth(),
        marginTop: 0.04 * DimensionHelper.getScreenHeight(),
        backgroundColor: '#3BBB5A'
    },
    styleTextButton: {
        color: R.colors.white100,
        fontSize: 15,
        fontFamily: R.fonts.medium
    }
});
