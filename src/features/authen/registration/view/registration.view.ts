import { Registration } from '../model/registration';

export interface RegistrationView {
    onFetchDataSuccess(location: Registration): void;
    onFetchDataFail(error?: any): void;
}