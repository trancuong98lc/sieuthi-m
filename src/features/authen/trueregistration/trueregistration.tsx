import * as React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import R from 'res/R';
import { translate } from 'res/languages';
import { DimensionHelper } from 'helpers/dimension-helper';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';

export interface TrueregistrationScreenProps {}

export default class TrueregistrationScreen extends React.PureComponent<
  TrueregistrationScreenProps,
  any
> {
  constructor(props: TrueregistrationScreenProps) {
    super(props);
  }

  public render() {
    return (
      <View style={styles.container}>
        <FastImage source={R.images.ic_success} style={styles.styleimage} />
        <DailyMartText style={styles.styletext}>
          {translate('trueregistration')}
        </DailyMartText>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: R.colors.white100
  },
  styleimage: {
    marginTop:
      DimensionHelper.getStatusBarHeight() +
      DimensionHelper.getScreenHeight() * 0.23,
    height: 100,
    width: 100
  },
  styletext: {
    marginTop: 20,
    color: '#333333',
    fontSize: 15
  }
});
