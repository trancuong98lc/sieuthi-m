import { Otp } from "../model/otp";

export interface OtpView {
     onFetchDataSuccess(otp: Otp): void;
     onFetchDataFail(error?: any): void;
 }