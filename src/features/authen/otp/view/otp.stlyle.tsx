import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    header: {
        flexDirection: 'row',
        backgroundColor: R.colors.white100
    },
    styleImageButton: {
        width: 40,
        height: 40,
        marginTop: 10
    },
    textHeader: {
        fontSize: 18,
        marginTop: 10,
        fontFamily: R.fonts.bold,
        color: '#686E7F',
        marginLeft: DimensionHelper.getScreenWidth() * 0.25
    },
    textveryfiscreen: {
        fontSize: 13,
        marginTop: 100,
        textAlign: 'center',
        color: '#686E7F'
    },
    styleTextInputs: {
        width: 45,
        height: 45,
        marginTop: 40,
        marginLeft: 10,
        borderRadius: 5
    },
    styleTextInput: {
        marginLeft: 12,
        fontSize: 13,
        width: 50
    },
    Button1: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 0.05 * DimensionHelper.getScreenHeight(),
        width: DimensionHelper.getScreenWidth() * 0.845,
        height: 0.109 * DimensionHelper.getScreenWidth(),
        borderRadius: 30,
        backgroundColor: '#3BBB5A'
    },
    Button2: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: DimensionHelper.getScreenWidth() * 0.867,
        height: 0.068 * DimensionHelper.getScreenHeight(),
        //borderRadius: 30
    },
    textReCode:{
        color: '#FF0023',
        fontSize: 12,
        fontFamily: R.fonts.medium
    },
    counts:{
        color: '#FF0023',
        fontSize: 12,
        fontFamily: R.fonts.medium
    }
});
