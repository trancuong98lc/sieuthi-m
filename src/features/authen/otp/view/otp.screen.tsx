import firebase from 'react-native-firebase';
import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { Alert, Image, Platform, TouchableOpacity, View } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import { translate } from 'res/languages';
import R from 'res/R';
import {
  Complete_registrationScreen,
  HomeScreen,
  NewpasswordScreen
} from 'routing/screen-name';
import { goBack, navigate, resetStack } from 'routing/service-navigation';
import { Otp } from '../model/otp';
import { styles } from './otp.stlyle';
import { OtpView } from './otp.view';
import {
  showLoading,
  hideLoading
} from 'libraries/LoadingManager/LoadingModal';
import { showAlert } from 'libraries/BaseAlert';
import FastImage from 'react-native-fast-image';

let SET_RESEND_CODE = 60;
interface Props {
  route: any;
}

interface State {
  otp: Otp;
  phoneNumber: string;
  screen: string;
  confirmation: any;
  counts: number;
  isValid: string;
  showKeyboard: boolean;
}

export default class OtpScreen extends React.Component<Props, State>
  implements OtpView {
  refInput = React.createRef<CodeInput>();

  constructor(props: Props) {
    super(props);
    this.state = {
      otp: {},
      phoneNumber: props.route?.params?.phoneNumber ?? undefined,
      screen: props.route?.params?.screen ?? undefined,
      confirmation: null,
      counts: SET_RESEND_CODE,
      isValid: '',
      showKeyboard: true
    };
  }

  unsubscribe: any = null;

  onFetchDataSuccess(otp: Otp): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  counts: any;

  componentDidMount() {
    try {
      const { screen, phoneNumber } = this.state;
      this.SendOtp();
      this.reSendCode();
      let that = this;

      if (Platform.OS === 'android') {
        showLoading();
        this.unsubscribe = firebase.auth().onAuthStateChanged((user: any) => {
          if (user && user._user) {
            let phone = phoneNumber.replace('0', '+84');
            console.log('TCL: phone', phone);
            if (user._user.phoneNumber === phone) {
              that.onCheckNavigation();
            }
          } else {
            this.SendOtp();
          }
          hideLoading();
          // if (this.unsubscribe) this.unsubscribe();
        });
      }
    } catch (error) {
      console.log('componentDidMount -> error', error);
      hideLoading();
    }
  }

  componentWillUnmount() {
    this.signOutOTP();
  }

  signOutOTP = () => {
    if (Platform.OS === 'android') {
      this.unsubscribe && this.unsubscribe();
    }
  };

  SendOtp = () => {
    firebase
      .auth()
      .signInWithPhoneNumber(this.replaceNumberPhone())
      .then((confirmation: any) => {
        console.log('confirmation: ', confirmation);
        this.setState({ confirmation });
      })
      .catch((err) => {
        console.log('err: ', err);
      });
  };
  reSendCode = () => {
    this.counts = setInterval(() => {
      if (this.state.counts > 0) {
        this.setState({ counts: this.state.counts - 1 });
      }
    }, 1000);
  };
  replaceStar = () => {
    let phoneNumber = this.state.phoneNumber;
    if (phoneNumber.startsWith('0')) {
      return '********' + phoneNumber.slice(8);
    } else {
      return '*******' + phoneNumber.slice(7);
    }
  };
  replaceNumberPhone = () => {
    let phoneNumber = this.state.phoneNumber;
    if (phoneNumber.startsWith('0')) {
      return phoneNumber.replace('0', '+84');
    } else {
      return '+84' + phoneNumber;
    }
  };
  onCheckNavigation = () => {
    const { screen, phoneNumber } = this.state;

    if (screen == 'forgetPassWord') {
      navigate(NewpasswordScreen, { phoneNumber: phoneNumber });
      return;
    }
    if (screen == 'registration') {
      navigate(Complete_registrationScreen, { phoneNumber: phoneNumber });
      return;
    }
  };

  confirmCode = async () => {
    try {
      const value = this.refInput.current?.state.codeArr;
      const isValid = value.join('');
      if (!isValid) {
        return showAlert(translate('account.code_empty'));
      }
      const { confirmation } = this.state;
      showLoading();
      if (confirmation) {
        confirmation
          .confirm(isValid)
          .then((res: any) => {
            hideLoading();
            this.onCheckNavigation();
          })
          .catch((err: any) => {
            hideLoading();
            showAlert(translate('account.code_err'));
          });
      }
    } catch (error) {
      console.log('confirmCode -> error', error);
      hideLoading();
    }
  };
  _onFinishCheckingCode1(isValid: number) {
    // this.setState({ isValid });
    // console.log('isValid: ', isValid);
  }
  public render(): React.ReactNode {
    const isDisableVerifi = this.isDisableVerifi();
    const isDisableResendCode = this.isDisableResendCode();
    const { otp } = this.state;
    return (
      <Container style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={goBack}>
            <FastImage
              resizeMode={FastImage.resizeMode.contain}
              source={R.images.ic_back}
              style={styles.styleImageButton}
            />
          </TouchableOpacity>
          <DailyMartText style={styles.textHeader}>
            {translate('button.verify')}
          </DailyMartText>
        </View>

        <DailyMartText style={styles.textveryfiscreen}>
          {translate('textveryfiscreen')}
          <DailyMartText> {this.replaceStar()}</DailyMartText>
        </DailyMartText>

        <CodeInput
          codeLength={6}
          keyboardType="numeric"
          ref={this.refInput}
          //secureTextEntry
          activeColor="#686E7F"
          inactiveColor="#F4F4F4"
          autoFocus={true}
          ignoreCase={true}
          inputPosition="center"
          size={50}
          onFulfill={(isValid: number) => this._onFinishCheckingCode1(isValid)}
          containerStyle={{ marginTop: 30, flex: 0 }}
          codeInputStyle={{
            backgroundColor: '#F7F7F8',
            borderWidth: 0,
            color: '#686E7F',
            borderRadius: 5
          }}
          onChangeText={(text: string) => this.setState({ isValid: text })}
          value={this.state.isValid}
        />
        <Buttons
          styleButton={styles.Button1}
          onPress={this.confirmCode}
          //disabled={isDisableVerifi}
          //onPress={this.onCheckNavigation}
          textButton={translate('button.verify')}
          styleTextButton={{
            fontSize: 15,
            color: '#FFFFFF',
            fontFamily: R.fonts.medium
          }}
        />
        <TouchableOpacity
          style={styles.Button2}
          disabled={isDisableResendCode}
          onPress={this.onResendCode}
        >
          <DailyMartText style={styles.textReCode}>
            {translate('button.reotp') + ' '}
            {this.state.counts > 0 && (
              <DailyMartText style={styles.counts}>
                ( {this.state.counts} )
              </DailyMartText>
            )}
          </DailyMartText>
        </TouchableOpacity>
      </Container>
    );
  }
  onResendCode = () => {
    //const phoneNumber = this.replaceNumberPhone();
    this.setState({ counts: SET_RESEND_CODE });
    this.setState({ isValid: '' });
    this.SendOtp();
  };
  isDisableVerifi = () => {
    if (this.state.isValid.trim().length < 6) return true;
    return false;
  };
  isDisableResendCode = () => {
    if (this.state.counts !== 0) return true;
    return false;
  };
}
