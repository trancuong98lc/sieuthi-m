import supermarket from 'data/supermarket';
import { DimensionHelper } from 'helpers/dimension-helper';
import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import {
  Alert,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  NativeSegmentedControlIOSChangeEvent
} from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { HomeScreen, CityScreen, DistrictScreen } from 'routing/screen-name';
import { navigate, resetStack } from 'routing/service-navigation';
import { RequestStore, Store_Type, City, District } from './model/store-entity';
import ItemStore from './view/components/store-item';
import { Store2View } from './view/store.view';
import HeaderStore from './view/components/store-header';
import { NavigationNavigatorProps } from 'react-navigation';
import { SCREEN_NAME } from 'helpers/utils';
import { StoreAdapter } from './model/store-adapter';
import _ from 'lodash';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import { updateQuantityCart } from 'redux/actions';
import { connect } from 'react-redux';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import Commons from 'helpers/Commons';

interface Props {
  navigation: any;
  route: any;
  updateQuantityCart: (total: number) => void;
}
interface State {
  type: string;
  loading: boolean;
  DataStore: Store_Type[];
  valueCity: City | null;
  isloadMore: boolean;
  valueDistrict: District | null;
}
class StoreScreen extends React.Component<Props, State> {
  adapter: StoreAdapter;
  refHeader = React.createRef<HeaderStore>();
  params: RequestStore = {
    limit: String(15),
    page: String(1)
  };
  constructor(props: Props) {
    super(props);
    this.adapter = new StoreAdapter(this);
    this.state = {
      type: (props.route.params && props.route.params.screen) || '',
      valueCity: null,
      valueDistrict: null,
      isloadMore: false,
      loading: true,
      DataStore: []
    };
    this.onSearchText = _.debounce(this.onSearchText, 300);
  }

  componentDidMount = () => {
    console.log(this.state.type, 'assa');
  };

  onSearchText = (search: string) => {
    const params: RequestStore = {
      ...this.params,
      search: search,
      page: '1'
    };
    this.params = params;
    this.getStore(params);
  };

  getTotalProductCart = (idStore: String) => {
    if (Commons.idToken) {
      showLoading();
      ApiHelper.fetch(URL_API.CART_TOTAL_PRODUCT + idStore, null, true)
        .then((res) => {
          if (res.status == STATUS.SUCCESS) {
            hideLoading();
            resetStack(HomeScreen);
            this.props.updateQuantityCart(res.total || 0);
          } else {
            hideLoading();
            resetStack(HomeScreen);
            this.props.updateQuantityCart(0);
          }
        })
        .catch((err) => {
          hideLoading();
        });
      return;
    }
    resetStack(HomeScreen);
  };

  setValueCity = (valueCity: City) => {
    this.setState({ valueCity }, () => {
      delete this.params.districtId;
      const params: RequestStore = {
        ...this.params,
        cityId: valueCity._id
      };
      this.setValueDistrict(null);
      this.params = params;
      this.params.page = '1';
      this.getStore(this.params);
    });
  };

  setValueDistrict = (valueDistrict: District | null) => {
    this.setState({ valueDistrict }, () => {
      const params: RequestStore = {
        ...this.params,
        districtId: valueDistrict ? valueDistrict._id : undefined
      };
      delete this.params.districtId;
      this.params = params;
      this.params.page = '1';
      this.getStore(this.params);
    });
  };

  componentDidMount = () => {
    this.onReFresh();
  };

  onReFresh = () => {
    this.params.page = '1';
    this.adapter.getStore(this.params);
  };

  getStore = (params: RequestStore) => {
    setTimeout(() => {
      this.adapter.getStore(params);
    }, 300);
  };

  setLoading = (value: boolean) => {
    this.setState({ loading: value });
  };

  setDataStore = (data: Store_Type[], isloadMore: boolean) => {
    this.setState({ DataStore: data, isloadMore });
  };

  renderItem = ({ item, index }: { item: Store_Type; index: number }) => {
    return (
      <ItemStore
        getTotalProductCart={this.getTotalProductCart}
        item={item}
        index={index}
      />
    );
  };

  renderListFooter = () => {
    return <View style={{ height: 100 }} />;
  };

  onEndReched = () => {
    const { DataStore, isloadMore } = this.state;
    const params: RequestStore = {
      ...this.params,
      page: String(Number(this.params.page) + 1)
    };
    this.params = params;
    if (DataStore.length < Number(this.params.limit) || isloadMore) return;
    this.getStore(this.params);
  };

  _keyExtrator = (item: Store_Type) => item._id;

  gotoCity = () => {
    navigate(CityScreen, { onSetCity: this.setValueCity });
  };

  gotoDistrict = () => {
    const { valueCity } = this.state;
    navigate(DistrictScreen, {
      onSetDistrict: this.setValueDistrict,
      CityId: valueCity?._id
    });
  };

  public render() {
    const { type, DataStore, loading, valueCity, valueDistrict } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <HeaderStore
            onSearchText={this.onSearchText}
            checkRightText={type === SCREEN_NAME.HOME ? true : false}
            ref={this.refHeader}
          />
          <View style={{ flexDirection: 'row', marginVertical: 12 }}>
            <Buttons
              styleButton={styles.styleButton}
              textButton={
                (valueCity && valueCity?.name) || translate('button.all')
              }
              styleTextButton={styles.styleTextButton}
              source={R.images.ic_select}
              styleImageButton={styles.styleImageButton}
              onPress={this.gotoCity}
            />
            <Buttons
              styleButton={styles.styleButton}
              textButton={
                (valueDistrict && valueDistrict?.name) ||
                translate('button.all')
              }
              styleTextButton={styles.styleTextButton}
              source={R.images.ic_select}
              styleImageButton={styles.styleImageButton}
              onPress={this.gotoDistrict}
              disabled={
                !valueCity || valueCity?.name == 'Tất cả' ? true : false
              }
            />
          </View>
          <View style={styles.flatlist}>
            <FlatList
              data={DataStore}
              refreshing={loading}
              onRefresh={this.onReFresh}
              extraData={this.state}
              keyExtractor={this._keyExtrator}
              scrollEventThrottle={0.5}
              renderItem={this.renderItem}
              onEndReached={this.onEndReched}
              ListFooterComponent={this.renderListFooter}
            />
          </View>
        </View>
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  return {
    user
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateQuantityCart: (quantity: number) =>
      dispatch(updateQuantityCart(quantity))
  };
};

export default connect(mapStatesToProps, mapDispatchToProps)(StoreScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.white100,
    marginBottom: -DimensionHelper.getBottomSpace()
  },
  styleButton: {
    width: 0.45 * DimensionHelper.getScreenWidth(),
    height: 0.04 * DimensionHelper.getScreenHeight(),
    backgroundColor: R.colors.white100,
    borderRadius: 0,
    marginLeft: 14,
    borderColor: '#E5E5E5',
    borderWidth: 1
  },
  styleTextButton: {
    color: '#686E7F',
    fontSize: 15,
    width: 0.3 * DimensionHelper.getScreenWidth()
  },
  styleImageButton: {
    width: 12,
    height: 7,
    marginLeft: 20
  },
  flatlist: {
    flex: 1,
    backgroundColor: '#F7F7F8'
  },
  hr: {
    borderTopWidth: 10,
    borderColor: 'red'
  }
});
