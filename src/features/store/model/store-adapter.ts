/* 
  Created by dungnt at 08-10-2020 09:32:34
  Màn hình cá nhân app khách hàng
*/

import StoreScreen from '../store.screen';
import StoreServices from './store-services';
import { RequestStore } from './store-entity';
import { BaseResponse, STATUS } from 'types/BaseResponse';
import { showAlert } from 'libraries/BaseAlert';
import { translate } from 'res/languages';

export class StoreAdapter {
  view: StoreScreen;
  constructor(view: StoreScreen) {
    this.view = view;
  }

  getStore = (params: RequestStore) => {
    StoreServices.getInstance()
      .get(params)
      .then((result) => {
        if (result.status == STATUS.SUCCESS) {
          this.view.setLoading(false);
          let maxdata = result.data.length < 15;
          const data =
            Number(params.page) > 1
              ? [...this.view.state.DataStore, ...result.data]
              : result.data;
          this.view.setDataStore(data, maxdata);
        } else {
          this.view.setLoading(false);
          showAlert(translate(`status.${result.status}`));
        }
      })
      .catch((err) => {
        this.view.setLoading(false);
        console.log('StoreAdapter -> getStore -> err', err);
      });
  };
}
