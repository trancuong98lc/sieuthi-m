/* 
  Created by dungnt at 08-10-2020 09:32:34
  Màn hình cá nhân app khách hàng
*/

import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { BaseResponse } from 'types/BaseResponse';

export default class StoreServices {
  private static instance: StoreServices;

  static getInstance(): StoreServices {
    if (!StoreServices.instance) {
      StoreServices.instance = new StoreServices();
    }
    return StoreServices.instance;
  }

  async get(data: object): Promise<BaseResponse> {
    return await ApiHelper.fetch(URL_API.STORE_LIST, data, true);
  }
}
