export interface RequestStore {
  limit: string;
  page: string;
  search?: string;
  cityId?: string;
  districtId?: string;
}

export interface Store_Type {
  _id: string;
  createdAt: string;
  deletedAt?: string;
  status: boolean;
  cityId: string;
  districtId: string;
  name: string;
  address: string;
  location: string;
  citys: City[];
  districts: District[];
  phone: string;
}

export interface City {
  _id: string;
  name: string;
  createdAt: string;
}
export interface District {
  _id: string;
  name: string;
  cityId: string;
  createdAt: string;
}
