import { Store2 } from "../model/store-entity";

export interface Store2View {
     onFetchDataSuccess(store: Store2): void;
     onFetchDataFail(error?: any): void;
 }
 