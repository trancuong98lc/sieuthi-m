import city from 'data/city';
import { DimensionHelper } from 'helpers/dimension-helper';
import FlatListCity from 'libraries/flatlist/flatlists/flatlist.city';
import Container from 'libraries/main/container';
import * as React from 'react';
import { FlatList, Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import HeaderService from 'libraries/header/header-service';
import ItemCityDistrict from './components/store-item-district-city';
import { City } from '../model/store-entity';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
export interface Props {
  route: any;
}
interface states {
  DataCity: City[];
}

export default class CityScreen extends React.PureComponent<Props, states> {
  constructor(props: Props) {
    super(props);
    this.state = {
      DataCity: []
    };
  }

  componentDidMount = () => {
    this.getCity();
  };

  getCity = async () => {
    const params = {
      limit: 100,
      page: String(1)
    };
    const res = await ApiHelper.fetch(URL_API.CITY, params);
    if (res.status === STATUS.SUCCESS) {
      const allCity = {
        createdAt: "",
        customOrder: 0,
        deletedAt: null,
        name: "Tất cả"
      }
      const data = [...[allCity], ...res.data];
      this.setState({ DataCity: data });
    }
  };

  onPressItem = (item: City) => () => {
    const { onSetCity } = this.props.route.params;
    onSetCity && onSetCity(item);
    goBack();
  };

  renderItem = ({ item, index }: any) => {
    return (
      <ItemCityDistrict
        onPress={this.onPressItem(item)}
        item={item}
        index={index}
      />
    );
  };

  keyExtra = (item: City) => item._id;

  onEndReached = () => {};

  renderFooter = () => <View style={{ height: 200 }} />;

  public render() {
    return (
      <Container
        statusBarColor={R.colors.primaryColor}
        containerStyle={{ backgroundColor: 'white' }}
      >
        <HeaderService
          title={'listStore.selectCity'}
          leftIcon={'ic_close'}
          sizeIcon={20}
        />
        <View style={styles.flatlist}>
          <FlatList
            data={this.state.DataCity}
            extraData={this.state}
            keyExtractor={this.keyExtra}
            scrollEventThrottle={0.5}
            renderItem={this.renderItem}
            ListFooterComponent={this.renderFooter}
          />
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    marginTop: DimensionHelper.getStatusBarHeight() + 20,
    marginLeft: 25
  },
  ic_close: {
    width: 20,
    height: 20
  },
  text: {
    fontSize: 20,
    marginLeft: 70
  },
  flatlist: {
    marginTop: 10
  }
});
