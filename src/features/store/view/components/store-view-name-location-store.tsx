import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';
import { DimensionHelper } from 'helpers/dimension-helper';
import BaseIcon from 'libraries/baseicon/BaseIcon';

export interface ViewItemInstoreProps {
	source: any;
	name: string;
	color?: string;
}

export default class ViewItemInstore extends React.PureComponent<ViewItemInstoreProps, any> {
	constructor(props: ViewItemInstoreProps) {
		super(props);
	}

	public render() {
		const { source, name, color } = this.props;
		return (
			<View style={styles.name}>
				<BaseIcon icon={source} width={17} iconStyle={styles.addImage} />
				<DailyMartText style={[ styles.addText, { color } ]}>{name}</DailyMartText>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	name: {
		flexDirection: 'row',
		marginBottom: 10,
		alignItems: 'center',
		flex: 1
	},
	addText: {
		marginTop: 5,
		marginLeft: 10,
		flex: 1
	},
	addImage: {
		marginTop: 5
	}
});
