import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import TextInputs from 'libraries/textinput/textinput';
import { translate } from 'res/languages';
import { goBack, navigate } from 'routing/service-navigation';
import { HomeScreen } from 'routing/screen-name';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';

export interface HeaderStoreProps {
  checkRightText: boolean;
  onSearchText: (search: string) => void;
}

interface states {
  value: string;
}

export default class HeaderStore extends React.PureComponent<
  HeaderStoreProps,
  states
> {
  constructor(props: HeaderStoreProps) {
    super(props);
    this.state = {
      value: ''
    };
  }

  onChangeText = (value: string) => {
    this.setState({ value });
    this.props.onSearchText && this.props.onSearchText(value);
  };

  public render() {
    const { checkRightText } = this.props;
    const { value } = this.state;
    return (
      <View style={styles.header}>
        <TextInputs
          placeholder={translate('textinput.searchstore')}
          styleTextInputs={[
            styles.styleTextInputs,
            !checkRightText && { flex: 1, marginRight: 10 }
          ]}
          autoCapitalize={'none'}
          styleTextInput={styles.styleTextInput}
          source={R.images.ic_search}
          valie={value}
          onChangeText={this.onChangeText}
          styleImageTextInput={styles.styleImageTextInput}
          onPress={() => {}}
        />
        {checkRightText && (
          <TouchableOpacity
            onPress={() => (checkRightText ? goBack() : navigate(HomeScreen))}
          >
            <Text style={styles.textHeader}>{translate('button.delete')}</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: R.colors.primaryColor,

    alignItems: 'center',
    flexDirection: 'row'
  },

  textHeader: {
    marginTop: -5,
    marginLeft: 17,
    color: R.colors.white100,
    fontSize: 15
  },
  styleTextInputs: {
    marginTop: 10,
    marginLeft: 10,
    height: 0.084 * DimensionHelper.getScreenWidth(),
    width: 0.81 * DimensionHelper.getScreenWidth(),
    marginBottom: 20
  },
  styleTextInput: {
    marginLeft: 0.12 * DimensionHelper.getScreenWidth(),
    fontSize: 13,
    color: '#8D8D8D',
    padding: 0,
    width: DimensionHelper.getScreenWidth() * 0.6,
    position: 'absolute'
  },
  styleImageTextInput: {
    height: 15,
    width: 15,
    //marginLeft: -0.65 * DimensionHelper.getScreenWidth(),
    marginLeft: 20
  }
});
