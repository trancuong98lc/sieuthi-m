import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { goBack } from 'routing/service-navigation';
import DailyMartText from 'libraries/text/text-daily-mart';
import { City } from 'features/store/model/store-entity';
import { GENDER, GENDER_NAME } from 'types/user-type';
import { translate } from 'res/languages';
export interface ItemCityDistrictProps {
  item?: City | any;
  index: number;
  onPress: () => void;
  isGender?: boolean
}

export default class ItemCityDistrict extends React.PureComponent<
  ItemCityDistrictProps,
  any
  > {
  constructor(props: ItemCityDistrictProps) {
    super(props);
  }

  public render() {
    const { item, onPress, isGender } = this.props;
    return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <DailyMartText style={styles.flatListItem}>{(isGender ? translate(GENDER_NAME[item?.name]) : item?.name) || item.title}</DailyMartText>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomColor: '#686E7F',
    borderBottomWidth: 1,
    paddingVertical: 10,
    marginHorizontal: 12
    //flexDirection:'row',
    // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
    ///backgroundColor: 'mediumseagreen',
  },
  flatListItem: {
    marginTop: 10,
    fontSize: 18,
    color: '#686E7F'
  }
});
