import { DimensionHelper } from 'helpers/dimension-helper';
import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import R from 'res/R';
import { HomeScreen } from 'routing/screen-name';
import { navigate, resetStack } from 'routing/service-navigation';
import FastImage from 'react-native-fast-image';
import DailyMartText from 'libraries/text/text-daily-mart';
import ViewItemInstore from './store-view-name-location-store';
import { Store_Type } from 'features/store/model/store-entity';
import colors from 'res/colors';
import { reset } from 'i18n-js';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import Commons from 'helpers/Commons';
export interface ItemStoreProps {
  item: Store_Type;
  index: number;
  getTotalProductCart: (id: String) => void
}

export default class ItemStore extends React.PureComponent<
  ItemStoreProps,
  any
  > {
  constructor(props: ItemStoreProps) {
    super(props);
  }

  gotoHomeScren = () => {
    const { item } = this.props;
    AsyncStorageHelpers.saveObject(StorageKey.STORE, item);
    Commons.idStore = item._id
    Commons.store = item
    this.props.getTotalProductCart(item._id)
  };

  public render() {
    const { item } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={this.gotoHomeScren}>
        <ViewItemInstore

          source={R.images.ic_store}
          color={colors.primaryColor}
          name={item.name}
        />
        <ViewItemInstore source={R.images.ic_add} name={item.address || ''} />
        <ViewItemInstore source={R.images.ic_phone} name={item.phone || ''} />
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    marginTop: 10,
    marginHorizontal: 10,
    borderRadius: 6,
    paddingTop: 10,
    paddingHorizontal: 10
    // height: DimensionHelper.getScreenHeight() * 0.1
  },
  name: {
    flexDirection: 'row',
    padding: (DimensionHelper.getScreenHeight() * 0.1) / 6
  },
  nameImage: {
    width: 20,
    height: 20
  },
  nameText: {
    marginLeft: 10
  },
  add: {
    flexDirection: 'row',
    paddingLeft: (DimensionHelper.getScreenHeight() * 0.1) / 5
  },
  addImage: {
    width: 12,
    height: 17,
    marginTop: 5
  },
  addText: {
    marginTop: 5,
    marginLeft: 10
  }
});
