import district from 'data/district';
import { DimensionHelper } from 'helpers/dimension-helper';
import FlatListDistrict from 'libraries/flatlist/flatlists/flatlist.district';
import Container from 'libraries/main/container';
import * as React from 'react';
import { FlatList, Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import HeaderService from 'libraries/header/header-service';
import ItemCityDistrict from './components/store-item-district-city';
import { District } from '../model/store-entity';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
export interface Props {
  route: any;
}

interface states {
  dataWard: District[];
}

export default class WardScreen extends React.PureComponent<Props, states> {
  constructor(props: Props) {
    super(props);
    this.state = {
      dataWard: []
    };
  }

  componentDidMount = () => {
    this.getWard();
  };

  getWard = async () => {
    const { districtId } = this.props.route.params;
    const params = {
      limit: 300,
      page: String(1),
      districtId: districtId
    };
    const res = await ApiHelper.fetch(URL_API.WARD, params);
    if (res.status === STATUS.SUCCESS) {
      this.setState({ dataWard: res.data });
    }
  };

  onPressItem = (item: District) => () => {
    const { onSetWard, CityId } = this.props.route.params;
    onSetWard && onSetWard(item);
    goBack();
  };

  renderItem = ({ item, index }: any) => {
    return (
      <ItemCityDistrict
        onPress={this.onPressItem(item)}
        item={item}
        index={index}
      />
    );
  };

  keyExtra = (item: District) => item._id;

  onEndReached = () => { };

  renderFooter = () => <View style={{ height: 200 }} />;

  public render() {
    return (
      <Container
        statusBarColor={R.colors.primaryColor}
        containerStyle={{ backgroundColor: 'white' }}
      >
        <HeaderService
          title={'listStore.selectDistrict'}
          leftIcon={'ic_close'}
          sizeIcon={20}
        />
        <View style={styles.flatlist}>
          <FlatList
            data={this.state.dataWard}
            keyExtractor={this.keyExtra}
            scrollEventThrottle={0.5}
            renderItem={this.renderItem}
            ListFooterComponent={this.renderFooter}
          />
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    marginLeft: 70
  },
  hr: { height: 0.5, backgroundColor: '#686E7F', marginTop: 10 },
  flatlist: {
    marginTop: 10,
    flex: 1
  }
});
