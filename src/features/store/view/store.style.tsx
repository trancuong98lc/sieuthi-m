import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: R.colors.white100,
		marginBottom: -DimensionHelper.getBottomSpace()
	},
	header: {
		backgroundColor: R.colors.primaryColor,

		alignItems: 'center',
		flexDirection: 'row'
	},

	textHeader: {
		marginTop: -5,
		marginLeft: 17,
		color: R.colors.white100,
		fontSize: 15
	},
	styleTextInputs: {
		marginTop: 10,
		marginLeft: 10,
		height: 0.084 * DimensionHelper.getScreenWidth(),
		width: 0.81 * DimensionHelper.getScreenWidth(),
		marginBottom: 20
	},
	styleTextInput: {
		marginLeft: 0.12 * DimensionHelper.getScreenWidth(),
		fontSize: 13,
		color: '#8D8D8D',
		padding: 0,
		width: DimensionHelper.getScreenWidth() * 0.6,
		position: 'absolute'
	},
	styleImageTextInput: {
		height: 15,
		width: 15,
		//marginLeft: -0.65 * DimensionHelper.getScreenWidth(),
		marginLeft: 20
	},
	styleButton: {
		width: 0.45 * DimensionHelper.getScreenWidth(),
		height: 0.04 * DimensionHelper.getScreenHeight(),
		backgroundColor: R.colors.white100,
		borderRadius: 0,
		marginLeft: 14,
		borderColor: '#E5E5E5',
		borderWidth: 1
	},
	styleTextButton: {
		color: '#686E7F',
		fontSize: 15,
		width: 0.3 * DimensionHelper.getScreenWidth()
	},
	styleImageButton: {
		width: 12,
		height: 7,
		marginLeft: 20
	},
	flatlist: {
		backgroundColor: '#F7F7F8'
	},
	hr: {
		borderTopWidth: 10,
		borderColor: 'red'
	}
});
