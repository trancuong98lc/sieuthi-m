import { STATUS } from './../../../../types/BaseResponse';
import ApiHelper from '../../../../helpers/api-helper';
import { URL_API } from '../../../../helpers/url-api';
import EndowDetailContainer from '../view/endow-detail.screen';
import {
  hideLoading,
  showLoading
} from '../../../../libraries/LoadingManager/LoadingModal';
import Commons from '../../../../helpers/Commons';

export class EndowDetailAdapter {
  private view: EndowDetailContainer;
  constructor(view: EndowDetailContainer) {
    this.view = view;
  }

  getEndowDetail = async (id: string) => {
    try {
      showLoading();
      const res = await ApiHelper.fetch(
        URL_API.ENDOW + Commons.idStore + '/' + id,
        {},
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setEndowDetail(res.data[0]);
        hideLoading();
      } else {
        this.view.setEndowDetail({});
        hideLoading();
      }
    } catch (error) {
        console.log("getEndowDetail: ", error);
        hideLoading();
    }
  };
}
