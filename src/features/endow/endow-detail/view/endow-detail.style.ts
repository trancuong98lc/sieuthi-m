import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom:-DimensionHelper.getBottomSpace()
      },
     endowDetailHeader: {
          backgroundColor: R.colors.primaryColor,
           flexDirection: 'row',
        paddingBottom:10
      },
      endowDetailHeaderImage: {
           width: 35,
           height: 35,
           marginTop: 10,
         marginLeft:10
      },
      endowDetailHeaderText: {
           fontFamily: R.fonts.bold,
           fontSize: 18,
           color: R.colors.white100,
           marginTop: 10,
           marginLeft:DimensionHelper.getScreenWidth()*0.25,
           
     },
     image: {
          width: DimensionHelper.getScreenWidth(),
          height: DimensionHelper.getScreenWidth() * 0.427,
          
     },
     viewLogo2: {
          backgroundColor: '#3BBB5A',
          width: DimensionHelper.getScreenWidth() * 0.27,
          height: DimensionHelper.getScreenWidth() * 0.14,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 1,
          borderColor: R.colors.white100,
          borderRadius: 6,
          marginTop: -DimensionHelper.getScreenWidth() * 0.07,
          marginLeft:DimensionHelper.getScreenWidth() * 0.36,
     },
     logo2:{
          width: DimensionHelper.getScreenWidth() * 0.215,
          height: DimensionHelper.getScreenWidth() * 0.05,
     },
     nameEndow: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: '#333333',
          textAlign: 'center',
          marginTop: 12,
          marginHorizontal:10
     },
     date: {
          fontSize: 15,
          color: '#828282',
          marginTop: 5,
          textAlign:'center'
     },
     hr: {
          height: 15,
          backgroundColor: '#F1F1F1',
          marginTop:16
     },
     contentEndow: {
          // width: DimensionHelper.getScreenWidth() * 0.9,
          marginTop: 15,
          marginHorizontal:20,
          //textAlign: 'center',
          lineHeight:DimensionHelper.getScreenHeight()*0.02,
          fontSize: 14,
          color: '#333333',
          flex:1,
          textAlign: 'justify'
     },
     buttonUse: {
          width: DimensionHelper.getScreenWidth() * 0.82,
          height: DimensionHelper.getScreenWidth() * 0.11,
          backgroundColor: '#3BBB5A',
          marginLeft: DimensionHelper.getScreenWidth() * 0.09,
          marginBottom:50,
          marginTop: 10
     },
     textButtonUse: {
          fontFamily: R.fonts.bold,
          fontSize:13,
          color:R.colors.white100

     }
})