import * as React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { EndowDetailView } from './endow-detail.view';
import Buttons from 'libraries/button/buttons';
import { styles } from './endow-detail.style';
import DailyMartText from 'libraries/text/text-daily-mart';
import R from 'res/R';
import { translate } from 'res/languages';
import { goBack, navigate } from 'routing/service-navigation';
import Container from 'libraries/main/container';
import HeaderService from 'libraries/header/header-service';
import { EndowDetailAdapter } from '../model/endow-detail.adapter';
import { EndowDetail } from '../model/endow-detail.type';
import moment from 'moment';
import FastImage from 'react-native-fast-image';
import { getMediaUrl } from 'helpers/helper-funtion';
import { ScrollView } from 'react-native-gesture-handler';
import { PromotionScreen } from 'routing/screen-name';
import Commons from 'helpers/Commons';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';

export interface EndowDetailScreenProps { 
  route: any
}

export interface EndowDetailStates {
  endowDetail: EndowDetail,
  loading: boolean
}

export default class EndowDetailScreen
  extends React.PureComponent<EndowDetailScreenProps, any> implements EndowDetailView {
  private adapter: EndowDetailAdapter;

  constructor(props: EndowDetailScreenProps) {
    super(props);
    this.adapter = new EndowDetailAdapter(this);
    this.state = {
      endowDetail: null,
      loading: true
    }
  }
  
  onFetchDataSuccess(
    endowDetail: import('../model/endow-detail').EndowDetail
  ): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  componentDidMount = () => {
    const id = this.props.route?.params?._id ?? undefined;
    this.adapter.getEndowDetail(id);
  }

  setEndowDetail = (endowDetail: EndowDetail) => {
    this.setState({
      endowDetail,
      loading: false
    });
  }

  useEndow = () => {
    const { endowDetail } = this.state;
    console.log("endowDetail: ", endowDetail)
    const applyingOnStoreIds = endowDetail.applyingOnStoreIds;
    const applyingTime = endowDetail.applyingTime;
    const now = new Date();
    if (applyingOnStoreIds.indexOf(Commons.idStore) == -1) {
      showAlert(translate('endow.endowNotExistStore'));
      return;
    }
    if (now < applyingTime.startDate) {
      showAlert(translate('endow.endowNotReachApplyTime'));
      return;
    }
    if (now > applyingTime.endDate) {
      showAlert(translate('endow.endowOutOfDate'));
      return;
    }
    if (endowDetail.statuspreferentialQuantity) {
      showAlert(translate('endow.endowRunOutOfProduct'));
      return;
    }

    navigate(PromotionScreen, {
      promotionId: endowDetail._id
    })
  }

  public render() {
    const { endowDetail } = this.state;
    
    if (endowDetail) {
      const { startDate, endDate }= endowDetail.applyingTime;
      return (
        <Container statusBarColor={R.colors.primaryColor}>
          <View style={styles.container}>
            <HeaderService title={'endow.endowDetail'} />
            <FastImage style={styles.image} source={getMediaUrl(endowDetail.medias && endowDetail.medias.length > 0 ? endowDetail.medias[0] : undefined)} />
            <View style={styles.viewLogo2}>
              <FastImage style={styles.logo2} source={R.images.logo2} />
            </View>
            <DailyMartText style={styles.nameEndow}>
              {endowDetail.name}
            </DailyMartText>
            <DailyMartText style={styles.date}>
              {translate('endow.since')} {moment(startDate).format('l')} {''}
              {translate('endow.to')} {moment(endDate).format('l')}
                        </DailyMartText>
            <View style={styles.hr} />
            <ScrollView showsVerticalScrollIndicator={false} alwaysBounceVertical={false}>
              <DailyMartText style={styles.contentEndow}>
                {endowDetail.content}
              </DailyMartText>
            </ScrollView>
            <Buttons
              styleButton={styles.buttonUse}
              styleTextButton={styles.textButtonUse}
              textButton={translate('button.use')}
              onPress={this.useEndow}
            />
          </View>
        </Container>
      );
    } else {
      return null;
    }
  }
}
