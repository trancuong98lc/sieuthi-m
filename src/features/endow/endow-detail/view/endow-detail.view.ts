import { EndowDetail } from "../model/endow-detail";

export interface EndowDetailView {
     onFetchDataSuccess(endowDetail: EndowDetail): void;
     onFetchDataFail(error?: any): void;
 }