import uudai from 'data/uudai';
import FlatListEndow from 'libraries/flatlist/flatlist.endow';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { styles } from './endow.style';
import { EndowView } from './endow.view';
import { ReqEndow, EndowAdapter } from '../model/endow.adapter';
import { Endows } from '../model/endow.type';
import { Endow } from 'features/home/model/home';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';

export interface ListEndowProps {
  isTab?: boolean
}

export interface ListEndowStates {
  endows: Endows[],
  loading: boolean,
  maxdata: boolean
}

export default class EndowScreen extends React.PureComponent<ListEndowProps, ListEndowStates>
  implements EndowView {
    private adapter: EndowAdapter;
    private onEndReachedCalledDuringMomentum: boolean;

  constructor(props: ListEndowProps) {
    super(props);
    this.adapter = new EndowAdapter(this);
    this.onEndReachedCalledDuringMomentum = true;
    this.state = {
      endows: [],
      loading: true,
      maxdata: true
    }
  }

  params = {
    page: 1,
    limit: 10
  }

  onFetchDataSuccess(endow: import('../model/endow').Endow): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  componentDidMount = () => {
    this.adapter.getEndows(this.params);
  }

  setEndows = (endows: Endows[]) => {
    this.setState({
      endows,
      loading: false
    })
  }

  setMaxData = (maxdata: boolean) => {
    this.setState({
      maxdata
    })
  }

  onRefresh = () => {
    const newParams =
    {
      limit: 10,
      page: 1,
    }
    this.params = newParams;
    this.adapter.getEndows(newParams);
  }

  onEndReached  = () => {
    const { loading, maxdata } = this.state;
    if (loading || this.onEndReachedCalledDuringMomentum || maxdata) {
      return;
    }
    const newParasm: any = {
      ...this.params,
      page: this.params.page + 1
    }
    this.params = newParasm
    this.adapter.getEndows(newParasm);
    this.onEndReachedCalledDuringMomentum = true;
  }

  onMomentumScrollBegin = () => {
    this.onEndReachedCalledDuringMomentum = false; 
  }

  keyExtractor = (item: Endows, index: number) => {
    return item._id.toString();
  }

  emptyListComponent = () => {
    if(!this.state.loading) {
      return <EmptylistComponent />;
    } else {
      return null;
    }
  }

  footerComponent = () => {
    if(!this.state.maxdata) {
      return <DailyMartText style={styles.footerFlatList}>Đang tải...</DailyMartText>;
    } else {
      return null;
    }
  }

  public render(): React.ReactNode {
    const { isTab } = this.props;
    const { endows, loading } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService isTab={isTab} title="endow.header" />
        <FlatList
          style={styles.flatList}
          data={endows}
          refreshing={loading}
          onRefresh={this.onRefresh}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.01}
          keyExtractor={this.keyExtractor}
          showsVerticalScrollIndicator={false}
          onMomentumScrollBegin={this.onMomentumScrollBegin}
          ListEmptyComponent={this.emptyListComponent}
          ListFooterComponent={this.footerComponent}
          // ListFooterComponent={(<View style={{ height: 100 }} />)}
          renderItem={({ item, index }) => {
            return (
              <FlatListEndow item={item} index={index} />
            );
          }}
        />
      </Container>
    );
  }
}
