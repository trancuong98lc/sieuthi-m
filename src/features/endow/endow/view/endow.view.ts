import { Endow } from "../model/endow";

export interface EndowView {
     onFetchDataSuccess(endow: Endow): void;
     onFetchDataFail(error?: any): void;
 }