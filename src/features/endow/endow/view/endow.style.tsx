import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";
import { getBottomSpace } from "react-native-iphone-x-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom: -DimensionHelper.getBottomSpace()
     },
     endowHeader: {
          backgroundColor: R.colors.primaryColor,
          paddingBottom: 17
     },
     endowHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          textAlign: 'center'

     },
     flatList: {
          marginTop: 6,
          marginBottom: 0
     },
     footerFlatList: {
          marginVertical: 10, 
          textAlign: "center", 
          color: R.colors.primaryColor
     }
});