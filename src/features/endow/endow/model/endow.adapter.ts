import { showAlert } from 'libraries/BaseAlert';
import { STATUS } from './../../../../types/BaseResponse';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import EndowContainer from '../view/endow.screen';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import Commons from 'helpers/Commons';
import { Endow } from '../../../home/model/home';

export interface ReqEndow {
    page: number;
    limit: number;
  }

export class EndowAdapter {
  private view: EndowContainer;
  constructor(view: EndowContainer) {
    this.view = view;
  }

  getEndows = async (params: ReqEndow) => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.ENDOW,
        params,
        true
      );
      if (res.status == STATUS.SUCCESS) {
        let data = params.page > 1 ? [...this.view.state.endows, ...res.data] : res.data;
        this.view.setEndows(data);
        this.view.setMaxData(data.length === res.total);
        // hideLoading();
      } else {
        // this.view.setEndows([]);
        this.view.setMaxData(false);
        // hideLoading();
      }
    } catch (error) {
        console.log("getEndows: ", error);
        // hideLoading();
    }
  };
}
