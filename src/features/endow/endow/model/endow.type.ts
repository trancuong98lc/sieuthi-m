
import { NavigationInitActionPayload } from 'react-navigation';

export interface EndowProps {
  route: NavigationInitActionPayload;
}

export interface EndowState {
  endows: Endows[], 
  loading: boolean
}


export interface Endows {
  _id: string,
  name: string,
}

