import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import { showAlert } from 'libraries/BaseAlert';
import { ProductDetailView } from 'product-detail/view/product-detail.view';
import { STATUS } from 'types/BaseResponse';

export class ProductDetailPresenter {
  private productDetailView: ProductDetailView;

  constructor(productDetailView: ProductDetailView) {
    this.productDetailView = productDetailView;
  }

  async onFetchProductsDetail(id: number): Promise<any> {
    try {
      const res = await ApiHelper.fetch(
        URL_API.PRODUCTS + `/${id}/${Commons.idStore}`,
        null,
        true
      );
      if (res.status === STATUS.SUCCESS) {
        this.productDetailView.onFetchDataSuccess(res.data || undefined);
      } else if (res.status === STATUS.RECORD_NOT_FOUND) {
        return showAlert('product-detail.not_suc');
      }
    } catch (error) {
      console.log('error: ', error);
      this.productDetailView.onFetchDataFail();
    }
  }
}
