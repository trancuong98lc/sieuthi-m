import { ProductDetail } from 'product-detail/model/product-detail';
import { Products, ProductsDetail } from 'types/category';

export interface ProductDetailView {
    onFetchDataSuccess(data: Products | undefined): void;
    onFetchDataFail(error?: any): void;
}
