import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import images from 'res/images';
import { translate } from 'res/languages';
import { CartScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { styles } from '../product-detail.style';
export interface HeaderDetailProductProps {
    totalProduct?: number
}
class HeaderDetailProduct extends React.PureComponent<HeaderDetailProductProps, any> {
    constructor(props: HeaderDetailProductProps) {
        super(props);
    }

    gotoCart = () => {
        navigate(CartScreen)
    }
    public render() {
        const { totalProduct } = this.props;
        return (
            <View style={styles.header}>
                <TouchableOpacity onPress={goBack} activeOpacity={0.8}>
                    <FastImage
                        source={images.ic_back3}
                        style={styles.headerimage1}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.gotoCart} activeOpacity={0.8}>
                    <FastImage
                        source={images.ic_cart}
                        style={styles.headerimage2}
                    />
                    {totalProduct !== undefined && totalProduct > 0 && 
                        <View style={styles.headernumber}>
                            <DailyMartText style={styles.headertext}>{totalProduct}</DailyMartText>
                        </View>
                    }
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStatesToProps = (state: any) => {
    const totalProduct = state.cart.quantity
    return { totalProduct }

}
export default connect(mapStatesToProps, null)(HeaderDetailProduct);



