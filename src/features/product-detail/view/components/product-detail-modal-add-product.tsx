import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Modal from 'react-native-modal';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import { translate } from 'res/languages';
import { CartScreen, LoginScreen, HomeScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { styles } from '../product-detail.style';
import HeaderDetailProduct from './product-detail-header';
import BtnModalProduct from './product-detail-botton-modal';
import { Products } from 'types/category';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import Commons from 'helpers/Commons';
import { ALERT_TYPE, showAlert, showAlertByType } from 'libraries/BaseAlert';
import { STATUS } from 'types/BaseResponse';
import { connect } from 'react-redux';
import { updateQuantityCart } from 'redux/actions';
import EventBus, { EventBusName } from 'helpers/event-bus';
export interface ModalAddProductProps {
  productsDetail: Products;
  quantity: number;
  updateQuantityCart: (quantity: number) => void;
}

interface States {
  isModalVisible: boolean;
  qty: number;
  reachedMaxProduct: boolean;
}

class ModalAddProduct extends React.PureComponent<
  ModalAddProductProps,
  States
> {
  constructor(props: ModalAddProductProps) {
    super(props);
    this.state = {
      isModalVisible: false,
      qty: 1,
      reachedMaxProduct: false
    };
  }

  onOpen = () => {
    this.setState({
      isModalVisible: true
    });
  };

  getTotalProductCart = (idStore: String) => {
    ApiHelper.fetch(URL_API.CART_TOTAL_PRODUCT + idStore, null, true)
      .then((res) => {
        if (res.status == STATUS.SUCCESS) {
          this.props.updateQuantityCart(res.total || 0);
        } else {
          this.onHide();
          setTimeout(() => {
            showAlert('button.addCartErr');
          }, 300);
        }
      })
      .catch((err) => {
        this.onHide();
        setTimeout(() => {
          showAlert('button.addCartErr');
        }, 300);
      });
  };

  onHide = () => {
    this.setState({ isModalVisible: false });
  };

  onCreateCart = async (isBuyNow?: boolean) => {
    try {
      const body = {
        storeId: Commons.idStore,
        item: {
          productId: this.props.productsDetail._id,
          qty: this.state.qty
        }
      };
      const res = await ApiHelper.post(URL_API.CART_CREATE, body, true);
      if (res.status == STATUS.SUCCESS) {
        EventBus.getInstance().post({
          type: EventBusName.CREATE_PRODUCT,
          payload: res.data.items
        });
        if (isBuyNow) {
          this.onHide();
          navigate(CartScreen);
          this.getTotalProductCart(Commons.idStore);
        } else {
          this.onHide();
          setTimeout(() => {
            showAlert('button.addCartSuccess', {
              success: true,
              callback: () => {
                goBack();
              }
            });
          }, 300);
          this.getTotalProductCart(Commons.idStore);
        }
      }
      if (res.status == STATUS.PRODUCT_IS_OUT_OF_STOCK) {
        this.onHide();
        setTimeout(() => {
          showAlert(`status.${res.status}`);
        }, 100);
      }
      if (res.status == STATUS.INVALID_TOKEN) {
        this.onHide();
        setTimeout(() => {
          return showAlertByType({
            title: translate('notify.header'),
            type: ALERT_TYPE.CONFIRM,
            message: translate('want_login'),
            callBackConfirm: () => {
              navigate(LoginScreen);
            }
          });
        }, 300);
      }
    } catch (error) {}
  };

  addCart = () => {
    this.onCreateCart();
  };

  buyNow = () => {
    this.onCreateCart(true);
  };

  onPre = () => {
    if (this.state.qty > 1) {
      this.setState({
        qty: this.state.qty - 1,
        reachedMaxProduct: false
      });
    }
  };

  onPlus = () => {
    if (this.state.qty === this.props.quantity) {
      this.setState({
        reachedMaxProduct: true
      });
    } else {
      this.setState({
        qty: this.state.qty + 1
      });
    }
  };

  public render() {
    const { qty, reachedMaxProduct } = this.state;
    const { productsDetail } = this.props;
    return (
      <Modal
        isVisible={this.state.isModalVisible}
        backdropOpacity={0.2}
        onBackdropPress={this.onHide}
        style={styles.view}
      >
        <View style={styles.modal}>
          <View style={styles.modalHeader}>
            <FastImage
              source={getMediaUrl(
                productsDetail! &&
                  productsDetail!.medias &&
                  productsDetail!.medias.length > 0 &&
                  productsDetail!.medias[0]
                  ? productsDetail!.medias[0]
                  : null
              )}
              style={styles.imageModal}
            />
            {/* <DailyMartText style={styles.newcost}>
                            {_formatPrice(productsDetail && productsDetail.price || 0) + ' đ'}
                        </DailyMartText> */}
            <View style={styles.cost}>
              {productsDetail ? (
                <DailyMartText style={styles.newcost}>
                  {`${_formatPrice(
                    productsDetail.promoFor
                      ? productsDetail.totalAll || 0
                      : productsDetail.price || 0
                  )} ₫`}
                </DailyMartText>
              ) : null}
              {productsDetail && productsDetail.promoFor ? (
                <DailyMartText style={styles.oldcost}>
                  {_formatPrice(productsDetail.price! || 0) + ' đ'}
                </DailyMartText>
              ) : null}
            </View>
          </View>
          <View style={styles.modalContent}>
            <DailyMartText style={styles.modalContenText}>
              {translate('textModalProductDetail')}
            </DailyMartText>
            <Buttons
              styleImageButton={styles.decrease}
              onPress={this.onPre}
              disabled={this.state.qty == 0 ? true : false}
              source={images.ic_decrease}
            />
            <DailyMartText style={styles.modalContenNumbder}>
              {qty}
            </DailyMartText>
            <Buttons
              styleButton={{ opacity: reachedMaxProduct ? 0.3 : 1 }}
              disabled={reachedMaxProduct}
              styleImageButton={styles.increase}
              onPress={this.onPlus}
              source={images.ic_increase}
            />
          </View>

          <BtnModalProduct
            onPressAddCart={this.addCart}
            onPressBuyNow={this.buyNow}
          />
        </View>
      </Modal>
    );
  }
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    updateQuantityCart: (quantity: number) =>
      dispatch(updateQuantityCart(quantity))
  };
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  ModalAddProduct
);
