import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import { translate } from 'res/languages';
import { goBack } from 'routing/service-navigation';
import { styles } from '../product-detail.style';
export interface BtnModalProductProps {
    onPressAddCart: () => void
    onPressBuyNow: () => void
}

export default class BtnModalProduct extends React.PureComponent<BtnModalProductProps, any> {
    constructor(props: BtnModalProductProps) {
        super(props);
    }

    public render() {
        const { onPressAddCart, onPressBuyNow } = this.props
        return (
            <View style={styles.modalButton}>
                <Buttons
                    styleButton={styles.addCart}
                    styleTextButton={styles.textButton}
                    textButton={translate('button.addCart')}
                    onPress={onPressAddCart}
                />
                <Buttons
                    styleButton={styles.buyNow}
                    styleTextButton={styles.textButton}
                    textButton={translate('button.buyNow')}
                    onPress={onPressBuyNow}
                />
            </View>
        );

    }
}
