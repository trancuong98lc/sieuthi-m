import { Platform, StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';

export const styles = StyleSheet.create({
    view: {
        justifyContent: 'flex-end',
        margin: 0
    },
    container: {
        backgroundColor: '#FFFFFF',
        flex: 1
    },
    header: {
        flexDirection: 'row',
        marginTop: 13,
        position: 'absolute'
    },
    line: {
        height: 8,
        backgroundColor: '#DCDCDC',
        marginTop: 10

    },
    headerimage1: {
        width: DimensionHelper.getScreenWidth() * 0.08,
        height: DimensionHelper.getScreenWidth() * 0.08,
        marginLeft: DimensionHelper.getScreenWidth() * 0.05
    },

    headerimage2: {
        //marginTop: -DimensionHelper.getScreenWidth() * 0.05,
        marginLeft: DimensionHelper.getScreenWidth() * 0.74,
        width: DimensionHelper.getScreenWidth() * 0.08,
        height: DimensionHelper.getScreenWidth() * 0.08
        //position: 'absolute',
    },
    headernumber: {
        backgroundColor: R.colors.primaryColor,
        width: DimensionHelper.getScreenWidth() * 0.056,
        height: DimensionHelper.getScreenWidth() * 0.056,
        borderRadius: DimensionHelper.getScreenWidth() * 0.04,
        marginLeft: DimensionHelper.getScreenWidth() * 0.715,
        marginTop: -DimensionHelper.getScreenWidth() * 0.01,
        borderWidth: 1,
        borderColor: R.colors.white100,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute'
    },
    headertext: {
        color: R.colors.white100,
        fontSize: 13
    },
    contentimage: {
        width: DimensionHelper.getScreenWidth(),
        height: DimensionHelper.getScreenHeight() * 0.4,
        //backgroundColor: 'red'
        //marginTop:DimensionHelper.getStatusBarHeight(),
        //marginHorizontal: 30,
        //position: 'absolute'
    },
    nameProduct: {
        fontFamily: R.fonts.bold,
        fontSize: 17,
        marginLeft: 20,
        marginTop: 30
    },
    unitProduct: {
        fontFamily: R.fonts.medium,
        fontSize: 14,
        marginLeft: 20,
        marginTop: 10,
        color: "#555555"
    },
    cost: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    newcost: {
        fontFamily: R.fonts.bold,
        fontSize: 22,
        marginTop: 15,
        color: R.colors.primaryColor,
        marginLeft: 20
    },
    oldcost: {
        fontFamily: R.fonts.bold,
        fontSize: 13,
        marginLeft: 15,
        marginTop: 15,
        color: '#9C9C9C',
        textDecorationLine: 'line-through'
    },
    productDetail: {
        fontFamily: R.fonts.medium,
        fontSize: 15,
        color: '#333333',
        marginLeft: 20,
        marginTop: 30
    },
    productDetailContent: {
        fontSize: 15,
        color: '#838383',
        marginHorizontal: 20,
        marginTop: 25,
        flex: 1
    },
    styleButton: {
        width: DimensionHelper.getScreenWidth() * 0.8,
        height: DimensionHelper.getScreenWidth() * 0.11,
        backgroundColor: R.colors.primaryColor,
        position: 'absolute',
        alignSelf: 'center',
        bottom: 10,
    },
    styleTextButton: {
        fontFamily: R.fonts.bold,
        fontSize: 13,
        color: R.colors.white100
    },
    modal: {
        backgroundColor: 'white',
        flex: 1,
        maxHeight: 320
    },
    modalHeader: {
        flexDirection: 'row',
        paddingLeft: 10,
    },

    imageModal: {
        height: 100,
        width: 100,
        borderWidth: 1,
        borderColor: '#E5E5E5',
        borderRadius: 5,
        marginTop: -50
    },
    decrease: {
        height: 50,
        width: 50,
        marginLeft: DimensionHelper.getScreenWidth() * 0.35
    },
    modalContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: 20,
        flex: 0.5
    },
    modalContenText: {
        fontFamily: R.fonts.bold,
        fontSize: 15
    },
    modalContenNumbder: {
        fontFamily: R.fonts.bold,
        fontSize: 15,
        marginHorizontal: 25
    },
    increase: {
        height: 50,
        width: 50
    },
    modalButton: {
        flexDirection: 'row',
        width: DimensionHelper.width,
        position: 'absolute',
        bottom: 0,
        height: 52 + (DimensionHelper.isIphoneX() ? 0 : DimensionHelper.getBottomSpace()),
        marginTop: DimensionHelper.getScreenHeight() * 0.1
    },
    addCart: {
        width: DimensionHelper.getScreenWidth() / 2,
        // height: 52,
        backgroundColor: R.colors.primaryColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 0
    },
    buyNow: {
        width: DimensionHelper.getScreenWidth() / 2,
        // height: 52,
        backgroundColor: '#0FA1E2',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 0
    },
    textButton: {
        fontFamily: R.fonts.bold,
        fontSize: 15,
        color: R.colors.white100
    }
});
