import { _formatPrice } from 'helpers/helper-funtion';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import Buttons from 'libraries/button/buttons';
import ImageSwiperComponent from 'libraries/Image/ImageSwiperComponent';
import { hideLoading, showLoading } from 'libraries/LoadingManager/LoadingModal';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextPrice from 'libraries/text/TextPrice';
import * as React from 'react';
import { Image, ScrollView, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import { Products } from 'types/category';
import { ProductDetailPresenter } from '../presenter/product-detail.presenter';
import HeaderDetailProduct from './components/product-detail-header';
import ModalAddProduct from './components/product-detail-modal-add-product';
import { styles } from './product-detail.style';
import { ProductDetailView } from './product-detail.view';


interface Props {
  route: any;
}
interface State {
  isModalVisible: boolean;
  productsDetail: Products | null;
  _id?: number;
}

export default class ProductDetailScreen extends React.PureComponent<Props, State>
  implements ProductDetailView {
  private presenter: ProductDetailPresenter;
  constructor(props: Props) {
    super(props);
    this.state = {
      isModalVisible: false,
      productsDetail: null,
      _id: props.route?.params?._id ?? undefined
    };
    this.presenter = new ProductDetailPresenter(this);
  }
  modalRef = React.createRef<ModalAddProduct>()
  quantity = this.props.route?.params?.quantity ?? undefined;
  componentDidMount(): void {
    this.onGetData();
  }

  private onGetData = (): void => {
    const { _id } = this.state;
    if (_id) {
      showLoading()
      this.presenter.onFetchProductsDetail(_id);
    }
  };

  onFetchDataSuccess(data: Products): void {
    hideLoading()
    if (data) {
      this.setState({ productsDetail: data });
    } else {
      showAlertByType({
        title: translate('notify.header'),
        message: translate('product-detail.not_suc'),
        options: [
          {
            text: translate('product-detail.close'),
            onPress: () => goBack()
          }
        ],
        onClose: () => { }
      });
    }
  }
  onFetchDataFail(error?: any): void {
    hideLoading()
    throw new Error('Method not implemented.');
  }

  showModal = () => {
    this.modalRef.current!.onOpen()
  }

  public render() {
    const { productsDetail } = this.state
    return (
      <Container statusBarColor="white">
        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
          <ImageSwiperComponent data={productsDetail?.medias || []} />
          <HeaderDetailProduct />
          <DailyMartText style={styles.nameProduct}>{this.state.productsDetail?.name}</DailyMartText>
          <DailyMartText style={styles.unitProduct}>{`${translate('unit')} ${this.state.productsDetail?.unit?.name}`}</DailyMartText>
          <View style={styles.cost}>
            {productsDetail ? <DailyMartText style={styles.newcost}>
              {`${_formatPrice(productsDetail.promoFor ? productsDetail.totalAll || 0 : productsDetail.price || 0)} ₫`}
            </DailyMartText> : null
            }
            {productsDetail && productsDetail.promoFor ?
              <TextPrice total={`   ${(productsDetail.price! || 0)} đ`} style={styles.oldcost} />
              : null}
          </View>
          <View style={styles.line} />
          <DailyMartText style={styles.productDetail}>
            {translate('product-detail.detail-name')}
          </DailyMartText>
          <DailyMartText style={styles.productDetailContent}>
            {this.state.productsDetail && this.state.productsDetail?.description || ''}
          </DailyMartText>

          <ModalAddProduct ref={this.modalRef} quantity={this.quantity} productsDetail={this.state.productsDetail} />
          <View style={{ height: 200 }} />
        </ScrollView>
        <Buttons
          testID={'modal-open-button'}
          styleButton={styles.styleButton}
          textButton={translate('button.addCart')}
          styleTextButton={styles.styleTextButton}
          onPress={this.showModal}
        />
      </Container>
    );
  }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
}
