import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import * as React from 'react';
import { View, StyleSheet, PushNotificationIOS, Platform } from 'react-native';
import * as RNLocalize from 'react-native-localize';
import { connect } from 'react-redux';
import { onUpdateCountReadNoti, updateQuantityCart, updateCountNoti, updateMessageCount } from 'redux/actions';
import { setI18nConfig, translate } from 'res/languages';
import { STATUS } from 'types/BaseResponse';
import OneSignal from 'react-native-onesignal';
import Config from 'react-native-config';
import { Notify, Payload, TYPE_NOTY } from 'features/notify/model/notify';
import { navigate, resetStack } from 'routing/service-navigation';
import { HomeScreen, QuesAndAnsScreen, RankBronzeScreen, ReceivedDetailScreen } from 'routing/screen-name';
import { showAlertByType } from 'libraries/BaseAlert';
import EventBus, { EventBusName } from 'helpers/event-bus';
import { User } from 'types/user-type';
import PushNotification from 'react-native-push-notification';

interface Props {
  updateQuantityCart: (quantity: number) => void;
  updateCountNoty: (quantity: number) => void;
  updateMessageCount: (messageCount: number) => void,
  messageCount: number,
  user: User
}

class RootComponent extends React.PureComponent<Props, any> {
  constructor(props: Props) {
    super(props);
    setI18nConfig();
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
    OneSignal.init(Config.ONESIGNAL_APP_ID, { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false });
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', this.onReceived);

    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.addEventListener('opened', this.onOpened);
  }



  onIds(device) {
    console.log('Device info: ', device);
    Commons.PlayerId = device.userId
  }


  private onReceived = (notification: any) => {
    if (!notification.payload) return
    if (!notification.payload.additionalData) return
    if (!notification.payload.additionalData.payload) return
    const item = notification.payload.additionalData.payload;
    if (item.type === TYPE_NOTY.MESSAGE_CMS) {
      const messageCount = this.props.messageCount;
      this.props.updateMessageCount(messageCount ? messageCount + 1 : 1);
    }
  };

  viewNotice = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.MAKE_NOTIFICATION, null, true)
      if (res.status === STATUS.SUCCESS) {
        this.props.updateCountNoty && this.props.updateCountNoty(res.data)
      }
    } catch (error) {

    }
  }

  onPressItem = (item: Notify) => {
    if (!item) return
    if (!item.payload) return
    const newItem: Payload = item.payload
    switch (newItem.type) {
      case TYPE_NOTY.RECEIVE:
      case TYPE_NOTY.RECEIVED:
      case TYPE_NOTY.CANCEL:
        this.viewNotice()
        navigate(ReceivedDetailScreen, { orderId: newItem.orderId, type: newItem.type })
        break;
      case TYPE_NOTY.PROMOTION:
        this.viewNotice()
        showAlertByType({
          title: translate('notify.header'),
          message: newItem.content || 'No Message',
          options: [{ text: 'OK' }],
          hideIcon: true
        });
        break;
      case TYPE_NOTY.HAPPY_BIRTHDAY:
        this.viewNotice()
        showAlertByType({
          title: newItem.title,
          message: newItem.content || 'No Message',
          options: [{ text: 'OK', onPress: () => { } }],
          hideIcon: true
        });
        break;
      case TYPE_NOTY.GENERAL:
        this.viewNotice()
        showAlertByType({
          title: translate('notify.header'),
          message: newItem.content || 'No Message',
          options: [{ text: 'OK', onPress: () => { } }],
          hideIcon: true
        });
        break;
      case TYPE_NOTY.USE_POINT:
        this.viewNotice()
        navigate(RankBronzeScreen)
        break;
      case TYPE_NOTY.MESSAGE_CMS:
        EventBus.getInstance().post({
          type: EventBusName.NEW_MESSAGE
        })
        navigate(QuesAndAnsScreen)
        break;
      default:
        this.viewNotice()
        break;
    }
  }

  private onOpened = (data: any) => {
    try {
      if (!data.notification) return
      if (!data.notification.payload) return
      if (!data.notification.payload.additionalData) return
      const item = data.notification.payload.additionalData
      this.onPressItem(item)
    } catch (error) {
      console.log('error: ', error);
    }
  };

  componentDidMount = () => {
    if (Commons.idStore) {
      this.getTotalProductCart(Commons.idStore)
      this.getCountNoty()
    }
  };

  getCountNoty = () => {
    ApiHelper.fetch(URL_API.COUNT_NOTIFICATION, null, true).then(res => {
      if (res.status == STATUS.SUCCESS && res.data) {
        this.props.updateCountNoty(res.data || 0)
      }
    })
  }


  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    // OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
  }

  getTotalProductCart = (idStore: String) => {
    ApiHelper.fetch(URL_API.CART_TOTAL_PRODUCT + idStore, null, true).then(res => {
      if (res.status == STATUS.SUCCESS) {
        this.props.updateQuantityCart(res.total || 0)
      }
    })
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  public render() {
    return (
      <View style={styles.container}>
        {this.props.children}
      </View>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  const { messageCount } = state.chat;
  return {
    user,
    messageCount
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateQuantityCart: (quantity: number) => dispatch(updateQuantityCart(quantity)),
    updateMessageCount: (messageCount: number) => dispatch(updateMessageCount(messageCount)),
    updateCountNoty: (quantity: number) => dispatch(updateCountNoti(quantity)),
  };
};

export default connect(mapStatesToProps, mapDispatchToProps)(RootComponent);
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});