import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import NewsDetailContainer from '../view/new-detail.screen';
import { ReqNews } from '../../new/model/news.type';

export class NewsDetailAdapter {
  private view: NewsDetailContainer;
  constructor(view: NewsDetailContainer) {
    this.view = view;
  }

  getNewsDetail = async (_id: string) => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        `${URL_API.HOTNEWS_DETAIL + _id}`,
        {},
        false
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setNewsDetail(res.data);
      } else {
        this.view.setNewsDetail(null);
      }
    } catch (error) {
        console.log("getNewsList: ", error);
        this.view.setNewsDetail(null);
    }
  };

  getNewsList = async (params: ReqNews) => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.HOTNEWS,
        params,
        false
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setNewsList(res.data);
      } else {
        this.view.setNewsList([]);
      }
    } catch (error) {
        console.log("getNewsList: ", error);
        this.view.setNewsList([]);
    }
  };
}
