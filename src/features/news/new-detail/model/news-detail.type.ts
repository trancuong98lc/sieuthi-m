
import { NavigationInitActionPayload } from 'react-navigation';
import { MediaType } from 'types/ConfigType';

export interface NewsDetail {
  _id: string,
  title: string,
  content: string,
  image: string,
  medias: MediaType[],
  createdAt: string
}

export interface ReqNews {
    _id: string
}
