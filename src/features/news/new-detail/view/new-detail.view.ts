import { NewDetail } from "../model/new-detail";

export interface NewsDetailView {
     onFetchDataSuccess(newDetail: NewDetail): void;
     onFetchDataFail(error?: any): void;
 }