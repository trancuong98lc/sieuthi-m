import tintuc from 'data/tintuc';
import FlatListYouCare from 'libraries/flatlist/flatlist-you-care';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { Dimensions, FlatList, Image, Platform, ScrollView, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { NewDetail } from '../model/new-detail';
import { styles } from './new-detail.style';
import { NewsDetailView } from './new-detail.view';
import { NewsDetailAdapter } from '../model/news-detail.adapter';
import { NewsDetail } from '../model/news-detail.type';
import { News } from '../../new/model/news.type';
import HeaderService from 'libraries/header/header-service';
import moment from 'moment';
import { getMediaUrl } from 'helpers/helper-funtion';
import HTML from "react-native-render-html";
import HTMLRender from '../../../component/HTMLRender';
import FastImage from 'react-native-fast-image';

export interface NewsDetailProps {
  route: any
}

export interface NewsDetailStates {
  newsDetail: NewsDetail | null,
  newsList: News[],
  webViewHeight: number
}

export default class NewDetailScreen extends React.PureComponent<NewsDetailProps, NewsDetailStates>
  implements NewsDetailView {
  private adapter: NewsDetailAdapter;
  private refScrollView: any;
  private newsId: string;
  private fullNewsList: News[]

  constructor(props: NewsDetailProps) {
    super(props);
    this.adapter = new NewsDetailAdapter(this);
    this.newsId = props.route?.params?._id ?? undefined;
    this.refScrollView = React.createRef();
    this.fullNewsList = [];
    this.state = {
      newsDetail: null,
      newsList: [],
      webViewHeight: 0
    }
  }

  params = {
    page: 1,
    limit: 6,
    status: true
  }

  componentDidMount = () => {
    this.adapter.getNewsDetail(this.newsId);
    this.adapter.getNewsList(this.params);
  }

  setNewsDetail = (newsDetail: NewsDetail | null) => {
    this.setState({
      newsDetail
    })
  }

  setNewsList = (newsList: News[]) => {
    if(!this.fullNewsList.length) {
      this.fullNewsList = newsList;
    }
    this.setState({
      newsList: this.filterNewsList(this.fullNewsList)
    })
  }

  getOtherNewsDetail = (newsId: string) => {
    this.newsId = newsId;
    this.adapter.getNewsDetail(this.newsId);
    this.setNewsList(this.state.newsList);
    this.refScrollView.scrollTo(0);
  }

  filterNewsList = (newsList: News[]) => {
    const newsId = this.newsId;
    newsList = newsList.filter(function (item) {
      return item._id !== newsId;
    });
    return newsList;
  }

  onFetchDataSuccess(newDetail: NewDetail): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  renderItem = ({ item, index }: { item: News, index: number }) => {
    return <FlatListYouCare item={item} index={index} getOtherNewsDetail={this.getOtherNewsDetail} />
  }

  keyExtractor = (item: News, index: number) => {
    return item._id.toString();
  }

  onNavigationStateChange = (navState: any) => {
    this.setState({
      webViewHeight: Number(navState.title)
    });
  }

  setRefScrollView = (refScrollView: any) => {
    return this.refScrollView = refScrollView;
  }

  public render() {
    const { newsDetail, newsList, webViewHeight } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <HeaderService title={'news.newDetail'} />
          <ScrollView ref={this.setRefScrollView} showsVerticalScrollIndicator={false}>
            <View>
              <DailyMartText style={styles.nameNew}>
                {newsDetail?.title}
              </DailyMartText>
              <DailyMartText style={styles.dateNew}>
                {moment(newsDetail?.createdAt).format('l')}
              </DailyMartText>
              <FastImage style={styles.imageNew1} source={getMediaUrl(newsDetail?.medias && newsDetail?.medias.length > 0 ? newsDetail?.medias[0] : undefined)} />
              <HTMLRender content={newsDetail?.content} baseFontStyle={{ color: '#333333', lineHeight: 21, fontSize: 14 }} />
            </View>
            <View style={styles.youCare}>
              <View style={styles.hr}></View>
              <DailyMartText style={styles.youCareText}>
                {translate('news.youCare')}
              </DailyMartText>
              <FlatList
                data={newsList}
                style={styles.flatlistYouCare}
                keyExtractor={this.keyExtractor}
                scrollEnabled={false}
                renderItem={this.renderItem}
              />
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}
