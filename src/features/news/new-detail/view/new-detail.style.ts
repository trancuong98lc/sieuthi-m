import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";
import colors from "res/colors";

export const styles = StyleSheet.create({
     container: {
          backgroundColor:R.colors.white100
     },
     header: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          alignItems: 'center',
          
     },
     buttonBack: {
          backgroundColor: R.colors.primaryColor,
          
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop:  5,
          marginBottom:12
     },
     new: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop:  5,
          marginBottom:10,
          marginLeft:DimensionHelper.getScreenWidth()*0.26
     },
     imageNew1: {
          width: DimensionHelper.getScreenWidth() * 0.937,
          height:DimensionHelper.getScreenWidth() * 0.4,
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:12,
     },
     nameNew: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#444444',
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:15
     },
     dateNew: {
          fontSize: 13,
          color: '#828282',
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:10
     },
     contentStyle: {
          paddingHorizontal: 10, 
          paddingTop: 10
     },
     contentTextStyle: {
          color: "#333333",
          fontSize: 14
     },
     contentNewDetail: {
          color: '#333333',
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:10,
     },
     youCare: {
          marginTop: 20
     },
     hr: {
          borderTopWidth: 10,
          borderColor: '#F2F2F2',
       
     },
     youCareText: {
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.031,
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#444444',
          marginTop:10
     },
     flatlistYouCare: {
          marginTop:10,
          marginBottom:DimensionHelper.getBottomSpace()+DimensionHelper.getScreenHeight()*0.08
     }
})