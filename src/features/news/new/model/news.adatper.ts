import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import NewsContainer from '../view/news.screen';
import { ReqNews } from '../model/news.type';

export class NewsAdapter {
  private view: NewsContainer;
  constructor(view: NewsContainer) {
    this.view = view;
  }

  getNewsList = async (params: ReqNews) => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.HOTNEWS,
        params,
        false
      );
      if (res.status == STATUS.SUCCESS) {
        let data = params.page > 1 ? [...this.view.state.news, ...res.data] : res.data;
        this.view.setNewsList(data, data.length === res.total);
      } else {
        this.view.setNewsList([], false);
      }
    } catch (error) {
        console.log("getNewsList: ", error);
        this.view.setNewsList([], false);
    }
  };
}
