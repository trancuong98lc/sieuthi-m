
import { NavigationInitActionPayload } from 'react-navigation';
import { MediaType } from 'types/ConfigType';

export interface News {
  _id: string,
  title: string,
  content: string,
  image: string,
  medias: MediaType[]
  createdAt: string
}

export interface ReqNews {
  page: number,
  limit: number,
  title?: string,
  status?: boolean
}
