import FlatListTintuc from 'libraries/flatlist/flatlist-home/flatlist.tintuc';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
  View
} from 'react-native';
import R from 'res/R';
import { NewDetailScreen } from 'routing/screen-name';
import { News } from '../model/news.type';
import { styles } from './news.style';
import { NewsView } from './news.view';
import { NewsAdapter } from '../model/news.adatper';
import HeaderService from 'libraries/header/header-service';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import { navigate } from 'routing/service-navigation';
import { getMediaUrl } from 'helpers/helper-funtion';

export interface NewsProps {}

export interface NewsStates {
  news: News[],
  loading: boolean,
  maxdata: boolean
}

export default class NewsScreen extends React.PureComponent<NewsProps, NewsStates>
  implements NewsView {
  private adapter: NewsAdapter;
  private onEndReachedCalledDuringMomentum: boolean;
  constructor(props: NewsProps) {
    super(props);
    this.state = {
      news: [],
      loading: true,
      maxdata: true
    }
    this.onEndReachedCalledDuringMomentum = true;
    this.adapter = new NewsAdapter(this);
  }

  onFetchDataSuccess(news: News): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  params: any = {
    page: 1,
    limit: 10,
    status: true
  }

  componentDidMount = () => {
    this.adapter.getNewsList(this.params);
  }

  setNewsList = (news: News[], maxdata: boolean) => {
    this.setState({
      news,
      loading: false,
      maxdata
    })
  }
  gotoNewsDetail = () => {
    navigate(NewDetailScreen, {
      _id: this.state.news[0]._id
    })
  }

  onRefresh = () => {
    const newParams =
    {
      limit: 10,
      page: 1,
      status: true
    }
    this.params = newParams;
    this.adapter.getNewsList(newParams);
  }

  onEndReached  = () => {
    const { loading, maxdata } = this.state;
    if (loading || this.onEndReachedCalledDuringMomentum || maxdata) {
      return;
    }
    const newParasm: any = {
      ...this.params,
      page: this.params.page + 1
    }
    this.params = newParasm
    this.adapter.getNewsList(newParasm);
    this.onEndReachedCalledDuringMomentum = true;
  }

  keyExtractor = (item: News, index: number) => {
    return item._id.toString();
  }

  renderItem = ({ item, index }: { item: News, index: number }) => {
    if(index === 0) {
      return (
        <>
        <TouchableOpacity onPress={this.gotoNewsDetail} style={styles.firstNews}>
            <FastImage style={styles.imageNews1} source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)} />
            <DailyMartText style={styles.nameNews}>
              {item.title}
            </DailyMartText>
            <DailyMartText style={styles.dateNews}>
              {moment(item.createdAt).format('l')}
            </DailyMartText>
        </TouchableOpacity>
        <View style={styles.hr} />
        </>
      )
    } else {
      return <FlatListTintuc item={item} index={index} />;
    }
  }

  onMomentumScrollBegin = () => {
    this.onEndReachedCalledDuringMomentum = false; 
  }

  emptyListComponent = () => {
    if(!this.state.loading) {
      return <EmptylistComponent />;
    } else {
      return null;
    }
  }

  footerComponent = () => {
    if(!this.state.maxdata) {
      return <DailyMartText style={styles.footerFlatList}>Đang tải...</DailyMartText>;
    } else {
      return null;
    }
  }

  public render() {
    const { news, loading } = this.state;
    const firstNews = news[0];

    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <HeaderService title={'news.new'} />
            <FlatList
              data={news}
              style={styles.flatlistTintuc}
              renderItem={this.renderItem}
              ListEmptyComponent={this.emptyListComponent}
              ListFooterComponent={this.footerComponent}
              refreshing={loading}
              onRefresh={this.onRefresh}
              onEndReached={this.onEndReached}
              onEndReachedThreshold={0.01}
              keyExtractor={this.keyExtractor}
              showsVerticalScrollIndicator={false}
              onMomentumScrollBegin={this.onMomentumScrollBegin}
            ></FlatList>
        </View>
      </Container>
    );
  }
}
