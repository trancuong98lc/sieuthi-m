import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from 'helpers/dimension-helper';
import colors from "res/colors";

export const styles = StyleSheet.create({
     container: {
          backgroundColor:R.colors.white100
     },
     news: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 5,
          marginBottom:10,
          marginLeft:DimensionHelper.getScreenWidth()*0.34
     },
     firstNews: {
     },
     imageNews1: {
          width: DimensionHelper.getScreenWidth() * 0.937,
          height:DimensionHelper.getScreenWidth() * 0.4,
          marginLeft: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:12,
     },
     nameNews: {
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color: "#444444",
          marginLeft: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:10
     },
     dateNews: {
          fontSize: 12,
          color: '#818181',
          marginLeft: DimensionHelper.getScreenWidth() * 0.031,
          marginTop:5
     },
     hr: {
          borderTopWidth: 1,
          borderColor: "#E5E5E5",
         marginVertical:10
     },
     flatlistTintuc: {
          marginTop: 15,
          backgroundColor: '#FFFFFF',
          marginBottom:DimensionHelper.getBottomSpace()+DimensionHelper.getScreenHeight()*0.08
     },
     footerFlatList: {
          marginVertical: 10, 
          textAlign: "center", 
          color: R.colors.primaryColor
     }
});