import { News } from "news/new/model/news";

export interface NewsView {
     onFetchDataSuccess(news: News): void;
     onFetchDataFail(error?: any): void;
 }