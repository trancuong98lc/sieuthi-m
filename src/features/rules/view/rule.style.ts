import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
     container: {
          backgroundColor:R.colors.white100,
          flex: 1,
          marginBottom:-DimensionHelper.getBottomSpace()
     },
     header: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          paddingBottom:10
     },
     buttonBack: {
          backgroundColor:R.colors.primaryColor
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop:  10,
          marginLeft:12
     },
     headerTitle: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginLeft:DimensionHelper.getScreenWidth()*0.15
     },
     contentRule: {
          marginTop: 26,
          marginHorizontal:12,
          fontSize: 14,
          lineHeight: 21,
          color:'#333333'
     }
     


})