import HTMLRender from 'features/component/HTMLRender';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { ScrollView } from 'react-native';
import R from 'res/R';
import { RuleAdapter } from '../model/rule.adapter';
import { styles } from './rule.style';
import { RuleView } from './rule.view';

export interface Props { }
export interface State {
  description: string
}

export default class RuleScreen extends React.PureComponent<Props, State>
  implements RuleView {
    private adapter: RuleAdapter;
  constructor(props: Props) {
    super(props);
    this.state = {
      description: ''
    }
    this.adapter = new RuleAdapter(this);
  }

  onFetchDataSuccess(search: import('../model/rule').Rule): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  componentDidMount = () => {
    this.adapter.getRule();
  }

  setRule = (description: string) => {
    this.setState({
      description
    })
  }

  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
          <HeaderService title={'personalNotLogin.rule'} />
          <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false} contentContainerStyle={{paddingBottom: 30}}>
            {/* <HTMLRender content={this.state.description} containerStyle={{ paddingTop: 15 }} baseFontStyle={{ color: '#333333', lineHeight: 21, fontSize: 14 }} /> */}
            <DailyMartText style={styles.contentRule}>
              {this.state.description}
            </DailyMartText>
          </ScrollView>
      </Container>
    );
  }
}
