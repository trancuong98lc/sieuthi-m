import { Rule } from "rules/model/rule";

export interface RuleView {
     onFetchDataSuccess(search: Rule): void;
     onFetchDataFail(error?: any): void;
 }
 