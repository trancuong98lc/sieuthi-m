import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import RuleScreen from '../view/rule.screen';

export class RuleAdapter {
  private view: RuleScreen;
  constructor(view: RuleScreen) {
    this.view = view;
  }

  getRule = async () => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.CONDITION_DELIVERY,
        {},
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setRule(res.data.description);
      } else {
      
      }
    } catch (error) {
        console.log("getRule: ", error);
    }
  };
}
