export interface Rule {
    _id: string,
    content: string,
    createdAt: string,
    deletedAt: string,
    status: string
}
