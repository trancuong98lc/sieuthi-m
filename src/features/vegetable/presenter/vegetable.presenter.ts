import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { VegetableView } from 'features/vegetable/view/vegetable.view';
import { STATUS } from 'types/BaseResponse';
import { ReqCategory } from 'features/home/model/home';
import { ProductReq } from '../model/vegetable';
import Commons from 'helpers/Commons';

export class VegetablePresenter {
  private vegetableView: VegetableView;

  constructor(vegetableView: VegetableView) {
    this.vegetableView = vegetableView;
  }

  async onFetchCategory(params: ReqCategory) {
    try {
      const res = await ApiHelper.fetch(URL_API.CATEGORIES, params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data && res.data.length > 0) {
          this.vegetableView.onFetchDataCateGorySuccess(
            res.data,
            true,
            res.total || 0
          );
        } else {
          this.vegetableView.onFetchDataCateGorySuccess([], false, 0);
        }
      } else {
        this.vegetableView.onFetchDataCateGorySuccess([], false, 0);
      }
    } catch (error) {
      console.log('error: ', error);
      this.vegetableView.onFetchDataCateGorySuccess([], false, 0);
      // this.homeView.onFetchDataFail();
    }
  }

  async onGetDataProduct(params: ProductReq) {
    try {
      const res = await ApiHelper.fetch(
        URL_API.PRODUCT + Commons.idStore,
        params,
        true
      );
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length > 0) {
          this.vegetableView.onFetchDataProductSuccess(res.data, true);
        } else {
          this.vegetableView.onFetchDataProductSuccess([], false);
        }
      } else {
        this.vegetableView.onFetchDataProductSuccess([], false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.vegetableView.onFetchDataProductSuccess([], false);
      // this.homeView.onFetchDataFail();
    }
  }
}
