export interface Vegetable {
  isModalVisible: false;
}

export interface ProductReq {
  page: number;
  limit: number;
  query?: string;
  categoryId?: string;
  categoryIds?: string[];
  featured?: string;
  new?: string;
  status?: string;
  storeId?: string;
  minPrice?: number;
  maxPrice?: number;
  promotionId?: string;
  search: string
}

export const STATUS_SEARCH = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE'
};
