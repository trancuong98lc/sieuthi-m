import { DimensionHelper } from 'helpers/dimension-helper';
import { StyleSheet } from 'react-native';
import R from 'res/R';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1,
    marginBottom: DimensionHelper.getBottomSpace() + 150
  },
  header: {
    width: '100%',
    backgroundColor: R.colors.primaryColor,
    alignItems: 'center',
    flexDirection: 'row'
  },
  bage: { width: 20, height: 20, borderRadius: 9999, backgroundColor: 'red', position: 'absolute', top: 5, right: -5, alignItems: 'center', justifyContent: 'center' },
  imageback: {
    height: 0.09 * DimensionHelper.getScreenWidth(),
    width: 0.09 * DimensionHelper.getScreenWidth(),
    marginTop: 0.035 * DimensionHelper.getScreenHeight(),
    marginHorizontal: 0.03 * DimensionHelper.getScreenWidth()
  },
  styleTextInputs: {
    marginTop: 50,
    marginBottom: 20
  },
  styleTextInput: {
    marginLeft: 0.1 * DimensionHelper.getScreenWidth(),
    fontSize: 13,
    width: 220,
    color: '#8D8D8D',
    padding: 0,
    position: 'absolute'
  },
  styleImageTextInput: {
    height: 15,
    width: 15,
    marginLeft: 18
  },
  imagefilter: {
    height: 0.038 * DimensionHelper.getScreenWidth(),
    width: 0.038 * DimensionHelper.getScreenWidth(),
    marginTop: 0.04 * DimensionHelper.getScreenHeight(),
    marginHorizontal: 0.045 * DimensionHelper.getScreenWidth()
  },
  item: {
    flexDirection: 'row',
    alignContent: 'space-between'
  },
  itemChild: {
    flexDirection: 'row'
  },
  itemText: {
    fontSize: 15,
    fontFamily: R.fonts.medium,
    color: '#3BBB5A',
    marginHorizontal: 0.095 * DimensionHelper.getScreenWidth(),
    marginVertical: 0.03 * DimensionHelper.getScreenWidth()
  },
  itemStyle1: {
    borderWidth: 0.5,
    height: 2,
    borderColor: '#3BBB5A',
    backgroundColor: '#3BBB5A'
  },
  itemStyle2: {
    borderWidth: 0.5,
    height: 17,
    borderColor: '#D4D4D4',
    marginVertical: 0.03 * DimensionHelper.getScreenWidth()
  },
  flatlistVegetable: {
    backgroundColor: '#F7F7F8',
    paddingHorizontal: 5,
    flex: 1
  },

  view: {
    flex: 1,
    margin: 0,
    marginLeft: 50
  },
  modal: {
    backgroundColor: 'white',
    flex: 1,
    height: DimensionHelper.height * 2,
    padding: 20,
  },
  filterHeader: {
    flexDirection: 'row',
    marginTop: DimensionHelper.getStatusBarHeight()
  },
  filterHeaderImagebutton: {
    width: 18,
    height: 18
  },
  filterHeaderText: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: R.colors.primaryColor,
    marginLeft: DimensionHelper.getScreenWidth() * 0.27
  },
  portfolio: {
    flexDirection: 'row',
    marginTop: 30
  },
  title1: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: '#333333'
  },
  buttonTitle1: {
    width: 10,
    height: 10,
    marginTop: 3,
    marginLeft: 10
  },
  buttonTitle1Text: {
    fontSize: 13,
    color: '#3BBB5A',
    marginLeft: 0.45 * DimensionHelper.getScreenWidth()
  },
  title2: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    marginVertical: 10,
    color: '#333333'
  },
  aboutCost: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  filterMin: {
    maxWidth: DimensionHelper.width / 2 - 50,
    height: 30,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: '#F1F1F1'
  },
  filterFooterTextinput: {
    width: '100%',
    height: 40,
    alignItems: 'center',
    color: '#4A4A4A'
    //justifyContent: 'center',
  },
  filterMax: {
    maxWidth: DimensionHelper.width / 2 - 50,
    paddingHorizontal: 10,
    height: 30,
    justifyContent: 'center',
    backgroundColor: '#F1F1F1'
  },
  reSetting: {
    width: DimensionHelper.getScreenWidth() * 0.44,
    // height: 40,
    borderTopWidth: 0.5,
    borderColor: '#909090',
    borderRadius: 0,
    // marginLeft: -20
  },
  search: {
    width: DimensionHelper.getScreenWidth() * 0.44,
    height: DimensionHelper.getScreenWidth() * 0.16,
    backgroundColor: R.colors.primaryColor,
    borderRadius: 0
  },
  filterFooterText: {
    fontSize: 15
  }
});
