import ProductItem from 'features/component/product-item-promotion';
import { ProductReq } from 'features/vegetable/model/vegetable';
import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import FlatlistVegetable from 'libraries/flatlist/flatlist.vegetable';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { View, StyleSheet, Text, FlatList, ListRenderItem, ActivityIndicator } from 'react-native';
import { translate } from 'res/languages';
import { STATUS } from 'types/BaseResponse';
import { Products, SEARCH_YES_NO, STATUS_SEARCH } from 'types/category';
import { styles } from '../vegetable.style';
import _ from 'lodash'
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';

export interface TabSellingProps {
    paramsProduct: ProductReq
}

export interface TabSellingStates {
    products: Products[],
    maxdata: boolean;
    loading: boolean,
}

export default class TabSelling extends React.PureComponent<TabSellingProps, TabSellingStates> {
    constructor(props: TabSellingProps) {
        super(props);
        this.state = {
            products: [],
            maxdata: false,
            loading: true,
        }
        this.getData = _.debounce(this.getData, 300)
    }

    paramsProduct: ProductReq = {
        ...this.props.paramsProduct,
        limit: 30,
        page: 1,
        status: STATUS_SEARCH.ACTIVE,
        featured: SEARCH_YES_NO.YES,
        storeId: Commons.idStore,
    }

    componentDidMount = () => {
        this.onReFresh()
    };


    onReFresh = () => {
        this.paramsProduct.page = 1
        this.getData(this.paramsProduct)

    }

    setLoading = () => {
        this.setState({
            loading: true
        })
    }

    getData = async (params: ProductReq) => {
        try {
            const res = await ApiHelper.fetch(
                URL_API.PRODUCT + Commons.idStore,
                params,
                true
            );
            if (res.status == STATUS.SUCCESS) {
                let maxdata = res.data.length == 0 || res.data.length < 30 ? true : false;
                const data = params.page > 1 ? [...this.state.products, ...res.data] : res.data

                this.setState({
                    maxdata,
                    products: data,
                    loading: false
                })
            } else {
                this.setState({
                    maxdata: true,
                    loading: false
                })
            }
        } catch (error) {
            this.setState({
                loading: false
            })
            // this.homeView.onFetchDataFail();
        }
    }
    isLoadmore: boolean = false;
    onEndReached = () => {
        let { maxdata } = this.state;
        if (maxdata) return;
        if (this.isLoadmore) return;
        if (this.state.products.length == 0 && this.state.products.length < 30) return
        this.isLoadmore = true;
        const newParasm: ProductReq = {
            ...this.paramsProduct,
            page: this.paramsProduct.page + 1
        }
        this.paramsProduct = newParasm
        this.getData(this.paramsProduct)
    }



    private keyExtractor = (item: Products): string => item._id.toString();

    private renderItem: ListRenderItem<Products> = ({ item, index }) => {
        return <ProductItem item={item} index={index} />;
    };

    renderFooter = () => <View style={{ height: 100 }} />


    ListEmptyComponent = () => {
        if (this.state.products.length > 0 || this.state.loading) return null;
        return (
            <EmptylistComponent />
        );
    };

    public render() {
        return (
            <FlatList
                data={this.state.products}
                numColumns={2}
                onRefresh={this.onReFresh}
                refreshing={this.state.loading}
                showsVerticalScrollIndicator={false}
                extraData={this.state}
                key="selling"
                style={styles.flatlistVegetable}
                onEndReached={this.onEndReached}
                ListFooterComponent={this.renderFooter}
                ListEmptyComponent={this.ListEmptyComponent}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
            />
        );
    }
}
