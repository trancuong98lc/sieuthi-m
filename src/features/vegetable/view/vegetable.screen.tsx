import Buttons from 'libraries/button/buttons';
import FlatListDanhmucsanpham from 'libraries/flatlist/flatlist-filter-product-portfolio';
import FlatlistVegetable from 'libraries/flatlist/flatlist.vegetable';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import {
  FlatList,
  Image,
  ListRenderItem,
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import Modal from 'react-native-modal';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import { Products, STATUS_SEARCH, SEARCH_YES_NO } from 'types/category';
import { VegetablePresenter } from 'features/vegetable/presenter/vegetable.presenter';
import danhmucsanpham from 'data/danhmucsanpham';
import { DimensionHelper } from 'helpers/dimension-helper';
import { styles } from './vegetable.style';
import { TabView, TabBar } from 'react-native-tab-view';
import { VegetableView } from './vegetable.view';
import { Category, ReqCategory } from 'features/home/model/home';
import HeaderSearchVegetTable from './components/HeaderSearchVegetTable';
import Container from 'libraries/main/container';
import { ProductReq } from '../model/vegetable';
import Commons from 'helpers/Commons';
import { SearchCommon, SearchNew, SearchSelling } from '../../../routing/screen-name';
import TabCommon from './TabViewScreen/TabCommon';
import TabNew from './TabViewScreen/TabNew';
import TabSelling from './TabViewScreen/TabSelling';
import _ from 'lodash'
import HistorySearch from 'features/search/view/filter-history-search';

interface Props {
  route: any;
}
interface State {
  isModalVisible: boolean;
  // vegetable: Vegetable;
  products: Products[];
  category?: Category;
  index: any
  loading: boolean
  isLoadMore: boolean
  maxdata: boolean;
  query: string,
  routes: { key: string, title: string }[]
}
export default class VegetableScreen extends React.PureComponent<Props, State> implements VegetableView {
  index: any =
    this.props.route &&
      this.props.route.params &&
      this.props.route.params.index
      ? this.props.route.params.index
      : 0;

  paramsCate: ReqCategory = {
    limit: '8',
    page: '1',
    status: STATUS_SEARCH.ACTIVE
  };

  paramsProduct: ProductReq = {
    limit: 30,
    page: 1,
    status: STATUS_SEARCH.ACTIVE,
    storeId: Commons.idStore,
  }
  private presenter: VegetablePresenter;

  refTabComon = React.createRef<TabCommon>()
  refTabNew = React.createRef<TabNew>()
  refTabSelling = React.createRef<TabSelling>()


  constructor(props: Props) {
    super(props);
    this.state = {
      isModalVisible: false,
      products: [],
      maxdata: false,
      category: props.route?.params?.category ?? undefined,
      query: props.route?.params?.query ?? "",
      loading: true,
      isLoadMore: true,
      index: this.index,
      routes: [
        {
          key: SearchCommon,
          title: 'vegetable.text1',
        },
        {
          key: SearchNew,
          title: 'vegetable.text2',
        },
        {
          key: SearchSelling,
          title: 'vegetable.text3',
        },
      ],
    };
    if (props.route?.params?.category?._id) {
      this.paramsProduct = { ...this.paramsProduct, categoryIds: [props.route?.params?.category?._id || ''] }
    }
    if (props.route?.params?.query) {
      this.paramsProduct = { ...this.paramsProduct, query: props.route?.params?.query }
    }
    this.presenter = new VegetablePresenter(this);
    this.onSeacrch = _.debounce(this.onSeacrch, 500)
  }





  refHeader = React.createRef<HeaderSearchVegetTable>()

  componentDidMount(): void {
    this.onGetCateGory()
    this.refHeader.current && this.refHeader.current!.onSetValue(this.state.query)
  }



  setLoading = (loading: boolean) => {
    this.setState({
      loading
    })
  }

  // onGetData = (): void => {
  //   const { category } = this.state;
  //   this.setLoading(true)
  //   this.presenter.onGetDataProduct(this.paramsProduct);
  // };

  onGetCateGory = (): void => {
    this.presenter.onFetchCategory(this.paramsCate)
  };



  onFetchDataCateGorySuccess(data: Category[], isViewmore: boolean, total: number): void {
    if (this.paramsCate.page == '1') {
      this.refHeader.current?.onSetCateGory(data)
      this.refHeader.current?.setTotalCate(total)
      this.refHeader.current?.onSetViewMore(isViewmore)
      return
    }
    this.refHeader.current?.onSetCateGory([...this.refHeader.current.state.CateGoryList, ...data], total)
    this.refHeader.current?.onSetViewMore(isViewmore)
  }


  onFetchDataSuccess(data: Products[]): void {
    if (data.length == 0) {
      this.setLoading(false)
    }
    let maxdata =
      data.length < 30;
    this.setState({ products: [...this.state.products, ...data], maxdata });
  }

  onFetchDataProductSuccess(data: any[], isLoadMore: boolean): void { }

  onSubmit = (data: any) => {
    this.paramsProduct = data
    if (this.state.index == 0) {
      const newParams = {
        ...this.refTabComon.current!.paramsProduct,
        ...this.paramsProduct
      }
      if (!data.minPrice) delete newParams.minPrice
      if (!data.maxPrice) delete newParams.maxPrice
      this.paramsProduct = data
      this.refTabComon.current!.paramsProduct = newParams
      this.refTabComon.current!.setLoading()
      this.refTabComon.current!.getData(newParams)
    }
    if (this.state.index == 1) {
      const newParams = {
        ...this.refTabNew.current!.paramsProduct,
        ...this.paramsProduct
      }
      if (!data.minPrice) delete newParams.minPrice
      if (!data.maxPrice) delete newParams.maxPrice
      this.paramsProduct = data
      this.refTabNew.current!.paramsProduct = newParams
      this.refTabComon.current!.setLoading()
      this.refTabNew.current!.getData(newParams)
    }
    if (this.state.index == 2) {
      const newParams = {
        ...this.refTabSelling.current!.paramsProduct,
        ...this.paramsProduct
      }
      if (!data.minPrice) delete newParams.minPrice
      if (!data.maxPrice) delete newParams.maxPrice
      this.paramsProduct = data
      this.refTabSelling.current!.paramsProduct = newParams
      this.refTabComon.current!.setLoading()
      this.refTabSelling.current!.getData(newParams)
    }
    // this.presenter.onGetDataProduct(this.paramsProduct)
  }

  onViewMoreCate = (data: any) => {

    const newParams: ReqCategory = {
      ...this.paramsCate,
      page: typeof data.page == 'string' ? data.page : String(Number(this.paramsCate.page) + data.page)
    }
    this.paramsCate = newParams
    this.presenter.onFetchCategory(this.paramsCate)
  }

  renderScene = ({ route }: any) => {
    switch (route.key) {
      case SearchCommon:
        return (
          <TabCommon paramsProduct={this.paramsProduct} ref={this.refTabComon} />
        );

      case SearchNew:
        return (
          <TabNew paramsProduct={this.paramsProduct} ref={this.refTabNew} />
        );

      case SearchSelling:
        return (
          <TabSelling paramsProduct={this.paramsProduct} ref={this.refTabSelling} />
        );

      default:
        return null;
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };



  onTabPressed = (index: number) => () => {
    this.setState({ index }, () => {
      if (this.state.index == 0) {
        const newParams = {
          ...this.refTabComon.current!.paramsProduct,
          ...this.paramsProduct
        }
        this.refTabComon.current!.paramsProduct = newParams
        this.refTabComon.current!.getData(newParams)
      }
      if (this.state.index == 1) {
        const newParams = {
          ...this.refTabNew.current!.paramsProduct,
          ...this.paramsProduct
        }
        this.refTabNew.current!.paramsProduct = newParams
        this.refTabNew.current!.getData(newParams)
      }
      if (this.state.index == 2) {
        const newParams = {
          ...this.refTabSelling.current!.paramsProduct,
          ...this.paramsProduct
        }

        this.refTabSelling.current!.paramsProduct = newParams
        this.refTabSelling.current!.getData(newParams)
      }
    });
  };

  onSeacrch = (search: string) => {
    const newParams = {
      ...this.paramsProduct,
      query: search
    }
    this.paramsProduct = newParams
    if (this.state.index == 0) {
      const newParams = {
        ...this.refTabComon.current!.paramsProduct,
        ...this.paramsProduct
      }
      this.refTabComon.current!.paramsProduct = newParams
      this.refTabComon.current!.getData(newParams)
    }
    if (this.state.index == 1) {
      const newParams = {
        ...this.refTabNew.current!.paramsProduct,
        ...this.paramsProduct
      }
      this.refTabNew.current!.paramsProduct = newParams
      this.refTabNew.current!.getData(newParams)
    }
    if (this.state.index == 2) {
      const newParams = {
        ...this.refTabSelling.current!.paramsProduct,
        ...this.paramsProduct
      }

      this.refTabSelling.current!.paramsProduct = newParams
      this.refTabSelling.current!.getData(newParams)
    }
  }


  renderTabBar = (props: any) => {
    let { routes, index } = props.navigationState;
    return (
      <View style={styles.itemChild}>
        {routes.map((route: any, positionFocus: number) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this.onTabPressed(positionFocus)}
            style={[
              styles2.btnLabel,
              positionFocus === this.state.index &&
              styles2.btnActive,

            ]}
          >
            <DailyMartText
              fontStyle="semibold"
              style={[
                styles2.label,
                positionFocus === this.state.index &&
                styles2.labelActive,
              ]}
            >
              {translate(route.title)}
            </DailyMartText>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  public render() {
    const { category } = this.state
    return (
      <Container>
        <HeaderSearchVegetTable
          paramsCate={this.paramsCate}
          paramsProduct={this.paramsProduct}
          ref={this.refHeader}
          category={category}
          onSeacrch={this.onSeacrch}
          onSubmit={this.onSubmit}
          onViewMoreCate={this.onViewMoreCate}
        />
        <TabView
          navigationState={this.state}
          renderScene={this.renderScene}
          renderTabBar={this.renderTabBar}
          onIndexChange={index => this.setState({ index })}
          initialLayout={styles2.inititalLayout}
          lazy
        />
      </Container >
    );
  }
}

const styles2 = StyleSheet.create({
  btnLabel: {
    justifyContent: 'center',
    alignItems: 'center',
    width: DimensionHelper.width / 3,
    paddingVertical: 13,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#eee'
  },
  btnActive: {
    borderBottomWidth: 2,
    borderBottomColor: R.colors.primaryColor
  },
  label: {
    fontSize: 14,
    alignContent: 'center',
    textAlign: 'center',
    color: '#111'
  },
  labelActive: {
    color: R.colors.primaryColor
  },
  inititalLayout: {
    width: DimensionHelper.width,
    height: 0
  }
});
