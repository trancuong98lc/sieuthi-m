import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, ViewStyle, TextStyle } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import { Category } from 'features/home/model/home';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { color } from 'react-native-reanimated';
import colors from 'res/colors';
import DailyMartText from 'libraries/text/text-daily-mart';
export interface ItemCateGorySearchProps {
  item: Category;
  index: number;
  touchableOpacity?: ViewStyle;
  text?: TextStyle;
  onPressItemCate?: (item: Category, index: number) => void
}

export default class ItemCateGorySearch extends React.Component<ItemCateGorySearchProps, any> {
  constructor(props: ItemCateGorySearchProps) {
    super(props);
  }

  onPressItem = () => {
    const { item, index, onPressItemCate } = this.props;
    onPressItemCate && onPressItemCate(item, index)
  }

  public render() {
    const { item, index } = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={this.onPressItem}
        style={[styles.container, { marginLeft: index % 2 == 0 ? 0 : 10, backgroundColor: item.active ? colors.primaryColor : "#F1F1F1" }]}
      >
        <DailyMartText numberOfLines={2} style={[styles.name, { color: item.active ? 'white' : '#4A4A4A' }]}>{item.name}</DailyMartText>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 40,
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: '#F1F1F1',
    marginTop: 10,
    paddingHorizontal: 10,
    maxWidth: DimensionHelper.width / 2 - 50,
    justifyContent: 'center'
  },
  name: {
    fontSize: 13,
  }
});
