import { Category, ReqCategory } from 'features/home/model/home';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  Animated,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import images from 'res/images';
import Modal from 'react-native-modal';
import { translate } from 'res/languages';
import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import { DimensionHelper } from 'helpers/dimension-helper';
import { goBack, navigate } from 'routing/service-navigation';
import colors from 'res/colors';
import { styles } from '../vegetable.style';
import FlatListDanhmucsanpham from 'libraries/flatlist/flatlist-filter-product-portfolio';
import ItemCateGorySearch from './ItemCateGorySearch';
import { ProductReq } from 'features/vegetable/model/vegetable';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { SearchScreen } from 'routing/screen-name';
import Commons from 'helpers/Commons';
import { showAlert } from 'libraries/BaseAlert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import BaseToast, { ToastType } from 'libraries/BaseToast/BaseToast';
import { showToast } from 'libraries/BaseToast';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';

export interface HeaderSearchVegetTableProps {
  category?: Category;
  onSubmit: (data: any) => void;
  onSeacrch: (search: string) => void;
  onViewMoreCate: (data: any) => void;
  paramsProduct: ProductReq;
  paramsCate: ReqCategory;
}

interface HeaderSearchVegetTableStates {
  isVisible: boolean;
  showKeyborad: boolean;
  CateGoryList: Category[];
  minPrice: string;
  maxPrice: string;
  search: string;
  viewMore: boolean | null;
  totalCate: number;
}

export default class HeaderSearchVegetTable extends React.PureComponent<
  HeaderSearchVegetTableProps,
  HeaderSearchVegetTableStates
> {
  constructor(props: HeaderSearchVegetTableProps) {
    super(props);
    this.state = {
      isVisible: false,
      CateGoryList: [],
      totalCate: 0,
      viewMore: null,
      showKeyborad: false,
      minPrice: '',
      search: '',
      maxPrice: ''
    };
  }

  onDraw = () => {
    this.setState({
      isVisible: true
    });
  };

  onSetCateGory = (CateGoryList: Category[]) => {
    this.setState({ CateGoryList });
  };

  onSetViewMore = (viewMore: boolean) => {
    this.setState({
      viewMore
    });
  };

  toggleModal = () => {
    this.setState({
      isVisible: false
    });
  };

  setTotalCate = (totalCate: number) => {
    this.setState({
      totalCate: totalCate || 0
    });
  };

  componentDidUpdate = (
    prevProps: HeaderSearchVegetTableProps,
    prevState: HeaderSearchVegetTableStates
  ) => {
    if (prevProps.category && prevState.isVisible !== this.state.isVisible) {
      this.onPressCate(prevProps.category!);
      this.setTotalCate(prevState.totalCate);
    }
  };
  onShowKeyBoard = () => {
    this.setState({ showKeyborad: true });
  };

  onHideKeyBoard = () => {
    this.setState({ showKeyborad: false });
  };

  onDissmiss = () => {
    Keyboard.dismiss();
  };

  onPressCate = (item: Category, index?: number) => {
    const { CateGoryList } = this.state;
    const newCateGory = [...CateGoryList];
    newCateGory.map((e) => {
      if (e && e._id == item._id) {
        e.active = !e.active ? true : false;
      }
      return e;
    });
    this.onSetCateGory(newCateGory);
  };

  resetSearch = () => {
    const { category } = this.props;
    const { CateGoryList } = this.state;
    const newCateGory = [...CateGoryList];
    newCateGory.map((e) => {
      if (category && e._id != category?._id) {
        e.active = false;
      }
      return e;
    });

    this.onSetCateGory(newCateGory);
    this.setState({
      minPrice: '',
      maxPrice: ''
    });
  };
  getDataStringIds = (data: Category[]) => {
    const data2: any[] = data.map((e: Category) => {
      if (e.active) return e._id;
    });
    return data2 || [];
  };

  handelSubmit = () => {
    const { onSubmit } = this.props;
    const { minPrice, maxPrice, CateGoryList } = this.state;
    if (!minPrice) delete this.props.paramsProduct.minPrice;
    if (!maxPrice) delete this.props.paramsProduct.maxPrice;
    const newParams: ProductReq = {
      ...this.props.paramsProduct
    };
    console.log(
      'HeaderSearchVegetTable -> handelSubmit -> newParams',
      newParams
    );
    if (minPrice) newParams.minPrice = Number(minPrice);
    if (maxPrice) newParams.maxPrice = Number(maxPrice);
    if (Number(minPrice) && Number(maxPrice)) {
      if (Number(minPrice) > Number(maxPrice)) {
        this.toggleModal();
        return setTimeout(() => {
          showAlert('not_price', { success: false });
        }, 300);
      }
    }
    const cate = CateGoryList.filter((e: Category) => {
      if (e.active) return { id: e._id };
    });
    if (newParams.categoryIds && newParams.categoryIds.length > 0) {
      newParams.categoryIds =
        cate.length > 0 ? this.getDataStringIds(cate) : newParams.categoryIds;
    }
    if (cate.length > 0) {
      newParams.categoryIds = this.getDataStringIds(cate);
    }
    onSubmit && onSubmit(newParams);
    this.toggleModal();
  };

  onChangeText = (key: any) => (value: any) => {
    if (key == 'search') {
      this.props.onSeacrch && this.props.onSeacrch(value);
    }
    this.setState({ [key]: value });
  };

  onSetValue = (value: string) => {
    this.setState({
      search: value
    });
  };

  onShowMore = () => {
    const { onViewMoreCate } = this.props;
    onViewMoreCate && onViewMoreCate({ page: this.state.viewMore ? 1 : '1' });
  };

  keyExtrator = (item: Category, index: number) => item._id.toString();

  renderItem = ({ item, index }: { item: Category; index: number }) => {
    return (
      <ItemCateGorySearch
        onPressItemCate={this.onPressCate}
        item={item}
        index={index}
      />
    );
  };

  footer = () => {
    if (this.state.CateGoryList.length < 8) return null;
    return (
      <View style={styles.portfolio}>
        <Buttons
          textButton={
            this.state.viewMore
              ? translate('view_more')
              : translate('view_less')
          }
          source={this.state.viewMore ? images.ic_all : images.ic_less}
          styleImageButton={styles.buttonTitle1}
          onPress={this.onShowMore}
          styleTextButton={{ color: colors.primaryColor }}
          styleButton={{
            alignItems: 'center',
            width: '100%',
            justifyContent: 'center'
          }}
        />
      </View>
    );
  };

  gotoSearch = () => {
    navigate(SearchScreen);
  };

  onPressRight = () => {
    this.setState({ search: '' });
  };

  public render() {
    const { category } = this.props;
    const { isVisible, CateGoryList, minPrice, maxPrice, search } = this.state;
    return (
      <View style={stylesw.header}>
        <BaseIcon
          name="ic_back2"
          width={35}
          iconStyle={{ flex: 1 }}
          onPress={goBack}
        />
        {/* <TouchableOpacity onPress={this.gotoSearchScreen} activeOpacity={0.6}> */}
        <TextInputs
          placeholder={translate('textinput.searchvegetable', {
            name:
              category && category?.name
                ? category?.name
                : Commons.store && Commons.store.name
                ? Commons.store.name
                : 'DailyMark'
          })}
          showDeleteText
          styleTextInputs={stylesw.styleTextInputs}
          styleTextInput={stylesw.styleTextInput}
          source={images.ic_search}
          editable={true}
          value={search}
          onPressRight={this.onPressRight}
          // onTouchStart={this.gotoSearch}
          onChangeText={this.onChangeText('search')}
          styleImageTextInput={stylesw.styleImageTextInput}
        />
        {/* </TouchableOpacity> */}

        <BaseIcon
          name="ic_filter"
          width={20}
          iconStyle={{ flex: 1 }}
          onPress={this.onDraw}
        />
        <Modal
          onBackdropPress={this.toggleModal}
          animationIn="slideInRight"
          animationOut="slideOutRight"
          style={[stylesw.container]}
          isVisible={isVisible}
        >
          <TouchableWithoutFeedback onPress={this.onHideKeyBoard}>
            <KeyboardAwareScrollView
              showsVerticalScrollIndicator={false}
              enableOnAndroid
              enableAutomaticScroll
              style={styles.modal}
            >
              <View style={{ flex: 1, height: DimensionHelper.height * 0.7 }}>
                <View style={styles.filterHeader}>
                  <Buttons
                    source={images.ic_close}
                    styleImageButton={styles.filterHeaderImagebutton}
                    onPress={this.toggleModal}
                  />
                  <DailyMartText style={styles.filterHeaderText}>
                    {translate('filter.header')}
                  </DailyMartText>
                </View>
                <View style={styles.portfolio}>
                  <DailyMartText style={styles.title1}>
                    {`${translate('filter.title1')} ( ${
                      this.state.totalCate
                    } )`}
                  </DailyMartText>
                  {/* <Buttons
                                    textButton={translate('button.all')}
                                    source={images.ic_all}
                                    styleImageButton={styles.buttonTitle1}
                                    styleTextButton={styles.buttonTitle1Text}
                                /> */}
                </View>
                <View>
                  <FlatList
                    data={CateGoryList}
                    numColumns={2}
                    keyExtractor={this.keyExtrator}
                    extraData={this.state}
                    ListFooterComponent={this.footer}
                    renderItem={this.renderItem}
                  />
                </View>

                <DailyMartText style={styles.title2}>
                  {translate('filter.title2') + ' (đ)'}
                </DailyMartText>
                <View style={styles.aboutCost}>
                  <TextInputs
                    styleTextInput={styles.filterFooterTextinput}
                    styleTextInputs={styles.filterMin}
                    value={minPrice}
                    autoFocus={false}
                    returnKeyType="done"
                    onSubmitEditing={this.onHideKeyBoard}
                    onTouchStart={this.onShowKeyBoard}
                    onChangeText={this.onChangeText('minPrice')}
                    placeholder={translate('textinput.min')}
                  />
                  <TextInputs
                    styleTextInput={styles.filterFooterTextinput}
                    styleTextInputs={styles.filterMax}
                    value={maxPrice}
                    autoFocus={false}
                    returnKeyType="done"
                    onSubmitEditing={this.onHideKeyBoard}
                    onTouchStart={this.onShowKeyBoard}
                    onChangeText={this.onChangeText('maxPrice')}
                    placeholder={translate('textinput.max')}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </TouchableWithoutFeedback>
          {!this.state.showKeyborad && (
            <View
              style={{
                flexDirection: 'row',
                position: 'absolute',
                bottom: 0,
                right: -1
              }}
            >
              <Buttons
                styleButton={styles.reSetting}
                styleTextButton={[
                  styles.filterFooterText,
                  { color: colors.primaryColor }
                ]}
                textButton={translate('button.resetting')}
                onPress={this.resetSearch}
              />
              <Buttons
                styleButton={styles.search}
                styleTextButton={[
                  styles.filterFooterText,
                  { color: colors.white100 }
                ]}
                textButton={translate('button.search')}
                onPress={this.handelSubmit}
              />
            </View>
          )}
        </Modal>
      </View>
    );
  }
}

const stylesw = StyleSheet.create({
  header: {
    backgroundColor: colors.primaryColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    paddingTop: getStatusBarHeight() - 10,
    paddingBottom: 12
  },
  styleTextInputs: {
    paddingHorizontal: 12,
    height: 35,
    marginHorizontal: 12,
    width: '100%',
    flex: 1
  },
  container: {
    borderColor: 'rgba(0, 0, 0, 0.01)',
    margin: 0,
    flex: 0,
    bottom: 0,
    position: 'absolute',
    width: DimensionHelper.getScreenWidth() - 50,
    backgroundColor: colors.white100,
    paddingBottom: DimensionHelper.getBottomSpace(),
    top: 0,
    right: 0
  },
  styleTextInput: {
    fontSize: 13,
    color: '#8D8D8D',
    alignItems: 'center',
    flex: 1,
    maxHeight: 35
  },

  styleImageTextInput: {
    height: 15,
    width: 15
  }
});
