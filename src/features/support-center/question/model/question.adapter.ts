import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import QuestionContainer from '../view/question.screen';

export class QuestionAdapter {
  private view: QuestionContainer;
  constructor(view: QuestionContainer) {
    this.view = view;
  }

  getSupport = async () => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.SUPPORT,
        {},
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setSupport(res.data);
      } else {
        
      }
    } catch (error) {
        console.log("getSupport: ", error);
        this.view.setSupport([]);
    }
  };

  getContact = async () => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.CONTACT,
        {},
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setContact(res.data);
      } else {
        
      }
    } catch (error) {
        console.log("getSupport: ", error);
    }
  };
}
