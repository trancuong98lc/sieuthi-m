import Buttons from 'libraries/button/buttons';
import FlatListSupport from 'libraries/flatlist/flatlist-support';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import {
  FlatList,
  Image,
  ListRenderItem,
  View,
  Linking,
  Text,
  TouchableOpacity
} from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { AnswerScreen, QuesAndAnsScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { Support } from 'types/support';
import support from 'data/support';
import { styles } from './question.style';
import { QuestionView } from './question.view';
import HeaderService from 'libraries/header/header-service';
import FastImage from 'react-native-fast-image';
import colors from 'res/colors';
import { QuestionAdapter } from '../model/question.adapter';
import { Contact } from 'features/contact/model/contact';
import NoInternet from 'libraries/noInternet/NoInternet';

export interface Props {}
interface State {
  loading: boolean;
  keyWord: string;
  support: Support[];
  contact: Contact;
}
export default class QuestionScreen extends React.PureComponent<Props, State>
  implements QuestionView {
  private adapter: QuestionAdapter;
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: true,
      keyWord: '',
      support: [],
      contact: {
        phoneNumber: '',
        email: ''
      }
    };
    this.adapter = new QuestionAdapter(this);
  }

  isConnected: boolean | null = true;

  onFetchDataSuccess(data: Support): void {
    //this.setState({ support: data });
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  componentDidMount = () => {
    this.adapter.getSupport();
    this.adapter.getContact();
  };

  setSupport = (support: Support[]) => {
    this.setState({
      support
    });
  };

  setContact = (contact: Contact) => {
    this.setState({
      contact: {
        phoneNumber: contact.phoneNumber,
        email: contact.email
      }
    });
  };

  private onPressItem = (item: Support, index: number) => (): void => {
    navigate(AnswerScreen, {
      questionSelected: index,
      _id: item._id,
      support: this.state.support
    });
  };

  gotoChat = () => {
    navigate(QuesAndAnsScreen);
  };


  sendEmail = () => {
    const { email } = this.state.contact;
    if (email !== '') {
      Linking.openURL(`mailto:${email}`);
    }
  };

  makePhoneCall = () => {
    const { phoneNumber } = this.state.contact;
    if (phoneNumber !== '') {
      Linking.openURL(`tel:${phoneNumber}`);
    }
  };

  private keyExtractor = (item: Support): string => item._id.toString();

  private renderItem: ListRenderItem<Support> = ({ item, index }) => {
    return (
      <FlatListSupport
        item={item}
        index={index}
        selected={[]}
        onShow={this.onPressItem(item, index)}
      />
    );
  };

  private itemSeparator = () => {
    return <View style={styles.FlatListSupportItemSeparator} />;
  };
  // private ListHeaderComponent = (): any => {
  //     const { keyWord, support,loading  } = this.state;
  //     if (loading || keyWord.trim().length > 0) return null;
  //     return (
  //         <>
  //             <FaqHeader data={support} />
  //             {support.length > 0 && <View style={styles.viewSpace} />}
  //         </>
  //     );
  // };
  public render() {
    const { support, contact } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View>
          <HeaderService title={'support.header'} />
          <FlatList
            data={support}
            style={styles.FlatListSupport}
            showsHorizontalScrollIndicator={false}
            alwaysBounceVertical={false}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={this.itemSeparator}
          />
          <View style={styles.content}>
            <TouchableOpacity style={styles.item} onPress={this.gotoChat}>
              <FastImage
                style={styles.itemImage}
                source={R.images.ic_chatSupport}
              />
              <DailyMartText style={styles.itemText}>
                {translate('support.chat')}
              </DailyMartText>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item} onPress={this.sendEmail}>
              <FastImage
                style={styles.itemImage}
                source={R.images.ic_emailSupport}
              />
              <DailyMartText style={styles.itemText}>
                {translate('support.mail')}
              </DailyMartText>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item} onPress={this.makePhoneCall}>
              <FastImage
                style={styles.itemImage}
                source={R.images.ic_phoneSupport}
              />
              <DailyMartText style={styles.itemText}>
                {translate('support.phone')}
              </DailyMartText>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
}
