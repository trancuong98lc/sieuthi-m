import { Question } from "../model/question";
import { Support } from "types/support";

export interface QuestionView {
     onFetchDataSuccess(data: Support): void;
     onFetchDataFail(error?: any): void;
 }