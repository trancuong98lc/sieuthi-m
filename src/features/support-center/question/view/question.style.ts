import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";
import colors from "res/colors";

export const styles = StyleSheet.create({
     
     header: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          alignItems: 'center',
          //paddingBottom:20
          
     },
     buttonBack: {
          backgroundColor: R.colors.primaryColor,
          
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop:  10,
          marginBottom: 13,
          marginLeft:12
     },
     new: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginBottom:10,
          marginLeft:DimensionHelper.getScreenWidth()*0.18
     },
     FlatListSupport: {
          maxHeight: DimensionHelper.getScreenHeight() * 0.5,
          marginTop:10,
     },
     FlatListSupportItemSeparator: {
          borderTopWidth: 1, 
          borderTopColor: 
          colors.grey200, width: "100%"
     },
     content: {
          marginTop: 10,
          backgroundColor: R.colors.white100,
          height:DimensionHelper.getScreenHeight()*0.4,
          borderTopWidth: 10,
          borderTopColor: colors.grey200
     },
     item: {
          flexDirection: 'row',
          alignItems: 'center',
          marginTop:20,
     },
     itemImage: {
          height: 30,
          width: 30,
          marginLeft:20
     },
     itemText: {
          marginLeft: 15,
          fontSize: 15,
          color:'#333333'
     }
})