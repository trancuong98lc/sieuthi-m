import FlatListSupport from 'libraries/flatlist/flatlist-support';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import * as React from 'react';
import { FlatList, ListRenderItem, View } from 'react-native';
import R from 'res/R';
import { Support } from 'types/support';
import { styles } from './answer.style';
import { AnswerView } from './answer.view';

export interface Props {
  route: any;
  item: Support;
}
export interface State {
  support: Support[];
  id: number;
  questionSelected?: Support;
  loading: boolean;
  selectedItem: number[]
}
export default class AnswerScreen extends React.PureComponent<Props, State>
  implements AnswerView {
  private refQuestions: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      support: props.route?.params?.support ?? undefined,
      id: props.route?.params?._id ?? undefined,
      loading: true,
      selectedItem: [props.route?.params?.questionSelected ?? undefined],
    };
    this.refQuestions = React.createRef();
  }

  onFetchQuestionByGroupSuccess(arg0: any, arg1: boolean) {
    throw new Error('Method not implemented.');
  }
  onFetchDataSuccess(data: Support): void {
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  private onPressItem = (index: number) => (): void => {
    let selectedItem = [...this.state.selectedItem];
    if(selectedItem.indexOf(index) < 0) {
      selectedItem = [...selectedItem, ...[index]];
    }
    this.setState({
      loading: false,
      selectedItem: selectedItem
    });
    this.refQuestions.scrollToIndex({animated: true, index: index});
  };

  private onHideItem = (index: number) => (): void => {
    let selectedItem = [...this.state.selectedItem];
    selectedItem.splice(selectedItem.indexOf(index), 1);
    this.setState({
      loading: false,
      selectedItem: selectedItem
    });
  }

  private keyExtractor = (item: Support): string => item._id.toString();
  
  private renderItemQuestion: ListRenderItem<Support> = ({ item, index }) => {
    return (
      <FlatListSupport
        item={item}
        index={index}
        selected={this.state.selectedItem}
        onShow={this.onPressItem(index)}
        onHide={this.onHideItem(index)}
      />
    );
  };

  setRefQuestions = (refQuestions: any) => {
    this.refQuestions = refQuestions;
  }

  public render() {
    const { support } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <HeaderService title={'support.header'} />
            <FlatList
              data={support}
              ref={this.setRefQuestions}
              extraData={this.state}
              style={styles.FlatListSupport}
              showsHorizontalScrollIndicator={false}
              renderItem={this.renderItemQuestion}
              keyExtractor={this.keyExtractor}
              showsVerticalScrollIndicator={false}
            />
          </View>
      </Container>
    );
  }
}
