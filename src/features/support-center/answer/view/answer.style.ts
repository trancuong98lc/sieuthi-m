import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          flex: 1,
       paddingBottom: DimensionHelper.getBottomSpace()   
     },
     header: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          alignItems: 'center',
          //paddingBottom:20
          
     },
     buttonBack: {
          backgroundColor: R.colors.primaryColor,
          
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop:  10,
          marginBottom: 13,
          marginLeft:12
     },
     new: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginBottom:10,
          marginLeft:DimensionHelper.getScreenWidth()*0.18
     },
     answer: {
          //height: DimensionHelper.getScreenHeight() * 0.42,
          //borderWidth:1,
          paddingHorizontal: 12,
          backgroundColor: R.colors.white100,
          // marginTop:10
     },
     questionDetail: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#333333',
          //height: 50,
          marginTop: 22,
          paddingBottom:20
          
     },
     hrAnswer: {
          borderTopWidth: 1,
          borderColor: "#E5E5E5",
          marginLeft: -12,
       width:DimensionHelper.getScreenWidth()
     },
     answerText: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: '#333333',
          marginTop:18
     },
     answerDetail: {
          fontSize: 14,
          color: '#333333',
          marginTop: 15,
          lineHeight:23,
          // maxHeight: 0.5 * DimensionHelper.getScreenHeight()
     },
     HRAnswer: {
          marginTop:20,
          height: 8,
          backgroundColor: '#F1F1F1',
          marginLeft: -12,
       width:DimensionHelper.getScreenWidth()
     },
     FlatListSupport: {
          // marginBottom: -DimensionHelper.getBottomSpace(),
     },
     
})