import { Support } from "types/support";

export interface AnswerView {
     onFetchDataSuccess(data: Support): void;
     onFetchDataFail(error?: any): void;
 }