import { Support } from 'types/support';
import { AnswerView } from '../view/answer.view';

export class AnswerPresenter {
    private view: AnswerView;

    constructor(view: AnswerView) {
        this.view = view;
    }

    async onFetchQuestionByGroup(
        params: Support
    ): Promise<any> {
     //    try {
     //        const res = await ApiClient.query(GET_OFTEN_QUESTIONS, params);
     //        this.view.onFetchQuestionByGroupSuccess(
     //            res?.data?.getOftenQuestion?.items || [],
     //            !!params.after
     //        );
     //    } catch (error) {
     //        console.log('error: ', error);
     //        this.view.onFetchQuestionByGroupSuccess([], !!params.after);
     //    }
    }
}
