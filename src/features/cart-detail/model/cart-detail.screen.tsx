import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';

export interface AppProps {
}

export default class App extends React.PureComponent<AppProps, any> {
  constructor(props: AppProps) {
    super(props);
  }

  public render() {
    return (
      <View>
         <Text>App</Text>
      </View>
    );
  }
}
