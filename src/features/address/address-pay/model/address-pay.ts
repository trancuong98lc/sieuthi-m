export interface AddrPay {}
export interface AdressPay {
  city: City;
  cityId: string;
  createdAt: string;
  createdBy: string;
  deletedAt?: string;
  detail: string;
  district: City;
  districtId: string;
  isDefault: boolean;
  checked: boolean;
  item: boolean;
  lat: number;
  lon: number;
  receiverName: string;
  receiverPhone: string;
  subreceiverPhone?: string;
  updatedAt: string;
  ward: City;
  wardId: string;
  _id: string;
}

interface City {
  _id: String;
  name: string;
}
