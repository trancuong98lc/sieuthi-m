import { Platform, StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";
import { getBottomSpace } from "react-native-iphone-x-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom: -DimensionHelper.getBottomSpace()
     },
     addrPayHeader: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          paddingBottom: 10
     },
     addrPayHeaderImage: {
          width: 35,
          height: 35,
          marginTop: 10,
          marginLeft: 10
     },
     addrPayHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 10,
          marginLeft: DimensionHelper.getScreenWidth() * 0.2,

     },
     buttonAddAddr: {
          width: DimensionHelper.getScreenWidth() * 0.53,
          height: DimensionHelper.getScreenWidth() * 0.11,
          backgroundColor: R.colors.primaryColor,
          position: 'absolute',
          alignSelf: 'center',
          bottom: Platform.OS == 'ios' ? getBottomSpace() + 15 : 10,
     },
     textButtonAddAddr: {
          fontFamily: R.fonts.bold,
          fontSize: 13,
          color: R.colors.white100
     },



})