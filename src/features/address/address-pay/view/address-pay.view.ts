import { AddrPay } from "../model/address-pay";

export interface AddrPayView {
     onFetchDataSuccess(addrPay: AddrPay): void;
     onFetchDataFail(error?: any): void;
 }
 