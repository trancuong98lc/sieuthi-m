import customer from 'data/customer';
import ApiHelper from 'helpers/api-helper';
import EventBus, { EventBusName, EventBusType } from 'helpers/event-bus';
import { URL_API } from 'helpers/url-api';
import { reset } from 'i18n-js';
import Buttons from 'libraries/button/buttons';
import FlatListAddrPay from 'libraries/flatlist/flatlist.addr-pay';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, ListRenderItem, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { AddAddrPayScreen } from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { Subscription } from 'rxjs';
import { STATUS } from 'types/BaseResponse';
import { Customer } from 'types/category';
import { AddrPay, AdressPay } from '../model/address-pay';
import { styles } from './address-pay.style';
import { AddrPayView } from './address-pay.view';
import Swipeout from 'react-native-swipeout';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import FastImage from 'react-native-fast-image';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import TabHistoryReceived from 'features/history-order/received/view/components/TabHistoryReceived';

export interface Props {
  route: any;
}
export interface State {
  key?: string;
  valueAddress?: AdressPay;
  maxdata: boolean;
  isSelectAddr?: boolean;
  loading: boolean;
  dataAdress: AdressPay[];
  UserPhone: any;
}
export default class AddrPayScreen extends React.PureComponent<Props, State>
  implements AddrPayView {
  constructor(props: Props) {
    super(props);
    this.state = {
      key: props.route?.params?.key ?? undefined,
      maxdata: false,
      loading: true,
      UserPhone: null,
      valueAddress: props.route?.params?.valueAddress ?? undefined,
      isSelectAddr: true,
      dataAdress: []
    };
  }

  params = {
    limit: '15',
    page: '1'
  };
  onEndReachedCalledDuringMomentum = true;
  subScription = new Subscription();

  screen =
    this.props.route &&
    this.props.route.params &&
    this.props.route.params.screen
      ? this.props.route.params.screen
      : null;

  onFetchDataSuccess(addrPay: AdressPay[]): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }
  async componentDidMount() {
    await this.getUserSubPhone();
    this.subScription.add(
      EventBus.getInstance().events.subscribe(async (data: EventBusType) => {
        if (data.type == EventBusName.UPDATE_ADRESS && data.payload) {
          await this.getUserSubPhone();
          this.updateAdress(data.payload);
          return;
        }
        if (data.type == EventBusName.REMOVE_ADRESS_DETAIL && data.payload) {
          this.updateRemoveAdressCreate(data.payload);
          return;
        }
        if (data.type == EventBusName.CREATE_ADDRESS && data.payload) {
          await this.getUserSubPhone();
          this.updateAdressCreate(data.payload);
          return;
        }
      })
    );
    const key = this.state.key;
    this.onReFresh();
    //StatusBar.setBackgroundColor('white');
  }

  componentWillUnmount = () => {
    this.subScription && this.subScription.unsubscribe();
  };

  getUserSubPhone = async () => {
    let UserPhone = await AsyncStorageHelpers.getObject(
      StorageKey.PHONE_NUMBER_SUB
    );
    this.setState({
      UserPhone
    });
  };

  setAdressWithParams = (item: AdressPay) => {
    const { dataAdress } = this.state;
    const index = this.state.dataAdress.findIndex((e) => e._id == item._id);
    const newData = [...dataAdress];
    const newDataw: any = newData.map((e) => {
      if (e._id === item._id) {
        e.checked = true;
        return e;
      }
      if (e._id !== item._id) {
        e.checked = false;
        return e;
      }
    });
    this.setState({
      dataAdress: newDataw
    });
  };

  setAdress = (item: AdressPay) => {
    const { dataAdress } = this.state;
    const setValueCart = this.props.route?.params?.setValueCart ?? undefined;
    if (setValueCart) {
      setValueCart(item);
    }
    const newData = [...dataAdress];
    const newDataw: any = newData.map((e) => {
      if (e._id === item._id) {
        e.checked = true;
        return e;
      }
      if (e._id !== item._id) {
        e.checked = false;
        return e;
      }
    });
    this.setState(
      {
        dataAdress: newDataw
      },
      () => {
        goBack();
      }
    );
  };

  updateAdress = (data: AdressPay) => {
    const { dataAdress } = this.state;
    const index = this.state.dataAdress.findIndex((e) => e._id == data._id);
    const newData = [...dataAdress];
    if (data._id) {
      const newDataw: any = newData.map((e) => {
        if (e._id === data._id) {
          e = { ...e, ...data };
          return e;
        }
        if (e._id !== data._id && !data.isDefault) {
          return e;
        }
        if (e._id !== data._id && data.isDefault) {
          e.isDefault = false;
          return e;
        }
      });
      this.setState({
        dataAdress: newDataw
      });
    } else {
      this.onReFresh();
    }
  };

  updateAdressCreate = (data: AdressPay) => {
    const { dataAdress } = this.state;
    const newData = [data, ...dataAdress];
    this.setState({
      dataAdress: newData
    });
  };

  onReFresh = () => {
    this.params.page = '1';
    this.onGetListAddress(this.params);
  };

  onGetListAddress = async (params: any) => {
    try {
      const res = await ApiHelper.fetch(URL_API.LIST_DELIVERY, params, true);
      if (res.status === STATUS.SUCCESS && res.data) {
        let maxdata =
          res.data.length == 0 || res.data.length < 10 ? true : false;
        const data =
          Number(params.page) > 1
            ? [...this.state.dataAdress, ...res.data]
            : res.data;
        this.setState(
          {
            dataAdress: data,
            maxdata,
            loading: false
          },
          () => {
            if (this.state.valueAddress) {
              this.setAdressWithParams(this.state.valueAddress);
            }
          }
        );
      } else {
        this.onFetchDataSuccess([]);
        this.setState({
          maxdata: true,
          loading: false
        });
      }
    } catch (error) {
      this.setState({
        loading: false
      });
    }
  };

  isLoadmore: boolean = false;
  onEndReached = () => {
    let { maxdata } = this.state;
    if (maxdata) return;
    if (this.isLoadmore) return;
    this.isLoadmore = true;
    const newParasm: any = {
      ...this.params,
      page: String(Number(this.params.page) + 1)
    };
    this.params = newParasm;
    this.onGetListAddress(this.params);
  };

  keyExtractor = (item: AdressPay): string => item._id.toString();
  renderDelete = () => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <View
          style={{
            width: '100%',
            aspectRatio: 1 / 1,
            backgroundColor: '#E5334B',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={R.images.ic_trash_white}
            style={{ width: 25, height: 25 }}
          />
        </View>
      </View>
    );
  };
  updateRemoveAdressCreate = (data: AdressPay) => {
    const { dataAdress } = this.state;
    const newData = [...dataAdress];
    const newData2 = newData.filter((e) => e._id !== data._id);
    this.setState(
      {
        dataAdress: newData2
      },
      () => {
        EventBus.getInstance().post({
          type: EventBusName.REMOVE_ADRESS,
          payload: data
        });
      }
    );
  };

  removeAddress = async (item: AdressPay) => {
    try {
      showLoading();
      const res = await ApiHelper.deletes(
        URL_API.UPDATE_DELIVERY + item._id,
        null,
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.updateRemoveAdressCreate(item);
        showAlert('address.delete_succes', { success: true });
        hideLoading();
      } else {
        hideLoading();
        showAlert('address.delete_err');
      }
    } catch (error) {
      showAlert('address.delete_err');
      hideLoading();
    }
  };

  renderCustomer: ListRenderItem<AdressPay> = ({ item, index }) => {
    if (!item.checked) item['checked'] = false;
    return (
      <Swipeout
        autoClose
        sectionId={index}
        right={[
          {
            text: 'delete',
            color: 'black',
            backgroundColor: 'white',
            type: 'delete',
            onPress: () => {
              showAlertByType({
                title: translate('notify.header'),
                message: translate('del_add'),
                options: [
                  {
                    text: 'OK',
                    onPress: () => {
                      this.removeAddress(item);
                    }
                  }
                ]
              });
            },
            component: this.renderDelete()
          }
        ]}
        backgroundColor="white"
      >
        <FlatListAddrPay
          UserPhone={this.state.UserPhone}
          screen={this.screen}
          setAdress={this.setAdress}
          item={item}
          index={index}
        />
      </Swipeout>
    );
  };

  renderEmpty = () => {
    if (this.state.dataAdress.length > 0 || this.state.loading) return null;
    return <EmptylistComponent name={'address.no_address'} />;
  };

  listFooter = () => {
    return <View style={{ height: 100 }} />;
  };
  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService title="addrPay.addrShip" />
        <FlatList
          data={this.state.dataAdress}
          onRefresh={this.onReFresh}
          refreshing={this.state.loading}
          extraData={this.state}
          onEndReachedThreshold={0.01}
          removeClippedSubviews
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          keyExtractor={this.keyExtractor}
          ListFooterComponent={this.listFooter}
          ListEmptyComponent={this.renderEmpty}
          onEndReached={this.onEndReached}
          renderItem={this.renderCustomer}
        />

        <Buttons
          styleButton={styles.buttonAddAddr}
          styleTextButton={styles.textButtonAddAddr}
          textButton={translate('button.addAddr')}
          onPress={() => navigate(AddAddrPayScreen)}
        />
      </Container>
    );
  }
}
