import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom: -DimensionHelper.getBottomSpace()
     },
     line: {
          borderWidth: 8,
          width: 450,
          borderColor: '#EBEBEB'
     },
     addrPayHeader: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          paddingBottom: 10
     },
     addrPayHeaderImage: {
          width: 35,
          height: 35,
          marginTop: 10,
          marginLeft: 10
     },
     addrPayHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 15,
          marginLeft: DimensionHelper.getScreenWidth() * 0.2,

     },
     infor: {
          marginTop: 11,

     },
     name: {
          flexDirection: 'row',
          marginTop: 14,
          marginLeft: 20,

          alignItems: 'center'
     },
     nameText: {
          fontSize: 14,
          color: '#555555',
     },
     nameTextInputs: {
          backgroundColor: "#FFFFFF",
          // flex: 1,
          // marginLeft: DimensionHelper.getScreenWidth() * 0.22,
          paddingRight: 0,
          height: 30,

     },
     nameTextInput: {
          fontSize: 14,
          fontFamily: R.fonts.medium,
          minWidth:DimensionHelper.getScreenWidth() * 0.5,
          textAlign: 'right',
          padding: 0,
     },

     buttonAddAddr: {
          width: DimensionHelper.getScreenWidth() * 0.53,
          height: DimensionHelper.getScreenWidth() * 0.11,
          backgroundColor: R.colors.primaryColor,
          marginHorizontal: DimensionHelper.getScreenWidth() * 0.47 / 2,
          marginBottom: 40
     },
     specificAddr: {
          marginTop: 14,
          paddingBottom: 28,
          marginLeft: 20
     },
     specificAddrText: {
          fontSize: 14,
          color: '#555555',
          width: 110,
     },
     specificAddrTextInputs: {
          backgroundColor: "#FFFFFF",
          width: '100%',
          color: '#555555',
          marginTop: 18,
     },
     specificAddrTextInput: {

     },
     textButtonAddAddr: {
          fontFamily: R.fonts.bold,
          fontSize: 13,
          color: R.colors.white100
     },
     defaut: {
          flexDirection: 'row',
          alignItems: 'center',
          maxHeight: 70,
          paddingHorizontal: 20,
          flex: 1,
          //alignItems: 'center',
          //height:DimensionHelper.getScreenHeight()*0.35
     },
     defautText: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#555555',
          flex: 1,
     },
     defautViewButton: {
          height: 31,
          width: 58,
          //backgroundColor: '#F1F1F1',
          borderRadius: 31,
          justifyContent: 'center',
          marginRight: 20,
          marginTop: 20,

     },
     defautButton: {
          height: 29,
          width: 29,
          marginRight: 20
     }


})