import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { translate } from 'res/languages';
import { styles } from '../add-address-pay.style';
interface AppProps {
    title: string, placeHoder: string,
    isEdit?: boolean;
    isEditAdd?: boolean;
    isOrder?: boolean;
    phone?: boolean
    onPress?: () => void
}
interface states {
    value: string
    value2?: string
}

export default class BaseInputAdress extends React.PureComponent<AppProps, states> {
    constructor(props: AppProps) {
        super(props);
        this.state = {
            value: '',
            value2: '',
        }
    }

    setValue = (value: string) => {
        this.setState({
            value
        })
    }

    onChangeText = (value: string) => {
        this.setState({
            value
        })
    }
    setValue2 = (value2: string) => {
        this.setState({
            value
        })
    }

    onChangeText2 = (value2: string) => {
        this.setState({
            value2
        })
    }

    public render() {
        const { title, placeHoder, isEdit, onPress, phone, isEditAdd, isOrder } = this.props
        return (
            <>
                <View style={styles.name} >
                    <DailyMartText style={styles.nameText}>{translate(title)}</DailyMartText>
                    <TouchableOpacity onPress={!isEdit ? onPress : () => { }} style={{ alignItems: 'flex-end', flex: 1, justifyContent: 'center', paddingRight: 24 }} activeOpacity={1} >
                        <TextInputs
                            placeholder={translate(placeHoder)}
                            editable={isEdit}
                            value={this.state.value}
                            keyboardType={phone ? "numeric" : 'default'}
                            onTouchStart={!isEdit ? onPress : () => { }}
                            onChangeText={this.onChangeText}
                            styleTextInputs={styles.nameTextInputs}
                            styleTextInput={styles.nameTextInput}
                        />
                    </TouchableOpacity>
                </View>
                <View style={stylesw.line} />
                {phone && <View style={styles.name} >
                    <DailyMartText style={styles.nameText}>{translate("address.phone_number_2")}</DailyMartText>
                    <TouchableOpacity onPress={!isEdit ? onPress : () => { }} style={{ alignItems: 'flex-end', flex: 1, justifyContent: 'center', paddingRight: 24 }} activeOpacity={1} >
                        <TextInputs
                            placeholder={translate("address.phone_number_2")}
                            editable={isEdit}
                            value={this.state.value2}
                            keyboardType={phone ? "numeric" : 'default'}
                            onChangeText={this.onChangeText2}
                            styleTextInputs={styles.nameTextInputs}
                            styleTextInput={styles.nameTextInput}
                        />
                    </TouchableOpacity>
                </View>}
                {phone && <View style={stylesw.line} />}
            </>
        );
    }
}

const stylesw = StyleSheet.create({
    line: {
        borderWidth: 0.5,
        width: DimensionHelper.width * 0.9,
        alignSelf: 'center',
        borderColor: '#EBEBEB',
        marginTop: 10
    }
});
