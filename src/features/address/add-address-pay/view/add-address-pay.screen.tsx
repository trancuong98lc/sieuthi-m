import Buttons from 'libraries/button/buttons';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import { Switch } from 'react-native-paper';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import {
  Keyboard,
  TextInput,
  View,
  TouchableWithoutFeedback
} from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import {
  AddrPayScreen,
  CityScreen,
  DistrictScreen,
  SearchLocation,
  WardScreen
} from 'routing/screen-name';
import { goBack, navigate } from 'routing/service-navigation';
import { AddAddrPay } from '../model/add-address-pay';
import { styles } from './add-address-pay.style';
import { AddAddrPayView } from './add-address-pay.view';
import BaseInputAdress from './components/InputBaseAdress';
import { City, District } from 'features/store/model/store-entity';
import { showAlert, showAlertByType } from 'libraries/BaseAlert';
import { validatePhone } from 'helpers/helper-funtion';
import { NAME_LENGTH } from 'global/constants';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import Axios from 'axios';
import _ from 'lodash';
import EventBus, { EventBusName } from 'helpers/event-bus';
import { connect } from 'react-redux';
import { User } from 'types/user-type';
import { database } from 'react-native-firebase';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import {
  AddrPay,
  AdressPay
} from 'features/address/address-pay/model/address-pay';
import Config from 'react-native-config';

export interface Props {
  route: any;
  user: User;
}
export interface State {
  isDefault: boolean;
  showKeyborad: boolean;
  valueCity: City | null;
  valueDistrict: District | null;
  valueWard: any | null;
  valueLocation: any | null;
  valueAdd: string;
  _id: string;
  detailPay: AddrPay | null;
  isEdit: boolean;
  isOrder: boolean;
}
class AddAddrPayScreen extends React.PureComponent<Props, State>
  implements AddAddrPayView {
  constructor(props: Props) {
    super(props);
    this.state = {
      isDefault: false,
      detailPay: null,
      isEdit: props.route?.params?.isEdit ?? false,
      isOrder: props.route?.params?.isOrder ?? false,
      _id: props.route?.params?._id ?? '',
      showKeyborad: true,
      valueCity: null,
      valueDistrict: null,
      valueWard: null,
      valueLocation: null,
      valueAdd: ''
    };
    this.ongetLocation = _.debounce(this.ongetLocation, 300);
  }

  refName = React.createRef<BaseInputAdress>();
  refPhone = React.createRef<BaseInputAdress>();
  refCity = React.createRef<BaseInputAdress>();
  refDistric = React.createRef<BaseInputAdress>();
  refward = React.createRef<BaseInputAdress>();
  onFetchDataSuccess(addAddrPay: AddAddrPay): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }
  async componentDidMount() {
    Keyboard.addListener('keyboardDidShow', this.onShowKeyBoard);
    Keyboard.addListener('keyboardDidHide', this.onHideKeyBoard);
    if (this.state.isEdit) {
      this.getDetailAdress();
    } else {
      const { user } = this.props;
      if (user && user.phoneNumber) {
        this.refPhone.current!.onChangeText(user.phoneNumber);
      }
    }
    // this.presenter.onStart();
    // ApiHelper.fetch(Config.API_URL).then((res) => {});
  }

  getDetailAdress = async () => {
    let UserPhone = await AsyncStorageHelpers.getObject(
      StorageKey.PHONE_NUMBER_SUB
    );
    try {
      const res = await ApiHelper.fetch(
        URL_API.DETAILT_DELIVERY + this.state._id,
        null,
        true
      );
      if (res.status === STATUS.SUCCESS && res.data) {
        this.setState({
          detailPay: res.data,
          valueCity: res.data.city || null,
          valueDistrict: res.data.district || null,
          valueWard: res.data.ward || null,
          valueLocation:
            res.data.lat && res.data.lon
              ? { lat: res.data.lat, lng: res.data.lon }
              : null,
          valueAdd: res.data.detail.trim() || '',
          isDefault: res.data.isDefault || false
        });
        this.refName.current!.onChangeText(res.data.receiverName || '');
        this.refPhone.current!.onChangeText(res.data.receiverPhone || '');
        if (UserPhone) {
          if (UserPhone[res.data._id]) {
            this.refPhone.current!.onChangeText2(UserPhone[res.data._id] || '');
          }
        }
        this.refCity.current!.setValue(res.data.city.name || '');
        this.refDistric.current!.setValue(res.data.district.name || '');
        this.refward.current!.setValue(res.data.ward.name || '');
      }
    } catch (error) {}
  };

  onShowKeyBoard = () => {
    this.setState({ showKeyborad: false });
  };

  onHideKeyBoard = () => {
    this.setState({ showKeyborad: true });
  };
  isDefault = () => {
    Keyboard.dismiss();
    this.setState({ isDefault: !this.state.isDefault });
  };

  setValueCity = (valueCity: City) => {
    this.setState({ valueCity }, () => {
      this.refCity.current!.setValue(valueCity.name);
      // const params = {
      //   cityId: valueCity._id
      // };
      this.setValueDistrict(null);
      this.setValueWard(null);
      this.refDistric.current!.setValue('');
      this.refward.current!.setValue('');
    });
  };

  setValueDistrict = (valueDistrict: District | null) => {
    if (valueDistrict) {
      this.setState({ valueDistrict }, () => {
        this.refDistric.current!.setValue(valueDistrict.name);
      });
    } else {
      this.setState({ valueDistrict: null });
      this.setValueWard(null);
      this.refward.current!.setValue('');
    }
  };

  setValueWard = (valueWard: any | null) => {
    if (valueWard) {
      this.setState({ valueWard }, () => {
        this.refward.current!.setValue(valueWard.name);
      });
    } else {
      this.setState({ valueWard: null });
    }
  };

  gotoCity = () => {
    Keyboard.dismiss();
    navigate(CityScreen, { onSetCity: this.setValueCity });
  };

  gotoDistrict = () => {
    Keyboard.dismiss();
    const { valueCity } = this.state;
    if (!valueCity) return showAlert('not_city');
    navigate(DistrictScreen, {
      onSetDistrict: this.setValueDistrict,
      CityId: valueCity?._id
    });
  };

  gotoWard = () => {
    Keyboard.dismiss();
    const { valueDistrict } = this.state;
    if (!valueDistrict) return showAlert('not_District');
    navigate(WardScreen, {
      onSetWard: this.setValueWard,
      districtId: valueDistrict?._id
    });
  };

  saveAdress = async () => {
    Keyboard.dismiss();
    let UserPhone = await AsyncStorageHelpers.getObject(
      StorageKey.PHONE_NUMBER_SUB
    );
    try {
      let phoneNumber: string = this.refPhone.current!.state.value;
      let subphoneNumber: string | undefined = this.refPhone.current!.state
        .value2;
      let name: string = this.refName.current!.state.value;
      name = name.trim();
      phoneNumber = phoneNumber.trim();

      if (name.trim().length < NAME_LENGTH.MIN || name.length == 0) {
        return showAlert('account.fullname_from_2character2');
      }

      if (name.trim().length > 100) {
        return showAlert('account.fullname_max2');
      }
      if (phoneNumber.length == 0) {
        return showAlert('account.empty_phone');
      }
      if (!phoneNumber.startsWith('0')) {
        return showAlert('alert.phoneNotValid');
      }
      if (phoneNumber.trim() == '') {
        return showAlert('account.empty_phone');
      }
      if (phoneNumber.length > 10 || phoneNumber.length < 10) {
        return showAlert('account.max_Phone');
      }
      if (subphoneNumber) {
        subphoneNumber = subphoneNumber.trim();
        if (subphoneNumber.length == 0) {
          return showAlert('account.empty_phone');
        }
        if (!subphoneNumber.startsWith('0')) {
          return showAlert('alert.phoneNotValid');
        }
        if (subphoneNumber.trim() == '') {
          return showAlert('account.empty_phone');
        }
        if (subphoneNumber.length > 10 || subphoneNumber.length < 10) {
          return showAlert('account.max_Phone');
        }
      }

      if (!this.state.valueCity) {
        return showAlert('not_city');
      }
      if (!this.state.valueDistrict) {
        return showAlert('not_District');
      }

      if (!this.state.valueWard) {
        return showAlert('not_ward');
      }
      if (!validatePhone(phoneNumber)) {
        return showAlert('alert.phoneNotValid');
      }
      if (
        this.state.valueAdd.trim().length == 0 ||
        this.state.valueAdd.trim() == ''
      ) {
        return showAlert('address.add_empty');
      }
      if (!this.state.valueLocation) {
        return showAlert('address.not_choice_add');
      }
      const body = {
        receiverName: this.refName.current?.state.value,
        receiverPhone: this.refPhone.current?.state.value,
        cityId: this.state.valueCity?._id,
        districtId: this.state.valueDistrict?._id,
        wardId: this.state.valueWard?._id,
        lat: this.state.valueLocation.lat,
        lon: this.state.valueLocation.lng,
        detail: this.state.valueAdd.trim(),
        isDefault: this.state.isDefault
      };
      showLoading();
      let res = null;
      if (!this.state.isEdit) {
        res = await ApiHelper.post(URL_API.ADD_DELIVERY, body, true);
      } else {
        res = await ApiHelper.put(
          URL_API.UPDATE_DELIVERY + this.state._id,
          body,
          true
        );
      }
      if (res && res.status == STATUS.SUCCESS) {
        if (!this.state.isEdit) {
          console.log('AddAddrPayScreen -> res', res);
          const PayloadBody = {
            city: this.state.valueCity,
            detail: this.state.valueAdd.trim(),
            district: this.state.valueDistrict,
            isDefault: this.state.isDefault,
            lat: this.state.valueLocation.lat,
            lon: this.state.valueLocation.lng,
            receiverName: this.refName.current!.state.value,
            receiverPhone: this.refPhone.current!.state.value,
            subreceiverPhone: this.refPhone.current!.state.value2,
            ward: this.state.valueWard,
            _id: this.state._id
          };
          if (PayloadBody.subreceiverPhone) {
            if (UserPhone) {
              AsyncStorageHelpers.saveObject(StorageKey.PHONE_NUMBER_SUB, {
                ...UserPhone,
                [res.data._id]: PayloadBody.subreceiverPhone
              });
            } else {
              AsyncStorageHelpers.saveObject(StorageKey.PHONE_NUMBER_SUB, {
                [res.data._id]: PayloadBody.subreceiverPhone
              });
            }
          }
          EventBus.getInstance().post({
            type: EventBusName.CREATE_ADDRESS,
            payload: PayloadBody
          });
          hideLoading();
          setTimeout(() => {
            showAlert('address.create_add_success', { success: true });
            goBack();
          }, 600);
        } else {
          const PayloadBody = {
            city: this.state.valueCity,
            detail: this.state.valueAdd.trim(),
            district: this.state.valueDistrict,
            isDefault: this.state.isDefault,
            lat: this.state.valueLocation.lat,
            lon: this.state.valueLocation.lng,
            receiverName: this.refName.current!.state.value,
            receiverPhone: this.refPhone.current!.state.value,
            subreceiverPhone: this.refPhone.current!.state.value2,
            ward: this.state.valueWard,
            _id: this.state._id
          };
          if (PayloadBody.subreceiverPhone) {
            if (UserPhone) {
              AsyncStorageHelpers.saveObject(StorageKey.PHONE_NUMBER_SUB, {
                ...UserPhone,
                [res.data._id]: PayloadBody.subreceiverPhone
              });
            } else {
              AsyncStorageHelpers.saveObject(StorageKey.PHONE_NUMBER_SUB, {
                [res.data._id]: PayloadBody.subreceiverPhone
              });
            }
          } else {
            if (UserPhone && UserPhone[res.data._id]) {
              delete UserPhone[res.data._id];
              AsyncStorageHelpers.saveObject(
                StorageKey.PHONE_NUMBER_SUB,
                UserPhone
              );
            }
          }
          EventBus.getInstance().post({
            type: EventBusName.UPDATE_ADRESS,
            payload: PayloadBody
          });
          hideLoading();
          setTimeout(() => {
            showAlert('address.update_susccess', { success: true });
            goBack();
          }, 1000);
        }
      } else {
        if (!this.state.isEdit) {
          showAlert('address.create_add_err');
          hideLoading();
        } else {
          showAlert('address.update_Eror');
          hideLoading();
        }
      }
    } catch (error) {
      if (!this.state.isEdit) {
        showAlert('address.create_add_err');
        hideLoading();
      } else {
        showAlert('address.update_Eror');
        hideLoading();
      }
      console.log('saveAdress -> error', error);
    }
  };

  onChangetext = (value: string) => {
    this.setState({
      valueAdd: value
    });
    this.ongetLocation();
  };

  ongetLocation = () => {
    Axios.get(
      `https://autosuggest.search.hereapi.com/v1/autosuggest?q=${
        this.state.valueCity?.name
      } ${this.state.valueDistrict?.name} ${
        this.state.valueWard?.name
      } ${this.state.valueAdd.trim()}&in=countryCode:VNM&at=21.02888,105.85464&lang=vi-VN&apiKey=${
        Config.APP_API_KEY_MAP
      }&limit=1`
    ).then((res) => {
      if (
        res.data &&
        res.data.items &&
        res.data.items.length > 0 &&
        res.data.items[0].position
      ) {
        this.onSetLocation(res.data.items[0].position);
      } else {
        this.onSetLocation(null);
      }
    });
  };

  validateDistric = () => {
    Keyboard.dismiss();
    if (!this.state.valueWard) {
      return showAlert('not_ward');
    }
  };

  onSetLocation = (value: any) => {
    this.setState({
      valueLocation: value
    });
  };

  gotoAdress = () => {
    navigate(SearchLocation, { onSetLocation: this.onSetLocation });
  };

  removeAddress = async (item: AdressPay) => {
    try {
      showLoading();
      const res = await ApiHelper.deletes(
        URL_API.UPDATE_DELIVERY + item._id,
        null,
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.updateRemoveAdressCreate(item);
        showAlert('address.delete_succes', { success: true });
        hideLoading();
      } else {
        hideLoading();
        showAlert('address.delete_err');
      }
    } catch (error) {
      showAlert('address.delete_err');
      hideLoading();
    }
  };

  updateRemoveAdressCreate = (data: AdressPay) => {
    goBack();
    EventBus.getInstance().post({
      type: EventBusName.REMOVE_ADRESS_DETAIL,
      payload: data
    });
  };

  onDeleteAdress = () => {
    if (this.state.detailPay) {
      showAlertByType({
        title: translate('notify.header'),
        message: translate('del_add'),
        options: [
          {
            text: 'OK',
            onPress: () => {
              this.removeAddress(this.state.detailPay);
            }
          }
        ]
      });
    }
  };

  public render() {
    const { isDefault, valueAdd, valueLocation, isEdit, isOrder } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <TouchableWithoutFeedback
          style={{ flex: 1 }}
          onPress={() => Keyboard.dismiss()}
        >
          <View style={styles.container}>
            <HeaderService
              title={isEdit ? 'addrPay.UpdateNewAddr' : 'addrPay.addNewAddr'}
              iconRight={isEdit ? 'ic_trash' : undefined}
              width={15}
              onPressRight={this.onDeleteAdress}
            />
            <View style={styles.infor}>
              <BaseInputAdress
                isEdit={true}
                ref={this.refName}
                title="addrPay.name"
                placeHoder="textinput.enterName"
              />
              <BaseInputAdress
                isEdit={true}
                isEditAdd={isEdit}
                isOrder={isOrder}
                ref={this.refPhone}
                title="addrPay.phone"
                phone
                placeHoder="textinput.enterPhone"
              />
              <BaseInputAdress
                isEdit={false}
                ref={this.refCity}
                onPress={this.gotoCity}
                title="addrPay.city"
                placeHoder="textinput.HN"
              />
              <BaseInputAdress
                isEdit={false}
                ref={this.refDistric}
                onPress={this.gotoDistrict}
                title="addrPay.district"
                placeHoder="textinput.CG"
              />
              <BaseInputAdress
                isEdit={false}
                ref={this.refward}
                onPress={this.gotoWard}
                title="addrPay.ward"
                placeHoder="textinput.DV"
              />
              <View style={styles.specificAddr}>
                <DailyMartText style={styles.specificAddrText}>
                  {translate('addrPay.specificAddr')}
                </DailyMartText>
                <TextInput
                  placeholder={translate('textinput.specificAddr')}
                  onChangeText={this.onChangetext}
                  value={valueAdd}
                  autoFocus={false}
                  editable={this.state.valueWard ? true : false}
                  onTouchStart={this.validateDistric}
                  style={styles.specificAddrTextInputs}
                  // styleTextInput={styles.specificAddrTextInput}
                />
              </View>
            </View>
            <View style={styles.line} />
            <View style={styles.defaut}>
              <DailyMartText style={styles.defautText}>
                {translate('addrPay.default')}
              </DailyMartText>
              <Switch
                value={isDefault}
                color={isDefault ? R.colors.primaryColor : R.colors.white70}
                ios_backgroundColor={R.colors.grey400}
                onValueChange={this.isDefault}
              />
            </View>
            {this.state.showKeyborad ? (
              <Buttons
                styleButton={styles.buttonAddAddr}
                styleTextButton={styles.textButtonAddAddr}
                textButton={translate('button.save')}
                onPress={this.saveAdress}
              />
            ) : null}
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  return {
    user
  };
};

export default connect(mapStatesToProps, null, null, { forwardRef: true })(
  AddAddrPayScreen
);
