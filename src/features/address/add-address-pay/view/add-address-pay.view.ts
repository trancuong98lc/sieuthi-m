import { AddAddrPay } from "../model/add-address-pay";

export interface AddAddrPayView {
     onFetchDataSuccess(addAddrPay: AddAddrPay): void;
     onFetchDataFail(error?: any): void;
 }