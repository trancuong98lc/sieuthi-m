import { Location } from './location';

export class LocationAdapter {
    adapt(json: any): Location {
        if (this.isAValid(json)) {
            return json;
        }
        return {};
    }

    private isAValid(json: any): boolean {
        try {
            if (json && typeof json === 'object') {
                return true;
            }
            return false;
        } catch (e) {
            return false;
        }
    }
}
