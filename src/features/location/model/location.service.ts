import Config from 'react-native-config';
import ApiHelper from 'helpers/api-helper';

export class LocationService {
    async retrieve(): Promise<any> {
        return ApiHelper.fetch(Config.API_URL)
            .then((response) => response)
            .catch((error) => error);
    }
}
