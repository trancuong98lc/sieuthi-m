import { LocationService } from './location.service';
import { Location } from './location';
import { LocationAdapter } from './location.adapter';

export class LocationRepository {
    private locationService: LocationService;

    private locationAdapter: LocationAdapter;

    constructor(
        locationService: LocationService,
        locationAdapter: LocationAdapter
    ) {
        this.locationService = locationService;
        this.locationAdapter = locationAdapter;
    }

    async load(): Promise<Location> {
        const response = await this.locationService.retrieve();

        return this.locationAdapter.adapt(response);
    }
}
