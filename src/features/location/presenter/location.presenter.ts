import { LocationView } from 'location/view/location.view';
import { LocationRepository } from 'location/model/location.repository';

export class LocationPresenter {
    private locationView: LocationView;

    private locationRepository: LocationRepository;

    constructor(
        locationView: LocationView,
        locationRepository: LocationRepository
    ) {
        this.locationView = locationView;
        this.locationRepository = locationRepository;
    }

    async onStart(): Promise<void> {
        try {
            const location = await this.locationRepository.load();
            this.locationView.onFetchDataSuccess(location);
        } catch (error) {
            this.locationView.onFetchDataFail(error);
        }
    }

    onDestroy() {
        console.log('onDestroy');
    }
}
