import { Location } from 'location/model/location';

export interface LocationView {
    onFetchDataSuccess(location: Location): void;
    onFetchDataFail(error?: any): void;
}
