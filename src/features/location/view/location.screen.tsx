import { Location } from 'location/model/location';
import { LocationAdapter } from 'location/model/location.adapter';
import { LocationRepository } from 'location/model/location.repository';
import { LocationService } from 'location/model/location.service';
import { LocationPresenter } from 'location/presenter/location.presenter';
import * as React from 'react';
import { Text, View } from 'react-native';
import { styles } from './location.style';
import { LocationView } from './location.view';

interface Props {}

interface State {
    location: Location;
}

export default class LocationScreen extends React.Component<Props, State>
    implements LocationView {
    private readonly presenter: LocationPresenter;

    constructor(props: Props) {
        super(props);
        this.state = {
            location: {}
        };

        this.presenter = new LocationPresenter(
            this,
            new LocationRepository(new LocationService(), new LocationAdapter())
        );
    }

    componentDidMount(): void {
        // this.presenter.onStart();
        // ApiHelper.fetch(Config.API_URL).then((res) => {});
    }

    onFetchDataSuccess(location: Location): void {
        this.setState({ location });
    }

    onFetchDataFail(error?: any): void {
        throw new Error('Method not implemented.');
    }

    public render(): React.ReactNode {
        const { location } = this.state;
        return (
            <View style={styles.container}>
                <Text>
                    Address: {location.city}, {location.country}
                </Text>
                <Text>Coordinates: {(location.lat, location.lon)}</Text>
                <Text>IP Address: {location.query}</Text>
            </View>
        );
    }
}
