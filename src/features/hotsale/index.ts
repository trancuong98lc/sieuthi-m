import HotsaleContainer from 'features/hotsale/view/hotsale.screen';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = (dispatch: Dispatch) => ({});

export const HotsaleScreen = connect(
  mapStateToProps, mapDispatchToProps
)(HotsaleContainer)

