import { Hotsale } from "../model/hotsale";

export interface HotsaleView {
     onFetchDataSuccess(location: Hotsale): void;
     onFetchDataFail(error?: any): void;
 }