import * as React from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import { HotsaleAdapter } from '../model/hotsale.adapter';
import { HotsaleProps, HotsaleState } from '../model/hotsale.type';
import FlatListDanhmucsanpham from 'libraries/flatlist/flatlist-hotsale/flatist.danhmucsanpham';
import FlatListHotsale from 'libraries/flatlist/flatlist-hotsale/flatlist.hotsale';
import Container from 'libraries/main/container';
import R from 'res/R';
import { styles } from './hotsale.style';
import { Categories, Hotsales } from '../model/hotsale.type';
import HeaderService from 'libraries/header/header-service';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
import { ReqHotsale } from '../model/hotsale.adapter';
import images from 'res/images';
import DailyMartText from 'libraries/text/text-daily-mart';
import { HotsaleScreen } from 'routing/screen-name';
import { connect } from 'react-redux';

export default class HotsaleContainer extends React.Component<HotsaleProps, HotsaleState> {
  private adapter: HotsaleAdapter;
  private onEndReachedCalledDuringMomentum: boolean;

  constructor(props: HotsaleProps) {
    super(props);
    this.adapter = new HotsaleAdapter(this);
    this.state = {
      categories: [],
      hotsales: [],
      selectedCategory: 0,
      loading: true,
      maxdata: true
    };
    this.onEndReachedCalledDuringMomentum = true;
  }

  componentDidMount = () => {
    this.adapter.getCategoy();
    this.adapter.getHotsaleByCategory(this.paramsCate);

  };

  setCategories = (categories: Categories[]) => {
    categories.unshift({ "_id": "", "name": "Tất cả", "image": "" });
    this.setState({
      categories
    });
  }

  setHotsales = (hotsales: Hotsales[]) => {
    this.setState({
      hotsales,
      loading: false
    })
  }

  setMaxdata = (maxdata: boolean) => {
    this.setState({
      maxdata
    });
  }

  changeCategory = (selectedCategory: number) => {
    const { categories } = this.state;
    const newParams: any = {
      ...this.paramsCate,
      categoryId: categories[selectedCategory]._id
    }
    this.paramsCate = newParams;
    this.setState({
      selectedCategory
    });
  }

  paramsCate: ReqHotsale = {
    limit: 10,
    page: 1,
    categoryId: ''
  };

  onRefresh = () => {
    const newParams =
    {
      limit: 10,
      page: 1,
      categoryId: this.paramsCate.categoryId
    }
    this.paramsCate = newParams;
    this.adapter.getHotsaleByCategory(newParams);
  }

  onEndReached = () => {
    const { loading, maxdata } = this.state;
    if (loading || this.onEndReachedCalledDuringMomentum || maxdata) {
      return;
    }
    const newParasm: any = {
      ...this.paramsCate,
      page: this.paramsCate.page + 1
    }
    this.paramsCate = newParasm
    this.adapter.getHotsaleByCategory(this.paramsCate);
    this.onEndReachedCalledDuringMomentum = true;
  }

  onMomentumScrollBegin = () => {
    this.onEndReachedCalledDuringMomentum = false;
  }

  emptyListComponent = () => {
    if (!this.state.loading) {
      return <EmptylistComponent />;
    } else {
      return null;
    }
  }

  keyExtractor = (item: Hotsales, index: number) => {
    return item._id.toString();
  }

  footerComponent = () => {
    if (!this.state.maxdata) {
      return <DailyMartText style={{ marginVertical: 10, textAlign: "center", color: R.colors.primaryColor }}>Đang tải...</DailyMartText>;
    } else {
      return null;
    }
  }

  public render(): React.ReactNode {
    const { categories, hotsales, selectedCategory, loading } = this.state;
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService title={"home.title2"} headerIconCenter={images.ic_hotsale} />
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={categories}
          horizontal
          style={styles.flatlistDanhmucsanpham}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
          renderItem={({ item, index }) => {
            return (
              <FlatListDanhmucsanpham
                item={item}
                index={index}
                selected={selectedCategory}
                changeCategory={this.changeCategory}
                getHotsalesByCategory={this.onRefresh}
              ></FlatListDanhmucsanpham>
            );
          }}
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={hotsales}
          numColumns={2}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
          style={styles.flatlistHotsale}
          ListEmptyComponent={this.emptyListComponent}
          ListFooterComponent={this.footerComponent}
          refreshing={loading}
          onRefresh={this.onRefresh}
          onMomentumScrollBegin={this.onMomentumScrollBegin}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.01}
          renderItem={({ item, index }) => {
            return (
              <FlatListHotsale item={item} index={index}></FlatListHotsale>
            );
          }}
        />
      </Container>
    );
  }
}