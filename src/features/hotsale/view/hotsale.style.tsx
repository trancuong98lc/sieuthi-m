import { StyleSheet } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
  container: {
    //flex: 1,
    backgroundColor: '#FFF',
    paddingBottom: -DimensionHelper.getBottomSpace()
  },
  header: {
    flexDirection: 'row',

    paddingBottom: 12,
    alignItems: 'center',
    backgroundColor: R.colors.primaryColor
  },
  imageheader1: {
    width: 0.085 * DimensionHelper.getScreenWidth(),
    height: 0.085 * DimensionHelper.getScreenWidth(),
    marginLeft: 10
  },
  imageheader2: {
    width: 0.04 * DimensionHelper.getScreenWidth(),
    height: 0.053 * DimensionHelper.getScreenWidth(),
    marginLeft: 0.263 * DimensionHelper.getScreenWidth()
  },
  textheader: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    marginLeft: 8,
    fontSize: 17
  },
  flatlistDanhmucsanpham: {
    backgroundColor: '#FFFFFF',
    //paddingBottom:28,
    height: 70,
    flexGrow: 0
  },
  flatlistHotsale: {
    backgroundColor: '#FFFFFF',
    //marginTop:10
    marginBottom: DimensionHelper.getBottomSpace()
  },
  footerFlatlistHotsale: {
    marginVertical: 10, 
    textAlign: "center", 
    color: R.colors.primaryColor
  }
});
