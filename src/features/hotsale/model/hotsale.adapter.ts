/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/

import { showAlert } from 'libraries/BaseAlert';
import { STATUS } from './../../../types/BaseResponse';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import HotsaleContainer from '../view/hotsale.screen';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';
import Commons from 'helpers/Commons';

export interface ReqHotsale {
  page: number;
  limit: number;
  categoryId?: string
}

export class HotsaleAdapter {
  private view: HotsaleContainer;
  constructor(view: HotsaleContainer) {
    this.view = view;
  }

  getCategoy = async () => {
    try {
        const res = await ApiHelper.fetch(URL_API.CATEGORIES, {}, true);
        if (res.status == STATUS.SUCCESS) {
            this.view.setCategories(res.data);
        } else {
          this.view.setCategories([]);
        }
      } catch (error) {
        console.log('error: ', error);
        this.view.setCategories([]);
      }
  }

  getHotsaleByCategory = async (params: ReqHotsale) => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        URL_API.HOTSALE + Commons.idStore,
        params,
        true
      );
      if (res.status == STATUS.SUCCESS) {
        let data = params.page > 1 ? [...this.view.state.hotsales, ...res.data] : res.data;
        this.view.setHotsales(data);
        this.view.setMaxdata(data.length === res.total);
        // hideLoading();
      } else {
        // this.view.setHotsales([]);
        this.view.setMaxdata(false);
        // hideLoading();
      }
    } catch (error) {
        console.log("getHotsaleByCategory: ", error);
        this.view.setHotsales([]);
        // this.view.setMaxdata(data.length === res.total);
        // hideLoading();
    }
  };
}
