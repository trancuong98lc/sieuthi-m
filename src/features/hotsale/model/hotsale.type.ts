
/* 
  Created by Trường at 10-16-2020 11:48:00
  Màn hình Hotsale
*/

import { NavigationInitActionPayload } from 'react-navigation';
import { MediaType } from 'types/ConfigType';

export interface HotsaleProps {
  route: NavigationInitActionPayload;
}

export interface HotsaleState {
  categories: Categories[],
  hotsales: Hotsales[],
  selectedCategory: number,
  loading: boolean,
  maxdata: boolean
}

export interface Categories {
	name: string;
  _id: string;
  image: string
}

export interface Hotsales {
  _id: string,
  name: string,
}

