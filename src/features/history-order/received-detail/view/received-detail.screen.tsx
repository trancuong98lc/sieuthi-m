import received from 'data/received';
import ApiHelper from 'helpers/api-helper';
import { _formatPrice } from 'helpers/helper-funtion';
import { URL_API } from 'helpers/url-api';
import Buttons from 'libraries/button/buttons';
import FlatListReceived from 'libraries/flatlist/flatlist-received';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Item } from 'react-native-paper/lib/typescript/src/components/List/List';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import { STATUS } from 'types/BaseResponse';
import { ReceivedDetail } from '../model/received-detail';
import { styles } from './received-detail.style';
import { ReceivedDetailView } from './received-detail.view';
import moment from 'moment'
import FastImage from 'react-native-fast-image';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import { ALERT_TYPE, showAlert, showAlertByType } from 'libraries/BaseAlert';
import { getBottomSpace, isIphoneX } from 'react-native-iphone-x-helper';
import { STATUS_ORDER } from 'global/constants';
import EventBus, { EventBusName } from 'helpers/event-bus';
import TextPrice from 'libraries/text/TextPrice';
import { StatusOrderName, StatusOrderNameTitle, StatusOrderIcon } from 'types/ConfigType';

export interface Props {
  route: any
}

export interface States {
  dataDetail: any,
  orderId: string
  type: string
}


export default class ReceivedDetailScreen
  extends React.PureComponent<Props, States>
  implements ReceivedDetailView {
  constructor(props: Props) {
    super(props);
    this.state = {
      orderId: props?.route?.params.orderId ?? '',
      type: props?.route?.params.type ?? '',
      dataDetail: null
    }
  }
  onFetchDataSuccess(receivedDetail: ReceivedDetail): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }
  componentDidMount = () => {
    this.getDetailOrder()
  };

  showCancelOrder = () => {
    showAlertByType({
      type: ALERT_TYPE.CONFIRM,
      title: translate('cancel_order'),
      message: translate("want_cancel_order"),
      callBackConfirm: () => this.onCancel()
    })
  }

  onCancel = async () => {
    try {
      const params = {
        orderId: this.state.orderId,
        status: STATUS_ORDER.CANCEL
      }
      const res = await ApiHelper.post(URL_API.ORDER_UPDATE_STATUS, params, true)
      if (res.status === STATUS.SUCCESS) {
        EventBus.getInstance().post({
          type: EventBusName.UPDATE_ORDER_HISTORY,
          payload: res.data
        })
        showAlert('cancel_order_success', { success: true })
        setTimeout(() => {
          goBack()
        }, 500);
      } else {
        showAlert('cancel_confirmed_order')
      }
    } catch (error) {
      console.log("onCancel -> error", error)
      showAlert('cancel_order_err')
    }
  }
  getDetailOrder = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.ORDER_DETAIL + this.state.orderId, null, true)
      if (res.status == STATUS.SUCCESS) {
        this.setState({
          dataDetail: res.data
        })
      } else {
        this.setState({
          dataDetail: null
        })
      }
    } catch (error) {
      console.log("getDetailOrder -> error", error)

    }
  }

  renderItem = ({ item, index }: any) => {
    return (
      <FlatListReceived
        item={item}
        index={index}
      />
    );
  }

  getNameDistrict = (dataDetail: any) => {
    let name = ''
    if (dataDetail) {
      if (dataDetail.orderedDeliveryAddress) {
        name = `${dataDetail.orderedDeliveryAddress.wardName}, ${dataDetail.orderedDeliveryAddress.districtName}, ${dataDetail.orderedDeliveryAddress.cityName}` || ''
      }

    }
    return name
  }

  getTextHeader = (dataDetail: any) => {
    if (!dataDetail) return
    let name = ''
    const orderCode = `#${dataDetail && dataDetail.code && dataDetail.code.split('-').length > 0 ? (dataDetail.code.split('-').length > 1 ? dataDetail.code.split('-')[1] : dataDetail.code.split('-')[0]) : ''}`;
    const { status } = dataDetail;
    name = translate(StatusOrderNameTitle[status], { orderCode: orderCode })
    return name
  }

  getTextTimeHeader = (dataDetail: any) => {
    if (!dataDetail) return
    let name = ''
    if (this.state.type === 'RECEIVE') {
      name = translate('receivedDetail.timeIntend') + ' ' + moment(dataDetail.deliveryTime).format('DD-MM-YYYY HH:mm') || ''
    } else if (this.state.type === "RECEIVED") {
      name = translate('receivedDetail.timeDelivered') + ' ' + moment(dataDetail.updatedAt).format('DD-MM-YYYY HH:mm') || ''
    } else {
      name = translate('deletedDetail.timeDeleted') + ' ' + moment(dataDetail.updatedAt).format('DD-MM-YYYY HH:mm') || ''
    }
    return name
  }

  getNameHeader = () => {
    const { dataDetail } = this.state;
    if (!dataDetail) return

    let name: string = ""
    const { status } = dataDetail;
    name = translate(StatusOrderName[status])
    return name
  }

  getIconHeader = () => {
    const { dataDetail } = this.state;
    if (!dataDetail) return

    const { status } = dataDetail;
    let Icon = 'ic_waitPay'
    Icon = StatusOrderIcon[status]
    return Icon
  }

  public render() {
    const { dataDetail } = this.state
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService title="receivedDetail.header" />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.waitPay}>
            <View style={styles.waitPayHeader}>
              <BaseIcon name={this.getIconHeader()} width={19} />
              <DailyMartText style={styles.waitPayHeaderText}>
                {this.getNameHeader()}
              </DailyMartText>
            </View>
            <DailyMartText style={styles.waitPayContent}>
              {this.getTextHeader(dataDetail)}
            </DailyMartText>
            <DailyMartText style={styles.waitPayContent}>
              {this.getTextTimeHeader(dataDetail)}
            </DailyMartText>
          </View>
          <View style={styles.hr} />
          <View style={styles.waitPay}>
            <View style={styles.waitPayHeader}>
              <BaseIcon name='ic_personalAddr' width={22} />
              <DailyMartText style={styles.waitPayHeaderText}>
                {translate('receivedDetail.addrDelivery')}
              </DailyMartText>
            </View>
            <DailyMartText style={styles.waitPayContent}>
              {dataDetail && dataDetail.orderedDeliveryAddress && dataDetail.orderedDeliveryAddress.receiverName || ''}
            </DailyMartText>
            <DailyMartText style={styles.waitPayContent}>
              {dataDetail && dataDetail.orderedDeliveryAddress.receiverPhone || ''} {dataDetail && dataDetail.subreceiverPhone && '/'} {dataDetail && dataDetail.subreceiverPhone || ''}
            </DailyMartText>
            <DailyMartText style={styles.waitPayContent}>
              {dataDetail && dataDetail.orderedDeliveryAddress && dataDetail.orderedDeliveryAddress.detail || ''}
            </DailyMartText>
            <DailyMartText style={styles.waitPayContent}>
              {this.getNameDistrict(dataDetail)}
            </DailyMartText>
          </View>
          <View style={styles.HR}></View>
          <View style={styles.waitPay}>
            <View style={styles.waitPayHeader}>
              <BaseIcon name='methodPay' width={22} />
              <DailyMartText style={styles.waitPayHeaderText}>
                {translate('receivedDetail.methodPay')}
              </DailyMartText>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 7, marginLeft: 20 }}>
              <BaseIcon name='privacy' width={15} />
              <DailyMartText style={styles.codText}>
                {dataDetail && dataDetail.paymentMethod === "CASHING" ? translate('create_pay.pay1') : translate('create_pay.pay2')}
              </DailyMartText>
            </View>
            <DailyMartText style={styles.sumMoney}>
              {translate('receivedDetail.sumMoney')}
              <TextPrice style={{ color: R.colors.primaryColor, fontFamily: R.fonts.bold }} total={dataDetail && dataDetail.totalPrice || 0} />
            </DailyMartText>
          </View>
          <View style={styles.HR}></View>
          {dataDetail && <FlatList
            style={styles.flatlist}
            showsHorizontalScrollIndicator={false}
            data={[dataDetail]}
            keyExtractor={(item: any) => item._id.toString()}
            renderItem={this.renderItem}
          />}
          <View style={{ height: 150 }} />
        </ScrollView>
        {this.state.type == 'RECEIVE' && <TouchableOpacity onPress={this.showCancelOrder} style={styles2.btnCancel} activeOpacity={0.6}>
          <DailyMartText fontStyle="semibold" style={{ color: '#9B9B9B' }}>{translate('cancel_order')}</DailyMartText>
        </TouchableOpacity>}
      </Container>
    );
  }
}

const styles2 = StyleSheet.create({
  btnCancel: {
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: isIphoneX() ? getBottomSpace() : 10,
    height: 46,
    borderRadius: 23,
    backgroundColor: '#E5E5E5'
  }
});
