import { StyleSheet } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1,
    marginBottom: -DimensionHelper.getBottomSpace()
  },
  header: {
    backgroundColor: R.colors.primaryColor,
    flexDirection: 'row',
    alignItems: 'center'
  },
  buttonBack: {
    backgroundColor: R.colors.primaryColor
  },
  imageButtonBack: {
    height: 35,
    width: 35,
    marginTop: 5,
    marginBottom: 13,
    marginLeft: 12
  },
  new: {
    fontFamily: R.fonts.bold,
    fontSize: 18,
    color: R.colors.white100,
    marginTop: 5,
    marginBottom: 10,
    marginLeft: DimensionHelper.getScreenWidth() * 0.21
  },
  waitPay: {
    marginLeft: 12,
    marginTop: 20
  },
  waitPayHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  waitPayIcon: {
    height: 18,
    width: 17,
    resizeMode: 'contain'
  },
  waitPayHeaderText: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    color: '#333333',
    marginLeft: 7
  },
  waitPayContent: {
    fontSize: 15,
    color: '#555555',
    marginLeft: 20,
    paddingHorizontal: 12,
    flex: 1,
    marginTop: 7
  },
  hr: {
    borderTopWidth: 1,
    borderColor: '#F1F1F1',
    marginTop: 10
  },
  HR: {
    borderTopWidth: 10,
    borderColor: '#F1F1F1',
    marginTop: 10
  },
  codText: {
    fontSize: 14,
    color: '#0FA1E2',
    marginLeft: 3
  },
  sumMoney: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: R.colors.primaryColor,
    marginLeft: 20,
    marginTop: 10
  },
  flatlist: {
    marginLeft: 12
  }
});
