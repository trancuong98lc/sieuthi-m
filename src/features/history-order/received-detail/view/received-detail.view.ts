import { ReceivedDetail } from "../model/received-detail";

export interface ReceivedDetailView {
     onFetchDataSuccess(receivedDetail: ReceivedDetail): void;
     onFetchDataFail(error?: any): void;
 }