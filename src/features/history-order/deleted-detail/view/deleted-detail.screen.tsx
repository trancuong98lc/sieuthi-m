import * as React from 'react';
import { View, StyleSheet, Text, Image, FlatList } from 'react-native';

import Buttons from 'libraries/button/buttons';
import DailyMartText from 'libraries/text/text-daily-mart';
import { goBack } from 'routing/service-navigation';
import { translate } from 'res/languages';
import R from 'res/R';
import received from 'data/received';
import FlatListReceived from 'libraries/flatlist/flatlist-received';
import { ScrollView } from 'react-native-gesture-handler';
import { DeletedDetailView } from './deleted-detail.view';
import { DeletedDetail } from '../model/deleted-detail';
import { styles } from './deleted-detail.style';
import Container from 'libraries/main/container';
import FastImage from 'react-native-fast-image';

export interface Props {}

export default class DeletedDetailScreen extends React.PureComponent<Props, any>
  implements DeletedDetailView {
  constructor(props: Props) {
    super(props);
  }
  onFetchDataSuccess(deletedDetail: DeletedDetail): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <ScrollView>
            <View style={styles.header}>
              <Buttons
                onPress={goBack}
                source={R.images.ic_back2}
                styleButton={styles.buttonBack}
                styleImageButton={styles.imageButtonBack}
              />
              <DailyMartText style={styles.new}>
                {translate('receivedDetail.header')}
              </DailyMartText>
            </View>
            <View style={styles.waitPay}>
              <View style={styles.waitPayHeader}>
                <FastImage
                  source={R.images.ic_deleted}
                  style={styles.waitPayIcon}
                />
                <DailyMartText style={styles.waitPayHeaderText}>
                  {translate('deletedDetail.deleted')}
                </DailyMartText>
              </View>
              <DailyMartText style={styles.waitPayContent}>
                {translate('deletedDetail.deleted')}
              </DailyMartText>
              <DailyMartText style={styles.waitPayContent}>
                {translate('deletedDetail.timeDeleted')} {'20-03-2020 09:10'}{' '}
              </DailyMartText>
            </View>
            <View style={styles.hr}></View>
            <View style={styles.waitPay}>
              <View style={styles.waitPayHeader}>
                <FastImage
                  source={R.images.ic_personalAddr}
                  style={styles.waitPayIcon}
                />
                <DailyMartText style={styles.waitPayHeaderText}>
                  {translate('receivedDetail.addrDelivery')}
                </DailyMartText>
              </View>
              <DailyMartText style={styles.waitPayContent}>
                {translate('personalLogin.name')}
              </DailyMartText>
              <DailyMartText style={styles.waitPayContent}>
                {translate('personalLogin.phone')}
              </DailyMartText>
              <DailyMartText style={styles.waitPayContent}>
                {translate('personalLogin.addr')}
              </DailyMartText>
              <DailyMartText style={styles.waitPayContent}>
                {translate('receivedDetail.specificAddr')}
              </DailyMartText>
            </View>
            <View style={styles.HR}></View>
            <View style={styles.waitPay}>
              <View style={styles.waitPayHeader}>
                <FastImage source={R.images.methodPay} style={styles.waitPayIcon} />
                <DailyMartText style={styles.waitPayHeaderText}>
                  {translate('receivedDetail.methodPay')}
                </DailyMartText>
              </View>
              <DailyMartText style={styles.codText}>
                {translate('receivedDetail.cod')}
              </DailyMartText>
              <DailyMartText style={styles.sumMoney}>
                {translate('receivedDetail.sumMoney')}
                {'447.000 đ'}
              </DailyMartText>
            </View>
            <View style={styles.HR}></View>
            <FlatList
              style={styles.flatlist}
              showsHorizontalScrollIndicator={false}
              data={received.slice(0, 1)}
              renderItem={({ item, index }) => {
                return (
                  <FlatListReceived
                    item={item}
                    index={index}
                  ></FlatListReceived>
                );
              }}
            />
          </ScrollView>
        </View>
      </Container>
    );
  }
}
