import { DeletedDetail } from "../model/deleted-detail";

export interface DeletedDetailView {
     onFetchDataSuccess(deliveredDetail: DeletedDetail): void;
     onFetchDataFail(error?: any): void;
 }