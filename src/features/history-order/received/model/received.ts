import { CartAll } from 'features/cart/model/cart';

export interface Received {
  cart: CartAll;
  cartId: string;
  code: string;
  createdAt: string;
  createdBy: string;
  deletedAt?: string;
  deliveryAddressId: string;
  deliveryTime: string;
  name: string;
  orderedDeliveryAddress: OrderedDeliveryAddress;
  payment: null;
  paymentMethod: string;
  phoneNumber: string;
  status: string;
  totalPrice: number;
  _id: string;
}

export interface OrderedDeliveryAddress {
  cityId: string;
  cityName: string;
  createdAt: string;
  createdBy: string;
  deletedAt: string;
  detail: string;
  districtId: string;
  districtName: string;
  isDefault: boolean;
  lat: string | number;
  lon: string | number;
  receiverAddress: string;
  receiverName: string;
  receiverPhone: string;
  updatedAt: string;
  wardId: string;
  wardName: string;
  __v: string;
  _id: string;
}
