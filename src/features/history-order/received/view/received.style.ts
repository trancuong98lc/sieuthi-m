import { StyleSheet } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
  container: {
    //backgroundColor:R.colors.white100,
    flex: 1
  },
  header: {
    backgroundColor: R.colors.primaryColor,
    flexDirection: 'row',
    alignItems: 'center'
  },
  buttonBack: {
    backgroundColor: R.colors.primaryColor
  },
  imageButtonBack: {
    height: 35,
    width: 35,
    marginTop: 5,
    marginBottom: 13,
    marginLeft: 12
  },
  new: {
    fontFamily: R.fonts.bold,
    fontSize: 18,
    color: R.colors.white100,
    marginTop: 5,
    marginLeft: DimensionHelper.getScreenWidth() * 0.23
  },
  status: {
    flexDirection: 'row',
    paddingTop: 1
  },
  itemStatus: {},
  buttonReceived: {
    height: 45,
    width: DimensionHelper.getScreenWidth() / 3,
    borderRadius: 0
  },
  textReceived: {
    fontFamily: R.fonts.medium,
    fontSize: 15,
    color: R.colors.primaryColor
  },
  textDelivered: {
    fontFamily: R.fonts.medium,
    fontSize: 15,
    color: '#333333'
  },
  hr: {
    borderTopWidth: 2,
    borderColor: R.colors.primaryColor
  },
  viewFlatlist: {
    backgroundColor: R.colors.white100,
    marginTop: 10,
    marginLeft: 12,
    borderTopLeftRadius: 12
  },
  flatlist: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 10,
    backgroundColor: '#E5E5E5'
  }
});
