import { DimensionHelper } from 'helpers/dimension-helper';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { TabView } from 'react-native-tab-view';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack, resetStack } from 'routing/service-navigation';
import { HistoryCancelScreen, HistoryDeliveredScreen, HistoryReceivedScreen, HomeScreen } from '../../../../routing/screen-name';
import TabHistoryCancel from './components/TabhistoryCancel';
import TabHistoryReceived from './components/TabHistoryReceived';
import TabHistoryReceived2 from './components/TabHistoryReciced2';
import { ReceivedView } from './received.view';


export interface Props {
  route: any
}
interface State {
  // vegetable: Vegetable;
  index: any
  routes: { key: string, title: string }[],
}

export interface ReqHistory {
  status: string,
  after?: string,
  limit: string
}

export default class ReceivedScreen extends React.PureComponent<Props, State>
  implements ReceivedView {
  constructor(props: Props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {
          key: HistoryReceivedScreen,
          title: 'historyOrder.received',
        },
        {
          key: HistoryDeliveredScreen,
          title: 'historyOrder.delivered',
        },
        {
          key: HistoryCancelScreen,
          title: 'historyOrder.deleted',
        },
      ],
    }
  }

  componentDidMount = () => {
  };
  onFetchDataSuccess(received: import('../model/received').Received): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }



  renderScene = ({ route }: any) => {
    switch (route.key) {
      case HistoryReceivedScreen:
        return (
          <TabHistoryReceived />
        );

      case HistoryDeliveredScreen:
        return (
          <TabHistoryReceived2 />
        );

      case HistoryCancelScreen:
        return (
          <TabHistoryCancel />
        );

      default:
        return null;
    }
  };

  onTabPressed = (index: number) => () => {
    this.setState({ index }, () => {
      // if (index == 0) {
      //   const newParams = {
      //     ...this.paramsHis,
      //     status: STAUTS_HISTORY.ACKNOWLEDGED,
      //   }
      //   delete newParams.after
      //   this.paramsHis = newParams
      //   this.getOrderHistory(this.paramsHis)
      // }
      // if (index == 1) {
      //   const newParams = {
      //     ...this.paramsHis,
      //     status: STAUTS_HISTORY.DELIVERED,
      //   }
      //   delete newParams.after
      //   this.paramsHis = newParams
      //   this.getOrderHistory(this.paramsHis)
      // }
      // if (index == 2) {
      //   const newParams = {
      //     ...this.paramsHis,
      //     status: STAUTS_HISTORY.CANCEL,
      //   }
      //   delete newParams.after
      //   this.paramsHis = newParams
      //   this.getOrderHistory(this.paramsHis)
      // }
    });
  };


  renderTabBar = (props: any) => {
    let { routes, index } = props.navigationState;
    return (
      <View style={styles2.itemChild}>
        {routes.map((route: any, positionFocus: number) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this.onTabPressed(positionFocus)}
            style={[
              styles2.btnLabel,
              positionFocus === this.state.index &&
              styles2.btnActive,

            ]}
          >
            <DailyMartText
              fontStyle="semibold"
              style={[
                styles2.label,
                positionFocus === this.state.index &&
                styles2.labelActive,
              ]}
            >
              {translate(route.title)}
            </DailyMartText>
          </TouchableOpacity>
        ))}
      </View>
    );
  };
  onBackPress = () => {
    const screen = this.props.route?.params?.screen || null
    if (screen && screen == "menu") {
      goBack()
    } else {
      resetStack(HomeScreen)
    }
  }

  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService onBackPress={this.onBackPress} title="historyOrder.header" />

        <TabView
          navigationState={this.state}
          renderScene={this.renderScene}
          renderTabBar={this.renderTabBar}
          onIndexChange={index => this.setState({ index })}
          initialLayout={styles2.inititalLayout}
          lazy
        />
        {/* <View style={styles.container}>
          <View style={styles.header}>
            <Buttons
              onPress={goBack}
              source={R.images.ic_back2}
              styleButton={styles.buttonBack}
              styleImageButton={styles.imageButtonBack}
            />
            <DailyMartText style={styles.new}>
              {translate('historyOrder.header')}
            </DailyMartText>
          </View>
          <View style={styles.status}>
            <View style={styles.itemStatus}>
              <Buttons
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.received')}
                styleTextButton={styles.textReceived}
              />
              <View style={styles.hr}></View>
            </View>
            <View style={styles.itemStatus}>
              <Buttons
                onPress={() => navigate(DeliveredScreen)}
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.delivered')}
                styleTextButton={styles.textDelivered}
              />
            </View>
            <View style={styles.itemStatus}>
              <Buttons
                onPress={() => navigate(DeletedScreen)}
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.deleted')}
                styleTextButton={styles.textDelivered}
              />
            </View>
          </View>
          <View style={styles.viewFlatlist}>
            <FlatList
              style={styles.flatlist}
              showsHorizontalScrollIndicator={false}
              data={received.slice(1, 3)}
              renderItem={({ item, index }) => {
                return (
                  <FlatListReceived
                    item={item}
                    index={index}
                  ></FlatListReceived>
                );
              }}
            />
          </View>
        </View> */}
      </Container>
    );
  }
}

const styles2 = StyleSheet.create({
  btnLabel: {
    justifyContent: 'center',
    alignItems: 'center',
    width: DimensionHelper.width / 3,
    paddingVertical: 13,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#eee'
  },
  btnActive: {
    borderBottomWidth: 2,
    borderBottomColor: R.colors.primaryColor
  },
  label: {
    fontSize: 14,
    alignContent: 'center',
    textAlign: 'center',
    color: '#111',
    fontFamily: R.fonts.medium  
  },
  labelActive: {
    color: R.colors.primaryColor,
    fontFamily: R.fonts.bold 
  },
  inititalLayout: {
    width: DimensionHelper.width,
    height: 0
  },
  itemChild: {
    flexDirection: 'row'
  },
});
