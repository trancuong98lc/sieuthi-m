import { CartAll } from 'features/cart/model/cart';
import ApiHelper from 'helpers/api-helper';
import EventBus, { EventBusName, EventBusType } from 'helpers/event-bus';
import { URL_API } from 'helpers/url-api';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, ListRenderItem, View } from 'react-native';
import { translate } from 'res/languages';
import { Subscription } from 'rxjs';
import { STATUS } from 'types/BaseResponse';
import { STAUTS_HISTORY } from 'types/category';
import { Received } from '../../model/received';
import { ReqHistory } from '../received.screent';
import { styles } from '../received.style';
import ItemTab from './ItemTab';

export interface TabHistoryReceivedProps {
}
export interface TabHistoryReceivedStates {
    loading: boolean,
    maxdata: boolean,
    dataHistory: Received[]
}

export default class TabHistoryCancel extends React.PureComponent<TabHistoryReceivedProps, TabHistoryReceivedStates> {
    constructor(props: TabHistoryReceivedProps) {
        super(props);
        this.state = {
            loading: true,
            maxdata: false,
            dataHistory: [],
        }
    }

    paramsHis: ReqHistory = {
        limit: '10',
        status: STAUTS_HISTORY.CANCEL,
    }

    // componentDidMount = () => {
    //     this.onRefresh()
    // };
    subScription = new Subscription()

    componentDidMount = () => {
        this.onRefresh();
        this.subScription.add(EventBus.getInstance().events.subscribe((data: EventBusType) => {
            if (data.type == EventBusName.UPDATE_ORDER_HISTORY) {
                this.onRemoveReceived(data.payload)
            }
        }))
    };

    onRemoveReceived = (data: Received) => {
        const newData = [data, ...this.state.dataHistory]
        this.setState({
            dataHistory: newData
        })
    }

    componentWillUnmount = () => {
        this.subScription && this.subScription.unsubscribe()
    };


    onRefresh = () => {
        this.getOrderHistory(this.paramsHis)
    }
    isLoadmore: boolean = false;

    getOrderHistory = async (params: ReqHistory) => {
        this.setLoading()
        try {
            const res = await ApiHelper.fetch(URL_API.ORDER_HISTORY, params, true)
            if (res.status == STATUS.SUCCESS) {
                let maxdata = res.data.length < 10;
                this.isLoadmore = false
                const newData = params.after ? [...this.state.dataHistory, ...res.data] : res.data
                this.setState({
                    dataHistory: newData,
                    maxdata,
                    loading: false
                })
            } else {
                this.setState({
                    maxdata: true,
                    loading: false
                })
            }
        } catch (error) {

        }
    }

    setLoading = () => {
        this.setState({
            loading: !this.state.loading
        })
    }

    onEndReached = () => {
        let { maxdata } = this.state;
        if (maxdata) return;
        if (this.isLoadmore) return;
        if (this.state.dataHistory.length == 0 && this.state.dataHistory.length < 10) return
        this.isLoadmore = true;
        const newParasm: ReqHistory = {
            ...this.paramsHis,
            after: this.state.dataHistory[this.state.dataHistory.length - 1]._id
        }
        this.paramsHis = newParasm
        this.getOrderHistory(this.paramsHis)
    }



    private keyExtractor = (item: any, index: any): string => index.toString();

    private renderItem: ListRenderItem<any> = ({ item, index }) => {
        return <ItemTab type="CANCEL" isShow={true} item={item} index={index} />;
    };

    renderFooter = () => <View style={{ height: 100 }} />

    ListEmptyComponent = () => {
        if (this.state.dataHistory.length > 0 || this.state.loading) return null;
        return (<EmptylistComponent name="no_order" />);
    };

    public render() {
        return (
            <FlatList
                data={this.state.dataHistory}
                onRefresh={this.onRefresh}
                refreshing={this.state.loading}
                showsVerticalScrollIndicator={false}
                style={styles.flatlist}
                onEndReached={this.onEndReached}
                contentContainerStyle={{ alignItems: 'center' }}
                ListFooterComponent={this.renderFooter}
                ListEmptyComponent={this.ListEmptyComponent}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
            />
        );
    }
}
