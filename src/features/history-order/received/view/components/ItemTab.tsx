import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList, ImageBackground } from 'react-native';
import R from '/res/R';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartText from 'libraries/text/text-daily-mart';
import { navigate } from 'routing/service-navigation';
import { ReceivedDetailScreen } from 'routing/screen-name';
import FastImage from 'react-native-fast-image';
import { Store_Type } from 'features/store/model/store-entity';
import { Cart } from 'features/cart/model/cart';
import { getMediaUrl, _formatPrice } from 'helpers/helper-funtion';
import TextPrice from 'libraries/text/TextPrice';
import { translate } from 'res/languages';
import NumberFormat from 'react-number-format';

interface Items {
    orderCode: string;
    nameSupermarket: string;
    newcost: string;
    oldcost: string;
    image: any;
    sold: number;
    product: [];
}

export interface Props {
    item: Cart;
    index: number;

}
export interface PropsContainer {
    item: any;
    index: number;
    isShow: boolean;
    type: string
}

class ViewProduct extends React.PureComponent<Props, any> {
    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        // console.log(a);
        const { item } = this.props
        return (
            <View style={styles.containerProduct}>
                <View style={styles.hrProduct} />
                <View style={styles.itemProduct}>
                    <FastImage style={styles.imageProduct} source={getMediaUrl(item && item.medias && item.medias.length > 0 ? item?.medias[0] : null)} />
                    <View>
                        <DailyMartText style={styles.name}>{item?.name}</DailyMartText>
                        <TextPrice total={item?.price || 0} style={styles.cost} />
                        {/* <DailyMartText style={styles.cost}>{_formatPrice(item?.price || 0)}</DailyMartText> */}
                    </View>
                    <DailyMartText style={styles.count}> x{item?.qty}</DailyMartText>
                </View>
            </View>
        );
    }
}

export default class ItemTab extends React.PureComponent<PropsContainer, any> {
    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    renderItemProduct = ({ item, index }: any) => {
        const newItem: Cart = { ...item, ...item.product! }
        delete newItem.product
        return <ViewProduct item={newItem} index={index} />;
    };

    private keyExtractor = (item: Cart): string => item.productId.toString();

    gotoDetailHis = () => {
        const { item, type } = this.props
        navigate(ReceivedDetailScreen, { orderId: item._id, type: type })
    }

    public render() {
        // console.log(a);
        const { item, index, isShow } = this.props
        const { cart, orderedDeliveryAddress } = item
        const { orderedStore } = cart
        const store: Store_Type = orderedStore ? JSON.parse(orderedStore) : {}
        const code = item.code && item.code.split('-').length > 1 ? item.code.split('-') || '' : item.code || ''
        const { wardName, districtName, cityName } = orderedDeliveryAddress;
        return (
            <View style={[styles.container, { borderTopLeftRadius: index == 0 ? 10 : 0, }]}>
                <TouchableOpacity onPress={this.gotoDetailHis}>
                    <View style={styles.header}>
                        {isShow && <FastImage style={styles.imageCodeOrder} source={R.images.grounmasieuthi} >
                            <DailyMartText style={styles.textCodeOrder}>#{typeof code !== 'string' && code.length > 1 ? code[1].toString() : code}</DailyMartText>
                        </FastImage>}
                        <FastImage style={styles.image} source={R.images.ic_store} />
                        <DailyMartText style={styles.nameSupermarket}>{store ? store.name : ''}</DailyMartText>
                        <FastImage style={styles.ic_next} source={R.images.ic_nextPerson} />
                    </View>
                </TouchableOpacity>
                <View style={styles.deliveryAddress}>
                    <FastImage source={R.images.ic_personalAddr} style={styles.inforIcon} resizeMode={'contain'} />
                    <View style={{ marginLeft: 10, marginTop: 3 }}>
                        <DailyMartText style={{ fontFamily: R.fonts.bold }}>{translate('addrPay.addrShip')}</DailyMartText>
                        <DailyMartText style={{ marginTop: 5 }}>{orderedDeliveryAddress.detail}</DailyMartText>
                        <DailyMartText style={{ marginTop: 5 }}>{(wardName !== '' ? wardName + ', ' : '') + (districtName !== '' ? districtName + ', ' : '') + cityName}</DailyMartText>
                    </View>
                </View>
                <View style={styles.price}>
                    <FastImage source={R.images.methodPay} style={styles.inforIcon} resizeMode={'contain'} />
                    <DailyMartText style={styles.priceTitle}>{`${translate('receivedDetail.sumMoney')}`}
                        <NumberFormat
                            value={item.totalPrice}
                            displayType="text"
                            thousandSeparator="."
                            decimalSeparator=","
                            renderText={(value: any) => (

                                <DailyMartText style={styles.priceValue}> {`${value} đ`}</DailyMartText>
                            )}
                        />
                    </DailyMartText>

                </View>
                {/* <FlatList
                    data={cart.items}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItemProduct} /> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        paddingBottom: 10,
        marginBottom: 10,
        width: DimensionHelper.width - 10,
    },
    header: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center'
    },
    ViewCodeOrder: {
        justifyContent: 'center',
        alignContent: 'center',
        position: 'absolute',
    },
    imageCodeOrder: {
        height: 45,
        width: 130,
        marginLeft: -8,
        justifyContent: 'center',
        alignContent: 'center',

    },
    textCodeOrder: {
        fontSize: 14,
        color: R.colors.white100,
        marginLeft: 6
    },
    image: {
        height: 19,
        width: 19,
        marginLeft: 10
    },
    nameSupermarket: {
        fontFamily: R.fonts.medium,
        fontSize: 13,
        color: R.colors.primaryColor,
        marginLeft: 10,
        flex: 1
    },
    ic_next: {
        width: 8,
        height: 14,
        marginRight: 12
    },
    deliveryAddress: {
        paddingHorizontal: 15,
        paddingVertical: 5,
        marginVertical: 5,
        flexDirection: "row",
        borderTopWidth: 2,
        borderTopColor: '#F5F5F5',
    },
    inforIcon: {
        width: 20,
        height: 20
    },
    price: {
        marginHorizontal: 15,
        marginVertical: 5,
        flexDirection: "row"
    },
    priceTitle: {
        marginLeft: 10,
        marginTop: -3,
        fontFamily: R.fonts.bold,
        fontSize: 16, color:
            R.colors.primaryColor
    },
    priceValue: {
        fontFamily: R.fonts.bold,
        fontSize: 18,
        color: R.colors.primaryColor
    },
    containerProduct: {
        marginTop: 10
    },
    hrProduct: {
        borderTopWidth: 1,
        borderColor: '#F2F2F2'
        // borderColor:'red'
    },
    itemProduct: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        paddingRight: 10,
        alignItems: 'center',
        marginTop: 10
    },
    imageProduct: {
        height: 70,
        width: 70,
        marginLeft: 10
    },
    name: {
        fontFamily: R.fonts.bold,
        fontSize: 15,
        color: '#333333',
        width: DimensionHelper.getScreenWidth() * 0.6,
        marginLeft: 10
        // marginTop:-10
    },
    cost: {
        fontFamily: R.fonts.bold,
        fontSize: 15,
        color: '#3BBB5A',
        marginLeft: 10,
        marginTop: 40
    },
    count: {
        fontSize: 18,
        color: '#A8A8A8'
        //marginTop:40
    },
    hr: {
        borderTopWidth: 10,
        borderColor: '#F5F5F5',
        marginTop: 10
    }
});
