import { Received } from "../model/received";

export interface ReceivedView {
     onFetchDataSuccess(received: Received): void;
     onFetchDataFail(error?: any): void;
 }