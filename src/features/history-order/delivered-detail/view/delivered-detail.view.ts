import { DeliveredDetail } from "../model/delivered-detail";

export interface DeliveredDetailView {
     onFetchDataSuccess(deliveredDetail: DeliveredDetail): void;
     onFetchDataFail(error?: any): void;
 }