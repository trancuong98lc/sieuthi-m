import received from 'data/received';
import Buttons from 'libraries/button/buttons';
import FlatListDelivered from 'libraries/flatlist/flatlist-delivered';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import {
  DeletedScreen,
  PersonalLoginScreen,
  ReceivedScreen
} from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { Delivered } from '../model/delivered';
import { styles } from './delivered.style';
import { DeliveredView } from './delivered.view';

export interface Props { }

export default class DeliveredScreen extends React.PureComponent<Props, any>
  implements DeliveredView {
  constructor(props: Props) {
    super(props);
  }
  onFetchDataSuccess(delivered: Delivered): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Buttons
              onPress={() => navigate(PersonalLoginScreen)}
              source={R.images.ic_back2}
              styleButton={styles.buttonBack}
              styleImageButton={styles.imageButtonBack}
            />
            <DailyMartText style={styles.new}>
              {translate('historyOrder.header')}
            </DailyMartText>
          </View>
          <View style={styles.status}>
            <View style={styles.itemStatus}>
              <Buttons
                onPress={() => navigate(ReceivedScreen)}
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.received')}
                styleTextButton={styles.textNotActive}
              />
            </View>
            {/* <View style={styles.itemStatus}>
              <Buttons
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.delivered')}
                styleTextButton={styles.textActive}
              />
              <View style={styles.hr}></View>
            </View>
            <View style={styles.itemStatus}>
              <Buttons
                onPress={() => navigate(DeletedScreen)}
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.deleted')}
                styleTextButton={styles.textNotActive}
              />
            </View>*/}
          </View>
          <View style={styles.viewFlatlist}>
            <FlatList
              style={styles.flatlist}
              showsVerticalScrollIndicator={false}
              data={received}
              renderItem={({ item, index }) => {
                return <FlatListDelivered item={item} index={index} />;
              }}
            />
          </View>
        </View>
      </Container>
    );
  }
}
