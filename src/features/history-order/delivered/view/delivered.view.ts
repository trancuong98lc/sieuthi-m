import { Delivered } from "../model/delivered";


export interface DeliveredView {
     onFetchDataSuccess(delivered: Delivered): void;
     onFetchDataFail(error?: any): void;
 }