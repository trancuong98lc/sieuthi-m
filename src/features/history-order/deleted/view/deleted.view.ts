import { Deleted } from "../model/deleted";

export interface DeletedView {
     onFetchDataSuccess(deleted: Deleted): void;
     onFetchDataFail(error?: any): void;
 }