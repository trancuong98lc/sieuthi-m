import * as React from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import Buttons from 'libraries/button/buttons';
import { goBack, navigate } from 'routing/service-navigation';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import {
  ReceivedScreen,
  DeliveredScreen,
  PersonalLoginScreen
} from 'routing/screen-name';
import { Deleted } from '../model/deleted';
import { DeletedView } from './deleted.view';
import { styles } from './deleted.style';
import received from 'data/received';
import FlatListDeleted from 'libraries/flatlist/flatlist-deleted';
import Container from 'libraries/main/container';

export interface Props {}

export default class DeletedScreen extends React.PureComponent<Props, any>
  implements DeletedView {
  constructor(props: Props) {
    super(props);
  }
  onFetchDataSuccess(deleted: Deleted): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Buttons
              onPress={() => navigate(PersonalLoginScreen)}
              source={R.images.ic_back2}
              styleButton={styles.buttonBack}
              styleImageButton={styles.imageButtonBack}
            />
            <DailyMartText style={styles.new}>
              {translate('historyOrder.header')}
            </DailyMartText>
          </View>
          <View style={styles.status}>
            <View style={styles.itemStatus}>
              <Buttons
                onPress={() => navigate(ReceivedScreen)}
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.received')}
                styleTextButton={styles.textNotActive}
              />
            </View>
            <View style={styles.itemStatus}>
              <Buttons
                onPress={() => navigate(DeliveredScreen)}
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.delivered')}
                styleTextButton={styles.textNotActive}
              />
            </View>
            <View style={styles.itemStatus}>
              <Buttons
                styleButton={styles.buttonReceived}
                textButton={translate('historyOrder.deleted')}
                styleTextButton={styles.textActive}
              />
              <View style={styles.hr}></View>
            </View>
          </View>
          <View style={styles.viewFlatlist}>
            <FlatList
              style={styles.flatlist}
              showsVerticalScrollIndicator={false}
              data={received}
              renderItem={({ item, index }) => {
                return <FlatListDeleted item={item} index={index} />;
              }}
            />
          </View>
        </View>
      </Container>
    );
  }
}
