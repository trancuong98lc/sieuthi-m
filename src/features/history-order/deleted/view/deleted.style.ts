import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          //backgroundColor:R.colors.white100
     },
     header: {
          backgroundColor: R.colors.primaryColor,
          flexDirection: 'row',
          alignItems: 'center',
          
     },
     buttonBack: {
          backgroundColor: R.colors.primaryColor,
          
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop:  5,
          marginBottom: 13,
          marginLeft:12
     },
     new: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 5,
          marginBottom:10,
          marginLeft:DimensionHelper.getScreenWidth()*0.23
     },
     status: {
          flexDirection: 'row',
          paddingTop:1
     },
     itemStatus: {
          
          alignItems:'center'
     },
     buttonReceived:{
          height: 45,
          width: DimensionHelper.getScreenWidth() / 3,
          borderRadius:0
     },
     textActive: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color:R.colors.primaryColor
     },
     textNotActive: {
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color: '#333333',
          
     },
     hr: {
          borderTopWidth: 2,
          borderColor: R.colors.primaryColor,
          width: DimensionHelper.getScreenWidth() / 4,
     },
     viewFlatlist: {
          backgroundColor: R.colors.white100,
          marginTop: 10,
          marginLeft: 12,
          borderTopLeftRadius: 12,
          
          
     },
     flatlist: {
          marginBottom:DimensionHelper.getScreenHeight()*0.31
          //marginTop: 10,
          
          //marginLeft: -6,
          //paddingTop:10
          //backgroundColor: '#FFFFFF',
         
          
     }
})