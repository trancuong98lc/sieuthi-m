import hotsale from 'data/product';
import ProductPromotionItem from 'features/component/product-item-promotion';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import {
  FlatList, TouchableOpacity, View
} from 'react-native';
import { translate } from 'res/languages';
import { PromotionScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../../view/home.style';
import { Promotion } from '../../model/home';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import Commons from 'helpers/Commons';
import { STATUS } from 'types/BaseResponse';

export interface PromotionProps { }

export interface PromotionStates {
  promotions: Promotion[];
  loading: boolean,
}

export default class PromotionComponent extends React.PureComponent<PromotionProps, PromotionStates> {
  constructor(props: PromotionProps) {
    super(props);
    this.state = {
      promotions: [],
      loading: true
    }
  }

  params: any = {
    page: 1,
    limit: 10
  }

  componentDidMount = () => {
    this.getPromotions();
  }

  onRefresh = () => {
    const newParams =
    {
      limit: 10,
      page: 1,
    }
    this.params = newParams
    this.getPromotions();
  }

  getPromotions = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.PROMOTION + Commons.idStore, this.params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length == 0) {
        } else {
          this.setPromotions(res.data, false);
        }
      } else {
        this.setPromotions([], false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.setPromotions([], false);
    }
  }

  setPromotions = (promotions: Promotion[], loading: boolean) => {
    this.setState({
      promotions,
      loading: loading
    });
  }

  gotoPromotion = () => navigate(PromotionScreen);

  renderItem = ({ item, index }: any) => {
    return <ProductPromotionItem promotion style={{ paddingRight: 0, paddingBottom: 10 }} item={item} index={index} />;
  };

  keyExtractor = (item: Promotion, index: number) => {
    return item._id.toString();
  }

  public render() {
    const { promotions, loading } = this.state;
    return (
      <View style={styles.sanphamkhuyenmai}>
        <View style={styles.headersanphamkhuyenmai}>
          <DailyMartText style={styles.title3}>
            {translate('home.title4')}
          </DailyMartText>
          <TouchableOpacity
            activeOpacity={0.6}
            style={styles.buttonsanphamkhuyenmai}
            onPress={this.gotoPromotion}
          >
            <DailyMartText style={styles.textuudai}>
              {translate('button.seeall')}
            </DailyMartText>
            <BaseIcon name="ic_arrow_right" iconStyle={{ marginLeft: 5 }} width={13} />
          </TouchableOpacity>
        </View>
        {!loading && <FlatList
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.flatlistSanphamkhuyenmaiContent}
          data={promotions}
          horizontal
          keyExtractor={this.keyExtractor}
          style={styles.flatlistSanphamkhuyenmai}
          renderItem={this.renderItem}
        />}
      </View>
    );
  }
}
