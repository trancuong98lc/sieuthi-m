import tintuc from 'data/tintuc';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { navigate } from 'routing/service-navigation';
import { styles } from '../../view/home.style';
import { NewsScreen } from 'routing/screen-name';
import FlatListTintuc from 'libraries/flatlist/flatlist-home/flatlist.tintuc';
import ItemHotNews from './home-new-item-new';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import Commons from 'helpers/Commons';
import { STATUS } from 'types/BaseResponse';
import { HotNews } from '../../model/home';
import { loadOptions } from '@babel/core';

export interface HotNewsProps { }

export interface HotNewsStates {
  hotNews: HotNews[],
  loading: boolean
}

export default class HotNewsComponent extends React.PureComponent<HotNewsProps, HotNewsStates> {
  constructor(props: HotNewsProps) {
    super(props);
    this.state =  {
      hotNews: [],
      loading: true
    }
  }

  params: any = {
    page: 1,
    limit: 3,
    status: true
  }

  componentDidMount = () => {
    this.onFetchNews();
  }

  gotoNewScreen = () => {
    navigate(NewsScreen);
  };

  onRefresh = () => {
    const newParams =
    {
      limit: 10,
      page: 1,
      status: true
    }
    this.params = newParams
    this.onFetchNews();
  }

  onFetchNews = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.HOTNEWS, this.params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length == 0) {
        } else {
          this.setHotNews(res.data, false);
        }
      } else {
        this.setHotNews([], false);
      }
    } catch (error) {
      console.log('error onFetchNews: ', error);
      this.setHotNews([], false);
    }
  }

  setHotNews = (hotNews: HotNews[], loading: boolean) => {
    this.setState({
      hotNews,
      loading
    })
  }

  renderItem = ({ item, index }: any) => {
    return <ItemHotNews item={item} index={index} />;
  };

  keyExtractor = (item: HotNews, index: number) => {
    return item._id.toString();
  }

  public render() {
    const { hotNews, loading } = this.state;
    return (
      <View style={styles.sanphamkhuyenmai}>
        <View style={styles.headersanphamkhuyenmai}>
          <DailyMartText style={styles.title3}>
            {translate('home.title5')}
          </DailyMartText>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={this.gotoNewScreen}
            style={styles.buttontintuc}
          >
            <DailyMartText style={styles.textuudai}>
              {translate('button.seeall')}
            </DailyMartText>
            <BaseIcon name="ic_arrow_right" iconStyle={{ marginLeft: 5 }} width={13} />
          </TouchableOpacity>
        </View>
        <View>
          {!loading && <FlatList
            style={styles.flatlistTintuc}
            data={hotNews}
            maxToRenderPerBatch={3}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
          />}
        </View>
      </View>
    );
  }
}
