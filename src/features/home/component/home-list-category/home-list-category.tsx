import * as React from 'react';
import { View, StyleSheet, Text, FlatList, ListRenderItem } from 'react-native';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import { styles } from '../../view/home.style';
import ItemListCateGory from './home-list-category-item';
import { Category, ReqCategory } from 'features/home/model/home';
import { HomePresenter } from 'features/home/presenter/home.presenter';
import { STATUS_SEARCH } from 'types/category';
import ContentLoader from 'libraries/ContentLoader/LoaderImage';
import { ScrollView } from 'react-native-gesture-handler';
import { DimensionHelper } from 'helpers/dimension-helper';

export interface ListCateGoryProps {
  presenter: HomePresenter;
}

export interface StatesCategory {
  categories: Category[];
  loading: boolean,
  maxdata: boolean,
}

export default class ListCateGory extends React.PureComponent<ListCateGoryProps, StatesCategory> {
  constructor(props: ListCateGoryProps) {
    super(props);
    this.state = {
      categories: [],
      loading: true,
      maxdata: true
    };
  }

  paramsCate: ReqCategory = {
    limit: '100',
    page: '1',
    status: STATUS_SEARCH.ACTIVE
  };

  componentDidMount = () => {
    this.props.presenter.onFetchCategory(this.paramsCate);
  };

  onRefresh = () => {
    const newParams =
    {
      limit: '100',
      page: '1',
      status: STATUS_SEARCH.ACTIVE
    }
    this.paramsCate = newParams
    this.props.presenter.onFetchCategory(newParams);
  }

  setMaxData = (maxdata: boolean) => {
    this.setState({
      maxdata
    });
  }

  setCateGory = (categories: Category[], loading: boolean) => {
    this.setState({
      categories: categories,
      loading
    });
  };

  keyExtractor = (item: Category): string => item._id.toString();

  renderItem = ({ item, index }: { item: Category, index: number }) => {
    return <ItemListCateGory item={item} index={index} />;
  };

  onScroll = (e: any) => {
    // if (this.state.categories.length >= 8 && this.state.maxdata) {
    //   this.paramsCate.page = String(Number(this.paramsCate.page) + 1)
    //   this.props.presenter.onFetchCategory(this.paramsCate);
    // }

  }

  public render() {
    const { categories, loading } = this.state;
    return (
      <View>
        <DailyMartText style={styles.title1}>{translate('home.title1')}</DailyMartText>
        <ScrollView scrollEnabled={true} onScrollEndDrag={this.onScroll} horizontal showsHorizontalScrollIndicator={false}>
          {!loading && < FlatList
            data={categories}
            style={styles.flatlistDanhmucsanpham}
            key={categories.length % 2 == 0 ? categories.length / 2 : Math.round(categories.length / 2)}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexDirection: 'column',
              // flexWrap: 'wrap'
            }}
            numColumns={categories.length % 2 == 0 ? categories.length / 2 : Math.round(categories.length / 2)}
            scrollEnabled={false}
            showsHorizontalScrollIndicator={false}
            initialNumToRender={8}
            extraData={this.state.categories}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
          />}
        </ScrollView>
        <DailyMartText style={{ height: 8, backgroundColor: '#DCDCDC' }} />
      </View>
    );
  }
}
