import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import { navigate } from 'routing/service-navigation';
import { VegetableScreen } from 'routing/screen-name';
import { Category } from 'features/home/model/home';
import { getMediaUrl } from 'helpers/helper-funtion';
import FastImage from 'react-native-fast-image';
import ContentLoader from 'libraries/ContentLoader/LoaderImage';
import { PHOTO_SIZE } from 'types/ConfigType';
import DailyMartText from 'libraries/text/text-daily-mart';
export interface ListCateGoryProps {
  item: Category;
  index: number;
}

export default class ItemListCateGory extends React.PureComponent<
  ListCateGoryProps,
  any
  > {
  constructor(props: ListCateGoryProps) {
    super(props);
  }


  onNavigate = () => {
    const { item } = this.props;
    navigate(VegetableScreen, {
      category: item
    });
  };
  public render() {
    const { item } = this.props;
    return (
      // <View style={styles.container}>
      <TouchableOpacity
        style={styles.container}
        key={item._id}
        activeOpacity={0.8}
        onPress={this.onNavigate}
      >
        <FastImage
          source={getMediaUrl(item.thumbnail, PHOTO_SIZE.Medium)}
          style={styles.image}
        />
        <DailyMartText numberOfLines={2} style={styles.name}>{item.name}</DailyMartText>
      </TouchableOpacity>
      // </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: DimensionHelper.getScreenWidth() / 4,
    paddingHorizontal: 12,
    marginVertical: 5

    //justifyContent: 'center',
    // alignContent:'center'
    //flexDirection:'row',
    // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen': 'tomato'
    ///backgroundColor: 'mediumseagreen',
  },
  image: {
    width: '75%',
    height: 55,
    marginBottom: 5,
    borderRadius: 5,
  },
  name: {
    fontSize: 13,
    color: '#666666'
  }
});
