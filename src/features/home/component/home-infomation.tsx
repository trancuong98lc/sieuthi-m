import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
//import LinearGradient from 'react-native-linear-gradient';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { Image, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { STATUS } from 'types/BaseResponse';
import { styles } from '../view/home.style';

export interface InfomationProps {}
export interface InfomationState {

}

export default class Infomation extends React.PureComponent<
  InfomationProps,
  InfomationState
> {
  constructor(props: InfomationProps) {
    super(props);
    this.state = {
      information: {
        name: '',
        email: '',
        address: '',
        phoneNumber: '',
        description: ''
      }
    }
  }

  componentDidMount = () => {
    this.getInformation();
  }

  getInformation = async () => {
    try {
      const res = await ApiHelper.fetch(
        `${URL_API.CONTACT}`,
        {},
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.setInformation(res.data);
      } else {
        // this.setInformation(null);
      }
    } catch (error) {
        console.log("getContact: ", error);
        // this.setInformation(null);
    }
  };

  setInformation = (information: any) => {
    this.setState({
      information
    })
  }

  public render() {
    const { information } = this.state;
    return (
      <View style={styles.contact}>
        <DailyMartText style={styles.contacttitle1}>
          {translate('contact.title1')}
        </DailyMartText>
        <DailyMartText style={styles.contacttitle2}>
          {information.name ? information.name : translate('contact.title2')}
        </DailyMartText>
        <View style={styles.contactconten}>
          <FastImage
            source={R.images.phone_contact}
            style={styles.imagecontact}
          ></FastImage>
          <DailyMartText style={styles.textcontact}>
            {information.phoneNumber ? information.phoneNumber : translate('contact.phone')}
          </DailyMartText>
        </View>
        <View style={styles.contactconten}>
          <FastImage
            source={R.images.emai_contact}
            style={styles.imagecontact}
          ></FastImage>
          <DailyMartText style={styles.textcontact}>
            {information.email ? information.email : translate('contact.email')}
          </DailyMartText>
        </View>
        <View style={styles.contactconten}>
          <FastImage
            source={R.images.addr_contact}
            style={styles.imagecontact}
          ></FastImage>
          <DailyMartText style={styles.textcontact}>
            {information.address ? information.address :  translate('contact.addr')}
          </DailyMartText>
        </View>
      </View>
    );
  }
}
