import ApiHelper from 'helpers/api-helper';
import { DimensionHelper } from 'helpers/dimension-helper';
import { URL_API } from 'helpers/url-api';
import ImageSwiperComponent from 'libraries/Image/ImageSwiperComponent';
import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import colors from 'res/colors';
import R from 'res/R';
import { STATUS } from 'types/BaseResponse';
import { MediaType } from 'types/ConfigType';
import { styles } from '../view/home.style';

export interface HomeBannerProps {}
export interface HomeBannerState {
  images: MediaType[];
}

export default class HomeBanner extends React.PureComponent<
  HomeBannerProps,
  any
> {
  constructor(props: HomeBannerProps) {
    super(props);
    this.state = {
      images: []
    };
  }

  componentDidMount = () => {
    this.getBanner();
  };

  getBanner = async () => {
    try {
      const res = await ApiHelper.fetch(
        URL_API.BANNER,
        { page: 1, limit: 40 },
        false
      );
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length == 0) {
          this.setBanner([]);
        } else {
          this.setBanner(res.data);
        }
      } else {
        this.setBanner([]);
      }
    } catch (error) {
      console.log('error getBanner: ', error);
    }
  };

  setBanner = (images: MediaType[]) => {
    this.setState({
      images
    });
  };

  public render() {
    const { images } = this.state;
    return (
      <View style={{ backgroundColor: 'white' }}>
        <FastImage
          source={R.images.header_home}
          resizeMode={FastImage.resizeMode.cover}
          style={{ width: '100%', height: 120 }}
        />
        {/* <FastImage source={R.images.home_banner} style={styles.homebanner} /> */}
        {images.length ? (
          <ImageSwiperComponent
            imgStyle={{ borderRadius: 5 }}
            banner
            autoplayTimeout={5}
            data={images}
            height={0.39 * DimensionHelper.getScreenWidth()}
            style={styles.homebanner}
          />
        ) : (
          <FastImage source={R.images.home_banner} style={styles.homebanner} />
        )}
      </View>
    );
  }
}
