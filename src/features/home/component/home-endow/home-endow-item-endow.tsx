import * as React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';

import { EndowDetailScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import FastImage from 'react-native-fast-image';
import { getMediaUrl } from 'helpers/helper-funtion';
import moment from 'moment';
import { translate } from 'res/languages';

interface Items {
  name: string;
  newcost: string;
  oldcost: string;
  image: any;
  sold: number;
}

export interface ItemEndowProps {
  item?: any;
  index: number;
}

export default class ItemEndow extends React.PureComponent<
  ItemEndowProps,
  any
  > {
  constructor(props: ItemEndowProps) {
    super(props);
    this.state = {};
  }

  navigateEndowDetail = () => {
    const { item } = this.props;
    navigate(EndowDetailScreen, {
      _id: item._id
    });
  }

  public render() {
    const { item } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={this.navigateEndowDetail}>
        <FastImage source={getMediaUrl(item.medias && item.medias.length > 0 ? item.medias[0] : undefined)} style={styles.image} />
        <View style={styles.text}>
          <DailyMartText numberOfLines={2} style={styles.name}>
            {item.name}
          </DailyMartText>
          <DailyMartText style={styles.date}>
            {translate('endow.since')} {moment(item.applyingTime.startDate).format('l')} {''}
            {translate('endow.to')} {moment(item.applyingTime.endDate).format('l')}
          </DailyMartText>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginBottom: 20,
    paddingBottom: 5,
    marginLeft: 15,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: R.colors.grey300
  },
  image: {
    width: 0.73 * DimensionHelper.getScreenWidth(),
    height: 0.30 * DimensionHelper.getScreenWidth(),
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
    borderBottomWidth: 1,
    borderBottomColor: R.colors.grey300
  },
  text: {
    backgroundColor: '#FFFFFF',
    width: 0.73 * DimensionHelper.getScreenWidth(),
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  name: {
    fontSize: 13,
    color: '#444444',
    marginVertical: 5,
    fontFamily: R.fonts.medium
  },
  date: {
    fontSize: 10,
    color: '#818181',
  }
});
