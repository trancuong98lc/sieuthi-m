import * as React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { styles } from '../../view/home.style';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import colors from 'res/colors';
import ItemEndow from './home-endow-item-endow';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import { navigate } from 'routing/service-navigation';
import { EndowScreen } from 'routing/screen-name';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import Commons from 'helpers/Commons';
import { STATUS } from 'types/BaseResponse';
import { Endow } from '../../model/home';

export interface ListEndowProps { }

export interface ListEndowStates {
  endows: Endow[];
  loading: boolean,
  // maxdata: boolean,
}

export default class EndowComponent extends React.PureComponent<ListEndowProps, ListEndowStates> {
  constructor(props: ListEndowProps) {
    super(props);
    this.state = {
      endows: [],
      loading: true 
    }
  }

  params = {
    page: 1,
    limit: 5
  }

  componentDidMount = () => {
    this.getEndow();
  }

  renderItem = ({ item, index }: any) => {
    return <ItemEndow item={item} index={index} />;
  };

  gotoEndow = () => {
    navigate(EndowScreen)
  }

  onRefresh = () => {
    const newParams =
    {
      limit: 5,
      page: 1,
    }
    this.params = newParams
    this.getEndow();
  }

  getEndow = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.ENDOW, this.params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length == 0) {
        } else {
          this.setEndow(res.data, false);
        }
      } else {
        this.setEndow([], false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.setEndow([], false);
    }
  }

  setEndow = (endows: Endow[], loading: boolean) => {
    this.setState({
      endows,
      loading: loading
    });
  }

  keyExtractor = (item: Endow) : string => {
    return item._id.toString();
  }

  public render() {
    const { endows, loading } = this.state;
    return (
      <View style={styles.sanphamkhuyenmai}>
        <View style={styles.headersanphamkhuyenmai}>
          <DailyMartText style={styles.title3}>{translate('home.title3')}</DailyMartText>
          <TouchableOpacity style={stylesEndow.viewAll} activeOpacity={0.6} onPress={this.gotoEndow}>
            <DailyMartText style={stylesEndow.textAll}>{translate('button.seeall')}</DailyMartText>
            <BaseIcon name="ic_arrow_right" iconStyle={stylesEndow.iconStyle} width={13} />
          </TouchableOpacity>
        </View>
        <View>
          {!loading && <FlatList
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.flatlistUudaiContent}
            data={endows}
            horizontal
            keyExtractor={this.keyExtractor}
            style={styles.flatlistUudai}
            renderItem={this.renderItem}
          />}
        </View>
        <DailyMartText style={{ height: 8, backgroundColor: '#DCDCDC' }} />
      </View>
    );
  }
}

const stylesEndow = StyleSheet.create({
  viewAll: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  textAll: {
    color: colors.primaryColor
  },
  iconStyle: {
    marginLeft: 5
  }
});
