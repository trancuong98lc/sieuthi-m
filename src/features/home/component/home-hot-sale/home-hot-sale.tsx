import hotsale from 'data/product';
import ProductItemSale from 'features/component/product-item-sell';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { HotsaleScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { styles } from '../../view/home.style';
import { ReqHotsale } from '../../../hotsale/model/hotsale.adapter';
import { Hotsale } from '../../model/home';
import { updateConfig } from 'redux/actions';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import Commons from 'helpers/Commons';
import { STATUS } from 'types/BaseResponse';
import { connect } from 'react-redux';
import { Config } from 'types/ConfigType';
// import { HomePresenter } from 'features/home/presenter/home.presenter';

export interface ListHotsaleProps {
  // presenter: HomePresenter;
  quantityHotsale: number,
  updateConfig: (config: Config) => void,
}

export interface ListHotsaleState {
  hotsales: Hotsale[];
  loading: boolean,
}

class ListHotsale extends React.PureComponent<ListHotsaleProps, ListHotsaleState> {
  constructor(props: ListHotsaleProps) {
    super(props);
    this.state = {
      hotsales: [],
      loading: true,
    };
  }

  componentDidMount = () => {
    this.getGeneralConfig();
  }


  getGeneralConfig = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.GENERAL_CONFIG, {}, true);
      if (res.status == STATUS.SUCCESS) {
        this.props.updateConfig(res.data);
        this.onFetchHotsale();
      } else {
        this.onFetchHotsale();
      }
    } catch (error) {
      console.log('getGeneralConfig error: ', error);
      this.onFetchHotsale();
    }
  }

  onFetchHotsale = async () => {
    const params = {
      limit: this.props.quantityHotsale,
      page: 1,
    }
    try {
      const res = await ApiHelper.fetch(URL_API.HOTSALE + Commons.idStore, params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length == 0) {
        } else {
          this.setHotsale(res.data, false);
        }
      } else {
        this.setHotsale([], false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.setHotsale([], false);
    }
  }

  params: ReqHotsale = {
    limit: this.props.quantityHotsale,
    page: 1,
  };

  onRefresh = () => {

    const newParams =
    {
      limit: this.props.quantityHotsale,
      page: 1,
    }
    this.params = newParams
    this.onFetchHotsale();
  }

  setHotsale = (hotsales: Hotsale[], loading: boolean) => {
    this.setState({
      hotsales: hotsales,
      loading
    });
  };

  gotoHotSaleScreen = () => {
    navigate(HotsaleScreen);
  };

  renderItemHotDeal = ({ item, index }: any) => {
    return <ProductItemSale item={item} index={index} />;
  };

  keyExtractor = (item: Hotsale, index: number) => {
    return item._id.toString();
  }

  public render() {
    const { hotsales, loading } = this.state;
    if (!loading && hotsales.length) {
      return (
        <>
          <View style={styles.hotsale}>
            <View style={styles.headerhotsale}>
              <FastImage source={R.images.ic_hotsale} style={styles.imageHotsale} />
              <DailyMartText style={styles.title2}>
                {translate('home.title2')}
              </DailyMartText>
              <TouchableOpacity
                style={styles.buttonhotsale}
                onPress={this.gotoHotSaleScreen}
              >
                <DailyMartText
                  style={{
                    color: R.colors.white100,
                    fontSize: 13,
                  }}
                >
                  {translate('button.seemore')}
                </DailyMartText>
              </TouchableOpacity>
              <View style={styles.buttonhotsaleBackground} />
            </View>
            <View style={styles.flatlistHotsale}>
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={hotsales}
                horizontal
                renderItem={this.renderItemHotDeal}
                keyExtractor={this.keyExtractor}
              />
            </View>
          </View>
          <DailyMartText style={{ height: 8, backgroundColor: '#DCDCDC' }} />
        </>
      );
    } else {
      return null;
    }
  }
}

const mapStatesToProps = (state: any) => {
  const { quantityHotsale } = state.config;
  return {
    quantityHotsale
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateConfig: (config: Config) => dispatch(updateConfig(config)),
  };
}

export default connect(mapStatesToProps, mapDispatchToProps, null, { forwardRef: true })(ListHotsale);