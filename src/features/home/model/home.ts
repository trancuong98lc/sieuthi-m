import { MediaType, StatusSearch } from 'types/ConfigType';

export interface Home {}

export interface Category {
  createdAt: string;
  deletedAt: string;
  name: string;
  slug: string;
  status: string;
  thumbnail: MediaType;
  thumbnailId: string;
  active?: boolean;
  _id: string;
}

export interface ReqCategory {
  limit: string;
  page: string;
  status?: string;
  name?: string;
}

export interface Hotsale {
  _id: string,
  name: string
}

export interface Endow {
  _id: string,
  name: string,
  endDate: string,
  startDate: string
}

export interface Promotion { 
  _id: string,
  name: string
}

export interface HotNews {
  _id: string,
  title: string,
  content: string,
  createdAt: string,
  image: string,
}



