import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import EventBus, { EventBusName, EventBusType } from 'helpers/event-bus';
import { URL_API } from 'helpers/url-api';
import { t } from 'i18n-js';
import { ALERT_TYPE, showAlertByType } from 'libraries/BaseAlert';
import ContentLoader from 'libraries/ContentLoader/LoaderImage';
//import LinearGradient from 'react-native-linear-gradient';
import HeaderHome from 'libraries/header/Header-Home';
import Container from 'libraries/main/container';
import _ from 'lodash';

import * as React from 'react';
import { Keyboard, RefreshControl, ScrollView, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { onRefreshToken, onUpdateUserInfo, updateCountNoti, updateQuantityCart, updateMessageCount } from 'redux/actions';
import colors from 'res/colors';
import R from 'res/R';
import { Subscription } from 'rxjs';
import { STATUS } from 'types/BaseResponse';
import { Category } from 'types/category';
import { User } from 'types/user-type';
import HomeBanner from '../component/home-banner';
import EndowComponent from '../component/home-endow/home-endow';
import Hotsale from '../component/home-hot-sale/home-hot-sale';
import Infomation from '../component/home-infomation';
import ListCateGory from '../component/home-list-category/home-list-category';
import HotNew from '../component/home-new/home-new';
import Promotion from '../component/home-promotion/home-promotion';
import { HomePresenter } from '../presenter/home.presenter';
import OneSignal from 'react-native-onesignal';
interface Props {
  onRefreshToken: () => Promise<void>,
  user: User,
  countNoti: number,
  onUpdateUser: (data: User) => void;
  updateCountNoty: (quantity: number) => void;
  updateQuantityCart: (quantity: number) => void;
  updateMessageCount: (messageCount: number) => void
}

interface State {
  //home: Home;
  categories: Category[];
  backGround: string;
  refreshing: boolean
}
class HomeScreen extends React.PureComponent<Props, State> {
  presenter: HomePresenter;
  socket: SocketIOClient.Socket = {} as SocketIOClient.Socket
  refListCateGory = React.createRef<ListCateGory>()
  constructor(props: Props) {
    super(props);
    this.state = {
      categories: [],
      backGround: R.colors.primaryColor,
      refreshing: true
    };
    this.presenter = new HomePresenter(this, this.refListCateGory);
    this.initialAPI = _.debounce(this.initialAPI, 100);

  }
  refHomeBanner = React.createRef<HomeBanner>()
  refHotsale = React.createRef<Hotsale>();
  refEndow = React.createRef<EndowComponent>()
  refPromotion = React.createRef<Promotion>()
  refNews = React.createRef<HotNew>()
  subScription = new Subscription()
  componentDidMount(): void {
    const { user } = this.props
    if (user && user._id) {
      OneSignal.sendTags({ user_id: user._id })
      this.getCountNoty()
      this.getUnreadMessageCount();
    } else {
      this.props.updateCountNoty(0)
    }
    this.subScription.add(EventBus.getInstance().events.subscribe((data: EventBusType) => {
      if (data.type == EventBusName.UPDATE_COUNT_CART) {
        this.props.updateQuantityCart(data.payload)
        return
      }
    }))
    Keyboard.dismiss()
    this.onRefresh()
  }


  componentWillUnmount = () => {
    this.subScription && this.subScription.unsubscribe()
  };



  getTotalProductCart = (idStore: String) => {
    ApiHelper.fetch(URL_API.CART_TOTAL_PRODUCT + idStore, null, true).then(res => {
      if (res.status == STATUS.SUCCESS) {
        this.props.updateQuantityCart(res.total || 0)
      }
    })
  }



  private getUser = () => {
    this.presenter.getUser()
  }

  getUnreadMessageCount = async () => {
    const { user } = this.props
    try {
      const res = await ApiHelper.fetch(`${URL_API.MESSAGE_UNREAD}/${user._id}`, null, true);
      if (res.status === STATUS.SUCCESS) {
        this.props.updateMessageCount(res.data === 0 ? undefined : res.data);
      }
    } catch (error) {

    }
  }

  getCountNoty = () => {
    ApiHelper.fetch(URL_API.COUNT_NOTIFICATION, null, true).then(res => {
      if (res.status == STATUS.SUCCESS && res.data) {
        this.props.updateCountNoty(res.data || 0)
      } else {
        this.props.updateCountNoty(0)
      }
    })
  }


  onFetchDataSuccess(data: Category[]): void {
    this.setState({ categories: data });
  }

  onRefresh = async () => {
    this.refHomeBanner.current!.getBanner()
    if (Commons.idStore) {
      await this.getTotalProductCart(Commons.idStore)
    }
    await this.refHomeBanner.current!.getBanner()
    await this.refListCateGory.current!.onRefresh()
    await this.refHotsale.current!.getGeneralConfig()
    await this.refEndow.current!.onRefresh()
    await this.refPromotion.current!.onRefresh()
    await this.getUser()
    await this.initialAPI();
    this.setState({
      refreshing: false
    })
  }

  initialAPI() {
    const { user } = this.props
    if (user && user._id) {
      this.presenter.checkToRefreshToken();
    }
  }

  onScroll = (event: any) => {
    if (
      event.nativeEvent.contentOffset.y > 0 &&
      event.nativeEvent.contentOffset.y / 10 < 10
    ) {
      this.setBgColor(colors.white100);
    }
    if (
      event.nativeEvent.contentOffset.y > -150 &&
      event.nativeEvent.contentOffset.y <= 0
    ) {
      this.setBgColor(colors.primaryColor);
    }
  };

  setBgColor = (color: string) => {
    this.setState({
      backGround: color
    });
  };

  onResetNotification = async () => {
    try {
      const res = await ApiHelper.fetch(URL_API.RESET_NOTIFICATION, null, true)
      if (res.status === STATUS.SUCCESS) {
        this.props.updateCountNoty(0)
      }
    } catch (error) {
    }
  }

  onFetchDataFail(error?: any): void {
    // throw new Error('Method not implemented.');
  }
  public render() {
    const { countNoti } = this.props
    return (
      <Container
        containerStyle={{ backgroundColor: this.state.backGround }}
        home
      >
        <HeaderHome isEdit={false} bage={countNoti} onResetNotification={this.onResetNotification} />
        <ScrollView
          showsVerticalScrollIndicator={false}
          onScroll={this.onScroll}
          refreshControl={
            <RefreshControl
              colors={['#118189']}
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
          scrollEventThrottle={40}
        >
          <HomeBanner ref={this.refHomeBanner} />
          <View style={styles.viewContainer}>
            {/*Danh mục sản phẩm */}
            <ListCateGory ref={this.refListCateGory} presenter={this.presenter} />
            {/*Hot sale*/}
            <Hotsale ref={this.refHotsale} />
            {/* Ưu đãi */}
            <EndowComponent ref={this.refEndow} />
            {/* Khuyến Mại */}
            <Promotion ref={this.refPromotion} />
            {/* Tin tức */}
            <HotNew ref={this.refNews} />
            {/* Thông tin liên hệ */}
            <Infomation />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    backgroundColor: 'white'
  }
});

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  const { countNoti } = state.notificationReducer;

  return {
    user,
    countNoti
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onRefreshToken: () => dispatch(onRefreshToken()),
    updateQuantityCart: (quantity: number) => dispatch(updateQuantityCart(quantity)),
    onUpdateUser: (data: User) => dispatch(onUpdateUserInfo(data)),
    updateCountNoty: (quantity: number) => dispatch(updateCountNoti(quantity)),
    updateMessageCount: (messageCount: number) => dispatch(updateMessageCount(messageCount)),
  };
};

export default connect(mapStatesToProps, mapDispatchToProps)(HomeScreen);
