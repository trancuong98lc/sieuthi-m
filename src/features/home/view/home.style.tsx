import { StyleSheet } from 'react-native';
import { DimensionHelper } from 'helpers/dimension-helper';
import R from 'res/R';
import colors from 'res/colors';
export const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1
  },
  image: {
    width: DimensionHelper.getScreenWidth(),
    height: DimensionHelper.getScreenWidth() * 0.471
  },

  // scrollView: {
  //      position: 'absolute',
  //      marginTop: DimensionHelper.getScreenWidth() * 0.24,
  //      flex:1
  // },
  homebanner: {
    width: DimensionHelper.getScreenWidth(),
    height: 0.39 * DimensionHelper.getScreenWidth(),
    marginTop: -100,
    backgroundColor: 'transparent'
    // borderWidth:1
  },
  title1: {
    fontSize: 17,
    marginTop: 20,
    marginLeft: 10,
    fontFamily: R.fonts.bold,
    color: '#333333'
  },
  flatlistDanhmucsanpham: {
    marginTop: 20,
    marginBottom: 20
  },
  hotsale: {
    backgroundColor: R.colors.primaryColor,
    marginTop: 10,
    borderBottomColor: R.colors.primaryColor,
    borderBottomWidth: 15
  },
  headerhotsale: {
    flexDirection: 'row',
    // height: 0.055 * DimensionHelper.getScreenHeight(),
    alignItems: 'center',
    paddingVertical: 11
  },
  imageHotsale: {
    width: 17,
    height: 21,
    marginLeft: 10
  },
  title2: {
    color: R.colors.white100,
    marginLeft: 5,
    fontSize: 17,
    fontFamily: R.fonts.bold,
    flex: 1
  },
  buttonhotsale: {
    color: R.colors.white100,
    // marginLeft: 0.4* DimensionHelper.getScreenWidth(),
    fontSize: 13,
    paddingRight: 10
    //borderWidth: 1,
    // width: 100,

    // marginTop: -10,
    //backgroundColor:'red'
  },
  buttonhotsaleBackground: {
    backgroundColor: "#59C874",
    width: 0.4 * DimensionHelper.getScreenWidth(),
    height: 40,
    borderBottomStartRadius: 40,
    borderTopEndRadius: 20,
    position: "absolute",
    right: -0.1 * DimensionHelper.getScreenWidth(),
    top: 0,
    zIndex: -1
  },
  flatlistHotsale: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 15,
    padding: 10
  },
  flatlistUudai: {
    marginTop: 5,
    //backgroundColor: '#F7F7F8',
  },
  flatlistUudaiContent: {
    paddingRight: 15
  },
  uudai: {
    backgroundColor: '#F7F7F8'
  },
  title3: {
    color: '#333333',
    fontSize: 17,
    fontFamily: R.fonts.bold,
    flex: 1
  },
  headeruudai: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonuudai: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',

    paddingRight: 10,
    alignItems: 'center',
    height: 30
  },
  textuudai: {
    color: R.colors.primaryColor,
    fontSize: 13
  },
  next: {
    height: 12.69,
    width: 7.41,
    marginLeft: 5
  },
  sanphamkhuyenmai: {
    backgroundColor: '#FFFFFF',
    paddingTop: 10,
  },
  buttonsanphamkhuyenmai: {
    flexDirection: 'row',
    color: R.colors.white100,
    fontSize: 13,
    alignItems: 'center'
  },
  flatlistSanphamkhuyenmai: {
    marginTop: 15,
    backgroundColor: '#F7F7F8'
  },
  flatlistSanphamkhuyenmaiContent: {
    paddingRight: 10
  },
  headersanphamkhuyenmai: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingBottom: 5
    //marginTop:10
  },
  buttontintuc: {
    flexDirection: 'row',
    color: R.colors.white100,
    fontSize: 13,
    alignItems: 'center'
  },
  flatlistTintuc: {
    marginTop: 15,
    backgroundColor: '#FFFFFF'
  },
  contact: {
    marginTop: 10,
    paddingBottom: 30
  },
  contacttitle1: {
    color: '#3BBB5A',
    textAlign: 'center',
    marginTop: 20,
    fontSize: 15
  },
  contacttitle2: {
    color: '#3BBB5A',
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 20,
    fontSize: 18
  },
  contactconten: {
    flexDirection: 'row',
    marginTop: 18,
    alignContent: 'center',
    alignItems: 'center',
    width: DimensionHelper.getScreenWidth() - 60
  },
  imagecontact: {
    width: 27,
    height: 27,
    marginLeft: 20
  },
  textcontact: {
    marginLeft: 10,
    //color:'#000000',
    fontSize: 15
  }
});
