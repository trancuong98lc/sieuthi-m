import { Category, Hotsale } from 'types/category';

export interface HomeView {
    onFetchDataSuccess(data: Category[]): void;
    onFetchDataFail(error?: any): void;

    onFetchHotsaleSuccess(data: Hotsale[]): void;
    onFetchHotsaleFail(error?: any): void;
}
