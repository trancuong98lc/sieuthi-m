import HomeScreen from 'features/home/view/home.screen';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import { User } from 'types/user-type';
import moment from 'moment';
import { ReqCategory } from '../model/home';
import ListCateGory from '../component/home-list-category/home-list-category';
export class HomePresenter {
  private homeView: typeof HomeScreen;
  homeRef: any;
  constructor(homeView: typeof HomeScreen, ref: any) {
    this.homeView = homeView;
    this.homeRef = ref;
  }

  async onFetchCategory(params: ReqCategory) {
    try {
      const res = await ApiHelper.fetch(URL_API.CATEGORIES, params, true);
      // this.homeView;
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length == 0) {
          this.homeRef.current.setMaxData(false);
        } else {
          this.homeRef.current.setMaxData(true);
          this.homeRef.current.setCateGory(res.data, false);
        }
      } else {
        this.homeRef.current.setCateGory([], false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.homeRef.current.setCateGory([], false);
      // this.homeView.onFetchDataFail();
    }
  }

  async checkToRefreshToken(): Promise<void> {
    const User: User = await AsyncStorageHelpers.getObject(
      StorageKey.LOGIN_SESSION
    );
    if (User && User.expiresAt) {
      let now = moment(new Date());
      let expiredAt = moment(User.expiresAt);
      let diff = expiredAt.diff(now);
      const diffDuration = moment.duration(diff).asMilliseconds();
      //nếu gần đến thời gian hết hạn token thì refresh token (120p)
      if (diffDuration < 120 * 1000 * 60) {
        this.homeView.props.onRefreshToken().then(() => {});
      }
    }
  }

  getUser = async () => {
    try {
      const User = this.homeView.props.user;
      if (User && User._id) {
        const res = await ApiHelper.fetch(
          URL_API.PROFILE + User._id,
          null,
          true
        );
        if (res.status === STATUS.SUCCESS && res.user) {
          this.homeView.onUpdateUser(res.user!);
        }
      }
    } catch (error) {}
  };
}
