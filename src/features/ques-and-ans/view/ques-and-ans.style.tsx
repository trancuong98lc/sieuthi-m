import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor: R.colors.white100,
          flex: 1
     },
     chatHeader: {
          backgroundColor: R.colors.primaryColor,
          paddingBottom: 10
     },

     chatHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: DimensionHelper.getStatusBarHeight() + 12,
          textAlign: 'center',
          paddingBottom: 12
     },
     loadingView: { padding: 5, alignItems: 'center', justifyContent: 'center' },
});