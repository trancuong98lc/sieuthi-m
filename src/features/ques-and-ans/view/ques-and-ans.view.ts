import { QuesAndAns } from "ques-and-ans/model/ques-and-ans";

export interface QuesAndAnsView {
     onFetchDataSuccess(quesAndAns: QuesAndAns): void;
     onFetchDataFail(error?: any): void;
 }
 