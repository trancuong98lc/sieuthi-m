/* eslint-disable react/sort-comp */

import * as React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import colors from 'res/colors';
import R from '../../../../res/R';
import { MessagePosition } from 'types/MessageType';
import { translate, isVietnamese } from 'res/languages';
import Commons from 'helpers/Commons';
import { NUMBER_OF_LINES } from 'global/constants';
import { MessageProps } from '../component/MessageItem';
import DailyMartText from 'libraries/text/text-daily-mart';
import { DiffDate } from 'helpers/helper-funtion';
import { DimensionHelper } from 'helpers/dimension-helper';
import BaseIcon from 'libraries/baseicon/BaseIcon';

export default class MessageText extends React.Component<
    MessageProps,
    State
    > {
    state = {
        preViewLink: null,
        showTime: false
    };

    shouldComponentUpdate(nextProps: MessageProps, nextStates: IState) {
        if (
            JSON.stringify(this.props.message) !==
            JSON.stringify(nextProps.message)
        ) {
            return true;
        }

        if (this.props.isMiddleMessage !== nextProps.isMiddleMessage)
            return true;
        if (this.state.showTime !== nextStates.showTime) return true;
        if (
            JSON.stringify(this.state.preViewLink) !==
            JSON.stringify(nextStates.preViewLink)
        )
            return true;

        return false;
    }

    componentDidMount(): void {
    }

    onShowMsgTime = () => {
        this.setState({ showTime: !this.state.showTime });
    };

    render(): React.ReactNode {
        const { alignLeft, message, borderStyle } = this.props;

        return (
            <View style={{ marginTop: 1 }}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={this.onShowMsgTime}
                    style={[
                        styles.messageView,
                        borderStyle,
                        !alignLeft && styles.messageRight
                    ]}
                >
                    <DailyMartText
                        style={[
                            styles.message,
                            !alignLeft && styles.textRight
                        ]}
                    >
                        {message.text}
                    </DailyMartText>

                </TouchableOpacity>
                <View style={{ maxWidth: DimensionHelper.width * 0.68, flexDirection: 'row', alignSelf: !alignLeft ? 'flex-end' : "flex-start", marginTop: -5, marginRight: !alignLeft ? 1.3 : 0, marginLeft: !alignLeft ? 1.3 : 0 }}>
                    <BaseIcon name={!alignLeft ? "ic_right" : "ic_left"} style={{}} />
                </View>

                {
                    this.state.showTime && (
                        <DailyMartText
                            style={[
                                styles.timeStyle,
                                {
                                    marginLeft: !alignLeft ? 0 : 10,
                                    marginRight: !alignLeft ? 10 : 0,
                                    textAlign: !alignLeft ? 'right' : 'left'
                                }
                            ]}
                        >
                            {DiffDate(message.createdAt)}
                        </DailyMartText>
                    )
                }
            </View >
        );
    }
}

const styles = StyleSheet.create({
    message: {
        color: '#262628',
        fontSize: 16,
        textAlign: 'left'
    },
    timeStyle: {
        color: '#888',
        fontSize: 10,
        textAlign: 'left'
    },
    messageView: {
        backgroundColor: '#F7F7F8',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 15,
        // marginLeft: 10,
        marginRight: 2,
        maxWidth: DimensionHelper.width * 0.68
    },
    messageRight: {
        backgroundColor: R.colors.primaryColor
    },
    textRight: {
        color: R.colors.white100
    },
    firstMessageStyle: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 4
    },
    middleMessageStyle: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 4,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 4
    },
    lastMessageStyle: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 4,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    onlyMessageStyle: {
        borderRadius: 10
    },

    firstOtherMessageStyle: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 10
    },
    middleOtherMessageStyle: {
        borderTopLeftRadius: 4,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 10
    },
    lastOtherMessageStyle: {
        borderTopLeftRadius: 4,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    viewStyle: {
        marginHorizontal: 12,
        marginBottom: 10
    },
    forwardedMessage: {
        color: '#111',
        fontSize: 16,
        marginBottom: 5
    }
});
