import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { Message, SystemMessages } from 'types/MessageType';
import { translate } from 'res/languages';
import AstraText from 'libraries/AstraComponent/AstraText';
import moment from 'moment';

export interface Props {
  message: Message;
  isCreator: boolean;
  groupName: string;
  messageType: SystemMessages;
}

export default class SystemMessagesComponent extends React.PureComponent<
  Props,
  any
> {
  constructor(props: Props) {
    super(props);
  }

  checkMessage = () => {
    const { message, isCreator, groupName, messageType } = this.props;
    let { creator, mentionUsers, additionalData } = message || {};
    let messageText = '';
    switch (messageType) {
      case SystemMessages.CREATE_CONVERSATION:
        if (isCreator) {
          messageText = translate('chat.you_create_group', { groupName });
        } else {
          messageText = translate('chat.other_create_group', {
            groupName,
            creatorName:
              (additionalData && additionalData!.createdBy!.fullName) ||
              creator!.fullName ||
              '',
          });
        }
        break;

      case SystemMessages.ADD_USERS_TO_CONVERSATION: {
        let memberName = '';
        if (mentionUsers && mentionUsers.length > 0) {
          memberName = mentionUsers.map(e => e.fullName).join(', ');
        }
        if (isCreator) {
          messageText = translate('chat.you_addto_group', { memberName });
        } else {
          messageText = translate('chat.other_addto_group', {
            memberName,
            creatorName:
              (additionalData && additionalData!.createdBy!.fullName) ||
              creator!.fullName ||
              '',
          });
        }
        break;
      }
      case SystemMessages.UPDATE_AVATAR_CONVERSATION: {
        let memberName = '';
        if (mentionUsers && mentionUsers.length > 0) {
          memberName = mentionUsers.map(e => e.fullName).join(', ');
        }
        if (isCreator) {
          messageText = translate('chat.you_change_avatar_group', {
            memberName,
          });
        } else {
          messageText = translate('chat.other_change_avatar_group', {
            memberName,
            creatorName:
              (additionalData && additionalData!.createdBy!.fullName) || '',
          });
        }
        break;
      }
      case SystemMessages.RENAME_CONVERSATION: {
        let memberName = '';
        if (mentionUsers && mentionUsers.length > 0) {
          memberName = mentionUsers.map(e => e.fullName).join(', ');
        }
        if (isCreator) {
          messageText = translate('chat.you_rename_group', { groupName });
        } else {
          messageText = translate('chat.other_rename_group', {
            creatorName:
              (additionalData && additionalData!.createdBy!.fullName) ||
              creator!.fullName ||
              '',
            groupName,
          });
        }
        break;
      }
      case SystemMessages.LEFT_CONVERSATION: {
        let memberName = '';
        if (mentionUsers && mentionUsers.length > 0) {
          memberName = mentionUsers.map(e => e.fullName).join(', ');
        }
        messageText = translate('chat.left_the_group', { memberName });
        break;
      }

      case SystemMessages.REMOVE_USERS_FROM_CONVERSATION: {
        let memberName = '';
        if (mentionUsers && mentionUsers.length > 0) {
          memberName = mentionUsers.map(e => e.fullName).join(', ');
        }
        if (isCreator) {
          messageText = translate('chat.you_removed_member', { memberName });
        } else {
          messageText = translate('chat.other_removed_member', {
            memberName,
            creatorName:
              (additionalData && additionalData!.createdBy!.fullName) ||
              (additionalData &&
                additionalData!.users!.length > 0 &&
                additionalData!.users[0].fullName) ||
              creator?.fullName ||
              '',
          });
        }
        break;
      }

      case SystemMessages.NEW_CONVERSATION_ADMIN:
        let memberName = '';
        if (mentionUsers && mentionUsers.length > 0) {
          memberName = mentionUsers.map(e => e.fullName).join(', ');
        }
        if (isCreator) {
          messageText = translate('chat.you_assign_admin', { memberName });
        } else {
          messageText = translate('chat.other_assign_admin', {
            creatorName:
              (additionalData && additionalData!.createdBy!.fullName) ||
              creator!.fullName ||
              '',
            memberName,
          });
        }
        break;

      default:
        break;
    }

    return messageText;
  };

  public render() {
    let messageText = this.checkMessage();
    const { message } = this.props;

    return (
      <View style={styles.messageView}>
        <AstraText style={styles.time}>
          {moment(message.createdAt).format('DD/MM, HH:mm')}
        </AstraText>
        <AstraText style={styles.message}>{messageText}</AstraText>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  message: {
    color: '#888',
    fontSize: 12,
    textAlign: 'center',
  },
  messageView: {
    paddingHorizontal: 50,
    alignSelf: 'center',
    paddingTop: 8,
  },
  time: {
    color: '#888',
    fontSize: 12,
    paddingTop: 5,
    textAlign: 'center',
  },
});
