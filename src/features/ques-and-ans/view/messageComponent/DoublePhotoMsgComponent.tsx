import * as React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    StyleProp,
    ViewStyle
} from 'react-native';
import R from 'res/R';
import { PhotoSize } from 'global/constants';
import { Message } from 'types/MessageType';
import { translate, isVietnamese } from 'res/languages';
import DailyMartImage from 'libraries/Avatar/DailyMartImage';
import { DimensionHelper } from 'helpers/dimension-helper';
import { MediaType } from 'types/ConfigType';

export interface DoublePhotoMsgProps {
    medias: MediaType[];
    onShowActions?: () => void;
    onPhotoPreview: any;
    message?: Message;
    alignLeft?: boolean;
    borderStyle: StyleProp<ViewStyle>;
}

export interface DoublePhotoMsgState { }

export default class DoublePhotoMsgComponent extends React.Component<
    DoublePhotoMsgProps,
    DoublePhotoMsgState
    > {
    shouldComponentUpdate(nextProps: DoublePhotoMsgProps) {
        if (
            JSON.stringify(this.props.medias) !==
            JSON.stringify(nextProps.medias)
        ) {
            return true;
        }

        return false;
    }

    constructor(props: DoublePhotoMsgProps) {
        super(props);
        this.state = {};
    }

    renderDoubleImage = (medias: any[]) =>
        medias.map((item, index) => {
            let uri = item.uri || item.url;
            return (
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={this.props.onPhotoPreview(index)}
                >
                    <DailyMartImage
                        useImage
                        imageStyle={{
                            width: (DimensionHelper.width * 0.68) / 2,
                            height: 130,
                            borderTopLeftRadius:
                                index === 0
                                    ? this.props.borderStyle.borderTopLeftRadius
                                    : 0,
                            borderBottomLeftRadius:
                                index === 0
                                    ? this.props.borderStyle
                                        .borderBottomLeftRadius
                                    : 0,
                            borderTopRightRadius:
                                index === 1
                                    ? this.props.borderStyle
                                        .borderTopRightRadius
                                    : 0,
                            borderBottomRightRadius:
                                index === 1
                                    ? this.props.borderStyle
                                        .borderBottomRightRadius
                                    : 0
                        }}
                        url={uri}
                    />
                </TouchableOpacity>
            );
        });

    public render() {
        const { medias, alignLeft, message } = this.props;


        return (
            <TouchableOpacity
                activeOpacity={0.9}
                style={[
                    styles.messageView,
                    this.props.borderStyle, { backgroundColor: 'transparent' }
                ]}
            >
                <View style={{ flexDirection: 'row' }}>
                    {this.renderDoubleImage(medias)}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    iconPause: {
        position: 'absolute',
        zIndex: 10,
        top: 50,
        alignSelf: 'center'
    },
    textRight: {
        color: R.colors.white100
    },
    forwardedMessage: {
        color: R.colors.black1,
        fontSize: 16,
        marginBottom: 5,
        marginLeft: 10
    },
    messageView: {
        backgroundColor: R.colors.bgMessage,
        borderRadius: 20,
        paddingTop: 10,
        marginTop: 2
    },
    messageRight: {
        backgroundColor: R.colors.primaryColor
    }
});
