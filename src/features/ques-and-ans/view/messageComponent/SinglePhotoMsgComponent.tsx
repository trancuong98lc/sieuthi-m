
import * as React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    StyleProp,
    ViewStyle
} from 'react-native';
import { VIDEO_TYPES } from 'global/constants';
import { translate, isVietnamese } from 'res/languages';
import { Message } from 'types/MessageType';
import R from 'res/R';
import { MediaType } from 'types/ConfigType';
import { DimensionHelper } from 'helpers/dimension-helper';
import DailyMartImage from 'libraries/Avatar/DailyMartImage';

export interface SinglePhotoMsgProps {
    medias: MediaType[];
    onShowActions?: () => void;
    onPhotoPreview: any;
    message?: Message;
    alignLeft?: boolean;
    borderStyle: StyleProp<ViewStyle>;
}

export interface SinglePhotoMsgState { }

export default class SinglePhotoMsgComponent extends React.Component<
    SinglePhotoMsgProps,
    SinglePhotoMsgState
    > {
    constructor(props: SinglePhotoMsgProps) {
        super(props);
        this.state = {};
    }

    shouldComponentUpdate(nextProps: SinglePhotoMsgProps) {
        if (
            JSON.stringify(this.props.medias) !==
            JSON.stringify(nextProps.medias)
        ) {
            return true;
        }

        return false;
    }

    public render() {
        const { alignLeft, message } = this.props;
        return (
            <TouchableOpacity
                onPress={this.props.onPhotoPreview(0)}
                activeOpacity={0.9}
                style={[
                    styles.messageView,
                    this.props.borderStyle, { backgroundColor: 'transparent' }
                ]}
                onLongPress={this.props.onShowActions}
            >
                {this.renderAvatar()}
            </TouchableOpacity>
        );
    }

    renderAvatar() {
        const { medias, message, alignLeft } = this.props;
        let media = medias[0];
        if (!media) return null;
        let uri = media.url;

        let width =
            media.dimensions && media.dimensions.width < media.dimensions && media.dimensions.height
                ? DimensionHelper.width * 0.6
                : DimensionHelper.width * 0.68;
        let height = media.dimensions && media.dimensions.width < media.dimensions && media.dimensions.height ? 300 : 158;
        if (
            media.dimensions &&
            media.dimensions.width <= media.dimensions.height
        ) {
            height = 300;
            width = DimensionHelper.width * 0.6;
        }

        return (
            <View style={styles.content}>

                <View
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                    <DailyMartImage
                        useImage
                        imageStyle={[
                            {
                                width: width,
                                height: height,
                                borderRadius: 15
                            }
                        ]}
                        url={uri}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    iconPause: {
        position: 'absolute',
        zIndex: 10,
        alignSelf: 'center'
    },
    content: { justifyContent: 'center' },
    textRight: {
        maxWidth: DimensionHelper.width * 0.6,
        color: R.colors.white100
    },
    forwardedMessage: {
        color: R.colors.black1,
        fontSize: 16,
        marginBottom: 5,
        marginLeft: 10
    },
    messageView: {
        backgroundColor: R.colors.bgMessage,
        borderRadius: 20,
        paddingTop: 10,
        marginTop: 2
    },
    messageRight: {
        backgroundColor: R.colors.primaryColor
    }
});
