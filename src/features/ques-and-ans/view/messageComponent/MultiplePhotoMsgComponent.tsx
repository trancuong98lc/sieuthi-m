
import * as React from 'react';
import {
    FlatList,
    StyleProp,
    StyleSheet,
    TouchableOpacity,
    ViewStyle
} from 'react-native';
import { Message } from 'types/MessageType';
import R from 'res/R';
import { translate, isVietnamese } from 'res/languages';
import { MediaType } from 'types/ConfigType';
import DailyMartImage from 'libraries/Avatar/DailyMartImage';
import { DimensionHelper } from 'helpers/dimension-helper';

export interface MultiplePhotoMsgProps {
    medias: MediaType[];
    onShowActions?: () => void;
    onPhotoPreview: any;
    message?: Message;
    alignLeft?: boolean;
    borderStyle: StyleProp<ViewStyle>;
}

export interface MultiplePhotoMsgState { }

export default class MultiplePhotoMsgComponent extends React.Component<
    MultiplePhotoMsgProps,
    MultiplePhotoMsgState
    > {
    constructor(props: MultiplePhotoMsgProps) {
        super(props);
        this.state = {};
    }

    shouldComponentUpdate(nextProps: MultiplePhotoMsgProps) {
        if (
            JSON.stringify(this.props.medias) !==
            JSON.stringify(nextProps.medias)
        ) {
            return true;
        }

        return false;
    }

    keyExtractor = (item: MediaType, index: number) => item._id;

    public render() {
        const { alignLeft, message } = this.props;
        return (
            <TouchableOpacity
                activeOpacity={0.9}
                style={[
                    styles.messageView,
                    this.props.borderStyle, { backgroundColor: 'transparent' }
                ]}
                onLongPress={this.props.onShowActions}
            >

                <FlatList
                    data={this.props.medias}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                    numColumns={3}
                    style={{ flex: 1 }}
                />
            </TouchableOpacity>
        );
    }

    renderItem = ({ item, index }: any) => {
        let uri = item.uri || item.url;

        const {
            borderTopLeftRadius,
            borderBottomLeftRadius,
            borderTopRightRadius,
            borderBottomRightRadius
        } = this.props.borderStyle;
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={this.props.onPhotoPreview(index)}
                style={styles.itemView}
                onLongPress={this.props.onShowActions}
            >
                <DailyMartImage
                    useImage
                    imageStyle={{
                        // marginLeft: 1,
                        marginTop: 1,
                        width: (DimensionHelper.width * 0.68) / 3,
                        height: 80,
                        borderTopLeftRadius:
                            index % 3 == 0 && index == 0
                                ? borderTopLeftRadius
                                : 0,
                        borderBottomLeftRadius:
                            index % 3 == 0 &&
                                ((index == 0 && this.props.medias.length == 3) ||
                                    index >= this.props.medias.length - 3)
                                ? borderBottomLeftRadius
                                : 0,

                        borderTopRightRadius:
                            index == 2 ? borderTopRightRadius : 0,
                        borderBottomRightRadius:
                            (index % 3 == 2 && this.props.medias.length < 6) ||
                                index == this.props.medias.length - 1
                                ? borderBottomRightRadius
                                : 0
                    }}
                    // url={optimizedPhoto(item, PhotoSize.SMALL)}
                    url={uri}
                />
            </TouchableOpacity>
        );
    };
}

const styles = StyleSheet.create({
    iconPause: {
        position: 'absolute',
        zIndex: 10,
        alignSelf: 'center'
    },
    itemView: {
        justifyContent: 'center',
        marginTop: 3
    },
    textRight: {
        color: R.colors.white100
    },
    forwardedMessage: {
        color: R.colors.black1,
        fontSize: 16,
        marginBottom: 5,
        marginLeft: 10
    },
    messageView: {
        backgroundColor: R.colors.bgMessage,
        borderRadius: 15,
        paddingTop: 10,
        marginTop: 2
    },
    messageRight: {
        backgroundColor: R.colors.primaryColor
    }
});
