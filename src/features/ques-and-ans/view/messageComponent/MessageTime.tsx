import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import R from 'res/R';
import { MessageTimeProps } from '../../Types'
import AstraText from 'libraries/AstraComponent/AstraText';


export default class MessageTime extends React.PureComponent<MessageTimeProps> {
  render() {
    return (
      <View style={styles.content}>
        <AstraText style={styles.message}>{this.props.time}</AstraText>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    paddingTop: 15
  },
  message: {
    color: R.colors.black8,
    fontSize: 13,
    textAlign: 'center',
  },
})