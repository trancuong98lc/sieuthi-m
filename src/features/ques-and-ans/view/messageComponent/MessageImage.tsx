import * as React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
// import R from '../../../res/R';
// import { MessageImageProps, MessageImageStates } from '../Types'
// import AvatarView from '../../../libraries/AvatarView'
// import { MESSAGE_STATUS } from '../../../global/constants';
import FastImage from 'react-native-fast-image'
import { MESSAGE_STATUS } from 'global/constants';
import R from 'res/R';
import { getPhotoUrl } from 'helpers/helper-funtion';
import { Message, MessageImageProps, MessageImageStates } from 'types/MessageType';
import { MessageProps } from '../component/MessageItem';


export default class MessageImage extends React.PureComponent<MessageImageProps & MessageProps, MessageImageStates> {

  state = {
    imgWidth: this.props.imgWidth || 0,
    imgHeight: this.props.imgHeight || 0,
    loading: true
  }

  componentWillMount() {
    let { message, imgWidth, imgHeight } = this.props

    Image.getSize(message, (width: number, height: number) => {
      if (imgWidth && !imgHeight) {
        let h = height * (imgWidth / width)
        this.setState({
          imgWidth: imgWidth,
          imgHeight: h,
          loading: false
        })
      } else if (!imgWidth && imgHeight) {
        this.setState({
          imgWidth: width * (imgHeight / height),
          imgHeight,
          loading: false
        })
      } else {
        this.setState({ imgWidth: width, imgHeight: height, loading: false })
      }
    }, error => {


    })
  }

  render() {
    let { alignLeft, status, sameSender, message, lastItem } = this.props

    let { imgWidth, imgHeight } = this.state
    return (
      <View style={[
        styles.content,
        !alignLeft && styles.alignRight,
        sameSender && styles.mt,
        lastItem && styles.lastItem
      ]}>
        <View style={[
          styles.messageView,
          status === MESSAGE_STATUS.PENDING && styles.messagePedding,

        ]}>
          <FastImage source={{ uri: message }} style={[
            styles.image, { width: imgWidth, height: imgHeight },

          ]} />
        </View>
      </View>
    )
  }
  \
}

const styles = StyleSheet.create({
  mt: {
    marginTop: 5
  },
  content: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: 15,
    marginRight: 10
  },
  message: {
    fontSize: 16,
    textAlign: 'left',
  },
  messageView: {
    borderRadius: 15,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginLeft: 10,
    marginRight: 10,
    maxWidth: '70%',
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  textRight: {
    color: R.colors.white100
  },
  alignRight: {
    alignSelf: "flex-end"
  },
  icCheck: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginTop: -10,
    marginRight: -10,
    alignSelf: 'flex-end'
  },
  messagePedding: {
    borderTopRightRadius: 0
  },
  image: {
    borderRadius: 15,
    marginRight: 4,
  },
  lastItem: {
    paddingBottom: 10
  },
  hideCheck: {
    opacity: 0
  },

})