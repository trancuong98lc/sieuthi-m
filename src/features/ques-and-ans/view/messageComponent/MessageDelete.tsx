import DailyMartText from 'libraries/text/text-daily-mart';
import React, { PureComponent } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';

interface IProps {
  alignLeft?: boolean;
}

export default class MessageDelete extends PureComponent<IProps> {
  render() {
    return (
      <View>
        <View style={[styles.content_text, , { marginLeft: this.props.alignLeft ? 10 : 0 }]}>
          <DailyMartText style={[styles.text]}>{translate('chat.remove_message')}</DailyMartText>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mt: {
    marginTop: 5,
  },
  content: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'flex-end',
    marginTop: 15,
    paddingRight: 15,
  },
  message: {
    color: R.colors.black1,
    fontSize: 16,
    textAlign: 'left',
  },
  messageView: {
    backgroundColor: R.colors.bgMessage,
    borderRadius: 15,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginLeft: 10,
    marginRight: 2,
    maxWidth: '70%',
  },
  messageRight: {
    backgroundColor: R.colors.primaryColor,
  },
  textRight: {
    color: R.colors.white100,
  },
  alignRight: {
    alignSelf: 'flex-end',
  },
  icCheck: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    borderRadius: 7,
    marginTop: -10,
    marginRight: -10,
    alignSelf: 'flex-end',
  },
  messagePedding: {
    borderTopRightRadius: 0,
  },
  hideCheck: {
    opacity: 0,
  },
  lastItem: {
    paddingBottom: 10,
  },
  content_text: {
    borderRadius: 15,
    backgroundColor: '#F4F4F4',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  text: {
    fontStyle: 'italic',
    color: '#AAAAAA',
  },
});
