import Commons from 'helpers/Commons';
import { getPhotoUrl } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';
import moment from 'moment';
import * as React from 'react';
import { Image, StyleSheet, View, StyleProp, ViewStyle } from 'react-native';
import FastImage from 'react-native-fast-image';
import images from 'res/images';
import R from 'res/R';
import { Message, MessagePosition } from 'types/MessageType';
import { User } from 'types/user-type';
import DoublePhotoMsgComponent from '../messageComponent/DoublePhotoMsgComponent';
import MessageDelete from '../messageComponent/MessageDelete';
import MessageText from '../messageComponent/MessageText';
import MultiplePhotoMsgComponent from '../messageComponent/MultiplePhotoMsgComponent';
import SinglePhotoMsgComponent from '../messageComponent/SinglePhotoMsgComponent';
import SystemMessagesComponent from './SystemMessagesComponent';

export interface MessageProps {
  message: Message;
  avatar?: string | null;
  user?: User;
  alignLeft?: boolean;
  sameSender?: boolean;
  lastItem?: boolean;
  latestMsgIsMine?: Message;
  index?: number;
  isDiffDate?: boolean;
  isMiddleMessage?: MessagePosition;
  borderStyle?: any;
}

export interface MessageState {}

export default class MessageItem extends React.Component<
  MessageProps,
  MessageState
> {
  onPhotoPreview = (index: number) => (): void => {};

  isCreator = () => {
    const { message } = this.props;
    if (
      (!Commons.idUser && !message) ||
      !message.additionalData ||
      !message.additionalData.createdBy!._id ||
      !message.creator ||
      !message.creator!._id
    )
      return false;
    if (
      (message.creator && message.creator!._id == Commons.idUser) ||
      (message.additionalData &&
        message.additionalData.createdBy!._id == Commons.idUser)
    )
      return true;
  };

  private getBorderStyle(isOtherPersion: boolean): StyleProp<ViewStyle> {
    const { isMiddleMessage } = this.props;
    switch (isMiddleMessage) {
      case MessagePosition.FIRST:
        return isOtherPersion
          ? styles.firstOtherMessageStyle
          : styles.firstMessageStyle;

      case MessagePosition.MIDDLE:
        return isOtherPersion
          ? styles.middleOtherMessageStyle
          : styles.middleMessageStyle;

      case MessagePosition.LAST:
        return isOtherPersion
          ? styles.lastOtherMessageStyle
          : styles.lastMessageStyle;

      default:
        break;
    }
    return styles.onlyMessageStyle;
  }

  renderContent(message: Message, alignLeft?: boolean): JSX.Element {
    const borderStyle = this.getBorderStyle(alignLeft!);

    if (message.deletedAt)
      return (
        <MessageDelete
          //   message={item.message}
          //   status={status}
          alignLeft={alignLeft}

          //   imgWidth={DimensionHelpers.width * 0.65}
          //   lastItem={lastItem}
        />
      );
    if (!message.medias || message.medias.length === 0)
      return (
        <MessageText
          borderStyle={borderStyle}
          isMiddleMessage={this.props.isMiddleMessage}
          message={message}
          alignLeft={alignLeft}
        />
      );

    if (message.medias.length === 1)
      return (
        <SinglePhotoMsgComponent
          borderStyle={borderStyle}
          message={message}
          onPhotoPreview={this.onPhotoPreview}
          medias={message.medias}
          alignLeft={alignLeft}
        />
      );

    if (message.medias.length === 2)
      return (
        <DoublePhotoMsgComponent
          borderStyle={borderStyle}
          message={message}
          onPhotoPreview={this.onPhotoPreview}
          medias={message.medias}
          alignLeft={alignLeft}
        />
      );
    if (message.medias.length > 2)
      return (
        <MultiplePhotoMsgComponent
          borderStyle={borderStyle}
          message={message}
          onPhotoPreview={this.onPhotoPreview}
          medias={(message.medias && message.medias) || []}
          alignLeft={alignLeft}
        />
      );
  }

  renderDiffTime(message: Message): JSX.Element | null {
    if (!this.props.isDiffDate) return null;
    return (
      <DailyMartText
        style={{ alignSelf: 'center', fontSize: 12, color: '#888888' }}
      >
        {moment(message.createdAt).format('DD/MM, HH:mm')}
      </DailyMartText>
    );
  }

  checkShowSystemMessage = (type?: string) => {
    if (!type) return false;
    const values = Object.values('');
    return values.includes(type);
  };

  render(): React.ReactNode {
    let { alignLeft, sameSender, message, lastItem } = this.props;
    let showSystemMessage = this.checkShowSystemMessage(message.text);
    if (showSystemMessage && message.system) {
      return (
        <SystemMessagesComponent
          message={message}
          isCreator={false}
          messageType={message.text}
        />
      );
    }

    return (
      <View style={{ marginBottom: sameSender ? 0 : 15 }}>
        {this.renderDiffTime(message)}
        <View
          style={[
            styles.content,
            !alignLeft && styles.alignRight,
            sameSender && styles.mt,
            lastItem && styles.lastItem
          ]}
        >
          <View style={{ flexDirection: 'row' }}>
            {this.renderContent(message, alignLeft)}
          </View>
        </View>
        {/* <FastImage
          source={images.ic_close}
          style={{ width: 20, height: 20 }}
          resizeMode={FastImage.resizeMode.contain}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mt: {
    // marginTop: 5
  },
  content: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'flex-end'
  },
  message: {
    color: R.colors.black1,
    fontSize: 16,
    textAlign: 'left'
  },
  messageView: {
    backgroundColor: R.colors.bgMessage,
    borderRadius: 15,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginLeft: 10,
    marginRight: 2,
    maxWidth: '70%'
  },
  messageRight: {
    backgroundColor: R.colors.primaryColor
  },
  textRight: {
    color: R.colors.white100
  },
  alignRight: {
    alignSelf: 'flex-end'
  },
  icCheck: {
    marginLeft: 2,
    width: 14,
    height: 14,
    resizeMode: 'contain',
    borderRadius: 7,
    marginTop: -10,
    alignSelf: 'flex-end'
  },
  messagePedding: {
    borderTopRightRadius: 0
  },
  hideCheck: {
    opacity: 0
  },
  lastItem: {
    paddingBottom: 10
  },

  firstMessageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0
  },
  middleMessageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0
  },
  lastMessageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0
  },
  onlyMessageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0
  },

  firstOtherMessageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 0
  },
  middleOtherMessageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 10
  },
  lastOtherMessageStyle: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10
  }
});
