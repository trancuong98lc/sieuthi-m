import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { Message } from 'types/MessageType';
import { translate } from 'res/languages';
import moment from 'moment';
import DailyMartText from 'libraries/text/text-daily-mart';

export interface Props {
  message: Message;
  isCreator: boolean;
  messageType: any;
}

export default class SystemMessagesComponent extends React.PureComponent<
  Props,
  any
  > {
  constructor(props: Props) {
    super(props);
  }

  checkMessage = () => {
    const { message, isCreator, groupName, messageType } = this.props;
    let messageText = '';

    return messageText;
  };

  public render() {
    let messageText = this.checkMessage();
    const { message } = this.props;

    return (
      <View style={styles.messageView}>
        <DailyMartText style={styles.time}>
          {moment(message.createdAt).format('DD/MM, HH:mm')}
        </DailyMartText>
        <DailyMartText style={styles.message}>{messageText}</DailyMartText>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  message: {
    color: '#888',
    fontSize: 12,
    textAlign: 'center',
  },
  messageView: {
    paddingHorizontal: 50,
    alignSelf: 'center',
    paddingTop: 8,
  },
  time: {
    color: '#888',
    fontSize: 12,
    paddingTop: 5,
    textAlign: 'center',
  },
});
