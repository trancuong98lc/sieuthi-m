/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable react/sort-comp */
import * as React from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Platform,
    PermissionsAndroid,
    LayoutChangeEvent,
    ScrollView, Keyboard
} from 'react-native';
import R from 'res/R';
import { translate } from 'res/languages';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import { VIDEO_TYPES, TEXT_DESCRIPTION_LENGTH } from 'global/constants';
import Commons from 'helpers/Commons';
import { MediaType, MediaSelectedType } from 'types/ConfigType';
import { User } from 'types/user-type';
import { PermissionHelper } from 'helpers/permission-helper';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import colors from 'res/colors';
import ImagePicker from 'react-native-image-crop-picker';
import PopupChooseMedias from 'libraries/choose-media/popup-choose-medias';
import FastImage from 'react-native-fast-image';
interface BoxSendProps {
    onSendText?: (message: string) => void;
    onSendImages?: (media: MediaSelectedType[]) => void;
    titleProduct?: string;
    users?: User[];
    isGroupChat?: boolean;
}

interface BoxSendStates {
    message: string;
    editorHeight: any,
    textInputPaddingBottom: number

}

export default class BoxSend extends React.PureComponent<
    BoxSendProps,
    BoxSendStates
    > {
    state = {
        message: this.props.titleProduct
            ? `${translate('add_product_service.im_interested_in')} "${this.props.titleProduct
            }". ${translate('add_product_service.please_advise_me')}`
            : '',
        editorHeight: 38,
        scrollContentInset: { top: 0, bottom: 0, left: 0, right: 0 },
        textInputPaddingBottom: 10

    };
    inputRef = React.createRef<TextInput>();
    scroll: any = null
    refChooseImage = React.createRef<PopupChooseMedias>();

    keyboardDidShowListener: any;
    keyboardDidHideListener: any;

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          this._keyboardDidShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
          'keyboardDidHide',
          this._keyboardDidHide,
        );
    }

    _keyboardDidShow = (e: any) => {
        this.setState({
            textInputPaddingBottom: e.endCoordinates.height + 10
        })
    }

    _keyboardDidHide = () => {
        this.setState({
            textInputPaddingBottom: 10
        })
    }



    onFocusInput = () => {
        this.inputRef.current?.focus();
    };


    onLayoutInputChanged = (event: LayoutChangeEvent) => {
        var { height } = event.nativeEvent.layout;
        this.setState({
            editorHeight: height
        });
    };




    onSendText = () => {
        const { message } = this.state;
        const { onSendText } = this.props;
        if (message.trim() !== '' && onSendText) {
            this.setState({ message: '' }, () => {
                onSendText(message);
            });
        }
    };

    onChangeText = (message: string) => {
        this.setState({ message: message });
    };

    cameraPressed: boolean = false;
    timeoutCameraPressed: any;

    chooseImages = async () => {
        this.refChooseImage.current?.onShow()
    };

    onSeleceImage = async (images: MediaSelectedType[]): Promise<any> => {
        if (images && images.length > 0) {
            const { onSendImages } = this.props;
            onSendImages && onSendImages(images);
        }
    };

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    render() {
        return (
            <View style={[styles.content, {paddingBottom: Platform.OS == "ios" ? this.state.textInputPaddingBottom : 10 }]}>
                <BaseIcon name="ic_camera" onPress={this.chooseImages} width={30} style={{ marginRight: 10 }} />
                <TouchableWithoutFeedback onPress={this.onFocusInput}>
                    <View style={[styles.inputView, { height: this.state.editorHeight }]}>
                        <ScrollView
                            ref={(scroll) => {
                                this.scroll = scroll;
                            }}
                            showsVerticalScrollIndicator={false}
                            style={{ maxHeight: 150 }}
                            onContentSizeChange={() => {
                                this.scroll && this.scroll.scrollToEnd({ animated: true });
                            }}
                        >
                            <View
                                onLayout={this.onLayoutInputChanged}
                                style={[styles.formmatedTextWrapper]}
                            >
                                <TextInput
                                    value={this.state.message}
                                    placeholder={translate('chat.enter_message')}
                                    style={styles.input}
                                    placeholderTextColor={R.colors.grey400}
                                    multiline
                                    autoFocus={false}
                                    numberOfLines={2}
                                    onChangeText={this.onChangeText}
                                    ref={this.inputRef}
                                />

                            </View>
                        </ScrollView>
                    </View>
                </TouchableWithoutFeedback>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={this.onSendText}
                    style={styles.btnSend}
                >
                    <FastImage source={R.images.ic_send} style={styles.iconSend} />
                </TouchableOpacity>
                <PopupChooseMedias ref={this.refChooseImage} onImagesSelected={this.onSeleceImage} />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        borderTopWidth: 0.5,
        borderTopColor: colors.primaryColor,
        paddingVertical: 10,
        alignItems: 'center'
    },
    icon: {
        width: 24,
        height: 24,
        alignSelf: 'center'
    },
    iconSend: {
        width: 30,
        height: 30,
        alignSelf: 'center'
    },
    message: {
        color: '#AAAAAA',
        fontSize: 17,
        textAlign: 'center',
        paddingTop: 20
    },
    input: {
        minHeight: 38,
        fontSize: 14,
        textAlignVertical: 'top',
        fontWeight: '400',
        position: 'absolute',
        top: 0,
        paddingHorizontal: 5,
        lineHeight: 16,
        paddingTop: Platform.OS === 'ios' ? 12 : 12,
        color: '#111111',
        // color: 'transparent',
        width: '100%'
    },
    formmatedTextWrapper: {
        minHeight: 38,
        color: '#111111',
        // marginTop: Platform.OS == 'ios' ? 10 : 1,
        paddingHorizontal: 5,
        // paddingVertical: Platform.OS == 'ios' ? 10 : 1,
        width: '100%'
    },
    placeholderText: {
        color: '#888888',
        fontWeight: '400',
        fontSize: 14
    },
    inputGroup: {
        flex: 1
    },
    inputView: {
        flexDirection: 'row',
        flex: 1,
        borderRadius: 8,
        minHeight: 38,
        backgroundColor: '#F7F7F8',
        maxHeight: 150,
        paddingHorizontal: 12,
        marginRight: 10,
        alignItems: 'center',
        // paddingVertical: 5
    },
    btn: {
        paddingLeft: 8
    },
    btnSend: {}
});
