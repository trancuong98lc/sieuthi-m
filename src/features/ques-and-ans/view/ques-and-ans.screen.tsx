import { LIMIT_RECORD, MessageType } from 'global/constants';
import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import { ALERT_TYPE, showAlert, showAlertByType } from 'libraries/BaseAlert';
import { showToast } from 'libraries/BaseToast';
import { ToastType } from 'libraries/BaseToast/BaseToast';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import NoInternet from 'libraries/noInternet/NoInternet';
import moment from 'moment';
import * as React from 'react';
import { ActivityIndicator, FlatList, View } from 'react-native';
import { connect } from 'react-redux';
import { updateMessageCount } from 'redux/actions';
import { translate } from 'res/languages';
import { LoginScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';
import { STATUS } from 'types/BaseResponse';
import { MediaSelectedType, MediaType } from 'types/ConfigType';
import { Message, MessagePosition } from 'types/MessageType';
import { User } from 'types/user-type';
import { PrivateChatStates } from '../model/ques-and-ans';
import BoxSend from './component/BoxSend';
import MessageItem from './component/MessageItem';
import { styles } from './ques-and-ans.style';

export interface Props {
  user: User;
  isTab?: boolean;
  updateMessageCount: (messageCount: number | undefined) => void;
}

export interface InputMessageApp {
  toConversationId?: string;
  mediaIds?: string[];
  text?: string;
  medias?: MediaType[];
  type: MessageType;
}

interface ItemT {
  item: Message;
  index: number;
}

class QuesAndAnsScreen extends React.PureComponent<Props, PrivateChatStates> {
  socket: SocketIOClient.Socket = {} as SocketIOClient.Socket;
  constructor(props: Props) {
    super(props);
    this.state = {
      histories: [],
      totalUser: 0,
      // fromNotify: props.navigation.getParam('fromNotify', false),
      // fromPage: props.navigation.getParam('fromPage', false),
      // conversation: props.navigation.getParam('conversation', undefined), // fake data
      // fromGroup: props.navigation.getParam('fromGroup', false),
      itemSelected: {},
      loading: true,
      maxData: false,
      boxChat: false,
      latestMessageOfMine: undefined,
      isGroupOnline: false
    };
    this.socket = ApiHelper.SubscribeSocket();
  }
  scrollView: any = null;
  isLoadingMore = true;
  isConnected: boolean | null = true;

  paramsHisMesss = {
    limit: LIMIT_RECORD.MESSAGES,
    page: 1
  };

  componentDidMount = () => {
    const isLogin = Commons.idToken ? true : false;
    if (!isLogin) {
      return showAlertByType({
        title: translate('notify.header'),
        type: ALERT_TYPE.CONFIRM,
        message: translate('want_login'),
        callBackConfirm: () => {
          navigate(LoginScreen);
        }
      });
    }
    this.socket.connect();
    this.socket.on('admin-send-message-to-app', (res: any) => {
      this.setState({
        histories: [res, ...this.state.histories]
      });
    });
    this.socket.on('app-to-app', (res: any) => {
      // let exists = false;
      // const length =
      //   this.state.histories.length > 2
      //     ? 2
      //     : this.state.histories.length;

      // // Check duplicate, chỉ kiểm tra 2 phần tử đầu tiên đảm bảo performance
      // for (let index = 0; index < length; index++) {
      //   const message = this.state.histories[index];
      //   if (res && message._id === res._id || !message.tempId) {
      //     exists = true;
      //   }
      // }
      // if (!exists) {
      // khi nào có avatar thì chỉnh chỗ này tiếp
      const histories = [res, ...this.state.histories];
      this.setState({ histories }, this.onScrollToTop);
      // Set timeout vì lúc đó seen veef trước cả response đã gửi message
      // }
    });
    this.props.updateMessageCount(undefined);
    this.onLoadData();
  };

  componentWillUnmount() {
    this.socket.disconnect();
  }

  checkDiffTime(index: number) {
    const { histories } = this.state;
    // let NewTimeDate: any = {}
    if (index === histories.length - 1) return true;
    // const dateCurrentMessage = moment(histories[index].createdAt).dayOfYear();
    // const datePreviousCurrentMessage = moment(histories[index + 1].createdAt).dayOfYear();
    // if (dateCurrentMessage == datePreviousCurrentMessage) {
    //   if (!NewTimeDate[0]) {
    //     NewTimeDate = {
    //       index: histories[index].createdAt
    //     }
    //   }
    //   if (NewTimeDate) {
    //     if (moment(histories[index].createdAt).minutes() - moment(NewTimeDate[index]).minutes() > 5) {
    //       NewTimeDate[index] = moment(histories[index].createdAt)
    //       return true
    //     }
    //   }

    // }

    // if (dateCurrentMessage !== datePreviousCurrentMessage) {
    //   return true;
    // }

    const currentMessageCreatedAt = moment(histories[index].createdAt);
    const previousMessageCreatedAt = moment(histories[index + 1].createdAt);

    const dateCurrentMessage = currentMessageCreatedAt.dayOfYear();
    const datePreviousCurrentMessage = previousMessageCreatedAt.dayOfYear();
    if (
      dateCurrentMessage !== datePreviousCurrentMessage ||
      currentMessageCreatedAt.hours() !== previousMessageCreatedAt.hours() ||
      currentMessageCreatedAt.minutes() - previousMessageCreatedAt.minutes() > 5
    ) {
      return true;
    }

    return false;
  }

  private findMiddleMessageOfUser = (
    currentIndex: number,
    isDiffDate: boolean
  ): MessagePosition => {
    const currentMessage = this.state.histories[currentIndex];

    if (
      currentIndex === this.state.histories.length - 1 &&
      currentIndex !== 0 &&
      isDiffDate
    ) {
      const nextMessage = this.state.histories[currentIndex - 1];
      if (currentMessage.createdBy === nextMessage.createdBy)
        return MessagePosition.FIRST;
      return MessagePosition.ONLY;
    }

    if (currentIndex === this.state.histories.length - 1)
      return MessagePosition.NONE;

    const previousMessage = this.state.histories[currentIndex + 1];
    if (
      currentIndex === 0 &&
      currentIndex < this.state.histories.length - 1 &&
      !isDiffDate &&
      currentMessage.createdBy === previousMessage.createdBy
    ) {
      return MessagePosition.LAST;
    }

    if (currentIndex === 0) return MessagePosition.NONE;

    const nextMessage = this.state.histories[currentIndex - 1];

    // Tin nhắn nằm giữa 2 khoảng thời gian

    const isDiffDateWithNextMsg = this.checkDiffTime(currentIndex - 1);
    if (isDiffDate && isDiffDateWithNextMsg) {
      return MessagePosition.ONLY;
    }

    if (isDiffDate && currentMessage.createdBy === nextMessage.createdBy)
      return MessagePosition.FIRST;

    if (isDiffDate && currentMessage.createdBy !== nextMessage.createdBy) {
      return MessagePosition.ONLY;
    }

    if (nextMessage.system && previousMessage.system) {
      return MessagePosition.ONLY;
    }

    if (
      nextMessage.system &&
      previousMessage.createdBy === currentMessage.createdBy
    ) {
      return MessagePosition.LAST;
    }

    if (
      nextMessage.system &&
      previousMessage.createdBy !== currentMessage.createdBy
    ) {
      return MessagePosition.ONLY;
    }

    if (
      nextMessage.createdBy === currentMessage.createdBy &&
      currentMessage.createdBy === previousMessage.createdBy
    )
      return MessagePosition.MIDDLE;

    if (
      currentMessage.createdBy !== nextMessage.createdBy &&
      currentMessage.createdBy === previousMessage.createdBy
    )
      return MessagePosition.LAST;

    if (
      currentMessage.createdBy !== previousMessage.createdBy &&
      currentMessage.createdBy === nextMessage.createdBy
    ) {
      return MessagePosition.FIRST;
    }

    return MessagePosition.ONLY;
  };

  renderItem = ({ item, index }: ItemT) => {
    const { user } = this.props;
    const { histories } = this.state;

    // check người gửi là người đăng nhập app để hiện message bên phải
    const alignLeft = user._id !== item.createdBy;

    // check cùng 1 người gửi để hiện avartar 1 lần

    const lastItem = index === 0;

    const isDiffDate: boolean = this.checkDiffTime(index);

    const isMiddleMessage = this.findMiddleMessageOfUser(index, isDiffDate);

    return (
      <MessageItem
        isDiffDate={isDiffDate}
        index={index}
        isMiddleMessage={isMiddleMessage}
        latestMsgIsMine={this.state.latestMessageOfMine}
        message={item}
        user={user}
        alignLeft={alignLeft}
        lastItem={lastItem}
      />
    );
  };

  keyExtractor = (item: any, index: number): string =>
    item._id.toString() || index.toString();

  onEndReached = (): void => {
    this.loadMoreDatas();
  };

  loadMoreDatas = () => {
    const { maxData, histories } = this.state;
    if (maxData) return;

    if (this.isLoadingMore) return;
    this.isLoadingMore = true;

    const page = this.paramsHisMesss.page + 1;
    this.onLoadData(page);
  };

  onLoadData = async (page?: number) => {
    const { user } = this.props;
    try {
      this.setState({ loading: true });
      if (page) this.paramsHisMesss.page = page;
      const res = await ApiHelper.fetch(
        `${URL_API.MESSAGE}/${user._id}`,
        this.paramsHisMesss,
        true
      );
      this.isLoadingMore = false;
      if (res.status == STATUS.SUCCESS && res.data) {
        if (res.data.length > 0) {
          this.setState({
            histories:
              page && page > 1
                ? [...this.state.histories, ...res.data]
                : res.data,
            loading: false,
            maxData: res.data.length < LIMIT_RECORD.MESSAGES ? true : false
          });
        } else {
          this.setState({ maxData: true, loading: false });
        }
      }
    } catch (error) {
      this.setState({ loading: false });
    }
  };

  onSendMessageText = (text: string) => {
    const { user } = this.props;
    const { conversationId, boxChat } = this.state;
    const tempId = Date.now();

    const message: Message = {
      _id: `${tempId}`,
      text: text.trim(),
      conversationId,
      createdBy: user._id,
      createdAt: tempId,
      updatedAt: tempId,
      creator: user,
      tempId: tempId
    };
    this.onScrollToTop();
    this.doSendMessageText(message);
  };

  onScrollToTop = async () => {
    console.log('meni');
    const { histories } = this.state;
    if (histories.length) {
      setTimeout(() => {
        this.scrollView?.scrollToIndex({ index: 0 });
      }, 200);
    }
  };

  onSendImages = async (localMedias: MediaType[]) => {
    try {
      const { user } = this.props;
      const { conversationId, boxChat } = this.state;
      const tempId = Date.now();

      const medias: MediaType[] = [];
      localMedias.forEach((localMedia, index) => {
        medias.push({
          _id: localMedia._id,
          url: localMedia.uri || localMedia.url,
          uri: localMedia.uri || localMedia.url,
          createdAt: user._id,
          type: MessageType.MEDIA,
          thumbnail: localMedia.thumbnail,
          thumbnail2x: localMedia.thumbnail2x,
          originalRotation:
            localMedia && localMedia.originalRotation
              ? localMedia.originalRotation
              : 0,
          dimensions: {
            width: localMedia.width,
            height: localMedia.height
          }
        });
      });
      const message: Message = {
        _id: `${tempId}`,
        conversationId,
        createdBy: user._id,
        createdAt: tempId,
        updatedAt: tempId,
        creator: user,
        medias: medias,
        tempId: `${tempId}`,
        localMedias
      };
      // const histories = [message, ...this.state.histories];

      this.onScrollToTop();
      this.doSendMessageImage(message);
    } catch (error) {}
  };

  findIdImage = (medias: MediaType[]): string[] =>
    medias!.map((e: MediaType) => {
      return e._id;
    });

  doSendMessageImage(message: Message) {
    let data: InputMessageApp = {
      medias: message.medias,
      mediaIds: this.findIdImage(message.medias!),
      type: MessageType.MEDIA
    };
    ApiHelper.post(URL_API.MESSAGE + `/${message.createdBy}`, data, true)
      .then((res) => {
        if (res.status === STATUS.SUCCESS && res.data) {
          // this.updateMessage(message._id, res.data);
        }
      })
      .catch((err) => {});
  }

  onInternetChange = (isConnected: boolean | null): void => {
    if (isConnected === this.isConnected) return;
    this.isConnected = isConnected;
    // Gửi lại các tin nhắn lỗi chưa gửi được
    if (isConnected && !this.state.loading) {
      for (let index = this.state.histories.length - 1; index >= 0; index--) {
        const message = this.state.histories[index];
        if (message.tempId && message.tempId !== 0) {
          if (message.medias && message.medias.length > 0) {
            // Send message image again
            this.onUploadImages(message);
          } else {
            // Send message text again
            this.doSendMessageText(message);
          }
        }
      }
    }
  };

  doSendMessageText(message: Message) {
    let data: InputMessageApp = {
      text: message.text,
      type: MessageType.TEXT
    };
    ApiHelper.post(URL_API.MESSAGE + `/${message.createdBy}`, data, true)
      .then((res) => {
        if (res.status === STATUS.SUCCESS && res.data) {
          // this.updateMessage(message._id, res.data);
        }
      })
      .catch((err) => {
        if (!err.response) {
          return showToast('no_internet', { type: ToastType.ERROR });
        }
      });
  }

  updateMessage = (
    tempId: number | string,
    message?: Message,
    histories?: Message[]
  ) => {
    try {
      if (message) {
        if (histories) {
          this.setState({ histories });
        }

        const currentMessageJustSentPosition = this.state.histories.findIndex(
          (message: Message) => {
            return message._id === `${tempId}`;
          }
        );

        const tempHistories = [...this.state.histories];
        tempHistories.splice(currentMessageJustSentPosition, 1, message);

        this.setState(
          {
            histories: tempHistories
          },
          () => {}
        );
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  renderFooterComponent = (): JSX.Element | null => {
    if (
      this.state.histories.length < LIMIT_RECORD.MESSAGES ||
      this.state.maxData
    )
      return null;
    return (
      <View style={styles.loadingView}>
        <ActivityIndicator size="small" />
      </View>
    );
  };

  public render() {
    const isLogin = Commons.idToken ? true : false;
    const { isTab } = this.props;
    const { histories } = this.state;
    return (
      <Container>
        <NoInternet onInternetChange={this.onInternetChange} />
        <HeaderService title={'Daily Mart'} isTab={isTab} />
        <FlatList
          inverted
          bounces={false}
          // key={histories.length / 2}
          data={histories}
          extraData={histories}
          style={{ flex: 1 }}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          ref={(ref) => (this.scrollView = ref)}
          ListFooterComponent={this.renderFooterComponent}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.5}
        />
        {isLogin && (
          <BoxSend
            onSendText={this.onSendMessageText}
            onSendImages={this.onSendImages}
          />
        )}
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  const { isLogin, screen } = state.isLogin;
  return {
    user,
    isLogin,
    screen
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    updateMessageCount: (messageCount: number) =>
      dispatch(updateMessageCount(messageCount))
  };
};

export default connect(mapStatesToProps, mapDispatchToProps)(QuesAndAnsScreen);
