import { Message } from 'types/MessageType';

export interface PrivateChatStates {
  fromNotify?: boolean;
  boxChat?: boolean;
  histories: Message[];
  itemSelected: any;
  isSearch?: boolean;
  navigation?: any;
  totalUser: number;
  conversationId?: string;
  loading: boolean;
  maxData: boolean;
  fromPage: boolean;
  after?: string;
  q: string;
  latestMessageOfMine?: Message;
  fromGroup: boolean;
  isGroupOnline: boolean;
}
