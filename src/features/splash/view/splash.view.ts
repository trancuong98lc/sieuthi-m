import { Splash } from 'splash/model/splash';

export interface SplashView {
  onFetchDataSuccess(splash: Splash): void;
  onFetchDataFail(error?: any): void;
}
