import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import Container from 'libraries/main/container';
import moment from 'moment';
import * as React from 'react';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { onRefreshToken, onUpdateUserInfo, updateCountNoti, updateQuantityCart } from 'redux/actions';
import R from 'res/R';
import { HomeScreen, LoginScreen, StoreScreen } from 'routing/screen-name';
import { resetStack } from 'routing/service-navigation';
import { Splash } from 'splash/model/splash';
import { User } from 'types/user-type';
import { styles } from './splash.style';
import Commons from 'helpers/Commons';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
export interface Props {
  onUpdateUserInfo: (data: User) => void;
  onRefreshToken: () => Promise<void>;
  updateCountNoty: (quantity: number) => void;
  updateQuantityCart: (quantity: number) => void;
}
export interface State { }
class SplashScreen extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
  }
  onFetchDataSuccess(splash: Splash): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  timeoutNav: any = null;

  switchToScreen(screen: string) {
    if (this.timeoutNav) clearTimeout(this.timeoutNav);
    this.timeoutNav = setTimeout(() => {
      resetStack(screen);
    }, 800);
  }

  async checkToRefreshToken(): Promise<void> {
    const User: User = await AsyncStorageHelpers.getObject(StorageKey.LOGIN_SESSION)
    console.log("SplashScreen -> User", User)
    if (User && User.expiresAt) {
      let now = moment(new Date());
      let expiredAt = moment(User.expiresAt);
      let diff = expiredAt.diff(now);
      const diffDuration = moment.duration(diff).asMilliseconds();
      //nếu gần đến thời gian hết hạn token thì refresh token (120p)
      if (diffDuration < 120 * 1000 * 60) {
        this.props.onRefreshToken().then(() => { });
      }
    }
  }

  initial(): void {
    this.switchToScreen(HomeScreen);
  }

  getAsscesTokenMap = () => {
    // const
    // ApiHelper.post('https://autosuggest.search.hereapi.com/v1/',)
  }

  getTotalProductCart = (idStore: String) => {
    ApiHelper.fetch(URL_API.CART_TOTAL_PRODUCT + idStore, null, true).then(res => {
      if (res.status == STATUS.SUCCESS) {
        this.props.updateQuantityCart(res.total || 0)
      }
    })
  }

  getCountNoty = () => {
    ApiHelper.fetch(URL_API.COUNT_NOTIFICATION, null, true).then(res => {
      if (res.status == STATUS.SUCCESS && res.data) {
        this.props.updateCountNoty(res.data || 0)
      }
    })
  }


  async componentDidMount() {
    let user = await AsyncStorageHelpers.getObject(StorageKey.LOGIN_SESSION);
    let id_token = await AsyncStorageHelpers.getObject(StorageKey.ID_TOKEN);
    let id_user = await AsyncStorageHelpers.getObject(StorageKey.ID_USER);
    let store = await AsyncStorageHelpers.getObject(StorageKey.STORE);
    if (store) {
      Commons.idToken = id_token;
      Commons.idUser = id_user;
      Commons.idStore = store._id
      Commons.store = store
      if (id_token && user) {
        this.props.onUpdateUserInfo(user);
        this.getTotalProductCart(store._id)
        this.getCountNoty()
      }
      this.initial();
      await this.checkToRefreshToken(user);
    } else if (!store) {
      this.switchToScreen(StoreScreen);
      Commons.idToken = id_token;
      Commons.idUser = id_user;
      Commons.idStore = ''
      Commons.store = null
      this.props.onUpdateUserInfo(user);
      await this.checkToRefreshToken(user);
    }
  }

  public render() {
    return (
      <Container style={styles.container}>
        <FastImage source={R.images.mask} style={styles.styleImageMask} />
        <FastImage
          source={R.images.logo}
          style={styles.logo}
          resizeMode={FastImage.resizeMode.contain}
        />
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    onUpdateUserInfo: (data: User) => dispatch(onUpdateUserInfo(data)),
    updateQuantityCart: (quantity: number) => dispatch(updateQuantityCart(quantity)),
    updateCountNoty: (quantity: number) => dispatch(updateCountNoti(quantity)),
    onRefreshToken: () => {
      return new Promise((resolve, reject) => {
        dispatch(onRefreshToken(resolve, reject));
      });
    }
  }
};

export default connect(null, mapDispatchToProps)(SplashScreen);
