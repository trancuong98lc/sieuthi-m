import { DimensionHelper } from 'helpers/dimension-helper';
import { StyleSheet } from 'react-native';
import R from 'res/R';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1
  },
  styleImageMask: {
    width: DimensionHelper.getScreenWidth() * 0.309,
    height: DimensionHelper.getScreenHeight() * 0.16,
    resizeMode: 'contain',
    marginLeft: DimensionHelper.getScreenWidth() * 0.691
    //backgroundColor:'red'
  },
  logo: {
    width: 0.71 * DimensionHelper.getScreenWidth(),
    height: 0.125 * DimensionHelper.getScreenHeight(),
    //marginTop: - 0.09 * DimensionHelper.getScreenHeight(),
    resizeMode: 'contain',
    marginLeft: DimensionHelper.getScreenWidth() * 0.14,
    marginTop: DimensionHelper.getScreenHeight() * 0.12
    //backgroundColor:'red'
  }
  // view: {
  //      backgroundColor:R.colors.white100
  // }
});
