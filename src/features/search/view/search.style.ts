import { DimensionHelper } from 'helpers/dimension-helper';
import { StyleSheet } from 'react-native';
import R from 'res/R';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 13
  },
  styleTextInputs: {
    marginTop: 20,
    marginRight: 10,
    flex: 1,
    height: 40,
    marginBottom: 20,
    paddingRight: 20
  },
  styleTextInput: {
    flex: 1,
    fontSize: 13,
    marginRight: 5,
    // width: 250,
    color: '#8D8D8D'
    // position: 'absolute'
  },
  styleImageTextInput: {
    height: 15,
    width: 15,
    marginLeft: 20
  },
  buttonDelete: {},
  textButtonDelete: {
    fontFamily: R.fonts.bold,
    fontSize: 15,
    color: R.colors.primaryColor
  },
  secentSearch: {
    flexDirection: 'row',
    marginLeft: 20
  },
  textSecentSearch: {
    fontSize: 13,
    color: '#9B9B9B',
    flex: 1
  },
  buttonTrash: {
    width: 15,
    height: 18,
    marginRight: 25
  }
});
