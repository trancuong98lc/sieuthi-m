import * as React from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity } from 'react-native';
import { translate } from 'res/languages';
import { VegetableScreen } from 'routing/screen-name';
import { navigate } from 'routing/service-navigation';

export interface HistorySearchProps {
  dataSearch?: string[];
  ClickHistotyItem?: (data: any) => void;
}
export interface HistorySearchStates {
  showMore: boolean;
  dataSearch?: string[];
}

export default class HistorySearch extends React.PureComponent<
  HistorySearchProps,
  HistorySearchStates
  > {
  constructor(props: HistorySearchProps) {
    super(props);
    this.state = {
      showMore: false,
      dataSearch: this.props.dataSearch,
    };
  }

  showMoreData = () => {
    this.setState({
      showMore: !this.state.showMore,
    });
  };

  clickItem = (item: string) => () => {
    navigate(VegetableScreen, { query: item })
  };

  public render() {
    const { showMore, dataSearch } = this.state;
    const newDataSearch = this.props.dataSearch
    return (
      <>
        <View style={styles.container}>
          {newDataSearch &&
            newDataSearch?.length > 0 &&
            newDataSearch.map((item) => {
              return (
                <TouchableOpacity style={styles.btn} onPress={this.clickItem(item)}>
                  <Text style={styles.text}>{item}</Text>
                </TouchableOpacity>
              );
            })}
        </View>
        {/* {newDataSearch && newDataSearch?.length > 6 && (
          <TouchableOpacity style={styles.btnMore} onPress={this.showMoreData}>
            <Text>
              {showMore ? translate('search_filler.showLess') : translate('search_filler.showMore')}
            </Text>
          </TouchableOpacity>
        )} */}
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingHorizontal: 12
  },
  btn: {
    backgroundColor: '#F0F1F3',
    padding: 8,
    marginRight: 10,
    marginTop: 10,
    borderRadius: 5,
  },
  btnMore: {
    alignSelf: 'center',
    margin: 10,
  },
  text: {
    color: '#4D4C58',
    fontSize: 14,
  },
});
