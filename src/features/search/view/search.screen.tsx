import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import Buttons from 'libraries/button/buttons';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import { StatusBar, View } from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack, navigate } from 'routing/service-navigation';
import HistorySearch from './filter-history-search';
import { styles } from './search.style';
import { SearchView } from './search.view';
import _ from 'lodash';
import { dataSearch, SearchPresenter } from '../presenter/search-presenter';
import { connect } from 'react-redux';
import { User } from 'types/user-type';
import BaseAlert from 'libraries/BaseAlert/BaseAlert';
import colors from 'res/colors';
import { showAlert } from 'libraries/BaseAlert';
import Commons from 'helpers/Commons';
import { VegetableScreen } from 'routing/screen-name';

export interface Props {
  user: User;
}
export interface States {
  dataSearchHis: string[];
  keyWord: string;
}

class SearchScreen extends React.PureComponent<Props, States> {
  presenter: SearchPresenter;
  constructor(props: Props) {
    super(props);
    this.state = {
      dataSearchHis: [],
      keyWord: ''
    };
    this.presenter = new SearchPresenter(this);
  }
  baseAlertRef = React.createRef<BaseAlert>();
  textInput = React.createRef<TextInputs>();
  componentDidMount = () => {
    this.setSearchData(), this.textInput.current!.enableFocus();
  };

  onFetchDataSuccess(search: import('../model/search').Search): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  setSearchData = async () => {
    let { user } = this.props;
    let dataSearch =
      (await AsyncStorageHelpers.getObject(StorageKey.LIST_SEARCH)) || []; //lấy lịch sử tìm kiếm gần nhất
    let dataSearch2 =
      (await AsyncStorageHelpers.getObject('SEARCH_RECENT')) || []; //lấy lịch sử tìm kiếm gần nhất
    let Datalist: any[] = dataSearch
      ? dataSearch.filter((e: dataSearch) => e._id === user._id)
      : []; /// không có dữ liệu thì lấy mảng rỗng
    this.setState({
      dataSearchHis:
        Datalist.length > 0
          ? _.uniq([...Datalist[0].data, ...dataSearch2])
          : [...dataSearch2]
    });
  };

  setDataSearch = (data: dataSearch[], keyWord: string) => {
    let { user } = this.props;
    let Datalist: any[] = data
      ? data.filter((e: dataSearch) => e._id === user._id)
      : []; /// không có dữ liệu thì lấy mảng rỗng
    this.setState({
      dataSearchHis: Datalist.length > 0 ? Datalist[0].data : [],
      keyWord
    });
  };

  onChangeText = (value: string) => {
    this.setState({
      keyWord: value
    });
  };

  gotoSeach = () => {
    const { keyWord } = this.state;
    this.presenter.goToSearch(keyWord);
  };

  DeleteHiss = () => {
    if (this.state.dataSearchHis.length == 0) {
      return showAlert('empty_his');
    }
    this.baseAlertRef.current!.onShowByType({
      title: translate('notify.header'),
      message: translate('historySearch'),
      options: [
        {
          text: 'OK',
          buttonStyle: {
            color: 'white',
            backgroundColor: R.colors.primaryColor
          },
          onPress: async () => {
            this.setState(
              {
                dataSearchHis: []
              },
              () => {
                AsyncStorageHelpers.remove(StorageKey.LIST_SEARCH);
              }
            );
          }
        }
      ]
    });
  };

  handleSearchPress = async () => {
    let { keyWord, dataSearchHis } = this.state;
    let dataSearch2 = await AsyncStorageHelpers.getObject('SEARCH_RECENT');
    const data: any[] = dataSearch2 ? [...dataSearch2] : [];
    if (keyWord && keyWord.trim().length > 0) {
      data.unshift(keyWord);

      const data2 = _.uniq(data);

      if (data2.length > 5) {
        data2.pop();
      }
      this.setState({
        dataSearchHis: data2
      });
      console.log('aasa');
      AsyncStorageHelpers.saveArray('SEARCH_RECENT', data2);
      navigate(VegetableScreen, { query: keyWord });
    }
  };

  onPressRight = () => {
    this.setState({
      keyWord: ''
    });
  };

  public render() {
    const isLogin = Commons.idToken;
    return (
      <Container statusBarColor="white" style={styles.container}>
        <View style={styles.header}>
          <TextInputs
            placeholder={translate('textinput.search')}
            styleTextInputs={styles.styleTextInputs}
            styleTextInput={styles.styleTextInput}
            onChangeText={this.onChangeText}
            autoFocus={true}
            showDeleteText
            ref={this.textInput}
            source={R.images.ic_search}
            editable={true}
            onPressRight={this.onPressRight}
            value={this.state.keyWord}
            onSubmitEditing={isLogin ? this.gotoSeach : this.handleSearchPress}
            returnKeyType="search"
            styleImageTextInput={styles.styleImageTextInput}
          />
          <Buttons
            onPress={goBack}
            styleButton={styles.buttonDelete}
            textButton={translate('button.delete')}
            styleTextButton={styles.textButtonDelete}
          />
        </View>
        <View style={styles.secentSearch}>
          <DailyMartText style={styles.textSecentSearch}>
            {translate('recentSearch')}
          </DailyMartText>
          <Buttons
            source={R.images.ic_trash}
            onPress={this.DeleteHiss}
            styleImageButton={styles.buttonTrash}
          />
        </View>
        <HistorySearch
          dataSearch={
            this.state.dataSearchHis.length > 5
              ? this.state.dataSearchHis.slice(0, 5)
              : this.state.dataSearchHis
          }
        ></HistorySearch>
        <BaseAlert ref={this.baseAlertRef} />
      </Container>
    );
  }
}

const mapStatesToProps = (state: any) => {
  const { user } = state.userReducers;
  return {
    user
  };
};
export default connect(mapStatesToProps, null)(SearchScreen);
