import { Search } from 'search/model/search';
import { User } from 'types/user-type';

export interface SearchView {
  onFetchDataSuccess(search: Search): void;
  onFetchDataFail(error?: any): void;
  onSearchData(data: any): void;
  user: User;
}
