import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { VegetableView } from 'features/vegetable/view/vegetable.view';
import { STATUS } from 'types/BaseResponse';
import { ReqCategory } from 'features/home/model/home';
import { SearchView } from '../view/search.view';
import AsyncStorageHelpers, { StorageKey } from 'helpers/AsyncStorageHelpers';
import SearchScreen from '../view/search.screen';
import _ from 'lodash'
import { navigate } from 'routing/service-navigation';
import { VegetableScreen } from 'routing/screen-name';
export interface dataSearch {
    _id: String;
    data: String[];
}
export class SearchPresenter {
    SearchView: SearchScreen;

    constructor(SearchView: SearchScreen) {
        this.SearchView = SearchView;
    }

    async onFetchCategory(params: ReqCategory) {
        try {

        } catch (error) {
            // this.homeView.onFetchDataFail();
        }
    }

    async onGetDataProduct(params: any) {
        try {

        } catch (error) {

            // this.homeView.onFetchDataFail();
        }
    }

    goToSearch = async (keyWord: string) => {
        let dataSearch2 = await AsyncStorageHelpers.getObject('SEARCH_RECENT');

        if (keyWord) keyWord = keyWord.trim();
        const { user } = this.SearchView.props;
        let search: any = {};
        let dataSearch: dataSearch[] = await AsyncStorageHelpers.getObject(StorageKey.LIST_SEARCH);
        let dataAllsearch: dataSearch[] = dataSearch ? dataSearch : [];
        let dataSearchHis: any[] = dataAllsearch.filter((e: dataSearch) => {
            if (e && e._id == String(user._id)) {
                return e.data;
            }
        });
        keyWord !== '' &&
            (dataSearchHis.length > 0
                ? dataSearchHis[0].data.unshift(keyWord)
                : dataSearchHis.unshift(keyWord));
        const data = _.uniq([...dataSearchHis, ...dataSearch2 || []]);
        if (data.length > 5) {
            data.pop();
            AsyncStorageHelpers.saveArray('SEARCH_RECENT', data);
        }
        let dataExists = dataAllsearch.filter((e: dataSearch) => e._id === String(user._id)); /// mảng data đã có rồi
        let dataNew = [...dataAllsearch]; // mảng mới
        if (dataExists.length > 0) {
            dataNew = dataNew.map((e: dataSearch) => {
                if (e._id === String(user._id) && data[0] && data[0].data) e.data = _.uniq(data[0].data);
                return e;
            }); // thêm tiếp tục keywword vào mảng
        } else {
            // khi chưa có item nào thì push vào
            search['_id'] = String(user._id);
            search['data'] = data;
            dataExists.push(search);
        }
        if (keyWord != '') {
            AsyncStorageHelpers.saveObject(StorageKey.LIST_SEARCH, dataExists);

            navigate(VegetableScreen, { query: keyWord })
            this.SearchView.setDataSearch(dataExists, keyWord);
            return;
        }
        this.SearchView.setDataSearch(dataExists, '');
        return;
    };
}
