import { Notify } from "notify/model/notify";

export interface NotifyView {
     onFetchDataSuccess(notify: Notify): void;
     onFetchDataFail(error?: any): void;
 }