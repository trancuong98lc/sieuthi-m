import notify from 'data/notify';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import Buttons from 'libraries/button/buttons';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
import FlatListNotify from 'libraries/flatlist/flatlist-notify';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, View } from 'react-native';
import images from 'res/images';
import { translate } from 'res/languages';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import { STATUS } from 'types/BaseResponse';
import { Notify } from '../model/notify';
import { styles } from './notify.style';

export interface Props { }
export interface States {
  dataNoti: Notify[],
  loading: boolean,
  maxdata: boolean;
}

export default class NotifyScreen extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      dataNoti: [],
      maxdata: false,
      loading: true
    }
  }

  paramsNoty = {
    limit: '15',
    page: '1'
  }

  componentDidMount() {
    this.onReFresh()
  }

  resetNotification = () => {

  }

  onReFresh = () => {
    this.paramsNoty.page = '1'
    this.getNotificationScreen(this.paramsNoty)
  }

  getNotificationScreen = async (paramsNoty: any) => {
    try {

      const res = await ApiHelper.fetch(URL_API.LIST_NOTIFICATION, paramsNoty, true)
      if (res.status == STATUS.SUCCESS) {
        let maxdata = res.data.length == 0 || res.data.length < 10 ? true : false;
        const data = Number(paramsNoty.page) > 1 ? [...this.state.dataNoti, ...res.data] : res.data
        this.setState({
          dataNoti: [ ...this.state.dataNoti, ...res.data],
          loading: false,
          maxdata
        })
      } else {
        this.setState({
          dataNoti: [],
          maxdata: true,
          loading: false
        })
      }
    } catch (error) {
      this.setState({
        dataNoti: [],
        loading: false
      })
    }
  }

  _keyExtrator = (item: Notify) => item._id
  isLoadmore: boolean = false;
  onEndReached = () => {
    let { maxdata } = this.state;
    if (maxdata) return;
    if (this.isLoadmore) return;
    this.isLoadmore = true;
    const newParasm: any = {
      ...this.paramsNoty,
      page: String(Number(this.paramsNoty.page) + 1)
    }
    this.paramsNoty = newParasm
    this.getNotificationScreen(this.paramsNoty)
  }

  renderItem = ({ item, index }: { item: Notify, index: number }) => {
    return (
      <FlatListNotify item={item} index={index} />
    );
  }

  emptyData = () => {
    if (this.state.dataNoti.length > 0 || this.state.loading) return null;
    return <EmptylistComponent width={80} height={80} souce={images.ic_alarm_bell_sleep} name="no_noti" />
  }

  public render() {
    const { dataNoti, loading } = this.state
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService title="notify.header" />
        <View style={styles.hr} />
        <FlatList
          data={dataNoti}
          keyExtractor={this._keyExtrator}
          refreshing={loading}
          onRefresh={this.onReFresh}
          extraData={this.state}
          onEndReachedThreshold={0.7}
          ListFooterComponent={(<View style={{ height: 100 }} />)}
          ListEmptyComponent={this.emptyData}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.01}
          renderItem={this.renderItem}
        />
      </Container>
    );
  }
}
