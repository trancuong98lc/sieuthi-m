import { StyleSheet } from 'react-native';
import R from 'res/R';
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white100,
    flex: 1,
    marginBottom: -DimensionHelper.getBottomSpace()
  },
  notifyHeader: {
    backgroundColor: R.colors.primaryColor,
    flexDirection: 'row',
    paddingBottom: 10
  },
  notifyHeaderImage: {
    width: 35,
    height: 35,
    marginTop: 10,
    marginLeft: 10
  },
  notifyHeaderText: {
    fontFamily: R.fonts.bold,
    fontSize: 18,
    color: R.colors.white100,
    marginTop: 10,
    marginLeft: DimensionHelper.getScreenWidth() * 0.28
  },
  hr: {
    backgroundColor: '#F7F7F8',
    height: 10
  }
});
