export interface Notify {
  content: string;
  createdAt: string;
  deletedAt?: string;
  fromUser: string[];
  image: string;
  payload: Payload;
  status: boolean;
  title: string;
  _id: string;
}

export interface Payload {
  actor: string;
  userId: string;
  orderId: string;
  type: string;
  codeOrder: string;
}

export enum TYPE_NOTY {
  HAPPY_BIRTHDAY = 'Happy Birthday',
  CANCEL = 'CANCEL',
  RECEIVE = 'RECEIVE',
  RECEIVED = 'RECEIVED',
  GENERAL = 'General',
  PROMOTION = 'Promotion',
  USE_POINT = 'USE POINT',
  MESSAGE_CMS = 'MESSAGE CMS'
}
