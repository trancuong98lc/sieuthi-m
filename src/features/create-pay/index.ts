
/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/

import CreatePayContainer from 'features/create-pay/view/create-pay.screen';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = (dispatch: Dispatch) => ({});

export const CreatePayScreen = connect(
  mapStateToProps, mapDispatchToProps
)(CreatePayContainer)

