/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/

import * as React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import Container from 'libraries/main/container';
import HeaderService from 'libraries/header/header-service';
import R from "res/R";
import BaseIcon from "libraries/baseicon/BaseIcon";
import DailyMartText from "libraries/text/text-daily-mart";
import { translate } from "res/languages";
import { color } from "react-native-reanimated";
import colors from "res/colors";
import { BankData } from "features/create-pay/model";
import Buttons from "libraries/button/buttons";
interface CreatePayProps {
    nameIcon: string,
    title: string,
    showIcon?: boolean
    ishow?: boolean;
    onPress?: () => void;
    setIsShow?: (ishow: boolean) => void
}
interface CreatePayState {
    ishow: boolean;
}
export default class CreatePayComponent extends React.PureComponent<
    CreatePayProps,
    CreatePayState
    > {


    constructor(props: CreatePayProps) {
        super(props);
        this.state = {
            ishow: props.ishow || false
        }
    }

    setShowing = () => {
        const { showIcon, setIsShow, onPress } = this.props
        if (onPress) {
            onPress && onPress()
        }
        if (showIcon) {
            this.setState({
                ishow: !this.state.ishow
            })
            setIsShow && setIsShow(!this.state.ishow)
        }
    }

    public render(): React.ReactNode {
        const { nameIcon, title, showIcon } = this.props
        const { ishow } = this.state
        return (
            <View style={styles.container} >
                <BaseIcon name={nameIcon} width={33} />
                <TouchableOpacity style={styles.text} activeOpacity={0.8} onPress={this.setShowing}>
                    <DailyMartText style={{ flex: 1 }}>{translate(title)}</DailyMartText>
                    {showIcon && <BaseIcon name={ishow ? "ic_down_black" : "ic_up_black"} width={15} />}
                </TouchableOpacity>
            </View>
        );
    }
}

// styles
const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 12
    },
    button: {
        width: 19,
        height: 19,
        marginRight: 10,
        borderRadius: 19,

    },
    text: {
        marginLeft: 17,
        paddingVertical: 20,
        flex: 1,
        borderBottomColor: '#ECECEC',
        borderBottomWidth: 1,
        flexDirection: 'row',
        paddingRight: 24
    }
});


