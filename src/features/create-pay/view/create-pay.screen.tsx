/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/

import * as React from 'react';
import { View, StyleSheet, ScrollView, FlatList, TouchableOpacity, Animated, Easing, Image } from 'react-native';
import { CreatePayProps, CreatePayState, CreatePayAdapter, BankData } from '../model';
import Container from 'libraries/main/container';
import HeaderService from 'libraries/header/header-service';
import R from 'res/R';
import CreatePayComponent from './components/create-pay-view-pay';
import FastImage from 'react-native-fast-image';
import { getFileSize, getLogoUrl, getMediaUrl } from 'helpers/helper-funtion';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import colors from 'res/colors';
import BaseIcon from 'libraries/baseicon/BaseIcon';
import ImagePicker from 'react-native-image-crop-picker';
import { showAlert } from 'libraries/BaseAlert';
import { MediaType, PHOTO_SIZE } from 'types/ConfigType';
import { goBack } from 'routing/service-navigation';
import Config from 'react-native-config';

export const MAX_SIZE_IMAGE = 5000000;
export default class CreatePayContainer extends React.Component<CreatePayProps, CreatePayState> {
  private adapter: CreatePayAdapter;
  refCreate = React.createRef<CreatePayComponent>()
  constructor(props: CreatePayProps) {
    super(props);
    this.adapter = new CreatePayAdapter(this);
    this.state = {
      dataBank: [],
      valueImage: null,
      pathImage: '',
      isShow: false,
      fadeAnim: new Animated.Value(0),
    };

  }
  componentDidMount = () => {
    const { valuePay, setIspayment } = this.props.route.params
    this.adapter.getListBank();
    if (valuePay && setIspayment) {
      if (valuePay.type !== 'CASHING') {
        this.setIsShow(true)
        this.setState({
          valueImage: valuePay.image
        })
      } else {
      }
    }

  };

  setDatabank = (dataBank: BankData[]) => {
    this.setState({
      dataBank
    });
  };

  onPressButtonnotdelivered() {
    const animations = [
      Animated.timing(this.state.fadeAnim, {
        toValue: 1,
        duration: 500,
        easing: Easing.linear,
        useNativeDriver: true
      }),
    ];

    Animated.sequence(animations).start()
  }

  onPressIsHideFlatList = () => {
    const animations = [
      Animated.timing(this.state.fadeAnim, {
        toValue: 0,
        duration: 500,
        easing: Easing.linear,
        useNativeDriver: true
      }),
    ];

    Animated.sequence(animations).start()
  }



  renderItem = ({ item, index }: { item: BankData; index: number }) => {
    const { dataBank } = this.state;
    const { code } = this.props.route.params;
    return (
      <Animated.View style={[styles.containerItem, { borderBottomWidth: 1, borderColor: '#ECECEC' }, { opacity: this.state.fadeAnim }]}>
        <View style={styles.viewNum}>
          <FastImage
            source={getLogoUrl(item.logo)}
            resizeMode={FastImage.resizeMode.contain}
            style={{ width: 104, height: 48 }}
          />
          <DailyMartText style={styles.text}>{item.name || ''}</DailyMartText>
        </View>
        <View style={styles.viewNum}>
          <DailyMartText>{translate('create_pay.bank_owner')}</DailyMartText>
          <DailyMartText fontStyle="semibold" style={styles.text1}>
            {item.bankOwner || ''}
          </DailyMartText>
        </View>
        <View style={styles.viewNum}>
          <DailyMartText>{translate('create_pay.bank_number')}</DailyMartText>
          <DailyMartText fontStyle="semibold" style={styles.text1}>
            {item.bankNumber || ''}
          </DailyMartText>
        </View>
        {index == dataBank.length - 1 ? (
          <View style={styles.viewNote}>
            <DailyMartText>{translate('create_pay.content_bank')}</DailyMartText>
            <DailyMartText fontStyle="semibold" style={styles.text1}>
              {`${translate('create_pay.pay_for')} #${code}`}
            </DailyMartText>
          </View>
        ) : null}
      </Animated.View>
    );
  };

  setFilemImage = (valueImage: MediaType) => {
    this.setState({
      valueImage
    })
  }

  onChoosePhoto = async () => {
    await ImagePicker.openPicker({
      multiple: false,
      mediaType: 'photo',
      compressImageQuality: 1
    }).then(async (images: any) => {
      const fileSize = await getFileSize(images.path);
      if (fileSize > MAX_SIZE_IMAGE) {
        return showAlert('max_size_image');
      }
      const data = new FormData()
      data.append('file', {
        uri: images.path,
        type: 'image/jpeg',
        name: 'album.jpg'
      })
      data.append('width', images.width)
      data.append('height', images.height)
      this.setState({
        pathImage: images.path
      })
      this.adapter.upLoadImage(data)
    });
  };

  onAccept = () => {
    const { valuePay, setIspayment } = this.props.route.params
    if (!this.state.valueImage) return showAlert('create_pay.not_choose_image')
    setIspayment({
      type: 'BANKING',
      image: this.state.valueImage

    })
    goBack()
  }


  foter = () => {
    const { valueImage } = this.state
    return (
      <View style={styles.viewChose}>
        <DailyMartText style={styles.choseImageTitle}>{translate('create_pay.upload_image_bank')}</DailyMartText>
        <TouchableOpacity style={styles.choseImage} activeOpacity={0.8} onPress={this.onChoosePhoto}>
          <DailyMartText style={{ color: colors.primaryColor }}>{translate('chose_photos')}</DailyMartText>
        </TouchableOpacity>
      </View>
    );
  };

  removeImage = () => {
    this.setState({
      valueImage: null
    })
  }

  // renderImage = () => {
  //   const { valueImage } = this.state
  //   if (!valueImage) return <View style={{ height: 100 }} />
  //   return (

  //   );
  // };

  setIsShow = (isShow: boolean) => {
    this.setState({
      isShow
    }, () => {
      if (isShow) {
        this.onPressButtonnotdelivered()
      } else {
        this.onPressIsHideFlatList()
      }
    })
  }

  confirm = () => {
    const { valuePay, setIspayment } = this.props.route.params
    setIspayment({
      type: 'CASHING'
    })
    goBack()
  }

  public render(): React.ReactNode {
    const { isShow, valueImage } = this.state
    const { valuePay, setIspayment } = this.props.route.params

    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService title="create_pay.type_pay" />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <CreatePayComponent title="create_pay.pay1" onPress={this.confirm} nameIcon="create_pay1" />
            <CreatePayComponent setIsShow={this.setIsShow} ishow={isShow} ref={this.refCreate} title="create_pay.pay2" showIcon nameIcon="create_pay2" />
            {isShow && <FlatList
              ListFooterComponent={this.foter}
              data={this.state.dataBank}
              showsVerticalScrollIndicator={false}
              renderItem={this.renderItem}
            />}
          </View>
          {this.state.valueImage &&
            <View style={styles.viewImage}>
              <FastImage
                source={this.state.pathImage ? { uri: this.state.pathImage } : getMediaUrl(valueImage)}
                resizeMode={FastImage.resizeMode.cover}
                style={styles.image}
              />
              <BaseIcon name="ic_close_color" width={20} onPress={this.removeImage} style={styles.icon} />
            </View>}
          <View style={{ height: 100 }} />
        </ScrollView>
        {isShow && <TouchableOpacity style={styles.btnAccept} activeOpacity={0.8} onPress={this.onAccept}>
          <DailyMartText style={{ color: 'white' }}>{translate('dateTimePicker.ok')}</DailyMartText>
        </TouchableOpacity>}
      </Container>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: { flex: 1, },
  text: { flex: 1, marginLeft: 5, marginTop: -5 },
  text1: { flex: 1, marginLeft: 5 },
  viewNum: { flexDirection: 'row', marginTop: 20 },
  viewNote: {
    flexDirection: 'row',
    marginTop: 20,
    paddingVertical: 10,
    borderTopWidth: 1,
    // borderBottomWidth: 1,
    borderColor: '#ECECEC',
    marginHorizontal: -20,
    paddingHorizontal: 20
  },
  containerItem: {
    flex: 1,
    paddingBottom: 15,
    paddingHorizontal: 20
  },
  btnAccept: {
    paddingVertical: 10,
    paddingHorizontal: 30,
    backgroundColor: colors.primaryColor,
    position: 'absolute',
    alignSelf: 'center',
    borderRadius: 9999,
    bottom: 20
  },
  viewChose: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  choseImageTitle: {
    flex: 8 
  },
  choseImage: {
    flex: 2,
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 9999,
    borderWidth: 1,
    borderColor: colors.primaryColor
  },
  icon: { position: 'absolute', top: -5, right: -20 },
  viewImage: {
    width: 100,
    height: 100,
    marginTop: 10,
    paddingHorizontal: 12
  },
  image: {
    width: 100,
    height: 100,
  }
});
