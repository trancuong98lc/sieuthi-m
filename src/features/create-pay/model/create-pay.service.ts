/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/
export default class CreatePayService {
  private static instance: CreatePayService;

  static getInstance(): CreatePayService {
    if (!CreatePayService.instance) {
      CreatePayService.instance = new CreatePayService();
    }
    return CreatePayService.instance;
  }
}
