import { showAlert } from 'libraries/BaseAlert';
import { STATUS } from './../../../types/BaseResponse';
import { startAnimationHelper } from './../../../libraries/ContentLoader/shared';
import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/
import CreatePayContainer from '../view/create-pay.screen';
import CreatePayService from './create-pay.service';
import {
  hideLoading,
  showLoading
} from 'libraries/LoadingManager/LoadingModal';

export class CreatePayAdapter {
  private view: CreatePayContainer;
  constructor(view: CreatePayContainer) {
    this.view = view;
  }

  getListBank = async () => {
    try {
      const res = await ApiHelper.fetch(
        URL_API.BANK_LIST,
        {
          limit: '20',
          page: '1',
          status: true
        },
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setDatabank(res.data);
      } else {
        this.view.setDatabank([], '');
      }
    } catch (error) {}
  };

  upLoadImage = async (data: FormData) => {
    try {
      showLoading();
      const res = await ApiHelper.postForm(URL_API.UPLOAD_FILE, data, true);
      if (res.status == STATUS.SUCCESS) {
        hideLoading();
        this.view.setFilemImage(res.data);
      } else {
        showAlert('create_pay.upload_err');
        hideLoading();
      }
    } catch (error) {
      hideLoading();
      console.log('CreatePayAdapter -> upLoadImage -> error', error);
    }
  };
}
