/* 
  Created by Cường at 10-05-2020 11:01:17
  Màn hình Tạo mới màn hình thanh toán
*/

import { NavigationInitActionPayload } from 'react-navigation';
import { MediaType } from 'types/ConfigType';

export interface CreatePayProps {
  route: NavigationInitActionPayload;
}

export interface CreatePayState {
  dataBank: BankData[];
  valueImage: MediaType | null;
  pathImage: string;
  isShow: boolean;
  fadeAnim: any;
  contentPayment: string;
}

export interface BankData {
  bankNumber: string;
  bankOwner: string;
  createdAt: string;
  deletedAt?: null;
  logo: string;
  name: string;
  note: string;
  status: boolean;
  _id: string;
}
