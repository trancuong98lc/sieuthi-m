import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from 'helpers/dimension-helper';

export const styles = StyleSheet.create({
     container:  {
          backgroundColor: R.colors.white100,
          flex: 1,
          marginBottom:-DimensionHelper.getBottomSpace(),
     }, 
     cartHeader: {
         backgroundColor: R.colors.primaryColor,
       paddingBottom:10
     },
     cartHeaderText: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop: 15,
          textAlign: 'center',
          paddingBottom:10
     },
     name: {
          flexDirection: 'row',
          padding: DimensionHelper.getScreenHeight() * 0.1 / 6,
     },
     nameImage: {
          width: 17,
       height:17,   
     },
     nameText: {
          marginLeft: 10,
          fontFamily: R.fonts.medium,
          fontSize: 15,
          color:R.colors.primaryColor
     },
     add: {
          flexDirection: 'row',
          paddingLeft: DimensionHelper.getScreenHeight() * 0.1 / 5,
     },
     addImage: {
          width: 13,
          height: 18, 
     },
     addText: { 
          marginLeft: 10,
          fontFamily: R.fonts.medium,
          fontSize: 13,
          color:"#7A7A7A"
     },
     flatList: {
          marginTop: 10,
          backgroundColor: R.colors.white100,
          height:DimensionHelper.getScreenHeight()*0.7
     },
     cartFooter: {
          flexDirection: 'row',
          marginBottom: 0,
          height:80
     },
     sumCost: {
          borderWidth: 1,
          width: DimensionHelper.getScreenWidth() / 2,
          paddingLeft:20
          
     },
     text: {
          fontSize: 13,
          color: '#979797',
          marginTop:10
     },
     sum: {
          fontFamily: R.fonts.bold,
          color: '#E5334B',
          marginTop:8,
          fontSize:20
     },
     textPay: {
          fontFamily: R.fonts.bold,
          color: R.colors.white100,
          fontSize:20
     },
     pay: {
          width: DimensionHelper.getScreenWidth() / 2,
          backgroundColor: R.colors.primaryColor,
          borderRadius:0
     }


})