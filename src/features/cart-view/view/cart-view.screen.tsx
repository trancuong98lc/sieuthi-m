import cart from 'data/cart';
import { Cart } from 'features/cart/model/cart';
import FlatListCart from 'libraries/flatlist/flatlist.cart';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import * as React from 'react';
import { FlatList, Image, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { styles } from './cart-view.style';
import { CartViewView } from './cart-view.view';

export interface Props {}
interface State {
  deletedRowKey: any;
}
export default class CartViewScreen extends React.PureComponent<Props, State>
  implements CartViewView {
  constructor(props: Props) {
    super(props);
    this.state = {
      deletedRowKey: null
    };
  }
  onFetchDataSuccess(cart: Cart): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }
  refreshFlatList = (deletedKey: any) => {
    this.setState({ deletedRowKey: deletedKey });
  };
  public render() {
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <View style={styles.container}>
          <View style={{ flex: 1 }}>
            <View style={styles.cartHeader}>
              <DailyMartText style={styles.cartHeaderText}>
                {translate('cart.header')}
              </DailyMartText>
            </View>
            <View style={{ height: 11, backgroundColor: '#E5E5E5' }}></View>
            <View>
              <View style={styles.name}>
                <FastImage
                  resizeMode={FastImage.resizeMode.contain}
                  source={R.images.ic_store}
                  style={styles.nameImage}
                />
                <DailyMartText style={styles.nameText}>
                  {translate('cart.nameSupermarket')}
                </DailyMartText>
              </View>
              <View style={styles.add}>
                <FastImage
                  source={R.images.ic_add}
                  style={styles.addImage}
                ></FastImage>
                <DailyMartText style={styles.addText}>
                  {translate('cart.addr')}
                </DailyMartText>
              </View>
            </View>
            <View>
              <FlatList
                style={styles.flatList}
                data={cart}
                renderItem={({ item, index }) => {
                  return (
                    <FlatListCart
                      item={item}
                      index={index}
                      parentFlatList={this}
                    ></FlatListCart>
                  );
                }}
              ></FlatList>
            </View>
          </View>
        </View>
      </Container>
    );
  }
}
