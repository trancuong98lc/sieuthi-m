import { CartView } from "cart-view/model/cart-view";

export interface CartViewView {
     onFetchDataSuccess(cartView: CartView): void;
     onFetchDataFail(error?: any): void;
 }