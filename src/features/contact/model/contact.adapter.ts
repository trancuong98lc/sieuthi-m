import ApiHelper from 'helpers/api-helper';
import Commons from 'helpers/Commons';
import { URL_API } from 'helpers/url-api';
import { STATUS } from 'types/BaseResponse';
import ContactScreen from '../view/contact.screen';

export class ContactAdapter {
  private view: ContactScreen;
  constructor(view: ContactScreen) {
    this.view = view;
  }

  getContact = async () => {
    try {
      // showLoading();
      const res = await ApiHelper.fetch(
        `${URL_API.CONTACT}`,
        {},
        true
      );
      if (res.status == STATUS.SUCCESS) {
        this.view.setContact(res.data);
      } else {
        this.view.setContact(this.view.initContact);
      }
    } catch (error) {
        console.log("getContact: ", error);
        this.view.setContact(this.view.initContact);
    }
  };
}
