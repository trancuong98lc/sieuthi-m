export interface Contact {
     name: string,
     email: string,
     address: string,
     phoneNumber: string,
     description: string
}