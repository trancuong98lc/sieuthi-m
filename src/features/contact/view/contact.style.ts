import { StyleSheet } from "react-native";
import R from "res/R";
import { DimensionHelper } from "helpers/dimension-helper";

export const styles = StyleSheet.create({
     container: {
          backgroundColor:R.colors.white100,
          flex:1
     },
     header: {
		height: 50,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: 12
     },
     buttonBack: {
          backgroundColor:R.colors.white100,
     },
     imageButtonBack: {
          height: 35,
          width: 35,
          marginTop: 10,
          marginLeft:12
     },
     headerTitle: {
          fontFamily: R.fonts.bold,
          fontSize: 18,
          color: R.colors.white100,
          marginTop:  10,
          marginLeft:DimensionHelper.getScreenWidth()*0.15
     },
     logo: {
          width: DimensionHelper.getScreenWidth(),
          height: 107,
          resizeMode: "contain",
          marginTop:40
     },
     content: {
          backgroundColor: '#F7F7F8',
          flex: 1,
          marginTop:50
     },
     name: {
          fontFamily: R.fonts.bold,
          fontSize: 22,
          color: '#5E5E5E',
          marginTop:29,
          marginHorizontal:18,
     },
     addr: {
          marginHorizontal:18,
          textAlign: 'center',
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#5E5E5E',
          marginTop:17
     },
     contentText: {
          textAlign: 'center',
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#5E5E5E',
          marginHorizontal: 16,
          marginTop:34
     },
     phone: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#5E5E5E',
          marginHorizontal: 16,
          marginTop:34
     },
     email: {
          fontFamily: R.fonts.bold,
          fontSize: 15,
          color: '#5E5E5E',
          marginHorizontal: 16,
          marginTop:34
     }
})