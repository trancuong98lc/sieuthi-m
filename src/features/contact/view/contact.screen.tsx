import * as React from 'react';
import { View, Image, ScrollView } from 'react-native';
import Buttons from 'libraries/button/buttons';
import { goBack } from 'routing/service-navigation';
import R from 'res/R';
import DailyMartText from 'libraries/text/text-daily-mart';
import { translate } from 'res/languages';
import Container from 'libraries/main/container';
import { ContactView } from './contact.view';
import { Contact } from '../model/contact';
import { styles } from './contact.style';
import { ContactAdapter } from '../model/contact.adapter';
import FastImage from 'react-native-fast-image';

export interface Props {}
export interface State {
  contact: Contact;
}

export default class ContactScreen extends React.PureComponent<Props, any>
  implements ContactView {
  private adapter: ContactAdapter;
  public initContact: Contact;
  constructor(props: Props) {
    super(props);
    this.initContact = {
      name: '',
      email: '',
      address: '',
      phoneNumber: '',
      description: ''
    };
    this.adapter = new ContactAdapter(this);
    this.state = {
      contact: this.initContact
    };
  }

  componentDidMount = () => {
    this.adapter.getContact();
  };

  setContact = (contact: Contact) => {
    this.setState({
      contact
    });
  };

  onFetchDataSuccess(contacts: Contact): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  public render() {
    const { contact } = this.state;
    return (
      <Container statusBarColor={R.colors.white100}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Buttons
              onPress={() => goBack()}
              source={R.images.ic_back}
              styleButton={styles.buttonBack}
              styleImageButton={styles.imageButtonBack}
            />
            <DailyMartText style={{ fontSize: 18, fontWeight: 'bold' }}>
              {translate('contacts.header')}
            </DailyMartText>
            <View style={{ width: 50 }} />
          </View>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={R.images.logo}
            style={styles.logo}
          />
          <ScrollView
            style={styles.content}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ alignItems: 'center', paddingBottom: 50 }}
          >
            <DailyMartText style={styles.name}>
              {`${contact.name}`}
            </DailyMartText>
            <DailyMartText style={styles.addr}>
              {contact.address !== ''
                ? `${translate('contacts.addr')} ${contact.address}`
                : ''}
            </DailyMartText>
            <DailyMartText style={styles.contentText}>
              {`${contact.description}`}
            </DailyMartText>
            <DailyMartText style={styles.phone}>
              {contact.phoneNumber !== ''
                ? `${translate('contacts.phone')} ${contact.phoneNumber}`
                : ''}
            </DailyMartText>
            <DailyMartText style={styles.email}>
              {contact.email !== ''
                ? `${translate('contacts.email')} ${contact.email}`
                : ''}
            </DailyMartText>
          </ScrollView>
        </View>
      </Container>
    );
  }
}
