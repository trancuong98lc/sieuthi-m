import { Contact } from "contact/model/contact";

export interface ContactView {
     onFetchDataSuccess(contact: Contact): void;
     onFetchDataFail(error?: any): void;
 }