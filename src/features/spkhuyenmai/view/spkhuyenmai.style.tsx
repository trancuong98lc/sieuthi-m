import { DimensionHelper } from 'helpers/dimension-helper';
import { StyleSheet } from 'react-native';
import R from 'res/R';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F8',
    marginBottom: -DimensionHelper.getBottomSpace()
  },
  header: {
    flexDirection: 'row',
    //marginTop:  10,
    paddingBottom: 10,
    paddingTop: 10,
    backgroundColor: R.colors.primaryColor
  },
  modal: {
    flex: 1
  },
  filterHeader: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: DimensionHelper.getStatusBarHeight()
  },
  filterHeaderText: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: R.colors.primaryColor,
    marginLeft: DimensionHelper.getScreenWidth() * 0.27
  },
  portfolio: {
    flexDirection: 'row',
    marginTop: 30
  },
  title1: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    color: '#333333'
  },
  title2: {
    fontFamily: R.fonts.bold,
    fontSize: 16,
    marginVertical: 10,
    color: '#333333'
  },
  filterHeaderImagebutton: {
    width: 18,
    height: 18
  },
  imageheader1: {
    width: 0.085 * DimensionHelper.getScreenWidth(),
    height: 0.085 * DimensionHelper.getScreenWidth(),
    marginLeft: 10
  },
  imageheader2: {
    width: 0.038 * DimensionHelper.getScreenWidth(),
    height: 0.038 * DimensionHelper.getScreenWidth(),
    marginLeft: 0.2 * DimensionHelper.getScreenWidth()
  },
  textheader: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    marginLeft: 0.14 * DimensionHelper.getScreenWidth(),
    fontSize: 18,
    fontFamily: 'Quicksand-Bold'
  },
  flatlistSpkhuyenmai: {
    backgroundColor: '#F7F7F8',
    marginBottom: DimensionHelper.getBottomSpace()
  },
  aboutCost: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  filterMin: {
    maxWidth: DimensionHelper.width / 2 - 50,
    height: 30,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: '#F1F1F1'
  },
  buttonTitle1: {
    width: 10,
    height: 10,
    marginTop: 3,
    marginLeft: 10
  },
  filterFooterTextinput: {
    width: '100%',
    height: 40,
    alignItems: 'center',
    color: '#4A4A4A',
    textAlign: "center"
    //justifyContent: 'center',
  },
  filterMax: {
    maxWidth: DimensionHelper.width / 2 - 50,
    paddingHorizontal: 10,
    height: 30,
    justifyContent: 'center',
    backgroundColor: '#F1F1F1'
  },
  reSetting: {
    flex: 1,
    height: DimensionHelper.getScreenWidth() * 0.16,
    borderTopWidth: 0.5,
    borderColor: '#909090',
    borderRadius: 0,
  },
  search: {
    flex: 1,
    height: DimensionHelper.getScreenWidth() * 0.16,
    backgroundColor: R.colors.primaryColor,
    borderRadius: 0
  },
  filterFooterText: {
    fontSize: 15
  }
});
