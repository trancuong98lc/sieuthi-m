import { Category } from 'features/home/model/home';
import { Products } from 'types/category';

export interface PromotionView {
  onFetchDataCateGorySuccess(
    data: Category[],
    isViewMore?: boolean,
    total?: number
  ): void;
  onFetchDataSuccess(data: Products[]): void;
  onFetchDataFail(error?: any): void;
}
