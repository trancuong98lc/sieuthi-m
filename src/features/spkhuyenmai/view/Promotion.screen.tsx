import product from 'data/product';
import { Category, ReqCategory } from 'features/home/model/home';
import { ProductReq } from 'features/vegetable/model/vegetable';
import ItemCateGorySearch from 'features/vegetable/view/components/ItemCateGorySearch';
import Commons from 'helpers/Commons';
import { DimensionHelper } from 'helpers/dimension-helper';
import { _formatPrice } from 'helpers/helper-funtion';
import { showAlert } from 'libraries/BaseAlert';
import Buttons from 'libraries/button/buttons';
import EmptylistComponent from 'libraries/Empty/EmptylistComponent';
import FlatListSanphamkhuyenmai from 'libraries/flatlist/flatlist.san-pham-khuyen-mai';
import HeaderService from 'libraries/header/header-service';
import Container from 'libraries/main/container';
import DailyMartText from 'libraries/text/text-daily-mart';
import TextInputs from 'libraries/textinput/textinput';
import * as React from 'react';
import { FlatList, Keyboard, Platform, StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Modal from 'react-native-modal';
import { TextInput } from 'react-native-paper';
import colors from 'res/colors';
import images from 'res/images';
import { translate } from 'res/languages';
import R from 'res/R';
import { Products, STATUS_SEARCH } from 'types/category';
import { Spkhuyenmai } from '../model/spkhuyenmai';
import { PromotionPresenter } from '../presenter/promotion.presenter';
import { PromotionView } from './promotion.view';
import { styles } from './spkhuyenmai.style';

export interface Props {
  route: any,
  promotionId?: string
}

export interface States {
  spkhuyenmai: Spkhuyenmai;
  isVisible: boolean
  showKeyborad: boolean
  CateGoryList: Category[],
  products: Products[],
  minPrice: string,
  maxPrice: string
  search: string
  viewMore: boolean | null,
  totalCate: number,
  loading: boolean,
  maxdata: boolean
}

export default class PromotionScreen extends React.PureComponent<Props, States>
  implements PromotionView {
  presenter: PromotionPresenter;
  private onEndReachedCalledDuringMomentum: boolean;
  private refFlatList: any;

  paramsCate: ReqCategory = {
    limit: '8',
    page: '1',
    status: STATUS_SEARCH.ACTIVE
  };

  paramsProduct: ProductReq = {
    limit: 10,
    page: 1,
    status: STATUS_SEARCH.ACTIVE,
    storeId: Commons.idStore,
    promotionId: this.props.route?.params?.promotionId ?? undefined,
    search: ''
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      CateGoryList: [],
      products: [],
      isVisible: false,
      totalCate: 0,
      viewMore: null,
      showKeyborad: false,
      minPrice: '',
      search: '',
      maxPrice: '',
      spkhuyenmai: product,
      loading: true,
      maxdata: true
    }
    this.presenter = new PromotionPresenter(this);
    this.onEndReachedCalledDuringMomentum = true;
    this.refFlatList = React.createRef();
  }

  componentDidMount = () => {
    this.onGetCateGory();
    this.onGetProducts();
  };


  onGetCateGory = (): void => {
    this.presenter.onFetchCategory(this.paramsCate)
  };

  onFetchDataSuccess(location: Spkhuyenmai): void {
    throw new Error('Method not implemented.');
  }
  onFetchDataFail(error?: any): void {
    throw new Error('Method not implemented.');
  }

  onDraw = () => {
    this.setState({
      isVisible: true
    })
  }

  toggleModal = () => {
    this.setState({
      isVisible: false
    })
  }

  renderItem = ({ item, index }: { item: Products, index: number }) => {
    return (
      <FlatListSanphamkhuyenmai
        item={item}
        index={index}
      />
    );
  }

  onSetCateGory = (CateGoryList: Category[]) => {
    this.setState({ CateGoryList })
  }

  onGetProducts = () => {
    this.presenter.getPromotions(this.paramsProduct);
  }

  setProducts = (products: Products[], maxdata: boolean) => {
    this.setState({
      products,
      loading: false,
      maxdata
    });
  }

  onPressCate = (item: Category, index?: number) => {
    const { CateGoryList } = this.state
    const newCateGory = [...CateGoryList]
    newCateGory.map(e => {
      if (e && e._id == item._id) {
        e.active = !e.active ? true : false
      }
      return e
    })
    this.onSetCateGory(newCateGory)
  }


  setTotalCate = (totalCate: number) => {
    this.setState({
      totalCate: totalCate || 0
    })
  }


  onFetchDataCateGorySuccess(data: Category[], isViewmore: boolean, total: number): void {
    if (this.paramsCate.page == '1') {
      this.onSetCateGory(data)
      this.setTotalCate(total)
      this.onSetViewMore(isViewmore)
      return
    }
    this.onSetCateGory([...this.state.CateGoryList, ...data])
    this.onSetViewMore(isViewmore)
  }


  onSetViewMore = (viewMore: boolean) => {
    this.setState({
      viewMore
    })
  }


  onShowMore = () => {
    const params = { page: this.state.viewMore ? 1 : '1' }
    const newParams: ReqCategory = {
      ...this.paramsCate,
      page: typeof params.page == 'string' ? params.page : String(Number(this.paramsCate.page) + params.page)
    }
    this.paramsCate = newParams
    this.presenter.onFetchCategory(this.paramsCate)
  }

  keyExtratorModal = (item: Category, index: number) => item._id.toString()

  renderItemModal = ({ item, index }: { item: Category, index: number }) => {
    return <ItemCateGorySearch onPressItemCate={this.onPressCate} item={item} index={index} />
  }

  footer = () => {
    if (this.state.CateGoryList.length < 8) return null
    return <View style={styles.portfolio}>
      <Buttons
        textButton={this.state.viewMore ? translate('view_more') : translate('view_less')}
        source={this.state.viewMore ? images.ic_all : images.ic_less}
        styleImageButton={styles.buttonTitle1}
        onPress={this.onShowMore}
        styleTextButton={{ color: colors.primaryColor }}
        styleButton={{ alignItems: 'center', width: '100%', justifyContent: 'center', }}
      />
    </View>
  }

  onShowKeyBoard = () => {
    this.setState({ showKeyborad: true });
  };

  onHideKeyBoard = () => {
    this.setState({ showKeyborad: false });
  };

  onDissmiss = () => {
    Keyboard.dismiss();
  };

  onChangeText = (key: any) => (value: any) => {
    value = value.replace(/\./g, "");
    this.setState({ [key]: _formatPrice(value) })
  }

  resetSearch = () => {
    const { CateGoryList } = this.state
    const newCateGory = [...CateGoryList]
    newCateGory.map(e => {
      e.active = false
      return e
    })

    this.onSetCateGory(newCateGory)
    this.setState({
      minPrice: '',
      maxPrice: ''
    })
  }

  getDataStringIds = (data: Category[]) => {
    const data2: any[] = data.map((e: Category) => {
      if (e.active) return (e._id)
    })
    return data2 || []
  }

  // handelSubmit = () => {
  //   const { minPrice, maxPrice, CateGoryList } = this.state
  //   const newParams: ProductReq = {
  //     ...this.paramsProduct || {},
  //   }
  //   if (!minPrice) delete newParams.minPrice
  //   if (!maxPrice) delete newParams.maxPrice
  //   if (minPrice) newParams.minPrice = Number(minPrice)
  //   if (maxPrice) newParams.maxPrice = Number(maxPrice)
  //   if (Number(minPrice) && Number(maxPrice)) {
  //     if (Number(minPrice) > Number(maxPrice)) {
  //       this.toggleModal()
  //       return setTimeout(() => {
  //         showAlert('not_price', { success: false })
  //       }, 300);
  //     }
  //   }
  //   const cate = CateGoryList.filter((e: Category) => {
  //     if (e.active) return { id: e._id }
  //   })
  //   if (this.getDataStringIds(cate).length > 0) {
  //     newParams.categoryIds = this.getDataStringIds(cate)
  //   }
  //   if (cate.length > 0) {
  //     newParams.categoryIds = this.getDataStringIds(cate)
  //   }
  //   newParams.page = 1;
  //   this.paramsProduct = newParams;
  //   this.onGetProducts();
  //   this.toggleModal()
  // }

  handelSubmit = () => {
    const { minPrice, maxPrice, CateGoryList } = this.state
    const newParams: ProductReq = {
      ...this.paramsProduct || {},
    }
    let minPriceNumber = minPrice.replace(/\./g, "");;
    let maxPriceNumber = maxPrice.replace(/\./g, "");;

    if (!minPriceNumber) delete newParams.minPrice
    if (!maxPriceNumber) delete newParams.maxPrice
    if (minPriceNumber) newParams.minPrice = Number(minPriceNumber)
    if (maxPriceNumber) newParams.maxPrice = Number(maxPriceNumber)
    if (Number(minPriceNumber) && Number(maxPriceNumber)) {
      if (Number(minPriceNumber) > Number(maxPriceNumber)) {
        this.toggleModal()
        return setTimeout(() => {
          showAlert('not_price', { success: false })
        }, 300);
      }
    }
    const cate = CateGoryList.filter((e: Category) => {
      if (e.active) return { id: e._id }
    })
    if (this.getDataStringIds(cate).length > 0) {
      newParams.categoryIds = this.getDataStringIds(cate)
    }
    if (cate.length > 0) {
      newParams.categoryIds = this.getDataStringIds(cate)
    }
    if (cate.length == 0) {
      delete newParams.categoryIds;
    }

    newParams.page = 1;
    this.paramsProduct = newParams;
    this.onGetProducts();
    this.toggleModal();
    this.refFlatList.scrollToOffset({ animated: false, offset: 0 });
  }

  setMaxData = (maxdata: boolean) => {
    this.setState({
      maxdata
    })
  }

  keyExtractor = (item: Products, index: number) => {
    return item._id.toString();
  }

  onRefresh = () => {
    const newParams: any =
    {
      ...this.paramsProduct,
      page: 1
    }
    this.paramsProduct = newParams;
    this.presenter.getPromotions(newParams);
  }

  onEndReached = () => {
    const { loading, maxdata } = this.state;
    if (loading || this.onEndReachedCalledDuringMomentum || maxdata) {
      return;
    }
    const newParasm: any = {
      ...this.paramsProduct,
      page: this.paramsProduct.page + 1
    }
    this.paramsProduct = newParasm
    this.presenter.getPromotions(newParasm);
    this.onEndReachedCalledDuringMomentum = true;
  }

  onMomentumScrollBegin = () => {
    this.onEndReachedCalledDuringMomentum = false;
  }

  emptyListComponent = () => {
    if (!this.state.loading) {
      return <EmptylistComponent />;
    } else {
      return null;
    }
  }

  footerComponent = () => {
    if (!this.state.maxdata) {
      return <DailyMartText style={{ marginVertical: 10, textAlign: "center", color: R.colors.primaryColor }}>Đang tải...</DailyMartText>;
    } else {
      return null;
    }
  }

  setRefFlatList = (refFlatList: any) => {
    return this.refFlatList = refFlatList;
  }

  searchProduct = (search: string) => {
    const newParams: any =
    {
      ...this.paramsProduct,
      search: search
    }
    this.paramsProduct = newParams;
    this.presenter.getPromotions(newParams);
  }

  public render() {
    const { isVisible, CateGoryList, minPrice, maxPrice, products, loading } = this.state
    return (
      <Container statusBarColor={R.colors.primaryColor}>
        <HeaderService searchInput={true} searchInputChangeText={this.searchProduct} iconRight={'ic_filter'} width={20} onPressRight={this.onDraw} title="home.title4" />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={products}
          ref={this.setRefFlatList}
          numColumns={2}
          keyExtractor={this.keyExtractor}
          refreshing={loading}
          onRefresh={this.onRefresh}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.02}
          onMomentumScrollBegin={this.onMomentumScrollBegin}
          ListEmptyComponent={this.emptyListComponent}
          ListFooterComponent={this.footerComponent}
          style={styles.flatlistSpkhuyenmai}
          renderItem={this.renderItem}
        />

        <Modal
          onBackdropPress={this.toggleModal}
          animationIn="slideInRight"
          animationOut="slideOutRight"
          style={[stylesw.container]}
          isVisible={isVisible}
        >
          <TouchableWithoutFeedback onPress={this.onHideKeyBoard}>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false} enableOnAndroid enableAutomaticScroll style={styles.modal} contentContainerStyle={{ paddingBottom: DimensionHelper.getScreenHeight() * 0.16 }}>
              <View style={{ flex: 1, paddingHorizontal: 12 }}>
                <View style={styles.filterHeader}>
                  <Buttons
                    source={images.ic_close}
                    styleImageButton={styles.filterHeaderImagebutton}
                    onPress={this.toggleModal}
                  />
                  <DailyMartText style={styles.filterHeaderText}>
                    {translate('filter.header')}
                  </DailyMartText>
                </View>
                <View style={styles.portfolio}>
                  <DailyMartText style={styles.title1}>
                    {`${translate('filter.title1')} ( ${this.state.totalCate} )`}
                  </DailyMartText>
                </View>
                <FlatList
                  data={CateGoryList}
                  numColumns={2}
                  scrollEnabled={false}
                  keyExtractor={this.keyExtratorModal}
                  extraData={this.state}
                  ListFooterComponent={this.footer}
                  renderItem={this.renderItemModal}
                />
                <DailyMartText style={styles.title2}>
                  {translate('filter.title2') + ' (đ)'}
                </DailyMartText>
                <View style={styles.aboutCost}>
                  <TextInputs
                    styleTextInput={styles.filterFooterTextinput}
                    styleTextInputs={styles.filterMin}
                    value={minPrice}
                    autoFocus={false}
                    returnKeyType={"done"}
                    keyboardType={"numeric"}
                    onSubmitEditing={this.onHideKeyBoard}
                    onTouchStart={this.onShowKeyBoard}
                    onChangeText={this.onChangeText('minPrice')}
                    placeholder={translate('textinput.min')}
                  />
                  <TextInputs
                    styleTextInput={styles.filterFooterTextinput}
                    styleTextInputs={styles.filterMax}
                    value={maxPrice}
                    autoFocus={false}
                    returnKeyType={"done"}
                    keyboardType={"numeric"}
                    onSubmitEditing={this.onHideKeyBoard}
                    onTouchStart={this.onShowKeyBoard}
                    onChangeText={this.onChangeText('maxPrice')}
                    placeholder={translate('textinput.max')}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </TouchableWithoutFeedback>
          {!this.state.showKeyborad && <View style={stylesw.footerFilter}>
            <Buttons
              styleButton={styles.reSetting}
              styleTextButton={[styles.filterFooterText, { color: colors.primaryColor }]}
              textButton={translate('button.resetting')}
              onPress={this.resetSearch}
            />
            <Buttons
              styleButton={styles.search}
              styleTextButton={[styles.filterFooterText, { color: colors.white100 }]}
              textButton={translate('button.search')}
              onPress={this.handelSubmit}
            />
          </View>}
        </Modal>
      </Container>
    );
  }
}


const stylesw = StyleSheet.create({
  header: {
    backgroundColor: colors.primaryColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    paddingBottom: 12
  },
  styleTextInputs: {
    paddingHorizontal: 12,
    height: 35,
    marginHorizontal: 12,
    width: '100%',
    flex: 1
  },
  container: {
    borderColor: 'rgba(0, 0, 0, 0.01)',
    margin: 0,
    flex: 0,
    bottom: 0,
    position: 'absolute',
    width: DimensionHelper.getScreenWidth() - 50,
    paddingHorizontal: 5,
    backgroundColor: colors.white100,
    paddingBottom: DimensionHelper.getBottomSpace(),
    top: 0,
    right: 0,
    paddingTop: Platform.OS === "android" ? 15 : 0
  },
  styleTextInput: {
    fontSize: 13,
    color: '#8D8D8D',
    alignItems: 'center',
    flex: 1,
    maxHeight: 35
  },

  styleImageTextInput: {
    height: 15,
    width: 15
  },
  footerFilter: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left: 0, right: 0
    // right: -1,
  },
});
