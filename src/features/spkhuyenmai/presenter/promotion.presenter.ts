import ApiHelper from 'helpers/api-helper';
import { URL_API } from 'helpers/url-api';
import { VegetableView } from 'features/vegetable/view/vegetable.view';
import { STATUS } from 'types/BaseResponse';
import { ReqCategory } from 'features/home/model/home';
import Commons from 'helpers/Commons';
// import { PromotionView } from '../view/promotion.view';
import PromotionScreen from '../view/Promotion.screen';
import { ProductReq } from 'features/vegetable/model/vegetable';

export class PromotionPresenter {
  // private view: view;
  private view: PromotionScreen;

  constructor(view: PromotionScreen) {
    // this.view = view;
    this.view = view;
  }

  async onFetchCategory(params: ReqCategory) {
    try {
      const res = await ApiHelper.fetch(URL_API.CATEGORIES, params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data && res.data.length > 0) {
          this.view.onFetchDataCateGorySuccess(
            res.data,
            true,
            res.total || 0
          );
        } else {
          this.view.onFetchDataCateGorySuccess([], false, 0);
        }
      } else {
        this.view.onFetchDataCateGorySuccess([], false, 0);
      }
    } catch (error) {
      console.log('error: ', error);
      this.view.onFetchDataCateGorySuccess([], false, 0);
      // this.homeView.onFetchDataFail();
    }
  }

  getPromotions = async (params: ProductReq) => {
    try {
      const res = await ApiHelper.fetch(URL_API.PROMOTION + Commons.idStore, params, true);
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length) {
          let data = params.page > 1 ? [...this.view.state.products, ...res.data] : res.data;
          this.view.setProducts(data, data.length === res.total);
          // this.view.setMaxData(data.length === res.total);
        } else {
          this.view.setProducts([], true);
          // this.view.setMaxData(false);
        }
      } else {
        this.view.setProducts([], true);
        // this.view.setMaxData(false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.view.setProducts([], true);
    }
  }

  async onGetDataProduct(params: ProductReq) {
    try {
      const res = await ApiHelper.fetch(
        URL_API.PRODUCT + Commons.idStore,
        params,
        true
      );
      if (res.status == STATUS.SUCCESS) {
        if (res.data.length > 0) {
          this.view.onFetchDataSuccess(res.data, true);
        } else {
          this.view.onFetchDataSuccess([], false);
        }
      } else {
        this.view.onFetchDataSuccess([], false);
      }
    } catch (error) {
      console.log('error: ', error);
      this.view.onFetchDataSuccess([], false);
      // this.homeView.onFetchDataFail();
    }
  }
}
