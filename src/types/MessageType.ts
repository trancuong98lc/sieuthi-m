import { MediaType } from './ConfigType';
import { User } from './user-type';

export interface Message {
  _id: string; // id của message
  conversationId?: string; // id của cuộc thoaị giữa user với nhân viên
  type?: string; // loại tin nhắn (Media - Text - SYSTEM )
  additionalData?: {
    createdBy?: { _id: string; fullName?: String };
    users?: User;
    media?: MediaType;
  }; // các thông tin của ông gửi tin nhắn tới user
  system?: boolean | null; // xác nhận xem có phải tin nhắn hệ thống không
  mediaIds?: string[]; // id của ảnh
  medias?: MediaType[]; // tin nhắn với dạng ảnh trả về đầy đủ thông tin thumnail url ...
  text: string; // tin nhắn text
  createdBy?: string; // ai là người tạo tin nhắn
  creator?: User; // user tạo tin nhắn
  createdAt?: string | number; //....
  updatedAt?: string | number; //....
  deletedAt?: string | number; //....
  timeStampCreated?: number;
  tempId?: number; //....
  //   localMedias?: MediaSelectedType[];
  seenUserIds?: string[]; // những ng đã xem tin nhắn :D
}

export enum MessagePosition {
  FIRST = 'FIRST',
  MIDDLE = 'MIDDLE',
  LAST = 'LAST',
  NONE = 'NONE',
  ONLY = 'ONLY'
}

export interface MessageImageStates {
  imgWidth?: number;
  imgHeight?: number;
  loading: boolean;
}
export interface MessageImageProps {
  imgWidth: number;
  imgHeight: number;
  message: string;
}
