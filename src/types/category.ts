import { Store_Type } from 'features/store/model/store-entity';
import { MediaType } from './ConfigType';

export interface Category {
  categoryId: number;
  categoryName: string;
  retailerId: number;
  modifiedDate: Date;
  createdDate: Date;
  rank: number;
  isActive: boolean;
  image?: string;
}

export interface Hotsale {}

export interface Products {
  category: Category;
  categoryId: string;
  code: string;
  createdAt: string;
  deletedAt?: string;
  description?: string;
  featured: string;
  mediaIds: string[];
  medias?: MediaType[];
  name: string;
  new: string;
  price: number;
  priceDiscounted?: number;
  preferentialQuantity?: number,
  promotion?: {
    applyingType: string;
    preferentialAmount: number;
  };
  totalPriceDiscounted: number;
  productStore: Store_Type[];
  productStores: Store_Type;
  status: string;
  preferentialAmount: number;
  promoFor: string;
  totalAll: number;
  unit: UnitType;
  unitId: string;
  _id: string;
}

export interface UnitType {
  description: string;
  createdAt: string;
  deletedAt?: string;
  _id: string;
  name: string;
  active: boolean;
  score: number;
}

export interface ProductsDetail {
  createdDate: Date;
  id: number;
  retailerId: number;
  name: string;
  categoryId: number;
  basePrice: string;
  unit: string;
  description: string;
  images: [];
  inventories: [];
  allowsSale: boolean;
}
export interface Customer {
  id: number;
  name: string;
  addr: string;
  phone: string;
}

export const STATUS_SEARCH = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE'
};
export const SEARCH_YES_NO = {
  YES: 'YES',
  NO: 'NO'
};

export const STAUTS_HISTORY = {
  ACKNOWLEDGED: 'ACKNOWLEDGED',
  CANCEL: 'CANCEL',
  DELIVERED: 'DELIVERED'
};
