import { User } from './user-type';

export interface BaseResponse {
  data: any;
  items?: any;
  status: string;
  total?: number;
  user?: User;
  token?: string;
}
export interface BaseResponseLogIn {
  status: string;
  user: User;
  token: string;
  refreshToken: string;
  expiresAt?: string;
}

export const STATUS = {
  SUCCESS: 'SUCCESS',
  INVALID_TOKEN: 'INVALID_TOKEN',
  TEL_ALREADY_EXISTS: 'TEL_ALREADY_EXISTS',
  RECORD_NOT_FOUND: 'RECORD_NOT_FOUND',
  PRODUCT_UNAVAILABLE: 'PRODUCT_UNAVAILABLE',
  PRODUCT_IS_OUT_OF_STOCK: 'PRODUCT_IS_OUT_OF_STOCK',
  PRODUCT_DO_NOT_EXIST: 'PRODUCT_DO_NOT_EXIST',
  CART_INVALID: 'CART_INVALID',
  DELIVERY_ADDRESS_DO_NOT_EXISTS: 'DELIVERY_ADDRESS_DO_NOT_EXISTS'
};
