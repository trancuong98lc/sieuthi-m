export interface User {
  gender: string;
  online: string;
  point: number;
  lastOnlineTime: string;
  createdAt: string;
  deletedAt?: string;
  location?: any;
  _id: string;
  name: string;
  phoneNumber: string;
  avatar?: string;
  username: string;
  dateOfBirth?: string;
  email: string;
  pointRanking: PointRanking;
  pointAccumulated: number;
  priceAccumulated: number;
  pointUsed: number;
  age: string;
  expiresAt: string;
}

export interface PointRanking {
  createdAt: string;
  description: string;
  name: string;
  preferential: number;
  status: string;
  targetPoint: string;
  _id: string;
}

export const GENDER = {
  MALE: 'MALE',
  FEMALE: 'FEMALE',
  OTHER: 'OTHER'
};

export const GENDER_NAME = {
  [GENDER.MALE]: 'user.male',
  [GENDER.FEMALE]: 'user.female',
  [GENDER.OTHER]: 'user.other'
};
