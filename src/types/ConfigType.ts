export interface ConfigType {
  _id: string;
  key: string;
  value: string;
}

export interface EditPostConfigResponse {
  maxActions?: number;
  maxTime?: number;
}

export interface MediaConfigResponse {
  maxSizeVideo?: number;
  maxFiles?: number;
  maxSizeImage?: number;
  maxVideo?: number;
}

export interface CommentConfigResponse {
  maxPost?: number;
  maxComment?: number;
  timeReset?: number;
}

export interface MediaSelectedType {
  type: string;
  uri: string;
  filename: string;
  timestamp?: number;
  width: number;
  originalRotation?: number;
  height: number;
  compressing?: boolean;
  stt?: number;
}

export interface LiveStreamResponse {
  status?: string;
  facebookLiveStreamLink?: string;
  youtubeLiveStreamLink?: string;
}

export interface StatusSearch {
  ACTIVE: string;
  INACTIVE: string;
}

export const PHOTO_SIZE = {
  Small: 'Small',
  Medium: 'Medium',
  Large: 'Large'
};
export interface photoType {
  Small: string;
  Medium: string;
  Large: string;
}

export interface MediaType {
  createdAt: string;
  deletedAt?: string;
  thumbnail: string;
  originalRotation?: any;
  thumbnail2x: string;
  dimensions: {
    width: any;
    height: any;
  };
  type: string;
  uri?: string;
  url?: string;
  _id: string;
}

export interface Config {
  contentPayment: string,
  deliveryCondition: number,
  estimatedDeliveryTime: number,
  poinConversionRate: number,
  quantityHotsale: number
}

export const StatusOrder = {
  ALL :"ALL",
  NEW: "NEW",
  CANCEL: "CANCEL",
  PAID: "PAID",
  NOT_PAID: "NOT_PAID",
  DELIVERING: "DELIVERING",
  DELIVERED: "DELIVERED",
}
export const StatusOrderName ={
  [StatusOrder.NEW]: "receivedDetail.order_status_new",
  [StatusOrder.CANCEL]: "receivedDetail.order_status_cancel",
  [StatusOrder.PAID]: "receivedDetail.order_status_paid",
  [StatusOrder.NOT_PAID]: "receivedDetail.order_status_not_paid",
  [StatusOrder.DELIVERING]: "receivedDetail.order_status_delivering",
  [StatusOrder.DELIVERED]: "receivedDetail.order_status_delivered",
}

export const StatusOrderNameTitle ={
  [StatusOrder.NEW]: "receivedDetail.order_new",
  [StatusOrder.CANCEL]: "receivedDetail.order_cancelled",
  [StatusOrder.PAID]: "receivedDetail.order_paid",
  [StatusOrder.NOT_PAID]: "receivedDetail.order_not_paid",
  [StatusOrder.DELIVERING]: "receivedDetail.order_delivering",
  [StatusOrder.DELIVERED]: "receivedDetail.order_delivered",
}

export const StatusOrderIcon ={
  [StatusOrder.NEW]: "ic_waitPay",
  [StatusOrder.CANCEL]: "ic_deleted",
  [StatusOrder.PAID]: "ic_waitPay",
  [StatusOrder.NOT_PAID]: "ic_waitPay",
  [StatusOrder.DELIVERING]: "ic_waitPay",
  [StatusOrder.DELIVERED]: "ic_waitPay",
}

export enum ConfigKey {
  MAX_ACTIONS_EDIT_POST = 'MAX_ACTIONS_EDIT_POST',
  CONFIG_TIME_BANNER = 'CONFIG_TIME_BANNER',
  MAX_TIME_EDIT_POST = 'MAX_TIME_EDIT_POST',
  MAX_IMAGES_PAGE_PRODUCT = 'MAX_IMAGES_PAGE_PRODUCT',
  MAX_SIZE_VIDEO = 'MAX_SIZE_VIDEO',
  MAX_FILE_UPLOAD = 'MAX_FILE_UPLOAD',
  MAX_VIDEO_UPLOAD = 'MAX_VIDEO_UPLOAD',
  MAX_SIZE_IMAGE = 'MAX_SIZE_IMAGE',
  MAX_TAG_USER = 'MAX_TAG_USER',
  COMMENT_CONFIG_MAX_POST = 'COMMENT_CONFIG_MAX_POST',
  COMMENT_CONFIG_MAX_COMMENT = 'COMMENT_CONFIG_MAX_COMMENT',
  COMMENT_CONFIG_TIME_RESET = 'COMMENT_CONFIG_TIME_RESET',
  CONTENT_REVIEW_PAGE_MAX_LENGTH = 'CONTENT_REVIEW_PAGE_MAX_LENGTH',
  LIVE_STREAM_NOTIFY_ENABLE = 'LIVE_STREAM_NOTIFY_ENABLE',
  MAX_MEMBER_IN_GROUP_CHAT = 'MAX_MEMBER_IN_GROUP_CHAT',
  LIST_BANNER = 'LIST_BANNER',
  VIDEO_VIEW_THRESHOLD = 'VIDEO_VIEW_THRESHOLD',
  ENABLE_REFERRAL_CODE = 'ENABLE_REFERRAL_CODE'
}
