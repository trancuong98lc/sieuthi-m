export interface Support {
     _id: number,
     question: string,
     answer:string,
     description: string
}