import { NavigationContainer } from '@react-navigation/native';
import RootComponent from 'features/root/view/root-componet';
import * as React from 'react';
import { Platform, StatusBar, Text, TextInput } from 'react-native';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'redux/sagas';
import configureStore from 'redux/stores/configureStore';
import MainStack from 'routing/main-navigation';
import { navigationRef } from 'routing/service-navigation';
import BaseAlert from 'libraries/BaseAlert/BaseAlert';
import BaseAlertManager from '/libraries/BaseAlert/Manager';
import LoadingModal from 'libraries/LoadingManager/LoadingModal';
import LoadingManager from 'libraries/LoadingManager/LoadingManager';

import BaseToastManager from 'libraries/BaseToast/Manager';
import BaseToast from 'libraries/BaseToast/BaseToast';

if (Platform.OS === 'android') StatusBar.setTranslucent(true);

const sagaMiddleware = createSagaMiddleware();
const store = configureStore(sagaMiddleware);
sagaMiddleware.run(rootSaga);

interface IAppProps { }

interface IAppState { }

console.disableYellowBox = true;
StatusBar.setTranslucent(true);

Text.defaultProps = {};
Text.defaultProps.maxFontSizeMultiplier = 1.0;

TextInput.defaultProps = {};
TextInput.defaultProps.maxFontSizeMultiplier = 1.0;
TextInput.defaultProps.underlineColorAndroid = 'transparent';
export default class App extends React.Component {
  alertRef: any = null;
  loadingRef: any = null;
  // loadingRef: any = React.createRef<LoadingModal>();
  basToastRef: any = React.createRef<LoadingModal>();

  constructor(props: IAppProps) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.alertRef && BaseAlertManager.register(this.alertRef);
    this.loadingRef && LoadingManager.register(this.loadingRef);
    this.basToastRef && BaseToastManager.register(this.basToastRef);
  }

  componentWillUnmount() {
    LoadingManager && LoadingManager.unregister(this.loadingRef);
    this.alertRef && BaseAlertManager.unregister(this.alertRef);
    this.basToastRef && BaseToastManager.unregister(this.basToastRef);
  }

  setRefsAlert = (refs: any) => {
    this.alertRef = refs;
  };
  public render(): React.ReactNode {
    return (
      <Provider store={store}>
        <RootComponent>
          <BaseAlert ref={this.setRefsAlert} />
          <LoadingModal
            ref={(ref) => {
              this.loadingRef = ref;
            }}
          />
          <NavigationContainer ref={navigationRef} >
            <BaseToast
              ref={(ref) => {
                this.basToastRef = ref;
              }}
            />
            <MainStack />
          </NavigationContainer>
        </RootComponent>
      </Provider>
    );
  }
}
