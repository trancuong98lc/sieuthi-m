module.exports = {
	presets: [ 'module:metro-react-native-babel-preset' ],
	plugins: [
		[
			'module-resolver',
			{
				root: [ './src' ],
				extensions: [ '.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json' ],
				alias: {
					features: [ './src/features' ],
					libraries: [ './src/libraries' ],
					helpers: [ './src/helpers' ],
					redux: [ './src/redux' ],
					res: [ './src/res' ],
					routing: [ './src/routing' ]
				}
			}
		],
		[ '@babel/plugin-proposal-decorators', { legacy: true } ]
	]
};
